# Computer Science
I am currently studying _Computer Science_ at the __[Reykjavik University](https://www.ru.is/)__, so these notes are still evolving while I am learning the subjects. The local language is Icelandic and many courses at my school are therefor taught in Icelandic, which might lead to some weird wordings or things lost in translation in these notes.

I am such a __documentation nerd__, so of course I had to create a collection of information like this, even if it's mostly just for myself. It does please me if it can be of use to others as well and it would be lovely to hear about.

### Best way to learn is to explain things to others
> I just learned that it's called _the Feynmann Technique_, to learn by explaining the concepts to others. Identifying gaps in your explanations to find what you need to understand better. If you can properly explain a concept to others, then you understand it yourself and it cements the knowledge even better within your head. 
>
> Write it down as if you're teaching it to a child or anyone really. Write everything you know about the subject in a way that explains things and makes them understand what you are talking about. In a way it could even teach yourself if you happen to forget the details of the subject later on. The more effort you put into your notes and to really understand what you're explaining, the better the information will stick.
> 
> And when you improve your notes, you improve your knowledge, review what you've explained, identify if you misunderstood something or missing some information to properly explain something.

### Constantly evolving
>Please keep in mind that these notes are constantly evolving while I learn the subjects, correcting something that was incorrect or adding improvements to prevent confusion. I even have a habit of completely re-arranging the folder structure if needed, which would break any links if you happen to bookmark anything. 
>
>#### How I use the Double-quoteblocks
>> I've started to use double-quoteblocks (like this one, with the double vertical lines) if the text is straight from the source. Mainly keeping it so that the notes don't have holes in them, until I get the chance to re-write them in my own way (because if they haven't been re-written, then I haven't actually processed, more risk of it getting forgotten sooner than if I had bothered to explain existing texts in my own way)
>>
>> I use these double-quoteblocks also to keep previous versions of note sections, when I'm in the process of improving the notes by re-writing them again. It's just how I work.

### Navigating through the notes
> I make use of the `README` files within each directory, because of the neat feature of their content being automatically displayed when viewing these directories on _gitlab_ directly. These might contain the most accurate indexes for sub-files. 
>
> I try to avoid detailed indexes in upper level directories, mainly directing to the next README in question. This removes the need to update so many files if I happen to want to change the structure of things (wich can happen on a whim).
## [Hardware and Binary](hardware-assembly/README.md)
> Hardware, assembly, binary and memory

## [Operating Systems](os-styrikerfi/README.md)
> The bridge between hardware and users/programmers

## [Algorithms](algorithms/README.md)
> Behaviour of data structures and various things

## [Programming Language Concepts](prog-lang-concepts/README.md)
> Programming paradigms and languages 

## [Data analysis](gagnagreining/README.md)
> Analyse data, represent and visualization of data 
* [__Statistics__](statistics/README.md)
## [Software Engineering](software-engineering/README.md)
> Designing and development of software: Architecture, modeling, design, patterns, requirements, security... 