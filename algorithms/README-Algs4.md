Just a index of how things were structured in the course

## Analysis of Algorithms (1.4)
* Observations
* Mathematical models
* `Order of growth` classifications
* Complexity of loops
* Exponentials and logarithms
* Memory
* Dependencies on input

## Union-Find (1.5)
* Quick-find
* Quick-union

## Elementary Sorts (2.1)
* Selection sort
* Insertion sort
* <strike>shellsort</strike>
* Comparators
* Shuffling

## Merge sort (2.2)
* Mergesort
* bottom-up mergesort
* sorting complexity
* convex hull

## Quicksort (2.3)
* Quicksort
* Selection
* Duplicate keys
* System sorts

## Priority Queues (2.4)
* Binary heaps
* Heap sort
* event-driven simulation

## Symbol tables (3.1)
* Elementary implementations
* Ordered operations

## Balanced Search Trees (3.3)
* 2-3 search trees
* red-black BSTs
* B-trees

## Geometric Applications of BSTs
> line, 2d or 3d plane, or more? How do you organize and search for things in such a space.
* 1d range search
* line segment intersectioni
* kd trees

## Hash tables (3.4)
* Hash functions
* separate chaining
* linear probing
* context

### Symbol table Applications (3.5)

## Undirected graphs (4.1)
* dept-first search
* breath-first search
* Connected components

## Directed Graphs (4.2)
* digraph search
* topological sort

## Minimum Spanning Trees / MST (4.3)
* Greedy algorithm
* edge-weighted graph
* Kriskal's algorithm
* Prim's algorithm

## Shortest paths (4.4)
* Bellman-Ford algorithm
* Dijkstra's algorithm
* Seam carving

## String sorts (5.1)
* key-index counting
* LSD radix sort
* MSD radix sort
* 3-way radix quicksort
* suffix arrays

## Tries (5.2)
* R-way tries
* Ternary search tries
* Character-based operations

## Data compression (5.5)
* Run-length coding
* Huffman compression
* LZW compression