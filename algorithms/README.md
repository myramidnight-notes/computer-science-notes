[...back](../README.md)
# Data structures
We had to create these data structures from scratch in order to understand how they worked. `GSKI` (data structures) course at RU had us coding them in python, to figure out how they can be made efficiently and what the _big O_ calculations were (how complicate the program is in regard to time and size). `REIR` (algorythms) course extends on that, but was using java when I was taking it.

### Study materials
You should totally check out the online _course book_ [Algorithms 4th edition](https://algs4.cs.princeton.edu/home/) on these subjects. It is what is being used in `REIR`.

* [datastructure cheat sheet](https://algs4.cs.princeton.edu/cheatsheet/)
## Lists (Array)
### Linked lists
> A chain of items, they could be linked in one direction (can only walk through the list in one direction) or both ways (to be able to walk through the list in either direction)
* Linked Lists (1-way)
* Double linked lists (2-way)
* Symbol Table
### Stacks
> First in Last out, only one side of the structure is accessable, you don't take the bottom plate from a stack of plates.
## [Trees](trees/README.md)
> A data structure that has a root, nodes and branches. Binary trees have only two branches per node. A tree could have any number of branches depending on the type of tree.
### Binary Search trees (BST)
### Heaps

### Queues
> First in First Out, we stand in line and are processed in that order.

## Hasing
> The buckets! Hash a key from an item to represent it, then put it into the associated bucket. Then you will only have to search within a specific bucket based on the hash code. Good when you have multiple items that might have the same key.
## [Graphs](graphs.md)
## [Sorting](sorting)