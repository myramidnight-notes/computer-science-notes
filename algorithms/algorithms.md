> [Algorithms 4th edition](https://algs4.cs.princeton.edu/home/) 

# Analysis of Algorithms
There are good reasons for analyzing algorithms
* Predict performance (we do want our code to be efficient)
* Compare algorithms (find one that fits the situation best)
* Provide guarantees (know for sure what the worst/best/average cases can be)

The things we might be observing the most would be the time and memory a algorithm takes while running.

## Scientific methods
* __Observe__ some feature of the natural world (such as the computer itself)
* __Hypothesize__ a model that is consistent with the observations
* __Predict__ events using the hypothesis
* __Verify__ the predictions by making further observations
* __Validate__ by repeating until the hypothesis and observations agree.

### Principles
* Experiments must be reproducible
* Hypothesis must be falsifiable


## Simplification (tilde)
The _tilde notation_ indicates a simplifcation of the real number, a rough estimate of the results. 

### Ignore lower-order terms
* When `n` is large, terms are negligible
* when `n` is small, we don't care

![tilde-notations](../images/algs/tilde-notations.png)
![tilde-notations-graph](../images/algs/tilde-notations-graph.png)

## [Order of growth](order-of-growth.md)

## [Complexity of loops](algs-loops.md)