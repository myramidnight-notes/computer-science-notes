# Compression 
* Static model (kyrrlegt líkan)
    > Such as `ASCII` or `Morse code`
* Dynamic model (kvikt líkan)
    > Such as Huffman code
* Adaptive model (aðlaganlegt líkan)
    > Decoding starts at the beginning. such as `LZW`

## Huffman compression
> This compression allows us to create a optimal code to compile our string into. It can even be used to create secret or quick code, such as morse-code.
1. We first create a list of characters and count their frequency within the string. Each entry will be a node that we'll later combine into a tree.
1. Then we will select the two nodes with the lowest frequency and combine them together as a tree, the root stays empty but will be the combined number of frequencies. 
1. We continue selecting two nodes/sub-trees with the lowest frequency count
1. This continues until all nodes and sub-trees have been combined into a single tree.
1. Now we will assign the binary values `0` and `1` to the routes, and compile the code as we travel the tree from the root for each character we want to add to the compiled string.



## Lempel-Ziv-Welch (LZW) compression 
1. Create a symbol-table with string values and w-bit keys.
2. Populate the table with the single char values
3. Read a w-big key from the string
4. Find the associated string in the table and write it out
5. Update the table (by adding the string + next character)


