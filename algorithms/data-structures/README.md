# Data structures
They collect data that can be accessed. Each can be implemented in multiple ways, but each type have specific rules about how to insert, remove and find items within their structures.

These structures make use of lists to store their data, and then add the extra methods that are associated to the specified data structure. 

Tree-structures are generally type of linked lists (where each node will have reference to both the parent node and children)

## Lists
### Stack
>First in, last out
>* Insert items at the top
>* Remove items at the top 

### Queue
>First in, first out
>* Insert items at the front
>* Remove items from the back

### [Symbol table (dictionary)](symbol-table.md)
> A _red-black tree_ is often used in the implementation of System Symbol tables
## Binary Heaps
They are tree structures. Each item added to the tree is added as layers, keeping the tree balanced. A marker keeps track of where the last node was placed. When a new node is added, it is placed at the bottom and allowed to _swim up_ if it has higher priority than it's parent.
### Bottom-up binary heap
### Min/Max heaps
All items inserted into the heap will be sorted by size. If it is a max-heap, then the highest value item is at the root, while a min-heap would have the lowest value item at the root.