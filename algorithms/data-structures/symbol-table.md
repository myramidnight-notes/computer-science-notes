# Symbol tables (dictionaries/objects)

The following table shows implementations with different types of data structures. Search trees, specially self-balancing ones, are a very popular for implementing 
![search trees table](../../images/algs/search-tree-table.png)


## Implementation
Each item in the table will be a key-value pair, the inserted item being placed as value and assigned a unique key (duplicate keys will simply override the existing key with the new value). 

Items are referenced by key, rather than index because the keys tend to be sorted into place whenever a new item is inserted into the table (it makes finding the correct key easier).

These are the typical _dictionaries_ in python coding which are also called objects. The core of _object oriented programming_.

It is like keys to lockers, you can only access them through the keys. Pointers work in a similar manner (pointing to the location in memory where our data is stored).

## Behaviour
1. When inserting items to the symbol table, they are assigned a key. You should use _immutable_ types for keys in a symbol table, it can be anything sortable (such as `string`, `integer` and `double/float`)
    * Values cannot be `null` key should be removed from the table in such cases.
1. We search for the key and return the associated value.
