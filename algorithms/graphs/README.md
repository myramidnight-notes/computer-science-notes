
# Graphs
> Graphs contain vertices (nodes) and edges (connections between nodes). 
>* We can use `adjacency-matrix` or `adjacency-list` to represent our graphs.
## Terminology
* Graph (_net_), a group of vertices and connecting edges.
* Vertex (_hnóða / hnútur_), a dot/location on the map
* Edge (_leggur_), a connection between two vertices
* Path (_vegur_), a series of edges connected by edges.
* Cycle (_hringur_), a path whose first and last vertices are the same one.
* Acyclic, a graph that contains no circular roads where you can return to your starting vertex. This applies to both directed and undirected graphs.
* Degree (_gráða_), the number of edges connected to a vertex. A directed graph will have _in-degrees_ and _out-degrees_ that indicate the number of incoming and outgoing edges to a vertex.
* Set-of-edges (_grannnet_), a set of groups of connected edges and vertices. 

>### Euler road
> Can we create a path that visits all the edges exactly once?

>### Hamilton road
> Can we create a path that vists all the vertices exactly once?
## [Reprecentation of graphs](graph-representation.md)
> How will we represent a graph within the code?
>* Edge list (list of pairs of vertices)
>* Adjacency Matrix (2d matrix of edges)
>* Adjacency list (list of sets of neighboring vertices)
## Searching/mapping a graph
>### [Depth First Search (DFS)](search-dfs.md)
> This algorithm will mark vertices as soon as they are visited, which results in a list ordered by discovery.
>* Good for quickly finding all the connected vertices to a selected vertex.
>### [Breath First Search (BFS)](search-bfs.md)
> This algorithm will first locate all the vertices before marking them, which will result in a list based on the distance to the starting vertex.
>* Good for finding the closest vertices to a selected vertex based on distance (number of edges traveled)

>![Graph problems](images/graph-search-problems.png)
>* Biparte (splitting the vertices into two color groups, each vertex can only be connected directly to a vertex of the oposite color)

## Trees in graphs
> A tree in the context of graphs is __a connected acyclic graph__, meaning it contains no _circular paths_ and all verticies are reachable through those paths (not necessarily from every vertex though). A directed acyclic graph is reffered to as `DAG`.
>
> A __spanning tree of a graph__ is a subset of edges of that graph that contains all the verticies.
>
### [Minimum Spanning Trees (MST)](mst.md) 
> A _spanning tree_ that maps the shortest distance to any vertex from a starting point, as well as mapping the 
>#### Greedy MST
> The greedy method makes locally optimal choices that lead to a globallly optimal solution. So it selects the shortest/best edge currently available at each step of the process without regard for the long-term (such as ignoring better options that are out of reach, depending on the algorithm).
* __Greedy Correctness Proof__
    > If you would divide all the vertices into two groups, and make a cross-cut through all the edges that connect those two groups and then select the shortest edge among those to become the bridge between the groups. This will be the proof that the resulting graph has the most optimal selection of edges for the MST. Any cross-cut through the graph should only have a single _bridge_ crossing the gap.
* __Kruskal's Algorithm__
    > Going through the order of shortest edges anywhere on the graph. So while the algorithm is in process, it might have multiple unconnected groups of connected vertices, which will all be connected by the end of the process.
* __Prim's Algorithm__
    > This algorithm only looks at verticies reachable through edges that are directly connected to the net being processed, selecting the shortest edge among those. This means that there will always be a connected tree at every step of the process, unlike Kruskal's.

## [Digraphs (Directed graphs)](digraphs.md)
> Graphs where the direction of the edge matters
>* Bellman Ford's Algorithm
>* Dijkstra's Algorithm
>* Acrylic graphs

## [Shortest Paths](shortest-paths.md)