
## Directed Acrylic Graph (DAG)
>* There are no loops
>* Between any pair of nodes, there is only one possible direction to travel between them. If there is an edge `(a,b)` then there is no `(b,a)`.
>#### Topological order
> If we can arrange all the nodes of a Acrylic graph into a line with clear order between them, then we have a topologically ordered graph.
>* We use dept first search to find the topological order.