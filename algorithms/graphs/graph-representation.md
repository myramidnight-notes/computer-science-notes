# Representing graphs
>![representation](images/graph-representation.png)
## Edge list
>Edges are stored similar to points, it tells you where the edge starts and ends. Edge `(a,b)` goes from vertex `a` to `b`.
>
>The limit of this representation is that it only shows the edges, so if a vertex has no connected edges, then it will not be listed.
>![edge list](images/graph-edge-list.png)
## Adjacency matrix
> we can represent a graph with a `adjacency-matrix`, a two-dimensional matrix that lists all the possible vertices and you mark `1/true` if you can travel from one vertex to another. This works for directed graphs as well.
>
>The image below is showing a undirected graph, but if it were directed, then each directed edge would only have a single entry in the matrix.
![adjacency matrix](images/graph-adjacency-matrix.png)
## Adjacency list
> We can represent a graph as a list of neighbors: adjacency list. It tells you what nodes are reachable from a particular node.
>![adjacency list](images/graph-vertex-index.png)
