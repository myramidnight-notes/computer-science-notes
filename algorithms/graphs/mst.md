* [Greedy Algorithms (MIT openCourseWare)](https://www.youtube.com/watch?v=tKwnms5iRBU)
# Minimum Spanning Trees (MST)
The purpose of __minimum spanning trees__ is to reach all the vertices using as few _edges_ as possible.

__The resulting graph has to:__
* be connected 
* be Acyclic (contains no circular paths)
* include all the vertices.
* assign each edge a non-negative weight.
    > The weight could be distance, priority, difficulty or anything that makes one path preferrable to another based on what the graph is representing. 

## Greedy MST
1. Optimal substructure
    > optimal solutionto problem incorperates optimal solutions to subproblem/s. 
1. Greedy Choice Property:
    > locally optimal choices lead to a globallly optimal solution

>We can imagine splitting the graph into two groups with a cut through the graph. We make a list of all the edges that go through/over this cut and order them by weight/size.
* The cut does not have to be a straight line
* Each time we _cut_ through the graph, we will highlight the shortest edge across that cut (making a list of the shortest edges)
* We only make cuts where there is no highlighted edge. 
* We repeat this until we have V-1 edges highlighted. 


## Kruskal algorithm
> Kruskal's is a special case of the `greedy MST` algorithm. It does not care if the graph is connected during the process, just that all the vertices are visited.

1. Sorts all the edges by weight
1. Go through the sorted list of edges in order, starting with the shortest (lowest weight)
1. Select the shortest edge from the sorted list
    * If the selected edge will not create a circle with the highlighted edges, then add it,
    * else skip it.
1. This repeats until all vertices have been visited (connected to a highlighted edge)

> Bottlenecks on sorting and union-find 
## Prim's algorithm
> We only look at the __edges connected to current net__. Of all the connected untraveld edges, which one is the lightest? 
>* Bottlenecks on priority-queue