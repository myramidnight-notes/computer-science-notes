
# Breath First Search (BFS)

> Could view it as planning your sight-seeing trip. Good for finding shortest paths through a graph.
>* Look around for all connected vertices and adds them to a queue for places to visit. 
>* Once it has finished sight-seeing, it will mark the current node  as visited and move onto the next one in the queue. 
>* Add all the new unvisited nodes to the queue before moving on. 
>* This continues until we've visited all the places on the queue.
