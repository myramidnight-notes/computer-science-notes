
# Debt First Search (DFS)

> Uses recursion to find it's way around a graph. Explores the graph first before compiling a list.
>* You can imagine it having a ball of yarn as you travel
>* Mark each visited vertex and edge (in a maze this would be where the hallways connect). 
>* If you find your way into a place you've already visited, then you backtrack until you find a new place to visit.
>* Once there is nothing new to explore, then backtrack to the beginning. We're done.
## Trémaux maze exporation
You can imagine traveling through a maze using a string, marking every intersection that you come across so you can tell if you've been there before

1. Mark your starting point and start exploring the maze, keeping a rope that is attached to the starting point.
1. Whenever you come across an intersection, you will mark it before continuing on your exploration
1. If you come across a intersection that you've already makred, then you retrace your steps to find a path that you have not traveled.
1. Keep doing this until you have explored the entire maze.

![maze](images/graph-dfs-maze.png)

## DFS application: Flood fill
The depth-first-search algorithm can be used to quickly find a group of connected pixels on a image that share the same color-range. This is a feature like the _magic wand_ in photoshop that selects all the pixels in the area that are similar, or the bucket-tool.
