# Shortest Paths
The practical applications of a _shortest path_ algorithm could be 
* Map routing (gps)
* Seam carving
* Network routing
* Traffic routing/planning

> Can be represented with a Shortest Path Tree (SPT). Keeping track of the shortest edge to each point.
### Relaxed edges
> The term essentially refers to updating the total distance to a vertex from the starting vertex if a shorter path was discovered.

> Keep a list of nodes and distance to other nodes.
>* If `e =  v -> w` yields shorter path to w, update distTo[w] and edgeTo[w].

## Dijkstra's algorithm
>* [View demo](https://algs4.cs.princeton.edu/lectures/demo/44DemoDijkstra.pdf)
>* Shortest-Path problems, page 743 in KHR 8. edition

### The table method (from KHR)
We will keep a table with a column for each vertex in the graph, then each row will the shortest total distance from a given vertex. All distances will be infinate until we are able to reach them in the algorithm and mark down the total distance. 

1. We pick a starting vertex, add a row for it in the table.
    * Each visited vertex will be added to a set, in the order of visited to keep track.
1. We write the total distance to all reachable vertices on the graph, current location will get the distance 0. We add a footnote to each distance to indicate from what location the current shortest distance we are traveling.
1. When the row has been filled, we will need to pick the next vertex to visit: the one with the shortest distance from current location that we have not visited before.
1. We will again fill in the row with total distance from starting vertex to all accessable vertices. 
    * If the value of the previous row for a vertex is infinite, then we simply add the the total distance to that vertex wiht the footnote about current location.
    * If the previous total distance is shorter than from current location, then we carry down the value from the previous row.
    * If the current total distance to a vertex is shorter than in previous row, then we write the new value.
    * All unreachable vertices will have their values carried down from previous row.
1. This process repeats until we have visited all the vertices in the graph. 
1. When we have completed the table, the bottom row will contain the shortest distances to each of those vertices.

#### The programing version
>When programming, each vertex could be a node that keeps track of `distanceTo` current node from starting node, and `edgeTo` to keep track of from what node was traveled when the `distanceTo` was last updated. Essentially the same as the method above, but just keeping a single row that keeps getting updated.
>
> This method looks at the cross section of all edges leading to the current net for each pass. 
>* We pick a starting point
>* Look at all the edges currently touching our net
>* Like with Bellman's, we keep a list of vertices, distance to them and what edge was traveled.
>* Then we add the node that was closest to us to our net before moving to it. 
>* Again we chart all the distances from current node and update distances.
>* We then look at all nodes adjacent to our net (not just from current node) before we add the node currently closest to our starting point to our net before moving to it.
>* Once all the nodes have been added to the net, then we have charted the shortest path to all nodes from starting point. No extra passes through all the nodes. 

## Bellman Ford's algorithm
> [View demo](https://algs4.cs.princeton.edu/lectures/demo/44DemoBellmanFord.pdf)

_Dijkstra's_ algorithm might be faster and more popular than _Bellman Ford's_ algorithm, but it fails when the graph has _negative edge weights_ and fails to detect negative cycles and get stuck in a loop of endlessly shorter paths (going into the negative). In those cases we might go for using _Bellman Ford's_ instead.

The edges do not need to be selected in any specific order (just has to be reachable from the starting-node, being part of the explored graph), so the processing of identical graph could be different between people/programs but the end result should be the same.

What makes _Bellman Ford's_ algorithm so slow is the fact that it has to pass over the whole graph `V-1` times, checking every time if the shortest path changed from the previous pass. 


#### Behaviour
1. Initialize the algorithm
    * Create a table to keep track of the `distanceTo` each vertex from a specified starting vertex. Each column will represent a vertes, and there will be `V-1` rows (one for each pass over the grid, excluding the initial row)
    * The initial row will mark the distance to starting vertex as `0` and the rest will be `positive infinity`
    * We will have a list of all edges in no particular order. 
1. Select a edge from the list
    * we skip any edge that is not reachable in this particular pass (traveling from a vertex marked as distance of infinity, meaning it has not been discovered yet).
    * Update the total distance to all adjacent vertices from current vertex if the total distance is shorter than current value.
1. When we have traveled all accessable edges, then we go back to the start vertex and repeat the process.

>When we start, distance to all nodes is infinite, edge to other vertices is none/null. Edges keep the distance between nodes, and the node keeps score of distance from starting point.
>* Begin by picking a starting point.
>* We add all the adjacent edges to a queue. This queue is reused for each pass.
>* We make a list of the distance to each vertex is, and what the shortest edge to it was.
>* Then we travel to the next node on the list and repeat the process.
>* If any node was already charted, then we only update it if the distance to it became shorter through the current node.
>* If the distance to any node was updated while the graph was being charted, then we go back to the beginning and repeat the whole thing again.
>* We only stop if none of the nodes were updated during the pass. Now we know the shortest paths to each node from starting point.

## Acrylic shortest path
> [View demo](https://algs4.cs.princeton.edu/lectures/demo/44DemoAcyclicSP.pdf)
>
>* First we find the topological order of nodes from the starting point with __Depth first search__. This will decide the order in which we visit the nodes.
>* Then we do things like we did with Dijkstra's, by charting the shortest distance and which edge was traveled, but instead of choosing next node by distance, we go by the topological order.