# Order of growth
How much changes the performance of the algorithm based on the number of items/actions. 

![order of growth](../images/algs/order-of-growth.png)