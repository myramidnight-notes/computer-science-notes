# Binary search
We will have a sorted array, and look at the middle item. If our query is larger, then we look at the right half, if it is smaller then we go left, else if it is equal then we have found the query.

We repeat this until we either find the query or run out of items to compare.

> The search algorithm would be improved if we would also compare with the highest and lowest item, to make sure that the query is within the range of the array. But then we are also increasing the number of compares/actions, that could decrease performance. Perhaps it would just be a initial compare to check the range.