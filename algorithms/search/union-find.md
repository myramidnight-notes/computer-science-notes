Very handy to find out if two items are connected.

* __Union:__ Replacing sets `p` and `q` with their union/merged sets.

## Quick-find
All items in the array carry the ID of their set
> We would have an array of items, and each item would keep the ID of the set it belongs to.  This makes it easy to see at a glance if two separate items are actually connected based on if they sahre the same set ID.

## Quick-union
All connected items are connected to the same tree, and we just check if two items are connected to the same root in order to see if they are connected.
> Keeps the sets as trees, whenever we find two sets that are actually part of the same whole, the first set will go under the second one. To find the ID of any item in the tree, you would look at the what the root item is.
>
> This will not be a binary tree. The root of any tree being merged will join with the root of the new tree.
>
> The worst case for this union would be a long chain of items.

## Weighted Quick Union
This version will place the smaller tree under the larger tree (number of items within the tree, not it's height), this creates a rather balanced tree on average, making them generally shorter, compared to the basic _quick-union_.

### Dynamic-connectivity problem
If we have a grid of open and closed sites, and want to make a algorithm that determines if any open site on the grid is connected to the top of the grid, one solution would be to create a virtual top and bottom node that all nodes in the top row would be connected to.

![Connected](../images/union-find-connected.png)