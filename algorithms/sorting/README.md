# Sorting Algorithms
There are many ways to sort things, and picking the right algorithm for any case depends on  what is being sorted, if all items are unique or if there are a fixed number of unqique that repeat, or if there might be duplicates.

Most data we'll want to sort will be arrays, rather than linked lists. If the data isn't sorted, then the fastest way is to convert to array, sort, and then convert back. 

### Memory and stability of sorting algorithms
>Linked lists are typically used for dinamic data, sorting only makes sense for static data.
>
>![sorting algorithms](../../images/algs/sorting-algorithms.png)
>* _stable_ means identical items remain in the same order after sort.
>* _inplace_ means that it does not requre extra memory during the sorting process. Sorted items occupy the same storage as the original ones (items are moved as they are sorted, rather than the array being replaced with a sorted version). 
>
> Perhaps _merge-sort_ could take less memory (become semi-inplace) by using subarrays that are just references to sections of the original array.

This table basically shows that the sorting algorithms that have the best average number of actions are not inplace, and therefor require alot more memory (generally proportional to the size of the array being sorted). 

## Basic sorting types.

### Selection Sort
Selection sort is faster than insertion sort in practice.
1. Find the smallest item in the array and swap it with the first item in the array. Now the smallest item is in it's correct place.
1. Repeat this with the remaining unsorted items.

### Insertion Sort
1. Assume the first item is sorted.
1.  We have a pointer that keeps track of how far we've sorted the list from the first item.
1. Compare the pointer item to the previous item
    * If the previous item is larger, swap them, this cascades up the list until the item will not be swapped further.
1. Then we move the pointer to the next item in the array and repeat the process  until we have reached the end of the array.

### Shell sort
> This is a alternative way to do _Insertion sort_. We swap items at a set distance rather than adjacent. With each pass (when the sorting is finished with it's current gap distance), then we reduce the distance until the gap = 1 (comparing adjacent items) which will be the last pass.
>
> This method will reduce the number of items that are actually exchanging places.

## [Quick Sort (_divide and conqure_)](quick-sort.md)
This is generally the quickest way to sort items, using recursion to partition the list based on pivot points. Run time is `n log(n)` on average, `n^2` in the worst case.

## Merge Sort
This algorithm is also deploys a _divide and conqure_ method, wrking recursivly.

This algorithm preserves the order of identical items. And runtime will always be _n log(n)_ because it will always be splitting the partitions in half. The disdvantage of this algorithm is how much space it takes in proportion to the number of items.

> Recursive method that partitions the list into half until it cannot be split any more. 
>
> Then on our return trip from the recursions, we merge the partitions. We take the first item in either parition and use/print the one that is lower value, we do this until we have merged the partitions. 
>
> This will result in a correctly sorted list once we finally return out of the recursions.

## Priority Queue (heapsort)
1. All items are entered into the Priority Queue (which is a sorted search tree)
1. If the item is higher priority than the parent, then the item _swims_ up through the tree until it finds it's place.
1. Once all items are inserted, then we start removing the root item from the tree until all items have been removed. 
    * All the removed items are added to an array in order, which results in a sorted array, based on the type of priorit

## String Sort
As the name suggests, these algorithms are specially suited to sorting strings.