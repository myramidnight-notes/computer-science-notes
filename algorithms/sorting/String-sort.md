# String sorts
Sorting strings or integers (they'd be compared as if strings technically). What are the best way's to put them into order?

## Radix String Sort
### Least Sagnificant Digit first (LSD)
> Start comparing the last digit, and work your way towards the first. This will make as many passes as the lenght of the longest string.
>
> This algorithm is best suited for sorting strings that are purely numbers (adding the `0` padding at the front for shorter strings) or where all strings are of equal length. All strings of equal length could be considered as groups, and all strings within each group would be correctly sorted, but outside of that the strings might seem out of order.
1. Create a symbol-table that, where the key is each character in the alphabet we will be using. 
    *Each key will contain a array in this example.
1. Look at last chracter in each string to be sorted, 
1. find the matching key in the table and add the string to the associated array.
1. Continue until all the strings have been processed.
1. Then go through each key-value pair in the table and print out each string in order. 
    * This results in a list sorted by the last character.
1. Now we repeat the process again, now looking at the next character in each string. 
    1. This repeats until all characters in the longest string have been processed. 

### Most Sagnificant Digit first (MSD)
> Just by ordering all the strings by the first letter will create a roughly sorted list of strings. 
>
> This algorithm is better suited for strings of characters than _LSD_ is, specially if they are not of equal length.

1. We first look at the first character in each string and sort those in order.
1. Then we will group all strings that contain the same character together
    * This will result in groups that where each share the same prefix.
    * The reason for this sub-grouping is because else the list would get scrambled when we begin looking at the next character. We instead create sub-problems to be solved.
1. Then we go into each group and do the same thing again, looking at the next character.
1. Create sub-groups again for strings sharing a prefix.
1 We repeat this until all strings have been sorted. 
    * Each sub-group is done sorting when it has finished processing it's longest string. 

### 3-way string quicksort (Bentley & Sedgewick)
> It is a quicker take on MSD. Split everything into 3 groups. Group into subarrays that are less, equal or greater than the currently viewed character. Uses recursive calls to make sure that all arrays are sorted.
>* Group the strings together based on the current digit being viewed. We start on the left.
>* Then go into the first group that contains more than 1 item and start comparing the next digit.
>   * Create sub groups of any strings that are identical so far (we haven't seen the rest of the digits). 
>   * Any string that runs out of digits doesn't have to be compared anymore, because it has found it's place. Any longer strings within the same group can be considered heavier and will be placed below it.
>* When all strings have been sorted, then we would backtrack to adjacent groups and repeat the process until all strings have been compared.
>
>![3way string quicksort](../../images/algs/3way-stringsort.PNG)

## Suffix sort
>  We take the string and create sub-strings of increasing length starting at the last character in the string. Each sub-string will be attached to a index reference to where it begins in the original strong. Then we sort all those sub-strings.

> This groups together sub-strings with the same prefix, which makes it easy to do a prefix-search for that phrase and quickly find if the query appears in the string at all. Since each sub-string is attached to a index to where it's first character is located in the original string, we can easily navigate to the instance.

> Pudding all those sub-strings into a prefix-tree will make it easy to search for instances of a query.

> This is typically used for searching for instances of a word on a page when browsing the internet.
![suffix sort](../../images/algs/suffix-sort.png)