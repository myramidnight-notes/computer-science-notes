# Heap sort (Priority queues)
* __Stack__ removes the item most recently added (top of the stack)
* __Queue__ removes the item least recently added (add to the back, remove from the front)
* __Priority Queue__ removes the largest/smallest item, depending on the heap used (max-heap or min-heap, it will sort the heap accordingly).