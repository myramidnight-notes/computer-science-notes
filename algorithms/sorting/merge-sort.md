# Merge sort
It will split the array into sub-arrays, splitting it by half each time with a recursive function. Then when the array cannot be split any further, it will sort each half and merge them on the return trip.

If we split an array into two sub-arrays of equal size (`N/2`), then the number of actions to merge them would be `~ 1/2 N` in the best case and `~N` in the worst case.

The number of splits the array will do (levels) will be `log(N)`, number of sub-arrays will be `2 * ceil(log(N))`

## Sorting
When we are done splitting up the array into sub-arrays, then we can use a sorting algorithm on each sub-array (such as insertion or selection sort), then we merge each pair of sorted arrays to return the sorted array. We only sort the smallest sub-arrays, then the rest of the return trip will just be merging the sub arrays together for each recursion.

![top-bottom mergesort](../../images/algs/top-bottom-mergesort.png)
### Top-down mergesort
> Just sorting and merging the initial half of the original array before sorting and merging the other half. Then merge the two halfs last.
### Bottom-up mergesort
> Sort all the sub-arrays before beginning to merge each level of the recursion.

## Memory
Merge sort takes alot of space, because it needs to keep track of the two sub-arrays, as well as the merged array. 

## Practical improvements
* Merge sort has too much _overhead_ for tiny subarrays.
* Cutoff to insertion sort for about 10 items for the smallest sub-arrays, to reduce the overhead in memory.

## Convex hull
### Graham scan