# Quick sort
* [Quick-sort with hungarian folk dance](https://www.youtube.com/watch?v=ywWBy6J5gz8)
## Basic quick sort
This is generally the quickest way to sort items, using recursion to partition the list based on pivot points. Run time is `n log(n)` on average, `n^2` in the worst case.

It is not stable (does not preserve the order of identical items) because of all the swapping that is going on.

The most important thing to make this algorithm efficient is to pick our pivot point in the most optimal way so that the partitions are hopefully splitting the list in half each time. 

1. We shall make the first item in the array the _pivot point_, then we have pointers to the first and last item of the remaining array. Let's call them `hi` and `lo`
1. We can start with either `hi` or `lo`, commparing the items with the _pivot point_. All items that `hi` points at should be higher than the pivot, while `lo` should have lower value items. __Let's start with `hi` for this example.
1. Then we start comparing items to the pivot point
    1. If the `hi` pointer comes across an item that is less than the pivot point, then it swaps that item with the pivot point item.
    1. When a swap occurs, then we start moving the opposite pointer, in this case the `lo`. It starts moving towards the opposite end, and if it runs into an item that is larger than the pivot point, then it swaps that item with the pivot.
1. This pattern of swapping continues until both `hi` and `lo` meet with the pivot point, indicating that there are no more items to compare. __The pivot point has founds it correct position__.
1. Now we will run _quick sort_ on both the left and right partitions (the `hi` and `lo`) that haven't been sorted yet, excluding the previous pivot because it has been sorted. This repeats until all items in the array have been sorted (all items have been the _pivot point_ at some point)

### Practical improvements for Quicksort
* Find the best pivot item, closest to the median.
    * the _median-of-three_ is a popular method to find the best pivot point, where we look at the first, last and middle item in the list, we sort them and choose the middle item. This will be a good guess that the chosen pivot is close to the median of the list.

## Handling duplicate keys
How should quicksort handle duplicate keys (equal to the pivot point) when trying to sort things based on if they are larger or smaller than the pivot point? Should it stop on equal keys, or skip over them? 

> It is bad to skip over equal keys, because then you would have equal keys on either side. Best would be to collect all equal keys together (but how would you do that?)
>
> The solution is the 3-way quicksort, where it has not just partitions for larger and smaller items, but also for equal items.

### 3-way quicksort (entropy-optimal sorting)
