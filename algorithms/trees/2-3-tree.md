# 2-3 search trees
This tree structure maintains a symmetric order and perfect balance. It's height would be `lgN` in worst case, and `log_3 N` in the best case. This tree maintains 2 types of nodes.
* 2-node that holds 1 key  and 2 children 
* 3-node that holds 2 key and 3 children
* It has a 3rd temporary node, 4-node, that has 3 keys and 4 children. This node never stays in the tree, because it is transformed into two 2-nodes upon processing. 

## Splitting nodes in 2-3 tree
![2-3 tree](images/balanced-search-tree.png)

## Inserting into a 2-3 tree
When inserting new keys to a 2-3 BST, we will find placement for the key similarly to a binary tree, comparing if the new key is greater or less than the key of current node. 
* If we come into contact with a 2-node, then we check if it has any children. If it has none, then we combine the keys into a 3-node and we're done, else we travel down to the expected child node. 
* If we come into contact with a 3-node, then we check if it has any children. If it has children we travel to the child based on if our key is greater, less or inbetween the keys of current node, and continue on our way down.
  * When we come across a 3-node leaf, then we check if our key is greater, less or inbetween the two keys within the node. 
  * If we end up at a 2-node that has no children, then we transform the node into a 3-node with their two values.
  * When we are trying to add a new key to a 3-node leaf, then we create a temporary 4-node that will be split into two 2-nodes. 
      * We figure out which of the 3 keys is the middle one, and pass it up to the parent.
      * Then we split the 4-node into two 2-nodes. The children are split between them accordingly.
   * The process cascades upwards until we run into a 2-node and combine the keys into a 3-node and be done.