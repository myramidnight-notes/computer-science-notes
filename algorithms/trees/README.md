# Trees!
Structures with nodes and branches. 

## Binary Search Trees (BST)
> While a binary tree simply has two branches per node, a binary search tree will be organized by size based on the root value (generally the first item placed into the tree). All nodes on the right side of the tree are larger than the root, and all nodes on the left side of the tree are less than the root. This rule is true for any node within the tree.

## [2-3 search tree](2-3-tree.md)
> A balanced and symmetrical tree that maintains 2 types of nodes. 

### [Red Black Sorting Tree](red-black-tree.md)
> Red black search trees represent a 2-3 tree as a BST. It maintains balance by rotating the nodes clusters whenever needed. 
>
>The name comes from the fact that the printer at the workplace where this algorithm was developed could at the time (impressivly) print in both red and black . 

## [Balanced Search trees (B-trees)](b-tree.md)
## kd trees
> Tree that expands in k dimensions. Basically layers of binary trees for each dimension.

## [Tries](tries.md) 
> Trees that are specifically made for sorting strings. Each node has as many children as there are possible characters (the allowed alphabet). This tree makes it easy to find all strings that contain the same prefix.

### Ternary search tries