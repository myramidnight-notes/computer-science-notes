# B-tree (self balancing tree)
> Bayer-McCreight, 1972

> In computer science, a B-tree is a __self-balancing tree data structure__ that maintains sorted data and allows searches, sequential access, insertions, and deletions in logarithmic time. The B-tree generalizes the binary search tree, allowing for nodes with more than two children. Unlike other self-balancing binary search trees, the B-tree is well suited for storage systems that read and write relatively large blocks of data, such as disks. __It is commonly used in databases and file systems.__ 
>
> [Wikipedia](https://en.wikipedia.org/wiki/B-tree)


Generalize 2-3 trees by allowing up to `M - 1` key-link pairs per node.
* At least 2 key-link pairs at the root
* At least `M/2` key-link pairs in other nodes
* Externa nodes contain client keys (that lead to the associated data)
* Internal nodes contain copies of keys to guide search.

![B-tree example](images/b-tree-example.png)

## Inserting into a b-tree
1. Search for new key
1. Insert new item at bottom of tree
1. Split nodes with `M` key-link pairs on the way up the tree

![B-tree insert](images/b-tree-insert.png)

## Searching in a B-tree
1. Start at the root
1. Find where the search key belongs in the tree
    * Find the node where the key should reside
