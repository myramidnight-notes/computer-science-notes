# Red-Black sort tree (RB tree)
It is basically __binary search tree (BST)__ but can also be rendered as a 2-3 tree. Very popular when you need to detect if the structure requires sorting when new data is added.

We assign the colors `red` or `black` according to the following rules to keep track of sorting status.

### Study links

* [Algs4 chapter 3.3](https://algs4.cs.princeton.edu/33balanced/)
* [Video demo of RedBlackBST (from algs4)](https://algs4.cs.princeton.edu/lectures/demo/33DemoRedBlackBST.mov)

## Let me try to explain a Left Leaning RB tree

### Representations: 2-3 tree and binary tree in red-black.
* Red nodes will equal a 3-way node.
* black node is a 2-way/binary node.
> You can visualize each node being colored, or the link to their parent being colored, or both (since it all amounts to the same thing, showing what nodes are connected as 3-way node). 

![2-3 way tree in RedBlack sort tree](images/redblack_2-3_tree.PNG)

#### Setting the colors
1. Root is always black! (it has no parent link to color)
1. New nodes being added should be red. 
1. We only take action whenever there are two red nodes adjacent or linked together.

#### Sorting the red.
__All red connections/nodes should be on the left side whenever a tree has been stabilized.__ So if they are on the right, then they need to be turned, if both children are red then they do a color swap with parent, turning black and parent turns red.

### Turning the red nodes 
>#### Left turn
>Since the left nodes are smaller than parent and right are larger, by turning the group to the left we would put the right child up to replace the parent, while the parent now becomes the left child of the new parent.
>#### Right turn
>A right turn would move the smaller node up to replace the parent, while the parent becomes the right child of the new parent. 

### Add a node, mark it (or the connection) red.
1. Check if the node is a left child 
    * if true, then we're done with the sorting.
2. If the node was a right child, then check if there is a red left child. 
    * if true, then both children will swap color with the parent, becoming black while parent turns red). Jump to step 3.
    * if false, then make a left turn on the nodes, elevating the new node up to be the parent, while the parent turns into a left child. 
3.  Check if the parent of the red node is also a red left child.
    * if true, then we perform a right turn on the connected nodes. 
    * if false, then we're done. We expect all lone red nodes/connections that we run into to be left children.

These staps are then repated until we no longer run into connected or adjacent red nodes, turning them as needed.

### Handling the children of full nodes being turned.
The extra child of the promoted node will just attach itself to the newly demoted node. Look at the video below for a demo of the red-black BST in action.

