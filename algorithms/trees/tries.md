# Tries (re<u>trie</u>val tree)
Trees that are specifically made for sorting strings. Each node has as many children as there are possible characters (the allowed alphabet). This tree makes it easy to find all strings that contain the same prefix.

![Tries](images/tries.png)

## R-way trie
R-way tries make it easy to trace strings within the tree, but it will leave us with alot of null-links (number of unused leafs) and that means taking a whole lot more space than strictly nessiery.
* Each node has as many children as the number of characters we can use.
* The last character of a string within the trie will contain the key (the string) and value, most likely indicating the order of when the string was entered into the trie. If a node has no value, then it is not a string within the trie.
* If a string exists in the trie already, then the value of the latest instance of the word will overwrite the existing one.
* Each node has as many null leafs as the allowed alphabet, which means that a trie will usually take a whole lot more space than it's using.
* To delete a string, we travel to the node that is the last character of the string within the trie, and check if current node has no children then we can delete the node and travel up to the next letter, until we find a node that is within the word that still has children. Then we're done.

## Tenary search trie (TST)
This version of trie only keeps nodes for characters used, instead of making room for all possible characters. This results in less wasted space, alot fewer null links (empty possible leafs)
* Each node has 3 children: smaller, equal or larger.
* Now the root node has a key (a character). 
* If the key of current node that matches the string, then we go down the center branch if the string continues (the string can end on a left/right node).
* If the character does not match, then we go either left or right. 
* If we can spell out the full string that we're searching for but the value of that last node is null, then that string wasn't actually in the trie (that just means the prefix for it exists).

### TST with 