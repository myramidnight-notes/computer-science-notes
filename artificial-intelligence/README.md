# Artificial Intelligence
>"Artificial Intelligence, a modern approach" third edition, by Stuart Russel and Peter Norvig.

## Index

* ### Chapter 2: Intelligent Agents
    * #### [Agents](agents-environment/agents.md)
        > Concept of rationality
    * #### [Agent Program and Structure](agents-environment/agent-program.md)
    * #### [The nature of Environments](agents-environment/environments.md)
* ### Chapter 3: Solving problems by searching
    * #### [Solving problems](search/solving-problems.md)
    * #### [Searching for solutions](search/searching.md)
    * #### [Search algorithms](search/search-algs.md)