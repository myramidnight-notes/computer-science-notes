# Agent Program
Agent program is an __implementation of an agent function__. 

An __agent function__ is a map from the percept sequence (history of all things the agent has percieved to date) to an action.

## Structure of agents
### Agent Program and Architecture
> The job of AI is to design an __agent program__ that implements the _agent functions_, the mapping from percepts to actions. We assume this program will run on some sort of computing device with physical sensors and actulators, we call this the __architecture__
>
>_agent = architecture + program_

So instead of looking up in a table of _percept sequences_ that might be infinitely long (since the order of percepts will matter) to decide our actions, we will have a program that will decide the actions rather than looking through this table for the exact match of percepts.

So we can have this _agent program_ make choices of what actions to take, based on the current percepts and internal knowledge (previous actions might alter some variables, which allows the program to be aware of how to proceed based on previous actions, even if it might not have any knowledge of what happened before, it all depends on the programming).

## Types of agents
* ### Reflex Agents
    > It is the simplest kind of agent, that reacts to the _current_ percepts, ignoring any percept history. 
    >
    >So if we have a vacuum agent, it will have sensors to see if current spot is dirty or not.
    >```bash
    >if
    >   spot-is-dirty then suck-up-dirt 
    >else: 
    >   do-something-else
    >```
    >![](images/agent-type-reflex.png)
* ### Model-based reflex agents
    >The most effective way to handle partial observability is for the agent to _keep track of the part of the world it can't see now_. So it maintains some _internal state_ that depends on the percept history and thereby reflects at least some of the unobserved aspects of the current state.
    >
    >The reason it is called model based, is because the program will have a _model_ of how the world works, perhaps implemented with boolean variables that change depending on the percept history so that the agent can simply refer to those variables to know what's going on in the world.
    >
    >![](images/agent-type-model-reflex.png)
* ### Goal-based agents
    >Knowing something about the current state of the environment is not always enough to decide what to do. Sometimes we need to have some sort of a __goal__ to work towards, information that describes a desirable state.
    >![Model reflex goal based](images/agent-type-model-goal.png)
    >
    >Sometimes goal-based action selection is straightforward, such as when a goal satisfaction results immediatetly from a single action. Sometimes they are more tricky if the agent has to consider long sequences of twists and turns in order to achieve the goal.
    >* __Search__ and __planning__ are the subfields of AI devoted to finding actino sequences that achieve the agent's goal
* ### Utility-based agents
    >Goals alone are not enough to generate high-quality behaviour in most environments. Goals provide a crude binary distinction between "happy" and "unhappy" states. A more general performance measure should allow a comparison of different world states according to exactly how happy they would make the agent. Because "happy" does not sound very scientific, economists and computer scientists use the term __utility__ instead.
    >
    >An agent's __utility function__ is essentially an internalization of the performance measure. If the internal utility function agrees with the external performance measure, then an agent that chooses actions to maximize its utility will be rational according to the external performance measure.
    >
    >Technically speaking, a rational utility-based agent chooses the action that maximizes the __expected utility__ of the action outcomes, that is, the utility the agent expects to derive, on average, given the probabilities and utilities of each outcome.
    >
    >![](images/agent-type-model-utility.png)
* ### Learning agents
    >In many areas of AI, this is now the preferred method for creating state-of-the-art systems. Learning has another advantage, it allows the agent to operate in initially unknown environments and to become more competent than its initial knowledge alone might allow.
    >
    >![](images/agent-type-learning.png)
    >
    > A learning agent can be devided into four conceptual components
    >* The __learning element__ which is responsible for improvements
    >   >
    >* The __performance element__ which is responsible for selecting external actions
    >* The __critic__ provides feedback to the learning element on how the agent is doing, so that the learning element can then modify the performance element to do better in the future. 
    >   >The critic tells the learning element how well the agent is doing with respect to a fixed performance standard. The critic is necessary because the percepts themselves provide no indication of the agent's success. 
    >* The __problem generator__ is responsible for suggesting actions that will lead to new and informative experiences. 
    >   >The point is that if the performance element had its way, it would keep doing the actions that are best, given what it knows. But if the agent is willing to explore a little and do some perhaps suboptimal actions in the short run, it might discover much better actions for the long run. The problem generator's job is to suggest these exploratory actions.


## How do the components of the agent program work?

### State representation 
You can actually categorize the types of states and the transitions between them based on complexity and expressive power:
* __Automic__
    > In an __automic representation__ each state of the world is indivisible (cannot tell them appart), it has no internal structure. The state of the world is considered as a whole.
* __Factored__
    >A __factored representation__ splits up each state into a fixed set of _variables_ and _attributes_, each of which can have a _value_. While two atomic states have nothing in common, two different factored states can share some attributes.
* __Structured__
    > __Structured representation__ underlie _relational databases_ and _first-order logic_, _first-order probability models_, _knowledge-based learning_ and much of _natural language understanding_. It is a world where objects can be described explicitly. 

![](images/agent-prog-state-transitions.png)