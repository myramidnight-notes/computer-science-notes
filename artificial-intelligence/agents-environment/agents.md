> Chapter 2.1
# Agents and Environments
![](images/agent-environment.png)

## Agent
An __agent__ is anything that can be viewed as perceiving its __environment__ through __sensors__ and acting upon that environment through __actuators__.

> An agent's choice of actions at any given instant can depend on the entire percept sequence observed to date, but not on anything it hasn't perceived

* We use the term __percept__ to refer to the agent's perceptual inputs at any given instant.
* An agent's __percept sequence__ is the complete history of everything the agent has ever percieved.

### Agent functions
If we create a table of agent functions, and they depend on the _percept sequence_ in each case to decide what action the agent takes based on perception, that table can become infinitly long.

![](images/agent-vacuum-cleaner.png)


## Concept of rationality (good behaviour)
A __rational agent__ is one that does the right thing, conceptually speaking. Every entry in the table for the agent functions is filled out correctly.

>What is rational at any given time depends on four things:
>1. The performance measure that defines the criterion of success
>2. The agent's prior knowledge of the environment
>3. The actions that the agent can perform
>4. The agent's percepts sequence to date

### The definition of a rational agent
>_For each possible percept sequence, a rational agent should select an action that is expected to maximize its performance measure, given the evidence provided by the percept sequence and whatever built-in knowledge the agent has._

### How can we tell if the agent did well?
When an agent is plunked down in an environment, it generates a sequnce of actions according to the percepts it receives. This sequence of actions causes the environment to go through a sequence of states (such as a dirty room becomes a clean room after vacuum has sucked up all the dirt). If the sequence is desirable, then the agent has performed well. 

>This notion of desirability is captured by a __performance measure__ that evaluates any given sequence of environment states.

If the measure of performance would be how often the vacuum sucks up dirt, it could simply clean the room, dump the dirt and clean it again, to increase the count. 

>As a general rule, it is better to design performance measures according to what one actually wants in the environment, rather than according how one thinks the agent should behave

## Omniscience, Learning and Autonomy
### Omniscience
We need to be careful to distinquish between _rationality_ and __omniscience__
> An omniscient agent knows the _actual_ outcome of its actions and can act accordingly. But omniscience is impossible in reality.

An example could be us trying to cross the road. The road is empty in both directions, so our rational selves cross the street, but somewhere high above in the sky a cargo-door on an airliner fails and drops a crate, which would fall ontop of us and crush us while crossing the road. Was it irrational to cross the road? According to the traffic we would have crossed it safely, that would have been rational, but we are not omniscient.

This shows that rationality is not the same as perfection.

> Rationality maximizes _expected_ performance, while perfection maximizes _actual_ performance. 

### Learning
> Doing actions _in order to modify future percepts_ is sometimes called __information gathering__. It is an important part of rationality, like looking both ways when crossing the street.

We could send our example vacuum agent to explore an initially unknown environment, to learn something about it that will effect its perception (such as learning how large the area is, which might decide how it will tackle the task of cleaning it)


### Assumptions
If the environment is completely known then the agent does not need to percieve or learn, it simply acts. Such agents are fragile, they would not perform well if placed in a different environment.

An example that came in the book was a dung beetle making a nest, it would go find a ball of dung to plug up the hole to the nest. But if that ball of dung would be removed from the beetle WHILE it's in the process of pushing it back to the nest, it will continue towards the nest to plug the hole with a non-existant dung ball. It's behaviour has been built on the assumption that it will not loose the ball on the way to the nest.

>Not sure how accurate that fact is, but still a good example of behaviour based on assumptions about the environment.
>
>[True facts about the dung beetle (zeFrank)](https://www.youtube.com/watch?v=Q1zbgd6xpGQ)

### Autonomy
An agent that relies on the prior knowledge of its designer rather than its own percepts, we would say that agent lacks __autonomy__.

>A rational agent should be autonomous: It should learn what it can to compensate for partial or incorrect prior knowledge. 

After sufficient experience of its environment, the behaviour of a rational agent can become effectively _independent_ of its prior knowledge. Like evolution in a way.
