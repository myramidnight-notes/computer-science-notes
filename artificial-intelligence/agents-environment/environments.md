# The nature of Environment

## Specifying the task environment
We have to specify the performance measure, environment, agent's actuators and sensors. We have grouped them under the heading of the __task environment__. To describe the task environment, we use the _PEAS_ description.
1. Performance measure
    > What is the performance measure to which we desire? Desirable qualities.
1. Environment
    > The environment that the agent will be placed in
1. Actuators
    > The actuators that allow the agent to act within its environment
1. Sensors
    > Sensors to allow the agent to perceive its environment

![The PEAS description for self driving taxi](images/peas-description.png)

Not all agents have to be physical. We can have __software agents__ that live in a virtual environment, perceiving inputs from keyboards or other things. But it's essentially the same as for any other agent, their environment just happens to be virtual.

![PEAS 2](images/peas-description2.png)

## Properties of task environments
* ### Observability
    * __Fully observable__ 
        >A task environment is effectively fully observable if the sensors detect all aspects that are _relevant_ to the choice of action. Relevence, in turn, depends on the performance measure.
        >
        >Fully observable env are convenient because the agent does not need to maintain any internal state to keep track of the world.
    * __Partially observable__
        > An environment might be partilly observable because of noisy or inaccurate sensors, or because parts of the state are simply missing from the sensor data. An example would be a vacuum agent that can only detect if the current position is dirty, but not where it can find dirt in the room.
    * __Unobservable__
        > If an agent has no sensors at all, then the environment is unobservable. Anything it doesn't have sensors for is unobservable. But this does not always mean that the agent will be hopeless. We can sometimes use other senses to become aware of what we can't actually observe.
*  ### Agents
    * __Single Agent__
        >An agent that handles a crossword puzzle by itself is clearly in a single-agent environment. 
    * __Competative multi-agent__
        >A game of chess requires two players, so it requires two agents, therefor a __competative__ multiagent environment. 
    * __Cooperative multi-agent__
        >Vacuum agents working together to clean a large area would be an example of __cooperative__ multiagent environment.
* ### Deterministic vs. Stochastic
    * __Deterministic__

        > If the next state of the environment is completely determined by the current state and the action executed by the agent, then we say the environment is _deterministic_
    * __Stochastic__
        > If we cannot be sure of the next state, regardless of current actions, then the environment is considered _stocastic_. The word "stochastic" generally implies that uncertainty about outcomes is quantified in terms of probabilities.
    * __Uncertain__
        >We can say the environment is _uncertain_ if it is not fully observable or not deterministic. 
    * __Nondeterministic__
        > A _nondeterministic_ environment is one in which actions are characterized by their _possible_ outcomes, but no probabilities are attached to them. Nondeterministic environment descriptionsare usually associated with performance measures that require the agent to succeed for _all possible_ outcomes of its actions.
* ### Episodic vs. Sequential
    * __Episodic__
        >In an episodic task environment, the agent's experience is divided into atomic episodes. In each episode the agent receives a percept and then performs a single action. Crucially, the next episode does not depend on the actions taken in previous episodes, and current decision doesn't effect whether the next part is defective.
    * __Sequential__
        >In sequential environments, the current decision could affect all future decisions. 
* ### Static vs. Dynamic
    * __Dynamic__
        >If the environment can change while an agent is deliberating, then we say that environment is dynamic for that agent. Dynamic environments are constantly asking the agent what it wants to do, because it could have changed from it's previous perception.
        >
        >Taxi driving is clearly dynamic, the other cars and the taxi itself keep moving while the driving algorithm dithers about what to do next.
    * __Static__
        >It is the oposite of dynamic. Static environments are easy to deal with because the agent does not need to keep looking at the world while it is deciding on an action, nor need to worry about the passage of time.
        >
        >A crossword puzzle is static
    * __Semidynamic__
        >If the environment itself does not change with the passage of time, but the agent's performance score does, then we can say the environment is _semidynamic_
        >
        >Chess played with a clock is semidynamic, the clock effecting the score, but the board doesn't change with passage of time
* ### Discrete vs. Continuous
    * __Discrete__
        >Environment with a finite amount of distinct states is called _discrete_. Such as chess where pieces are either on the board or not. We are not taking into account the possible states of pieces between transitions, because that simply isn't relevant. Input from digital cameras is discrete, because every output from it is a static image (even in motion, it is built with static frames).
    * __Continuous__
        >A taxi driving is in a continuous state, because it's continously moving. 
* ### Known vs. Unknown
    This is strictly speaking not about the environment itself, but the agent's (or designer's) state of knowledge about the "laws of physics" of the environment. 
    * __Known__
        > In a known environment, the outcomes (or outcome probabilities if the environment is stochastic) for all actions are given.
        >
        >It is possible for a known environment to be partially observable, such as in a solitaire card game, we know what cards we can have, but we cannot observe all of them (the face-down cards).
    * __Unknown__
        > In a unknown environment, we obiously we will known nothing about that environment. The agent will have to learn how it works in order to make good decisions.
        >
        >A unknown environment can be fully observable, such as in video game that shows the entire game state on the screen, but we don't know what the buttons do until we try them.

![Env properties](images/env-properties.png)
> ### Examples
> #### Part-picking robot
> >We would describe it as episodic, because it normally considers each part in isolation. But if one day there is a large batch of defective parts, the robot should learn from several observations that the distribution of defects has changed, and should modify its behaviour for subsequent parts. 
> #### Medical diagnosis system
> >Medical-diagnosis task has been listed as a single-agent because the disease process in a patient is not profitably modeled as an agent, but a medical-diagnosis system might also have to deal with recalcitrant patients and sceptical staff, so the environment could have a multiagent aspect. Furthermore, medical diagnosis is episodic if one conceives of the task as selecting a diagnosis given a list of symtoms. The problem is sequential if the task can include proposing a series of tests, evaluating process over the course of treatment, and so on.
> #### Games
> > A chess tournament consists of a sequence of games, each game is an episode because (by and large) the contribution of the moves in one game to the agent's overall performance is not effected by the moves in its previous game. On the other hand, decision making within a single game is certainly sequential.
>
>>Backgammon is a board game where the board is fully observable, and you cannot determine where the next piece will be placed (so its stochastic). Each player takes a turn at placing a piece on the board, and all pieces placed on the board effect placement of later pieces (there for it is sequential). Static because the board never changes, and discrete because we don't care about where the pieces are until they are placed in their proper spot (no in betweens).