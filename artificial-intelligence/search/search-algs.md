# Search Algorithms
### Inrastructure for search algorithms
* `n.STATE`
    > The state in the _state space_ to which the node corresponds
* `n.PARENT`
    > The node in the _search tree_ that generated this node
* `n.ACTION`
    > The action that was applied to the parent to generate the node
* `n.PATH_COST`
    > The cost, traditionally denoted by `g(n)`, of the path from the initial state to the node, as indicated by the parent pointers.


```java
function CHILD_NODE(problem, parent, action) returns a node
    return a node with
        STATE = problem.RESULT(parent.STATE, action),
        PARENT = parent, 
        ACTION = action,
        PATH_COST = parent.PATH_COST + problem.STEP_COST(parent.STATE, action)
```

## Algorithms
* ### [Uniformed Search Strategies](search-uniform.md)
    * Breath-first Search (BFS)
        > It expands the _shallowest_ node first, layer by layer. Gets more _expensive_ on memory the deeper it goes.
    * Uniform-cost Search
        > It expands the nodes based on path costs, choosing the node that has the cheapest path cost to reach
        >```
        >f(n) = g(n)
        >```
        
    * Depth-first Search (DFS)
        > It always expands the _deepest_ node first, until it finds no successors, then it backtracks to the nearest unexpanded node and continues looking for the deepest nodes
    * Depth-limited Search
        > It will treat nodes at depth _l_ as if they have no successors, othervise it behaves just like depth-first search. It limits how deep the search will dive into the tree
    * Iterative deepening depth-first Search (IDS)
        > It is often used in combination with depth-first tree search, that finds the best depth limit. It does this by gradually increasing the limit until a goal is found.
        >
        >It combines the benefits of depth-first and breath-first search.
        >
        >It might seem wasteful since it has to repeat the search with each iteration
    * Bidirectional Search
        > It runs two searches simultaneously, one forward from the initial state, and the other backward from the goal state, hoping that the two searches meet in the middle.
        >
        >It is implemented by replacing the goal test with a check to see whether the frontiers of the two searches intersect, if they do, then a solution has been found
* ### [Informed (Heuristic) Search Strategies](search-heuristic.md)
    These are _best-first search_ strategies, which is a instance of tree-search and graph-search algorithm in which a node is selected for expansion based on an __evaluation function__. 

    * Greedy best-first Search
        > Tries to expand the node that is closest to the goal, on grounds that this is likely to lead to a solution quickly. It evaluates nodes by using just the heuristic function: 
        >```
        >f(n) = h(n)
        >```
    * A* Search (A-star search)
        >The most widely known form of best-first search. It evaluates nodes by combining the cost to reach the node and the cost to get from the node to the goal.
        >```
        >f(n) = g(n) + h(n)
        >```
        >_f(n)_ = estimated cost of the cheapest solution through _n_
        >
        >So it is trying to find the cheapest solution
    * Memory-bounded heuristic Search (MA*)
        >It reduces the memory requirements for A* search by adapting the idea of _iterative deepening search_ to the heuristic search context, resulting in a __iterative-deepening A\*__ (IDA*) algorithm.
        * Recursive best-first search (RBFS)
        * Simplified memory-bounded A* (SMA*)

    ![](images/search-heuristic-cost.png)