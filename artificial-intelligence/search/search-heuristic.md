# Informed (Heuristic) Search Strategies
The general approach we consider is called __best-first search__

> Just as a reminder, _g(n)_ is the path cost from the initial state to the node

### Evaluation Function _f(n)_
> The choice of _f_ determines the search strategy. 
### Heuristic Function _h(n)_
Most best-first algorithms include as a component _f_ a __heuristic function__, denotes as _h(n)_
> _h(n)_ = estimated cost of the cheapest path from the state at node _n_ to a goal state

Heuristic functions are the most common form in which additional knowledge of the problem is imparted to the search algorithm. We consider them (for now) to be arbitrary, non-negative, problem-specific functions, with one constraint:
* If _n_ is a goal node, then _h(n)_ = 0

## Greedy best-first Search
