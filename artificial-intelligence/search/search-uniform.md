# Uniformed Search Strategies
### Comparing the uniform search strategies
> ![](images/search-uniform-compare.png)

## Breath-first search
> Breath-first search is a simple strategy where the _shallowest_ unexpanded nodes are chosen for expansion. 
>1. The root node is expanded first
>1. then all the successors of the root node are expanded next
>1. and so on, level by level

* Breath-first search uses __FIFO queue__ for the frontier

>![](images/search-breath-first.png)

#### Time and memory of Breath-first search
>![](images/search-uniform-memory.png)
* The __memory requirements__ are a bigger problem for breath-first search than the _execution time_. 
    > The search algorithm might be fast in most cases, but speed is reduced when it spends a lot of time running around the memory while searching, which grows faster the deeper we go.
* Exponential-complexity search problems cannot be solved by uniform methods for any but the smallest instances

#### Pseudo-code implementation
```java
function BREATH_FIRST_SEARCH(problem) returns a solution, or failure
    node <- a node with STATE = problem.INITIAL_STATE, PATH_COST = 0
    if problem.GOAL_TEST(node.STATE) then return SOLUTION(node)
    frontier <- a FIFO queue with node as the only element
    explored <- an empty set
    loop do
        if EMPTY?(frontier) then return failure
        node <- POP(frontier) // chooses the shallowest node in frontier
        add node.STATE to explored
        for each action in problem.ACTIONS(node.STATE) do
            child <- CHILD_NODE(problem, node, action)
            if child.STATE is not in explored or frontier then
                if problem.GOAL_TEST(child.STATE) then return SOLUTION(child)
                frontier <- INSERT(child,frontier)
```

## Uniform-cost search
Instead of expanding the shallowest node, __uniform-cost search__ expands the node _n_ with the __lowest path cost__ _g(n)_. This cost will decide how _desirable_ the path is to the algorithm.

#### Significant Differences from breath-first search
>Besides for ordering the queue by path cost, there are two other significant differences from breath-first search
>1. The goal test is applied to a node when it is _selected for expansion_, rather than when it is first generated.
>    > The reason is that the goal node that is _generated_ may be on a suboptimal path
>1. A test is added in case a better path is found to a node currently on the frontier

It is easy to see that uniform-cost search is optimal in general.
1. First we observe that whenever uniform-cost search selects a node _n_ for expansion, the optimal path to that node has been found.
1. Because step costs are non-negative, paths never get shorter as nodes are added.

These two facts together imply that _uniform-cost search expands nodes in order of their optimal path cost_.
> hence, the first goal node selected for expansion must be the optimal solution

Uniform-cost search does not care about the number of steps in the path, only about their total cost.

#### Pseudo-code implementation
```java
function UNIFORM_COST_SEARCH(problem) returns a solution, or failure
    node<- a node with STATE = problem.INITIAL_STATE, PATH_COST = 0
    frontier <- a priority queue ordered by PATH_COST, with node as the only element
    explored <- an empty set
    loop do
        if EMPTY?(frontier) then return failure
        node <- POP(frontier) // chooses the lowest-cost in frontier 
        if problem.GOAL_TEST(node.STATE) then return SOLUTION(node)
        add node.STATE to explored
        for each action in problem.ACTIONS(node.STATE) do
            child <- CHILD_NODE(problem, node, action)
            if child.STATE is not in explored or frontier then
                frontier <- INSERT(child, frontier)
            else if child.STATE is in frontier with higher PATH_COST then
                replace that frontier node with child
```
> Uniform-cost search on a graph. The algorithm is identical to the gernal graph search algorithm in another example, except for the use of a priority queue and the addition of an extra check in case a shorter path to a frontier state is discovered. The data structure for _frontier_ needs to support efficient membership testing, so it should combine the capabilities of a priority queue and a hash table

## Depth-First Search (DFS)
>__Depth-first search__ always expands the _deepest_ node in the current frontier of the search tree, so it dives as deep as it can go, before it backtracks to find the nearest unexpanded node so it can continue diving deeper.
* It essentially starts going down one side of the tree, and then gradually searches the tree bottom-up from one corner until it finds a goal state.
* When it starts backtracking up the tree, it has confirmed what nodes are not goal states and can therefor remove them from memory (they are never going to visit those again anyway)
* Depth-first search uses __LIFO queue__
> ![](images/search-depth-first-expand.png)


## Depth-Limited Search (DLS)
> __Depth-limited search__ solves the failure of _depth-first_ search to handle infinite state spaces by supplying the search with a predetermined __depth limit__. 

* All the nodes at the given depth limit will be treated as though they have no successors
* It solves the infinite depth problem, but intrduces a source of incompleteness if we choose a depth limit less than the depth of the shallowest goal
* The depth-limited search will also be non-optimal if we choose a depth limit greater than the shallowest goal.
* Its __time complexity__ is _O(b<sup>l</sup>)_, where _l_ is the depth limit.
* A depth-limited search with the limit set to &infin; is the same as a depth-first search.

#### Pseudo-code of implementation
``` java
function DEPTH_LIMITED_SEARCH(problem, limit) returns a solution, or failure/cutoff
    return RECURSIVE_DLS(MAKE_NODE(problem.INITIAL_STATE), problem, limit) 

function RECURSIVE_DLS(node, problem, limit) returns a solution, or failure/cutoff
    if problem.GOAL_TEST(node.STATE) then return SOLUTION(node)
    else if limit = 0 then return cutoff
    else
        cutoff_occured? <- false
        for each action in problem.ACTIONS(node.STATE) do
            child <- CHILD_NODE(problem, node, action)
            result <- RECURSIVE_DLS(child, problem, limit - 1)
            if result = cutoff then cutoff_occured? <- true
            else if result != failure then return result
        if cutoff_occured? then return cutoff else return failure

```

## Iterative Deepening Depth-first search (IDS)
> Iterative deepening search is a general strategy, often used in combination with depth-first tree search, that finds the best depth limit.
>
>It does this by gradually increasing the limit until a goal is found

* This iterative way of increasing the depth limit means that this search will run multiple time, every time the limit is increased.
* Iterative deepening combines the benefits of _depth-first_ and _breath-first_ search.
    * Like depth-first search, its memory requirements are modest: _O(b*d)_
    * Like breath-first search, it is complete when the branching factor is finite, and it is optimal when the path cost is a nondecreasing function of depth of the node.

![](images/search-iterative-deep-expand.png)

#### Pseudo-code implementation
```java
function ITERATIVE_DEEPENING_SEARCH(problem) returns a solution, or failure
    for depth = 0 to infinity do
        result <- DEPTH_LIMITED_SEARCH(problem, depth)
        if result != cutoff then return result
```

## Bidirectional Search
>Bidirectional search run two searches simultaneously
>1. One search forward from the initial state
>1. Another search backward from the goal
>
>The hope is that the two searches meet in the middle.

Bidirectional search is implemented by replacing the goal test with a check to see whether the frontiers of the two searches intersect.
* If they meet in the middle, then a solution has been found
* The first solution found might not be optimal, even if they are both breath-first
    >Some additional search is required to make sure there isn't another short-cut across the gap
* The check can be done when each node is generated or selected for expansion, and with a __hash table__, it will take constant time.
* __Time complexity__ of bidirectional search using breath-first searches in both directions is _O(b<sup>d/2</sup>)_
    > The reduction in time complexity makes bidirectional searches attractive
* __Space complexity__ is also _O(b<sup>d/2</sup>)_
    >We can reduce this by roughly half if one of the two searches is done by iterative deepening, but at least one of the frontiers must be kept in memory so that the intersection check can be done
    * This space requirement is most sagnificant weakness of bidirectional search

![](images/search-bidirection-expand.png)