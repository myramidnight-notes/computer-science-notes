# Searching for Solutions
A solution is an action sequence, so search algorithms work by considering various possible action sequences.

* All possible action sequences are represented by a __search tree__
* The initial state is at the __root__ of the tree
* The __branches__ are the actions
* The __nodes__ correspond to states in the _state space_ of the problem.
* The __path__ we take towards a _goal node_ represents the action sequence

Initially we have no knowledge of what might actually be the most optimal path, even though we might know where all the nodes are and the edges between them. 

Until we start searching for a goal and a optimal path towards it, we will only know about our current state and actions that would lead us to other states.

### Expanding the tree
The first thing we can do when beginning a search is to __expand__ our current state, which will give us information about all the __child nodes__ that branch out from the __parent node__. This gives us information about our possible destinations.
* __expanding__ a node will reveal to us information about the _child nodes_ of the selected node.
* __child node__ branch out from their _parent node_. They are possible destinations that we could visit.
* __parent node__ is the node above us (since the root is generally at the top)
    > The _root_ is the only node in the tree without a parent. 
* __leaf node__ is any node that has _no children_. 
    >Though in search trees a _leaf_ does not always mean that it doesn't actually have children, but instead that we haven't confirmed it yet by __expanding__ the node (to get information about the node and it's children). 
* __frontier__ (sometimes known as _open list_) is a list of all leaf nodes that we can expand
    >A search is not done until all nodes have been removed from the frontier, which signals that there are no more nodes to visit

>![](images/search-expand.png)
>* The dark nodes have been expanded
>* The nodes with bold outline are in the frontier, available to be expanded to reveal new nodes.

## Search strategy
It is the algorithm to traverse a search tree that decides which nodes in the frontier we should expand next, in our search for a _goal state_. 

>Sometimes the same state can appear multiple times in a search-tree due to the fact that there might be multiple paths leading to it. The tree represents the various paths we can take, because we are searching for the most optimal path

### Repeated state
__Repeated state__ is a state that can appear multiple times down the same path, usually genered by a __loopy path__ (a circle in the graph).
> If we cannot detect when we've entered a loop, then we might be traveling down that path for infinity, never finding a goal state (we would probably have found it already if it was along the circle)

### Redundant paths
Loopy paths are a special case of _redundant paths_, which exist whenever there is more than one way to get from one state to another.

If there are multiple paths to a destination, we would generally pick the cheapest one, making the other paths redundant.

* #### Avoiding revisiting states (with Graph search)
    > "Algorithms that forget their history are doomed to repeat it"

    One way to avoid exploring redundant paths is to remember where we have been. To do this, we augment the _tree search_ algorithm with a data structure called the __explored set__ (or __closed list__), which remembers every expanded node. 

    Newly generated nodes that match previously generated nodes (the ones in the explored set or frontier) can be discarded instead of being added to the frontier. 

    ![](images/graph-search-tree.png)

    ![](images/graph-search.png)

    As every step moves a state from the frontier into the explored region while moving some states from the unexplored region into the frontier, we see the algorithm is _systematically_ examining the states in the state space, one by one, until it finds a solution.
