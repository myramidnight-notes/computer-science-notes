# Solving problems by searching

Intelligent agents are supposed to maximize their performance measure, and achieving this is easier to do by adopting a __goal__ to aim to satisfy.

### Problem Solving Agents
>* A __problem-solving agent__ is a type of _goal-based agent_. 
>* They use __atomic representations__, where the states of the world are considered as wholes

### Planning agents
> They are also goal-based agents that use more advanced __factored__ or __structured__ representations of the state of the world.

## Searching for a solution
The process of looking for a sequence of actions that reaches the goal is called __search__. 
1. A search algorithm takes a problem as input and returns a _solution_ in the form of an _action sequence_.
1. Once a solution is found, the actions it recommends can be carried out in a __execution phase__.
1. After formulating a goal and a problem to solve, the agent calls a _search procedure_ to solve it.
1. It uses the solution from the search procedure to guide its actions
1. Once the solution has been executed, the agent will formulate a new goal.


### Open-loop system
>Notice that while the agent is executin gthe solution sequence, it _ignores its percepts_ when choosing an action because it knows in advance what they will be. 
>
>An agent that carries out its plans with its eyes closed, so to speak, must be quite certain of what is going on.
>
>Control theorists call this an __open-loop__ system, because ignoring the percepts breaks the loop between agent and environment

## Well-defined problems and solutions
A _problem_ can be defined formally by 5 components:
1. The __initial state__ that the agent starts in
1. A description of the possible __actions__ available to the agent.
    >Given a particular state `s`, we could have a function `ACTIONS(s)` that would return a set of actions that can be executed in the given state `s`.We can say that each of these actions is __applicable__ in certain states.
1. A __transition model__: a description of what each action does.
    > Thes model is specified by a function `RESULT(s,a)` that returns the state that results from doing action `a` in state `s`. We also use the term __sucessor__ to refer to any state reachable from a given state by a single action.
    >
    >Together, the initial state, actions, and transition model implicitly define the __state space__ of the problem, the set of all states reachable from the initial state by any sequence of actions.
    >
    >The _state space_ forms a directed network/graph in which the nodes are states and the links between them are actions. A _path_ in the state space is a sequence of states connected by a sequence of actions.
1. The __goal test__, which determines whether a given state is a goal state.
    > Sometimes the goal is specified by an abstract property rather than an explicitly enumerated set of states. 
1. A __path cost__ function that assigns a numeric cost to each path in the _state space_.
    > The problem-solving agent chooses a cost function that reflects its own performance measure. 
    >
    >The __step cost__ of taking action `a` in state `s` to reach state `s'` is denoted by `c(s,a,s')`. These costs could be anything that would make a path desirable/undesirable, such as distance, time, scale, or anything else that could be represented as a number. 
    >
    >We assume that step costs are non-negative

A __solution__ to a problem is an action sequence that leads from the inital state to a goal state. Solution quality is measured by the path cost function, and an __optimal solution__ has the lowest path cost among all solutions.

## Formulating problems
### Abstraction
Making a plan to solve a problem is at best just a model, _a abstract mathematical description_, and not the real thing.

There is a lot of details and considerations left out when searcing for a solution, it is __abstracted__ to be quicker to calculate a possible solution.

> If the problem would be traveling, we might only take into account the distances and cost when generating a solution. Details like the weather conditions, rest stops, road conditions, radio stations, visability,... are left out. 
>
>It only takes into account what has the biggest effect on finding the most optimal solution. 

## Types of problems
1. Toy Problems
    > Toy problems are intended to illustrate or exercise various problem-solving methods. 
    >
    >* Vacuum world
    >* 8-puzzle
    >* 8 queens problem
    >* ...
1. Real-world problems
    > Real-world problems are ones whose solutions people actually care about. Such problems tend not to have a single agreed-upon description, but we can give the general flavor of their formulations.
    >
    >* Route-finding problems
    >* Touring problems
    >* The traveling salesperson problem
    >* Robot navigation
    >* Automatic assembly sequencing
    >* ...