# Tölvusamskipti (TSAM) 2021
>* Lecturer: Stephan Schiffel
>* Language: C/C++

The book for the course: [Computer Networks - Peter L Dordal](http://intronetworks.cs.luc.edu/current2/html/index.html)

* ### Overview and programming
    * [Overview - Network Systems and Applications](logistics/README.md)
    * [Network Programming](networking/README.md)
* ### [The Network Layers ](network_layers/README.md)
    * __OSI__ model (Open Systems Interconnection) 
    * __TCP/IP__ (Transmission Control Protocol/Internet Protocol) 
* ### Networked Applications, protocols, design issues, routing
* ### Internet Protocols: BGP, OSPF, etc.
* ### Windows Networking
* ### Video and Audio streaming (case studies)
* ### Content Delivery Networks
* ### Wireless and Mobile networks
* ### Network security

## Course Materials
* Introduction to Computer Networks (Peter L. Dordal, 2nd edition)
    * http://intronetworks.cs.luc.edu/
* COmputer Networking, a top-down approach (Kurose, Ross)

>Jitter is change in delay


### Reading material:
#### L1: Logistics and Introduction
>* [Dordal, chapter 1: Overview of Networks](http://intronetworks.cs.luc.edu/current2/html/intro.html)
>   * Sections 1.14, 1.15
>* [Advanced socket communication tutorial](http://users.pja.edu.pl/~jms/qnx/help/tcpip_4.25_en/prog_guide/sock_advanced_tut.html)
>* Optional: [On holy wars and a plea for peace](https://www.ietf.org/rfc/ien/ien137.txt)

#### L3: The physical layer
>* Dordal: Chapter 2, Ethernet Section 2 - 2.1.2
>* Optional: Untangling the world wide mesh of undersea cables, Bischof
>* Optional: [What happens if Russia attacks undersea internet cables (WIRED)](https://www.wired.com/story/russia-undersea-internet-cables/)

#### L5: Local Area Networking
>* Dordal:
>   * [1.9: LANs and Ethernet](http://intronetworks.cs.luc.edu/current2/html/intro.html#lans-and-ethernet)
>   * [2.1 - 2.1.3: Ethernet Basics](http://intronetworks.cs.luc.edu/current2/html/ethernet1.html)
>* Optional: [How the internet reaches antartica](https://www.pilotfiber.com/blog/how-the-internet-reaches-antarctica)

#### L6: Internetworking
>* Dorpal: 
>   * [1.10: IP internet protocol](http://intronetworks.cs.luc.edu/current2/ComputerNetworks/intro.html#ip-internet-protocol)

#### L7: The internet
>* [Dorpal: 9.6 - 9.7: IPv4](http://intronetworks.cs.luc.edu/current2/html/ipv4.html)
>* Optional: [BGP in 2019: The BGP table](https://blog.apnic.net/2020/01/14/bgp-in-2019-the-bgp-table/)

#### L8: TCP/IP
>* [Dorpal 8: Abstract Sliding Windows](http://intronetworks.cs.luc.edu/current2/html/slidingwindows.html)

#### L9+10: Reliable Transport and TCP/IP
>* Dordal: 
>   * [Chapter 8: Abstract sliding windows](http://intronetworks.cs.luc.edu/current2/html/slidingwindows.html) 
>   * [Chapter 17, sections 1 - 3: End-to-End, TCP header, TCP connection](http://intronetworks.cs.luc.edu/current2/html/tcpA.html#the-end-to-end-principle)

#### L11: TCP/IP and UDP
>* Dordal:
>   * [Chapter 18 sections 1 - 14: TCP Issues and Alternatives](http://intronetworks.cs.luc.edu/current2/html/tcpB.html)
>   * Chapter 19: [TCP Reno and Congestion Managment](http://intronetworks.cs.luc.edu/current2/html/reno.html#index-22)
>* Optional for fun: [Wave Effects in Boats: Those boats in Texas paraded at the wrong speed _ FT Alphaville](https://www.ft.com/content/f1208eb1-ad45-4ff9-902e-948ecf66ff8f)

#### L12: Advanced TCP/IP
>* [Dordal Chapter 19: TCP Reno and Congestion Management](http://intronetworks.cs.luc.edu/current2/html/reno.html#index-22)
#### L13, L14: Routing
>* Dordal:
>   * [chapter 13: Routing-Update Algorithms](http://intronetworks.cs.luc.edu/current2/html/routing.html)
>   * [chapter 15: Border Gateway Protocol](http://intronetworks.cs.luc.edu/current2/html/bgp.html)

### L15: Routing Protocols
>* [chapter 15: Border Gateway Protocol (BGP)]( http://intronetworks.cs.luc.edu/current2/html/bgp.html)

### L16, L17: DNS and TCP/IP in the Application Layer
>* [chapter 10: IPv4 companions: DNS](http://intronetworks.cs.luc.edu/current2/html/ipv4companions.html#dns)
>* [When is it appropriate to use UDP instead of TCP?](https://stackoverflow.com/questions/1099672/when-is-it-appropriate-to-use-udp-instead-of-tcp)
>* Optional:
>   * [QUIC - WIll it replace TCP/IP? (pdf)](https://www.snia.org/sites/default/files/ESF/QUIC-Will-It-Replace-TCPIP-Final.pdf)
>   * [Adventures with big MTU's and ICMP, bumping the IPv6 MTU](https://blog.cloudflare.com/increasing-ipv6-mtu/)
>   * [IP Fragmentation is broken](https://blog.cloudflare.com/ip-fragmentation-is-broken/)
>   * [The hidden cost of QUIC and TOU](https://www.snellman.net/blog/archive/2016-12-01-quic-tou/)
>   * [Why PS4 downloads are so slow](https://www.snellman.net/blog/archive/2017-08-19-slow-ps4-downloads/)

### L18: Intro to Cryptography
### L19: Cryptography and Certificates
>* [chapter 29: Public Key Encryption](http://intronetworks.cs.luc.edu/current2/html/publickey.html)    
>   * [Certificate Authorities](http://intronetworks.cs.luc.edu/current2/html/publickey.html#ca)

### L20: Wifi and Mobile
### L21: Queing
>* [Bufferbloat](https://www.bufferbloat.net/projects/bloat/wiki/Introduction/)
>* [Queuing Theory](https://queue-it.com/blog/queuing-theory/)

### L22: Network Security