## OSI Model (1984)
It is organized in 7 layers. Each layer will make use of all the layers below, and this list starts from the bottom up (Application being the top layer)

1. Physical
    > The physical connection between computers, cables or waves
1. Data link
    > Which bits should be sent, in what order. How do we organize who can recieve and send at what time, so the information doesn't get scrambled or end up in the wrong place (over wireless for example)
1. Network
    > How does a message get from one node to another in the network? How are the nodes organized? Messages need to be forwarded between nodes to reach it's destination, what path will it go? Each node will have an IP
1. Transport
    > Transport layer only thinks about end-points, it does not think about the nodes in between. It makes use of all the layers below.
1. Session
    > Multiple message that belong together are considered a session
1. Presentation
    > Handles how the message looks like to the application
1. Application
    > The interfaces that want to communicate, such as a email, file transfer, stream... and different protocols are used for different purposes. A streaming service won't care if some data was lost, but a file transfer needs to know if a package got lost and needs to be sent again.

Each layer depends on the previous layer.

The applicationk, presentation and session layers tend to be grouped together as one layer, because no one has managed to them well individually.

Data layer and physical layer also tend to be grouped together as one layer, since it's hard to seperate them.

So in practice, we essentially only have 3.5 layers that are typically used, which gave rise to the simplified __TCP/IP reference model__

### TCP/IP reference model

Here is a table that shows the difference in layers between the OSI and TCP/IP models
| OSI | TCP/IP |
| -- | -- |
| Application | Application
| Precentation |-
| Session | -
| Transport | Transport
| Network   | Internet
| Data link | Link
| Physical | -

> TCP uses the IP protocol under the hood, a separate header with information about the source and destination IP address. After that comes the TCP header which contains details such as which port number it should arrive at, and finally the HTTP header of the request. 
>
#### Ethernet packets
>The ethernet packet initally contains details of the MAC address of the source and destination, and it's payload would be the nested IP/TCP/HTTP.
>
> Could consider it as a way to not have to look too deep into the package that is being delivered in order get it to it's destination. It needs to find it's way from the local network, through the internet, to the correct network router at the destination, and the delivery people see what they need at each level.
>
> At the end of the network packet is information that lets us check if the content got messed up along the way, and _time gap_ so that packets don't overlap (because larger deliveries tend to need multiple packets)

## Example of protocols

| # | Layer name | Protocols |
| -- | -- | -- | 
| 7 | Application | HTTP, DNS, FTP, SMTP, IMAP, SSH, SIP
| 4 | Transport | TCP, UDP
| 3 | Network | IP, ICMP
| 2 | Link  | Ethernet (IEEE 802.3), Token Ring (IEEE 802.5), WIFI (IEEE 802.11), Bluetooth, USB, ARP

* __SMTP__ and __IMAP__ handle emails
* __SIP__ sends voice over IP, like a telephone service
* __TCP__ is connection oriented, handles that packages are sent again if they are lost
* __UDP__ just sends packages, and doesn't care if they arrive or not. Perfect for streaming services.
* __Token Ring__ was popular when hardware was expensive.
* __ARP__  is essentially on both layers 2 and 3.

## Terminology
* Sender has to know who the reciever is, known by their __IP__ and __Port number__
* Reciever has to be able to accept incoming connections.
* The IP and Port numbers are wrapped up into a __socket__

### Client-Server Architecture
* Clients initiate connections to servers
    * Client and Server are in a endless loop of waiting for messages, sending and recieving messages/responses
    * once the connection has been made, it doesn't really matter which is the client and which is the server, because they will both be doing the same thing, sending and recieving messages
* __Peer-to-peer__ is where the client and server are bundled as one.

## Socket Communication
A socket can be considered a pipe, an abstraction of everything that happens in between two computers communicating. A socket is identified by the pair of IP and port on each end.

> Berkeley Sockets API is what we use these days
> 1. Socket()
> 1. Bind()
> 1. Listen()
> 1. Accept()
> 1. Connect()
> 1. Send()
> 1. Recieve()
> 1. Close()
>
> Connection from client needs to be accepted by the server before client-server enter a loop of send and recieving until the connection is closed.

### Client-Server Communication (TCP)
The image shows a timeline, starting at the top going down.
![TCP communication](images/tcp_communication.png)
For the communication to work, the server needs to be ready to accept connections, basically it needs to be running first.
1. Creating a process
1. Binding to a port
1. Listen for anything arriving at the port
1. Accept incoming connections
1. Now the client and server will be in a loop of sending and recieving requests/responces.
1. When done, we close the connection (either client manually closes it, or the server detects if the client hasn't sent anything in a while, depends on the programming of course)