## IP to binary
IP addresses are simply 4 pairs of numbers, and if we get 16bits (4bytes), so each number gets a byte.

1. Convert each of the 4 numbers into binary
1. Arrange them in their slots.

### Simple way to convert number to binary:
1. Create a table of the decimal values for each binary digit:
    ```
    128 | 64 | 32 | 16 | 8 | 4 | 2 | 1
    ```
1. Subtract the value of that bit from the number
1. If it subtracts evenly, then set the bit to `1`, else it is `0` 
    * If you end up with zero after subtracting, then the remaining bits will automatically be set to zero as well.
1. Then repeat the process with each of the bits.

    ```
    changing 192 to binary:
        198 - 128 = 64 (set bit to 1)
        64 - 64 = 0 (set bit to 1)
    remaining bits are zero since nothing to subtract anymore

    128 | 64 | 32 | 16 | 8 | 4 | 2 | 1
     1  |  1 |  0 | 0  | 0 | 0 | 0 | 0
    ```