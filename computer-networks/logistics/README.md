## OSI model (Open Systems Interconnection)
The OSI model, developed in the late 1970s, is a layered description for communications and computer network protocol design. The OSI protocols. 

It is a layered architecture, divided into 7 layers. The connection essentially starts from the bottom up.
![](images/osi_layers.png)
* Pysical layer: the wires and hardware
* Datalink layer: 

## TCP/IP (Transmission Control Protocol / Internet Protocol)
The TCP is the simplified version of the OSI that we use today.

![](images/osi_tcp.png)
>This image shows how the original layers of OSI have been simplified in TCP/IP

It is the __Internet protocol suite__, which is a set of communications protocols used in the internet and similar computer networks. 

The internet protocol suite provides end-to-end data communication specifying how data should be packetized, addressed, transmitted, routed, and received.

### Internet Protocol Suite
![](images/layer_protocols.png)
>There are many more protocols, but these are the most common/basic

### Ports
While the _IP_ is a way to connect computers across the network, __ports__ identify access points on the computer that applications are using.
* Ports `0`..`1023` are __"Well Known Ports"__ assigned by the OS.
* Ports `1024`..`65535` are __Registered/Ephemeral ports__ available to applications. 
    * These are the ports programmers can define in the software.
* Each port can only be used by a single application, though each application could be using multiple ports.
