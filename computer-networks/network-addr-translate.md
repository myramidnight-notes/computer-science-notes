>* [NAT explained](https://www.youtube.com/watch?v=qij5qpHcbBk)
# Network Address Translation (NAT)
It is sometimes called __port forwarding__

IPv4 addresses are in limited supply, and in order to allow multiple computers to share a single IP, we need __network address translation__. They translate the __public IP__ into a __private IP__, allowing the local network to exist without same limits of available IP addresses. 

Ever wondered why all computers have the IP address that start the same? `192.168.0.0` is the private IP and their variations simply depend on the router software. And even if your neighbor might be using exactly the same address, they are on different networks. 

Each computer and device that needs an IP will get one from the router.

## NAT Table
This is how the router keeps track of what local devices and applications are trying to access the internet, translates their local/private IP address into a public one (which has it's own port number, does not have to be the same as was used on the application on the private network), rebuilds the data to contain this public address and sends it on its way. 

This translation will be kept in a __NAT forwarding table__ to keep track of things.

When data arrives from the internet to the router, it will translate the public address to the private address using the entry it created in the forwarding table before, rebuilds the data to have the local IP address instead of the public one, so the data can continue on it's way to the destination.

This might seem redundant, to be translating back and forth, but it is how we have managed to preserve the limited public addresses, by letting all private networks to reuse the same addresses, because the private addresses are only accessable within the private network.


## PAT (Port Address Forwarding) / Overload
