
# Network Layers
![OSI model](images/osi-model.jpg)
__OSI__  stands for __Open Systems interconnection__

Data actually moves backwards through these layers from the computer to its destination. From application to physical and back up to application. 

The _intermmediate nodes_ are the routers that manage communication between local and external networks. Things bounce around between networks until the data finds the host network



## The OSI network layers
* ### Layer 1: [Physical Layer](layer_physical/README.md) 
    > How we physically move data across the network? 
    * Copper wire, radiowaves, fiber optics.
    * Information Theory
        * Long distance communication
        * Nyquist Limit
    * Web Caching
* ### Layer 2: [DataLink Layer](layer_datalink/README.md) (_Link layer_)
    > How we get the data across the physical network. Radio waves? lasers? 
    * Interface to Network
    * Error detection and correction
    * Statistical Multiplexing
* ### Layer 3: [Network Layer](layer_network/README.md)
    >How do we get the data across the network, between routers? How does the data know where to go?
* ### Layer 4: [Transport Layer](layer_transport/README.md)
    > Packing the data so it can be sent across the network. Protocols and headers.
* ### Layers 5,6,7
    > These final layers are usually bunched together
    >* Layer 5: Session
    >* Layer 6: Presentation
    >* Layer 7: Interface

## OSI vs TCP/IP models
![TCP vs OSI models](images/osi_tcp_models.png)

These different models simply group the layers differently, layers that are difficult to seperate completely have been combined in the TCP model (there exist 5 layered TCP models, where __datalink layer__ and __physical layer__ are still seperate).

### What does each layer work with?
>* Physical layer deals with __bits__
>* Data layer uses __frames__ (groups of bits)
>* Network uses __packets__ (the groups of bits have a shape)
>* Transport deals with __segments__ (collected packets)
>* The application layers use __data__ (not segmented anymore). 
>
>Essentially different words for the same thing depending on the layer, it's contents being handled. 

## Bouncing across the internet
![](images/tcp_over_network.png)

This image shows neatly how data moves across the network, bouncing on intermediate networks until it reaches the destination network. How it gets _unpacked_ at each stop to read the information needed to forward it the next network on the route to the destination.

The path taken all depends on where the intermediate routers decide to forward the packets, so each packet might take a different route, as long as they all arrive at the same destination.

This is also a big reason why packets have sequence numbers, they rarely arrive in the same order as they were sent in because of routing.