# Data link Layer
Overview:
* Protocols and Layers
* Physical Signalling Sublayer
* Error detection and correction
* Connecting more than one machine

## Protocols and Layers

### Process communication: virtual vs. actual
>![model vs. actual](images/process_model.png)

### Design choices (tradeoffs)
* Fixed packet size vs. variable packet sizes
    > * Fixed packet size makes it easier to predict how long it will take to move all the data. 
    >* Small fixed packet sizes might have alot of wasted space (the payload size vs. the header/info )
* Synchronised vs. unsynchronised
    >* Synchronised: all packages sent at fixed times, makes it easier to decide who gets to send data when.
* Error correction vs.s retransmission of packets
    >* Should the reciever be able to correct a corrupted packet or get it resent? 
* Acknowledge packet or not
* Connectionless - connection oriented
    >* Connectionless: message can be sent without prior arrangement
    >* Connection: need to arrange for the connection to be made before sending message.

### Three main variants
| Type | example 
| -- | --
|Unacknowledged connectionless service| Ethernet
|Acknowledged connectionless service| Wifi
|Acknowledged connection-oriented service| TCP

* Transmission errors on ethernet are fairly rare
    > The Center itself can listen on the wire, while sending and sees whether someone else transmits at the same time, it can then resend the transmission if needed.
* Wifi
    > On WiFI the Center cannot actually listen on the wire, while sending at the same time (or it's very hard to do that), so it's very hard for the sender to figure out if there will be an overlap with someone else's transmission. You will have to assume that something went wrong unless you get confirmation from reciever that it arrived.
* TCP  
    > it is not actually a link layer protocol, it is a higher layer protocol, but it needs a handshake to set up a connection. TCP only handles each end, not the path in between (so packets might travel various paths to the destination), so the TCP just makes sure that all the data arrives and arrives in order.

### Unfortunate thing about so many standards..
>* There are alot of terms for essentially the same thing
>* Protocol data unit (a message from one side to the other) is called:
>    >* @ physical layer == __Bit__
>    >* @ data link layer == __Frame__
>    >* @ network layer == __Packet__
>    >* @ Transport Layer == __Segment__ (TCP) or Datagram (UDP)
>
> Different names for the same thing...

# Physical Signalling Sublayer
>__What does the lowest layer do?__
>
>How does the data link layer deal with the physical layer (the lowest layer so to speak), translate the transmission into a sequence of bits for the computer to understand.

>1. Upper layer (IP in this case) sends a packet to the data link layer
>1. data link layer wraps it with extra information to get it over to the other side intact
>1. Typically done on a separate chip/line driver card

### Data Link Functions
1. Provide a well-defined interface to the network layer
1. Deal with transmission errors
1. Regulate the flow of data so that slow recivers aren't swamped by fast senders

### Interface to Network
* Perform character encoding, transmission, reception and decoding
    * determing the start and end of packets (framing)
* Simple transmission error handling
    * error detection and correction
* Error recovery
    * This may be delegated to (or replaced by) a higher level
    * The datalink layer is not required to guarantee data integrity
    * "The upper layers will recover" (or die in the attempt)
        > The philosophy is basically the upper layers will recover if there is some problem on the lower layer. 

### Synchronised vs. Unsynhronised Communication
* Recurring problem/solution approach in datacomms
* Synchronous 
    * Synchronized by some kind of external clock
    * Reciever expects frames to arrive at fixed frequency
    * Uses clock to read frames from the wire
    * Problems include drift (clocks might be incorrect, slow/fast), and in the limit Einstein and Fisher
* Asynchronous Communication (not in sync)
    * Frames have to be start/end frame markers, or a known frame length
        > example: 1 start bit, 8 data bits, and 1 stop bit
    * Incurs a high overhead (extra information being sent)

> Synchronous communication at some level is much easier for everybody, if it works. The problem is that synchronizing becomes progressively harder the more nodes are involved, and the longer the distances. That said, a lot of low level synchronous protocols exist and are used very sucessfully for local communication

> Asynchronous: When the reciever is decoding the data, it looks for the pattern of a start 8 bit and a stop bit, and it finds that that it will "lock on" to the data and be able to decode the stream.

> e.g. is Serial Communication so UART, modem etc.


## [Framing: Data is sent in separate frames](framing.md)
Various ways to indicate the frames
* Transmit the lenght of the frame
* Fixed length Frames
* Character based frame signalling (Flag Byte)
    * Byte Stuffing and Bit Stuffing

## [Error detection and Correction](error-detect-correct.md)
* Single Bit errors
* Burst errors
* Parity Checks and Hamming Codes
* Error correction
* Cyclic Redundancy Checks (CRC)
* Error Control
* Flow Control
* Addressing
* Protocol Sub-Layers
* PPP (Point-To-Point Protocol)

## Connecting more than one Machine
### Statistical Multiplexing
* Fundamental feature in Communication Networks
* Assumption: that multiple hosts can share a connection
    > Provided they don't try to all use it simultaneously
* Also referred to as "__Multiple Access Protocol__"

>Widely used trick libraries with lending media, roads and cars, banks: relationship between cash or gold and bank deposits

#### Example: Time division multiplexer
>![](images/statistical-multiplexer.png)
>
> Time division multiplexing is the same idea, but defined slots are allocated to each transmitter/receiver

#### Example: Packet transmission
>Packet transmission on a link is referred to as __statistical multiplexing__.
>* There is no fixed allocation of packet transmissions
>* Packets are multiplexed as they arrive
>
>![](images/packet-transmission.png)

### Entineering Statistical Multiplexing
