
## Error detection and Correction
Errors in this case would come from the physical layer (noise, corruption in message..), things that happen to the signal along the way.

### Single Bit Errors
>Where only a single bit got switched
>![](images/single-bit-error.png)

### Burst Errors
>Type of error also related to medium, wireless is more prone to burst errors, and this in turn impacts protocol design
>
>![](images/burst-error.png)

### Error Detection: Parity Check

>1. ASCII: 12% overhead per character which tells us just how bad early data communication links were
>1. Later on the parity bit was dropped from ASCII and it supported an extended character set with accents etc.
>
> You can read more about parity bits here: 
> * [__Error Handling (under OS-styrikerfi): Parity Bit__](../../os-styrikerfi/input-output/error-handling.md)
>
>Parity bits lets you detect and locate a single bit error

#### Hamming Codes (correcting single bit errors)
>Richard Hamming 1947 forumulated Hamming codes.
>* Principle is that we add extra parity bits at powers of 2 at the data bits
>* Each parity bit calculates parity for a subset of bits in the message
>*  Parity bits themselves not included in checks
>
>This lets us use the parity bits to correct a single bit error

### Error Correction:
* Add redundant information to the frame
* Allows errors to be detected, and corrected
* Tradeoffs:
    * More information added, creating longer frames
        * Increases probability of errors, transmission cost/processing and cost of processing at receiver
* avoids cost of having to retransmit the frame

> Efficiency issues with networking transmissions are quite critical, especially at high speeds. Computers have to send a lot of packets these days.

* Increases size of message in proportion to redundancy
* Detects `n` bits of error, corrects `n-1` bits
* Important result:
    * Can never guarantee all errors will be detected
    * Can guarantee a lower bound
    * Cost of error correction/detection is a sagnificant overhead
* Adding bits to perform error detection takes up more of the channel, hence the limit

## Cyclic Redundancy Checks (CRC)
* Slightly more complicated form of error detection
* Based on theory of cyclic error-correction codes
* Takes advantage of _known_ additional information at each end
* Principle: divide string(frame) by a generator polynominal `G(x)`
* Add the remainder `R(x)` onto the end of the frame
* @Reciever divide frame by `G(x)`
    * non-zero remainder indicates error

> [CRC calculator](https://www.lammertbies.nl/comm/info/crc-calculation)

### How does CRC work?
* `D(x)`: Data
* `D'(x)`: data with appended `0`s (padded)
    * Let `D'(x) = P(x)G(x) + R(x)`

>![](images/crc-calculation.png)
>
>The reason this is so powerful, is that it takes advantage of pre-transmitted information (`G(x)`) and so reduces the extra information that needs to be transmitted

## Error Control
* How does the sender know it's being received?
    * Receiver sends back acknowledgement frames
        * `ACK` - Acknowledgement
        * `NACK` - Negative Acknowledgement: error in frame
    * Suppose the receiver doesn't get the frame at all?
        * Timer on acknowledgement is kept
        ' Must be recieved within specified interval
    * Suppose receiver gets multiple copies of a frame?
        * Frame header includes a sequence number

>1. Noise on links is usually not evenly distributed over time, but tends to come in bursts
>1. Timers and packet acknowledgements in particular are generally fairly applicable techniques used on higher layers at well

For the sender, it doesn't matter why they didn't get any acknowledgement, it would all look the same to them.
* If transmission got lost on way to receiver
* If Acknowledgement from receiver got lost on way to original sender
* If Acknowledgement arrives after timeout (assumed lost transmission)

All these cases would cause the sender to resend the transmission. And that is why the _sequence number_ is important in the header, it lets the receiver know if they are getting a copy of something already sent.

## Flow Control
What happens if one end __sends faster than the other can receive?__
1. Negotiate fixed speed between sender/receiver
1. Feedback-based: receiver tells sender to pause
* Typically fixed speed used at lower protocol levels
* Higher levels use feedback, eg. Ctrl-S, Ctrl-Q

## Addressing ("Is this transmission for me?")
How does each end identify the other? This isn't neccessarily an issue, it depends on the medium (a caple doesn't need to know where the ends are, they are plugged in).

This is more of an issue for _wifi_, _token rings_ or _eithernet switch_.

## Protocol Sub Layers

1. ATM is a fixed packet size (cell) core protocol, and approximately maps the network, data and physical layer.
1. This is one (of several, bluetooth is another) example of the protocol layers not behaving exactly the way the models present. The basic principle of layered seperation, and the nature of relationships between them, is correct:
    * But as long as those service relationships are honoured, you can slip whatever you want in between them.


### PPP (Point-to-point protocol) vs Broadcast
* Local wired protocols also exist that use broadcast e.g. Ethernet, token ring
* Typically below the group limits on capacity in local networks