
# Framing: Data is sent in separate frames

Receiver might have a problem finding the frame in a long sequence of bits

Possible solutions:
* Transmit the length of the frame
* Fixed length frame
* Flag bytes with byte stuffing
* Flag bits with bit stuffing

>![Relationship between packets and frames](images/packets_frames.png)

## Transmit the Length of the Frame
* Header field includes length of the frame
* Header itself should be fixed length
    * which restricts the length of the length field
    * which in turn restricts the packet size
* More importantly:
    * Difficult to recover from errors in the length count
    * How do you find the next header?

>1. Used more in upper layers: pioneered by DECNET
>1. While it's also possible to have variable length header fields, it is less desirable due to the additional complexity

## Fixed length frame
* Can make software's life easier, but not necessarily efficient
* Fragmentation occurs if message is longer than the frame
    * There is no optional frame length
    * Can be very inefficient
    * But does make some of the queuing problems a little easier
    * Reduces jitter
* ATM (Asynchronous Transfer Mode) cell size is (was) 53 bytes
    > Hardly used anymore

> __Jitter__: Strictly deviation from periodicity of a presumed periodic signal, such as if packets are synchronised to arrive at definite time intervals, jitter is the drift away from the expected arrival.

## Character based frame signalling
Also known as "__Flag Byte__"
* Main framing method from 1960 - 1975
* ASCII and EBCDIC character encodings include special characters:

|Char | Meaning 
|--|--
|SYN | Synchronous idle (026)
|STX | Start Text (002)
|ETX | End Text (002)
|EOT | End of Transmission (004)
| CRC | Cyclic redundancy check 

![Character based signalling frame](images/char-based-frame.png)
>1. Equivalent of tones used for signalling in early digital phone networks
>1. Needless to say, whilst some of these values are the same for both EBCDIC and ASCII, others weren't, and each had different variants, which is also true within the various versions of EBCDIC

#### ASCII (American Standard Code for Information Interchange)
>It is a 7-bit code. Many 8-bit codes contain ASCII as their lower half. The international counterpart of ASCII is known as ISO 646-IRV
>
>You can get the ASCII manual in terminal: `man ascii`

#### What if the flag byte occurs in the data?
>* Easily (inevitably) happens with binary data
>* Two techniques: Byte stuffing and Bit stuffing


### Byte Stuffing:
* If the sender sees a `FLAG` in the data, inject an `ESC` byte
* If receiver sees only one `FLAG`: genuine end of frame
* Otherwise remove the duplicate 
* Similarly for `ESC`, inject another `ESC`    

#### Byte stuffing example
>![Byte stuffing](images/byte-stuffing.png)
>
>This is generally used as a technique, such as sanitising user input in web forms as a security measure (so their input does not get read as code or commands) which is usually marked as a backslash

#### Flag Errors
* What if the `FLAG` or `ESC` bytes get corrupted
* All framing techniques are sensitive to errors
    * May either detect premature end of frame
    * Or run on too long into next frame
        * Frame `checksum` will fail at receiver
    * Can then find next frame by looking for `FLAG`

> So the bad frame will be dropped. What happens then depends on protocols at the datalink level and above. Some of the datalink protocols attempt to reseond, but more often the upper layers are relied on to recover

### Bit Oriented Framing
* Byte stuffing imposes a particular character format
* Bit stuffing just uses a pattern of bits to signal start/end frame
    * Each frame begins with Bit pattern `01111110` (6 bits in a row)
* Constant flags or `1`'s considered idle
* Bit stuffing inserts a `0` after any 5 bits in data
* Regardless of what the next character would be
* Reciever similarly removes the stuffed bits
* Invented by IBM in 1970 for SDLC (Synchronous Data Link Protocol)

#### Bit stuffing example
>![Bit stuffing](images/bit-stuffing.png)

#### Advantage of Bit stuffing
* Can vary the length of the flag
* Longer flag will reduce need for stuffing (less likely)
* Short packets use a short flag: less overhead and likelyhood

>* Can also combine with other techniques for efficiency/safety
>* example: send frame length, and use start/stop flags