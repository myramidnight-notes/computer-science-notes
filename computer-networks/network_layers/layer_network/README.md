# Networking
Overview:
* Terminology / Converntions
* Implications of real time
* History
* The socket() interface
* Threading
* "The devil is in the details"
## Terminology
### Client - Server
* #### Client
    >* Initiates connection to the server
    >* Interfaces directly to the User
    >* Communicates with the Server
* #### Server
    >* Waits for connections from Client
    >* Provides services to the client
    >* Communicates with the clients
    >* Handles many clients simultaneously
* #### Peer-to-peer
    >* Does both jobs, client and server
    >* Tendency for some peers to take on a server role

### Topologies
![](images/topology.png)

## Real Time
Everything takes time to complete, so we need to consider the __real time constraints__ of network communications. 

Real time programs can work in various ways:
* raise interupts (notify when something happens)
* operate in a loop (constantly monitor)
* poll (check regularily)

## Sequential vs Real Time debugging
* __Sequential__:
    * Run the program until it stops, figure it out
    * Interactive debuggers
* __Real time__:
    * Interactive debuggers can be very difficult to use
    * Typically use some _print_ statements
    * Heizenbugs: Adding any code can change timing
        > Heizenbugs can be wicked in that "what changed"may not be where the actual problem lies. The change just altered timing or something similar to expose existing bug.
    * Least intrusive: dump logs over UDP (_User Datagram Protocol_) to another computer
        > __UDP__ is a protocol to send datagrams to other hosts on the network. It is suitable for purposes where error checking and correction are either not necessary or are performed in the application.

## Implications of Statistical Multiplexing (sharing)
* Comes with nasty programming side-effects
    * state often has to be preserved for multiple entities
    * server handling many clients
        * stateless: state is communicated in message
        * stateful: clients and server maintain local information
    * Finite state machines often used to design

## Sockets
> A socket is __one endpoint of a two-way communication link between two programms on the network__. A socket is bound to a port number so that the TCP layer can identify the application that data is destined to be sent to. An endpoint is a combination of an IP address and a port number.

### Berkley Sockets API
|Primitive | Meaning 
| -- | --
|Socket | Create a new communication endpoint
| Bind | Attach a local address to a socket
| Listen | Announce willingness to accept connections
| Accept | Block caller until a connection request arrives
| Connect | Actively attempt to establish a connection
| Send | Send some data over the connection
| Receive | Recieve some data over the connection
| Close | Release the connection

### Socket Communication
![Socket Communication](images/socket-communication.png)
>1. Computer's have addresses (IP4 or IPV6, MAC etc.), and they have ports attached to those addresses.
>1. The port identifies to the network the application a packet is being sent to
>1. Programs open sockets to claim a port for their program, and link to the OS handling network communication.

## Threading
### Sequential(ish) vs. Threading
* Can simply be a loop sending/handling responses
* Limits capacity/connections the program can handle
* In port scanning slows things down, especially if randomizing 
    * Even fiberoptic networks much slower than processor
    * Program spends a lot of time waiting for response
* Two ways to resolve problem
    * Use threads: 1 thread per connection in progress
    * Use epoll with non-blocking sockets
        * non-blocking sockets return with any data in buffer

### Threading Library: pthread
Use pointers to functions to pass function for callback
```c++
void * task1 ( void *) ;
void * task1 ( void * ptr )
{
...
}
g ++ thread . cpp - lpthread
```

```c++
# include < pthread .H >
pthread_t threads [3]; // Array to store thread refs in
pthread_create (& threads [ noOfThread ] , NULL , task1 , NULL );
pthread_join ( threads [0] , NULL ); // wait for thread to terminate
pthread_self () // return ID of thread in which called
```

### Highlevel threading
1. Define thread reference variables
    * Must be reference value of type `pthread_t` for each thread.
1. Create an entry point for the thread
    * Pointer to a function the thread will start in
1. Create the thread
1. When the thread is finished `pthread_join` will remove it.

## Identifying services on ports
```c++
int servicefind ( int port ) {
switch ( port ){
    case 5:
        service = " Remote Job Entry ";
        break ;
    case 7:
        service = " ECHO ";
        break ;
    case 9:
        service = " MSN ";
        break ;
    case 18:
        service = " Message Sent Protocol ( MSP )";
        break ;
    case 20:
        service = " FTP ";
        break ;
    case 21:
        service = " FTP ";
        break ;
    case 22:
        service = " SSH ";
        break ;
}

```
> List can be found in `etc/services`