# Client-Server code snippets.
Code snippets to send "hello world" over the network, using `C++`


## Allocating a socket
```c++
// API : int socket ( int domain , int type , int protocol );
//
// Returns socket number or -1 for failure
//
// AF_INET :: IPv4 Protocols
// SOCK_STREAM TCP :: TCP
// SOCK_NONBLOCK :: set socket to be nonblocking
sockfd = socket ( AF_INET , SOCK_STREAM | SOCK_NONBLOCK , 0)

```
> We can use the command `man socket` to get the manual for sockets in terminal.

## Socket Options
```c++
// Turn on SO_REUSEADDR to allow socket to be quickly reused after
// program exit .
if( setsockopt ( sock , SOL_SOCKET , SO_REUSEADDR , & set , sizeof ( set ) ) < 0)
{
    perror (" Failed ␣to␣ set␣ SO_REUSEADDR :");
}
```

## Server:: bind()
> We need to bind the socket
```c++
// Initialise memory
memset (& sk_addr , 0 , sizeof ( sk_addr ));

// Set type of connection
sk_addr . sin_family = AF_INET ;
sk_addr . sin_addr . s_addr = INADDR_ANY ;
sk_addr . sin_port = htons ( portno );

// And bind address / port to socket
if( bind ( sock , ( struct sockaddr *) & sk_addr , sizeof ( sk_addr )) < 0)
{
    perror (" Failed ␣to␣ bind ␣to␣ socket :");
    return ( -1) ;
}

listen ( sock , 5) ;

```

## Socket sets
```c++
int listenSock ; // Socket for connections to server
int clientSock ; // Socket of connecting client
fd_set openSockets ; // Current open sockets
fd_set readSockets ; // Socket list for select ()
fd_set exceptSockets ; // Exception socket list
int maxfds ; // Passed to select () as max fd in set

// Add the listen socket to socket set
{
    FD_SET ( listenSock , & openSockets );
    maxfds = listenSock ; // There is only one socket so far
}

// Get modifiable copies of openSockets
readSockets = exceptSockets = openSockets ;

// Get a list of sockets waiting to be serviced
int n = select ( maxfds + 1, & readSockets , NULL , & exceptSockets , NULL ) ;

// Handle new connections to the server ?
```

## Handle new connections to the server
```c++
if( FD_ISSET ( listenSock , & readSockets ) )
{
    clientSock = accept ( listenSock , ( struct sockaddr *) & client ,
    & clientLen ) ;

    // Add new client socket to the list of sockets being monitored
    FD_SET ( clientSock , & openSockets );
    
    // update the max fd in our socket set
    maxfds = std :: max ( maxfds , clientSock ) ;
}

```


## Client-Server Communication (TCP)
1. Server first needs to be running, set up a socket.
1. Then the server listens for connections from client
1. Client asks for a connection
1. Server accepts connection
1. Server and Client start a loop of communication
1. Client closes the connection
![Client-Server Com](images/server_client_tcp.png)

## Client: Socket()

```c++
struct sockaddr_in server_addr ;
struct hostent * server ;
// socket () is the same on both sides
// although the presumption is the client will
// wait on the server
if (( sock = socket ( AF_INET , SOCK_STREAM , 0) ) < 0)
{
    perror (" Failed ␣to␣ open ␣ socket ");
    return ( -1) ;
}

```

## Client: connect()
```c++
// Setup socket address structure for connection
struct sockaddr_in serv_addr ;
serv_addr . sin_family = AF_INET ;
serv_addr . sin_port = htons ( atoi ( argv [2]) );

// Need to know the IP address of the server
if( inet_pton ( AF_INET , " 192.168.1.12 ", & serv_addr . sin_addr ) <= 0)
{
    perror (" failed ␣to␣ set␣ socket ␣ address ") ;
    exit (0) ;
}

// Connect to remote address
if( connect ( sock_fd , ( struct sockaddr *) & server_addr , sizeof ( server_addr
)) < 0)
{
    perror (" Could ␣ not ␣ connect ");
}
```
>`gethostbyname`, htons is part of a family of helper functions that make it easier to setup the socket() methods.


## Client: Sending Hellow World!
```c++
// Don ’t send the NULL character at end of string

send ( sock , " Hello ␣ World ", sizeof (" Hello ␣ World ") - 1, 0) ;

memset ( buffer , 0, sizeof ( buffer );
nread = read ( sock , buffer , sizeof ( buffer );

```