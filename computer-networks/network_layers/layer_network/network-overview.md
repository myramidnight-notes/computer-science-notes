## Sending IP packet from somewhere
### Steps we need to take:
We want to send IP packet to another host, not delaing with the application layer yet or transport layer or anything.

How do we construct the IP packet?
* IP knows nothing about ports
>1. Source IP
>1. Destination IP (or host name)
>1. Time to Live
>1. Data
>1. Length of whole packet (there can also be 'length of header' field)

How does the packet reach the destination?
How does it get to the first router
>1. We need to give the packet to the link layer that connects our machine to the router. It could be ethernet, so we pack all of that into ethernet frames
>       *  The IP packet becomes the payload of the ethernet frame. Eithernet frame has a header.
>       * It's purpose is to bring the packet to the next hop on the path.
>       * The frame does not need to know anything about final destination
>       * It needs to know the MAC address

* Ethernet Header: 
    * Source Mac, 
    * Destination Mac, 
    * MAC of Router
* ARP protocol is ontop of the ethernet frame, not on the IP
    > We are sending a ethernet frame where the payload is an ARP request

> We need to know the router IP address, otherwise we can't send the request because requests asks what is the MAC address for the IP is.
>
>We need to know the IP address to ask for the MAC address.

* [Packet guide to Core networks: Address Resolution Protocol](https://www.oreilly.com/library/view/packet-guide-to/9781449308094/ch04.html)