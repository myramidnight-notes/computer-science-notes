# The physical layer of networking
Connections aren't magic, we need to establish a physical connection before we can transmit and recieve information.

>#### Outline
>* Conceptual Models for Network Software
>* Network Layer Models
>* Information Theory Overview
>* Physical layer
>* Fiber-optic cables
>* wireless

## Standards
> "The nice thing about standards, is that we have so many of them" 

Here is a list of a few of them:

* __ISO__ (International Organisation for Standardization)
* __CCITT__ (Consultative Committee for International Telephony and Telegraphy)
* __IEEE__ (Institute of Electrical and Electronics Engineering)
* __IETF__ (Internet Engineering Task Force)
* __RFC__ (Request For Comments)
    * comments that might be published as formal standards. Anyone can submit an RFC

## Information Theory
> [View notes on Information Theory](information_theory.md)

## Physical Layer
In summary: 
* Copper drops information (noise)
* Glass breaks (fiber-optics)
* Wireless blocks
* satellites have really long delays

<hr>


### Copper wire
>Copper wire transmission dominated early networking. Cables dedicated for data communication (such as LAN caples) or phone lines (modems). 
>
>It has a relatively low capacity and lots of errors. The network was the bottleneck.
>
> Copper is still widely used in many countries less advanced (that don't have infrastructure for optic fibers or wireless) 

#### Issues with Copper
* The information carried by the copper wires can get corrupted by the noise of __cross-talk__ (information passing between neighboring wires) or even radio signals (since these wires can also act as antennas)

![Issues with copper wires](images/copper_wire_issues.png)
* Limited length (of about 100m)
* Requires repeaters to carry information further than limit
* High error rate (noise)

### [Fiber Optic Networking](fiber_optic.md)
> * Fiber-Optic Cabels
>* Electromagnetic Spectrum
>*  Fiber-Optic Latency
>* Speed of Light in Vacuum vs. Cable

### [Wireless](wireless.md)
>* Radio frequencies

### Other media
> "Never underestimate the bandwidth of a station wagon full of tapes hurthling down the highway." - Andrew S. Tannenbaum

There are alot of different ways to transport information around

It all depends on how much information you are trying to send across what type of medium and where it needs to go. 

* How long does it take us to put the data on the medium?
* How much can the medium carry?
* How long takes it to deliver the data?
    > Total time to send everything? multiple trips?

>Example could be sending terrabytes of information across a gigabit connection might take forever, maybe it would be quicker just to deliver it via car to the destination with a drive containing the data.
