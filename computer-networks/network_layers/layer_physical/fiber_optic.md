
# Fiber-Optic networking
Error rate went from 10<sup>-5</sup>/bit to almost 0



>![evolution of capacity](images/capacity_evolution.png)
>
>![10 Gb](images/10_gb.png)

## Fiber-Optic Cables

>![fiber-optic cables](images/fiber_optic_cable.png)
>
>![spectrum](images/optic_fiber_spectrum.png)



#### Advantages of Fiber-Optic Cable (compared to copper)
>* Thinner (important for cabling considerations)
>* Much higher capacity
>* Less signal degration with distance
>   * Requires fewer repeaters
>* Lower power
>* Almost 0 errror rate

#### Disadvantages of Fiber-Optic Cable
>* More expensive per meter than copper
>* Optical fiber cannot be easily joined together
>   * Splicing: training and specialised equipment requried
>* Ends have to be periodically inspected and cleaned
>   * Specialised training
>   * Equipment: Fiberscope
>* Fragile: must not be bent too much
>* Copper is usually already available

### Cable Managment
1. It is ok to bend fiber-optic cables a little, but not too much.
1. Unless otherwise specified, minimum bend radius should not be more than 15 times the cable's diameter
1. It is glass, it will break (and become badly degraded) if it's bent too much.


![Cable managment](images/optic_fiber_cable_managment.png)

### Cleaning
* 1-micrometer dust particle on a single-mode core can block up to 1% of the light passing through the cable.
    * 9-micrometer particle can completely block the core
    * For consideration, typical human hairs are 50-75 micrometers in diameter
* Oils and other residues will also degrade the signal (human fingers), so do not touch the ends.
* The laser can burn residues onto the surface, damaging equipment.
* __NEVER look into the end of a fiber-optic cable__ (it's a laser, even if it might be outside of the visible spectrum, will burn your eyes)
* __NEVER touch the end of a fiber optic cable__ (it will make the end dirty, even if can't see it)

#### Fiberscope
> It is a tool that allows you to inspect the surface of the ends of the fiber-optic cables.

## Fiber-optic latency
> The type of application that does care about exact calculations is typically fairly specialised, high frequency trading is probably the example that has done the most to drive work in this area.
* The speed of light in a vacuum is 299,792,458 m/s (3.34µs/km)
* In a fiber-optic cable the light is moving through a glass core
* Actual speed in fiber-optic cable depends on a few things:
    * Refractive index of glass core
    * Wavelength of light being used
* For most applications, this formula for latency works:
    >![latency formula](images/fiber_optic_latency.png)

## Speed of Light in Vacuum vs Cable

| Route | Distance | time in vacuum | time in fiber | Round-trip time in fiber
|--|--|--|--|--
|New York - San Francisco | 4,148 km | 14ms | 21ms | 42ms
| New York - London | 5,585 km | 19ms | 28ms | 56ms
| New York - Sydney | 15,993 km | 53ms | 80ms | 160ms
| Equatoral Circumference | 40,075 km | 133.7ms | 200ms | 200ms

### Hollow Core Fibers
> "Researchers create fiber network that operates at 99.7% speed of light"
>
>These hollow core fibers appear to be just becoming commercially available, not clear what the applications will be.

