# Wireless network

![wireless standard](images/wireless-standard.png)
1. With wireless technologies, the physical and data link layers use different protocols, and have notably different characteristics than copper and fiber.


## Wireless: Broadcast Medium
* Shannon definition of information was based on "difference"
* Broadcast media send the same information to all nodes (computers) 
    * Inherently lower information capacity than equivalent point-to-point network.
    * Nothing stops a broadcast being performed on a point-to-point network
### Disadvantages:
* High losses due to interference
    >only one can speak at a time, else there will be interference
* Frequency use has to be carefully regulated
* Higher delays and jitter
* Lower security
    >everyone can get the message sent over the wireless, not just the one it was intended for. That is why encryption is important, so it won't matter if someone gets the message, they won't be able to read it.

### Advantages
* Requires little setup
* no need for physical cables to access the network
