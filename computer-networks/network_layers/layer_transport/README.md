
### Reading materials
* [Reliable Data Transfer](http://www2.ic.uff.br/~michael/kr1999/3-transport/3_040-principles_rdt.htm)
* Dorpal chapters:
    * [TCP basics: end-to-end principle ](http://intronetworks.cs.luc.edu/current2/html/tcpA.html#end2end)
    * [Abstract Sliding Windows](http://intronetworks.cs.luc.edu/current2/html/slidingwindows.html)

# Network Transport Protocols
The two _transport protocols_ riding above the _IP layer_ are __TCP__ and __UDP__.

This is how we __transport data__ over the network!

## TCP 
>__TCP__ stands for __Transmission Control Protocol__
>
>It is __stream oriented__, meaning it sends the data in small (or large) packets over a stream, until all has been delivered.
>
>It is __connection oriented__, meaning that the connection must be established _before the beginning of any transfer_.
>
>It automatically uses the __sliding windows__ algorithm to achieve throughput relatively close to the maximum available

* ### [TCP basics](tcp_transport.md)
    >* TCP header and flags
    >* Establishing TCP connection
    >* Sequence numbers
    * [__Abstract Sliding Windows__](sliding-windows.md)
        >* `retransmit-on-timeout`: __ARQ protocols__
        >* `stop-and-wait`
        >* Packet loss
        >* Flow control
## UDP
>__UDP__ stands for __User Datagram Protocol__
>
>It does not require the source and destination to establish a _three-way handshake_ before transmission takes place. Also no need for end-to-end connection.
>
>_UDP_ is used where __reliabilty is not a requirement__, so there are no acknowledgements sent over this protocol. Not bothering with retransmitting lost/dropped packets. 
>
>It might not be _stream oriented_, but it's actually better for streaming services, where we don't really care if we missed something in between.

## Evolution of TCP
![](images/evolution-tcp.png)

## More
* ### [Advanced TCP](advanced-tcp.md)
* ### [Congestion Control](congestion-control.md)