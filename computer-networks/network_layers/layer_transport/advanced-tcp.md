# Advanced TCP
>More things to think about

## Network Overhead
> Also known as __administrative overhead__, which which incidentally gets worse the further up we go in the network stack to the application layer.
>* __Data is extra__, this is just about the overhead being wrapped around the actual data being delivered.

![Nesting of segments](images/nesting-of-segments.png)

* ### Packet overhead
    >* IP header, TCP/UDP Header, these add extra bytes to the whole thing.
    >* It is much more efficient to send large packets, provided they don't get lost too often. Then the __header overhead__ is smaller in relation to data being delivered.
    >* For some kinds of application requirements administrative overhead can become extremely significant very quickly

* ### Goodput (useful data delivered)
    >The number of useful information bits delivered by the network to a certain destination per unit of time. The amount of data considered excludes protocol overhead bits as well as retransmitted data packets.
    >
    >"Goodput is the user supplied traffic transmitted by the network from source to destination"

## Piggybacking ACK (Delayed acknowledgements)
>In two-way communication, whenever a frame is received, the receiver waits and does not send the control frame (ACK) back to the sender immediately. 
>
>The receiver waits until its network layer passes the next data packet. The delayed acknowledgement is then attached to this outgoing data frame.
>* __Advantages__: Improves efficiency and better use of the available bandwith (not filling it with ACK packets).
>* __Disadvantages__: The receiver can jam the service if it has nothing to send.
>   >It can be solved by enabling a counter (_receiver timeout_) when a data frame is received. Sender also adds a counter (_emitter timeout_), if the counter ends without receiving confirmation, the sender assumes packet loss and resends the frame.
>
>This feature potentially saved a lot of bandwith on slow networks by sending fewer ACKs, but this is hardly an issue on the faster networks we have these days.
>
>It can create __Silly Window Syndrome__, and interacts badly with __Nagle's Algorithm__

### Silly Window Syndrome (barely any data)
>It is a problem that can arise in poor implementations of the TCP protocol. 
>
>When the usable window shriniks to a __silly__ size and increasingly smaller segments are sent, to the point where packet headers exeed the amount of data in the packets.
>
>The greater number of packets being sent (to compensite for the small amount of data each carry), each with its own TCP header, dramatically increases the overhead even as the amount of actual data being sent decreases, leading to network congestion and a large loss of efficiency from degratded throughput.

### Nagle's Algorithm
>It attempts to improve the effieciency of TCP/IP networks by reducing the number of packets that needs to be sent over the network. 
>
>If we have 1 byte of data to send, it would have a 40-byte TCP/IP header attached to it, which is a huge overhead for such a small amount of data.
>
>__Nagle's algorithm__ works by combining a number of small outgoing messages by sending them all at once. Specifically, as long as there is a sent packet for which the sender has received no acknowledgment, the sender should keep buffering its output until it has a full packet's worth of output, thus allowing output to be sent all at once.
>
>Nagle's algorithm buffers data to be sent until it has a full segment or has not received a previous ACK

#### Issues with Nagle's Algorithm
>* It should be deisabled if working with real-time traffic, such as _interactive games_. 
>* Can be problematic with timeouts (Connection can be dropped, while Nagle is delaying traffic)
>* Can create a __deadlock with delayed ACKs__ (piggyback ACKs)
>   >Receiver waits for data to pickyback on, while sender waits for an ACK to send data.

### ACK Starvation
>_ACK starvation_ can occur if there is no traffic to piggyback on. Asymmetric traffic where data largly flows in one direction, with little or no traffic coming back.
>* Typicallly caused by some kind of bug in the underlying network software.

## TCP Timers
1. Retransmission (RTO)
    > Waiting for a segment ACK
1. Persistence
    > Used to periodically send window probes
1. Keepalive
    >Length of time without data before server tears down the connection. Default is 2 hours.
1. Time-Waited
    > Connection termination (TIME-WAIT)
