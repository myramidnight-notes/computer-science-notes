>* [Newer TCP Implementations](http://intronetworks.cs.luc.edu/1/html/newtcps.html)
# TCP Congestion Control

## Slow Start (Van Jacobson) Algorithm
* [Wired article about Van Jacobson](https://www.wired.com/2012/05/van-jacobson/)
    > All Van Jacobson wanted to do was upload a few documents to the internet. But back in 1985 it was slow as molasses. Twenty five years later, the digerati credits Jacobson with averting an ARPAnet meltdown that would have stunted the growth of the modern internet

![Van Jacobson algorithm](images/van-jacobson-slow-start.png)
* The _slow start_ refers to how the initial segment size is 1.
    > It still grows exponentially, because the size doubles
* Once it hits the threshold limit (usually set by the operating system) or a packet gets lost, it switches over to __congestion avoidance__
    >_Congestion avoidance_ has linear growth, increasing segment size by one each time until there is a packet loss.
* When a packet gets lost, the threshold gets halved (the Congestion Window), and __congestion avoidance__ starts again from the new threshold.



## TCP Tahoe (fast retransmit and fast recovery)
If TCP detects an out of order segment, then it sends back the ACK for the last segment that was actually in order, even if it has received segments that come after.
* TCP generally sends back ACK for the last segment that actually arrived in order, even if it has gotten later segments
    * The data won't be complete without that missing packet after all
    * Segments can arrive out of order, depending on the routes they traveled.
* We wait for __3 or more consecutive duplicate ACKs__ to arrive, before assuming that the packet was lost. There are a few things that we can do then:
    1. Retransmit immediately (__fast retransmission__)
    1. Reset the threshold to half of what the CWND was last time
    1. Each new duplicate ACK could increase the CWND by `MSS/CWND` (MSS = Maximum Segment Size)
>![](images/slow-start-congestion-avoidance.png)
>This implementation starts again with slow-start when a packet is lost. 
>* It assumes unacknowledged segments are due to network congestion
>   >Wireless and cellular networks are are prone to packet loss regardless of congestion
>* It performes badly with short-lived connections

<<<<<<< HEAD
#### TCP Tahoe Behaviour
>![](images/tcp-tahoe-behaviour.png)
=======
>>>>>>> dd1ea95d6ade2f46f18f3158241595a4e70f0bee

#### Slow Start implementation (state map)
>![State map of slow-start](images/slow-start-implementation.png)

<<<<<<< HEAD
## TCP Reno
![](images/tahoe-reno.png)

=======
>>>>>>> dd1ea95d6ade2f46f18f3158241595a4e70f0bee
## Random Early Detection (RED)
>![RED model](images/Random_Early_Detection.svg)
>* It drops packets that come in and can't be buffered
>   >Also called "__Tail Drop__". And it is biased against bursty traffic.
>
>To make this more fair, it would randomly drop packets, the more packets a host sends, more likely they are to be dropped. Dropping a packet would also trigger the host to lower their CWND size.

## Active Queue Managment (AQM)
>It is a policy of dropping packets inside a buffer associated with a NIC (Network Interface Controller) before that buffer becomes  full, often with the goal of reducing network congestion or improving end-to-end latency. 
>
>It detects early signs of congestion
>* __Benefits:__ Drop-tail queues have a tendency to penallise bursty flows, and to cause global synchronization between flows. And it provides endpoints with congestion indication before the queue is full.
>* __Drawbacks:__ Early AQM disciplines (notably RED and SRED) require careful tuning of their parameters in order to provide good performance. And because network engineers have been trained to avoid packet loss "why should I drop perfectly good packets when I still have free buffer space?"

## Explicit Congestion Notification (ECN)
>ECN allows end-to-end notification of network congestion without dropping packets, by marking them explicitly.  It is an optional feature that may be used between two ECN-enabled endpoints when the underlying network infrastructure also supports it.
>* Works in conjunction with AQM
>* Uses TCP header flags: `ECN-Echo (ECE)`, and `CWR` (Congestion Window Reduced)
>* It is not end-to-end
>* Must be supported by the routers of both sender and receiver
>* Packets from non-ECN flows are dropped (RED)

<<<<<<< HEAD

## TCP Cubic
>It is one of the newer TCP implementations, because fast high bandwidth internet has different needs than the internet in the olden days. 
>
>__CUBIC__ is a network congestion avoidance algorithm for TCP which can achieve high bandwidth connections over networks more quicly and reliably in the face of high latency than earlier algorithms. It helps optimize __long fat networks__.

![](images/tcp-cubic.png)

=======
## TCP Reno
![](images/tahoe-reno.png)

## TCP Cubic
>__CUBIC__ is a network congestion avoidance algorithm for TCP which can achieve high bandwidth connections over networks more quicly and reliably in the face of high latency than earlier algorithms. It helps optimize __long fat networks__.
>>>>>>> dd1ea95d6ade2f46f18f3158241595a4e70f0bee
