# Abstract Sliding Windows
This topic looks at how to build reliable data-transport layers on top of unreliable lower layers. 

The __end-to-end__ principle suggests that trying to achieve a reliable transport layer by building reliable lower layer is a misguided approach, and that implementing reliable endpoints of a connection (as described in this chapter) is the proper way to go.

## Retransmit-on-timeout (policy)
>It is a policy where if a packet is transmitted and no acknowledgement received during the timeout interval.
>
>Either side can implement this policy, to retransmit either data or acknowledgement.
* ### __ARQ__ protocols (Automatic Repeat reQuest)
    >Protocols where one side implements a _retransmit-on-timeout_ are called __ARQ protocols__ 

## Building Reliable Transport: Stop-and-wait 
Where the next packet isn't sent until an acknowledgement for previous packet is received, so there is only one packet in transit at any time. 

If there is no responce it will simply retransmit the same packet on timeout.

![](images/stopandwait.svg)

* ### Packet loss
    >A packet is considered if one of two things happen: 
    >![](images/data_ack_loss.svg)
    >1. when it fails to arrive at destination (__lost data__) 
    >1. or when the sender fails to receive acknowledgement that the packet was received (__lost ACK__), because if sender doesn't know that the packet was received, might as well be lost

* ### Retransmit-on-duplicate (strategy)
    >In the case of lost acknowledgement where data is retransmitted, the receiver will get __duplicate data__ delivered. If we assume that the receiver has implemented a __retransmit-on-duplicate__, then it will respond to duplicate data by retransmitting the ACK
    >
    >![](images/late_ACK.svg)

* ### Sorcerer's Apprentice bug
    >The __retransmit-on-duplicate__ can trigger a case of __the sorcerer's apprentice__ bug, where duplicate ACK will be sent for every received packet after the duplicate data was received, the sender sends the next packet upon receiving the ACK, but a duplicate of previous data has already been sent. 
    >
    >![](images/sorcerer.svg)
    >
    >Essentially a spiral that takes up __double the bandwidth__, even if the transfer completes normally.

## Flow Control
__Stop-and-wait__ is a simple form of __flow control__ that prevents data from arriving at the receiver faster than it can be handled.  

The receiver might wait before sending the ACK until the data has both arrived and been processed in cases where the processing time happens to be larger than the RTT (round-trip-time), because it needs to be ready to receive the next packet.

* ### ACK<sub>WAIT</sub>[n]
    >Waiting with sending ACK can cause new problems though, because the sender might time-out, and the receiver cannot send the ACK until it's done processing (to confirm that the packet wasn't corrupted on the way)
    >
    >The ACK<sub>WAIT</sub>[n] solves that, by letting the sender know that package `n` was received (to prevent retransmission) but that the sender should wait with sending the next packet.
    >
    >The sender then waits for a ACK<sub>GO</sub>[n] that tells it to proceed with sending the next packet. But that's a packet like any other and could get lost. This could be solved by switching to a timeout model to send the next packet.

## Sliding Windows
>__Sliding Windows__ is a strategy to keep as many packets in transit as the network can support, and it is also key to _managing congestion_.

While the _stop-and-wait_ is reliable, it is not very efficient, promoting idling and bottle-necking (the sender having to wait for the receiver to process each package before proceeding). 

* ### Sending without waiting
    >A way to imrpove overall throughput is to allow the sender to continue to transmit, __sending data without having to wait__ for the ACK to arrive. But we can't be sending packets too fast, or they'll end up in queues or getting dropped.
    
* ### Cummulative ACK
    >In this implementation, the ACKs would be cummulative, only needing to send the ACK with the highest `n` segment, telling the sender that all packets of lower `n` have been received, without needing to send individual ACK for each of them.


* ### The Window
    >A __window__ in this concept is simply a range of packets. When the sender has sent all the packets available in that window, it will wait for ACK that confirms that all the packets of current window have been received. 
    >
    >They keep track of `last_ACKed`, the highest packet `n` which an ACK arrived for, so the _window size_ counts from that last confirmation.
    >
    >It might receive ACK before it finishes sending all the packets (it all depends on implementation or dropped packets), this will still update the `last_ACKed`, which is what the window looks at when it slides forward to allow the sender to continue sending packets.

* #### Window Size (`winsize`)
    >The sender picks a __window size__ that will indicate how many packets it is allowed to send before they wait for ACK to arrive.
    >
    >So this size decides how many packets are in transit at once.
    >
    >![Sliding Windows Size](images/sliding_windows.svg)
    >This image shows how the window size effects the bandwidth, how much of it is utilized. 

## RTT (Round-trip-time) Calculations
1. `queue_time` = RTT<sub>actual</sub> - RTT<sub>noLoad</sub>
1. `throughput` = `winsize` / RTT<sub>actual</sub>
1. `queue_usage` = `throughput` * (RTT<sub>actual</sub> - RTT<sub>noLoad</sub>) <br>= `winsize` * (1 - RTT<sub>noLoad</sub>/RTT<sub>actual</sub>) 
1. RTT<sub>actual</sub> = `winsize` / `bottleneck_bandwidth` <br> `queue_usage` = `winsize` - `bandwidth` * RTT<sub>noLoad</sub>
1. RTT<sub>actual</sub> / RTT<sub>noLoad</sub> = `winsize`/`transit_capacity` <br>= (`transit_capacity` + `queue_usage`)/`transit_capacity`

## Congestion

There are a few things that factor in congestion:
1. throughput 
1. delay 
1. queue utilization 

![](images/winsize_knee.svg)

* ### Critical winsize value (Congestion Knee)
    >The critical `winsize` value is equal to `bandwidth`* RTT<sub>noLoad</sub>. This is also known as the __congestion knee__

    #### Winsize below the _knee_
    >* `throughput` is proportional to `winsize`
    >* `delay` is constant
    >* `queue utilization` in the steady state is zero
    
    #### Winsize larger than the _knee_
    >* `throughput` is constant (equal to the bottleneck bandwidth)
    >* `delay` increases linearly with `winsize`
    >* `queue utilization` increases linearly with `winsize`

* ### Power of connection
    >It is defined to be `throughput / RTT`. 
    >* For sliding windows below knee, the power is proprtional to window size.
    >* For sliding windows above the knee, since delay becomes proportional to `winsize`, the power is proportional to 1/`winsize`
    >
    >![](images/winsize_power.svg)