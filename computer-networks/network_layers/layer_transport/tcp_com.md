## TCP socket
* The _client_ doesn't need to call `bind()` because when it uses `connect()`, then it automatically binds the connection, sending the SYN information.
![TCP socket](images/TCPsockets.jpg)

## UDP socket
* Does not need `listen()` or `accept()` on the server
* The client does not need to `connect()`
    * Client uses `send` to the socket, here it automatically `binds`.
* Using `connect()` on a UDP socket (`SOCK_DGRAM`) will make the 

![TCP socket](images/UDPsockets.jpg)

