> [TCP basics: end-to-end principle ](http://intronetworks.cs.luc.edu/current2/html/tcpA.html#end2end)
# TCP Transport Basics

__TCP__ stands for __Transmission Control Protocol__
* It is __stream oriented__
    >meaning it sends the data in small (or large) packets over a stream, until all has been delivered.
* It is __connection oriented__
    >meaning that the connection must be established _before the beginning of any transfer_.
* _TCP_ is __reliable__ 
    >sequence numbers are used to ensure that all packets arrive at destination, to make sure no data is lost
* It automatically uses the __sliding windows__ algorithm to achieve throughput relatively close to the maximum available

### End-to-end principle
>The principle states that transport issues are the responsibility of the endpoints in question and thus should not be delegated to the core network.
>
>This idea has been very influential in TCP design, and the two issues in question are __data corruption__ and __congestion__
>
>* ### Checksum
>    >It is a way to protect against data corruption, or at the very least confirm if packet was corrupted or not. 
>* ### Congestion
>    >TCP is essentially the _only_ layer that addresses congestion managment.

## TCP Header
The image below shows the structure of the _TCP header_.
* _source_ and _destination_ ports are 16 bits each.

![TCP header](images/tcp_header.svg)

* ### Flags
    * __SYN__: stands for _synchronize_ 
        >It marks packets that are part of the new-connection handshake.
    * __ACK__
        >indicates that the __acknowledgement header__ is valid
    * __FIN__: is for _finish_
        >it marks the packet involved with closing the connection.>
    * __PSH__: is for _push_
        >it marks "_non-full_" packets, that should be delivered promptly at the far end.
    * __RST__: is for _reset_
        >it indcates various error conditions.
    * __URG__: is for _urgent_
        >part of a now-seldom-used mechanism for high-priority data.
    * __CWR__ and __ECE__ 
        >it is part of the _Explicit Congestion Notification_ mechanism.

## Establishing TCP connection
TCP connections are established via __three-way-handshake__, as it's known as. Where both ends have acknowledged each other and syncronized.

>![3way handshake](images/tcp_3wh.svg)
>1. `A` sends `B` a packet with the `SYN` bit set
>1. `B` responds with a `SYN` packet of its own, the `ACK` bit is now also set
>1. `A` responds to `B` with a `ACK`

After the handshake has been concluded and connection established, then data can be sent.

### Closing the connection
> To close the connection, similar exchange as the handshake happens, with the `FIN` bit set.
>
>![closing tcp connection](images/tcp_close1.svg)
>1. `A` sends a packet to `B` with the `FIN` bit set (or simply called _FIN packet_), announcing that it has finished sending the data.
>1. `B` sends `A` a packet with `ACK` to the `FIN`
>1. `B` may continue sending additional data to `A`
>1. When `B` is also ready to cease sending, it sends it's own `FIN` packet to `A`
>1. `A` sends ACK of the end of connection, which is the final packet of the exchange.

## Sequence Numbers
_Sequence numbers_ identify the order of packets, which gets incremented with each data packet.

Some programs like _wireshark_ might display the sequence numbers as if they start from zero, but it's just calculated based on the ISN, not the actual sequence number that the packet arrived with.


>||A sends|	B sends
>|--|--|--
>|1|	SYN, seq=0	 
>|2|	|	SYN+ACK, seq=0, ack=1 (expecting)
>|3|	ACK, seq=1, ack=1 (ACK of SYN)	 
>|4|	“abc”, seq=1, ack=1	 
>|5|	|	ACK, seq=1, ack=4
>|6|	“defg”, seq=4, ack=1	 
>|7|	|	seq=1, ack=8
>|8|	“foobar”, seq=8, ack=1	 
>|9|	|	seq=1, ack=14, “hello”
>|10|	seq=14, ack=6, “goodbye”	 
>|11,12|	seq=21, ack=6, FIN	|seq=6, ack=21 ;; ACK of “goodbye”, crossing packets
>|13|	|	seq=6, ack=22 ;; ACK of FIN
>|14|	|	seq=6, ack=22, FIN
>|15|	seq=22, ack=7 ;; ACK of FIN
>
>Here is the visual representation of that exchange:
>
>![](images/tcp_ladder_plain.svg)

### ISN (Initial Sequence Number)
>Each side chooses its own __initial sequence number__, and sends it with the initial `SYN`. This number does in fact not start with 0. But it is incremented by 1 each time.
>
>| | A, ISN=1000 | B, ISN=7000 
>|--|--|--
>|1	|SYN, seq=1000	 
>|2	||	SYN+ACK, seq=7000, ack=1001
>|3	|ACK, seq=1001, ack=7001	 
>|4	|“abc”, seq=1001, ack=7001	 
>|5	||	ACK, seq=7001, ack=1004
>|6	|“defg”, seq=1004, ack=7001	 
>|7	||	seq=7001, ack=1008
>|8	|“foobar”, seq=1008, ack=7001	 
>|9	| |	seq=7001, ack=1014, “hello”
>|10	|seq=1014, ack=7006, “goodbye”
>
> If `B` had not been _listening_ at the port to which `A` sent its `SYN`, its responce would have been `RST` (reset), meaning in this context "_communication refused_". 