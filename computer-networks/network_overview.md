# An overview of networks
> * [Intro to Networks](http://intronetworks.cs.luc.edu/current2/html/intro.html) (mainly read sections 1.14 and 1.15)
> * [Advanced Socket Communication Tutorial](http://users.pja.edu.pl/~jms/qnx/help/tcpip_4.25_en/prog_guide/sock_advanced_tut.html)

LANs (Local Area Network) are the _physical_ networks of computers, such as within a home, school or corporation. 

Each computer is identified by an __IP__ (internet protocol), a layer that provides abstraction for connecting multiple LANs into the internet.

__TCP__ deals with transport and connections and actually sending user data.

## Layers
The LAN, IP and TCP are often called layers

## Data Rate, Throughput and Bandwith

## Packets