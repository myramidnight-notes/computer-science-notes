

# Routing
* ### Routing Overview
* ### Routing 
    >* Routing is the process of selecting a path for traffic in a network or between multiple networks
    * #### Routing Table
        > Table stored in the router that lists known network destinations
* ### Forwarding
    >Optimized 
* ### Bridging
    >* Creates a single aggregate network from multiple network segments.
    >* Connecting two LAN segments 
    >* Relies on MAC address
    >* A tunnel to make it seem like machines are part of the same network, when they aren't.

### Autonomus System (AD)
* "The definition of AS has been unclear and ambiguous for some time... An AS has a connected group of one or more IP prefixes run by one or more network operators which has a SINGLE and CLEARLY DEFINED routing policy"
* So AS is basically a part of the internet or a network within the internet that has a clearly defined routing policy.
* There needs to be a cleary defined protocol within each AS that talk to each other and figure out the routing between these Autonomous systems.
* Usually each AS is fairly small,  so we can use something like dykstra within that network. But on a global scale, we cannot know distance to all nodes.  

### Facebook didn't exist for 5 hours.
> [Gone in minutes, Out for hours (article)](https://www.nytimes.com/2021/10/04/technology/facebook-down.html)
* Their border gateway protocol failed (configuration accident most likely)
* So facebook somehow managed to tell their top level router to stop advertising it's existence to other routers, so to the internet it didn't exist anymore.
    * So no traffic was being routed through facebook network
* Took them 5 hours to fix this error, because they couldn't access router anymore over internet, had to physically access the router.
    * The building to the router room used smart locks, that wanted to talk to facebook, which didn't exist on the network at that moment. How do you access a locked room that has no key currently? So that explained the time delay of them having to force their way into the router room.
    * Communication to whoever might help them get into the building probably went through facebook, so that added to the delay


## Internet Exchange Point (IXP)
* It is a physical site where equipment is located to different AS's