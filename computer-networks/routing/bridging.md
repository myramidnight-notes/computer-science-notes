# Bridging
A __network bridge__ is a computer networking device that creates a single, aggregate network from multiple communication networks or network segments. 

Bridging is distinct from routing. Routing allows multiple networks to communicate indipendently and yet remain seperate, whereas bridging connects two separate networks as if they were a single network.