# Routing Protocols

### Intermediate System to Intermediate System protocol (IS-IS)
>* It uses Link-State 
>* Uses Dijkstra algorithm to compute best path
>* Operates directly above level 2 (uses own protocol, not IP)
>    >Protocol neutral, can support IPv6 without modification

### [Routing Information Protocol](https://en.wikipedia.org/wiki/Routing_Information_Protocol) (RIP)
>* One of the oldest Long distance vector protocols
>* RIP prevents routing loops by implementing a limit on the number of hops allowed in a path from source to destination
>* It employs hop count as a routing metric, max 15 hops



### Better Approach to Mobile Ad-hoc Networking (B.A.T.M.A.N)
>It is a routing protocol for multi-hop mobile ad hoc networks and intended to replace the Optimized Link State Routing protocol (OLSR)

## General Routing Protocols
### Babel 
>* Distance Vector protocol
>* It is designed to be robust on both wireless mesh networks and wired networks.
## Exterior Gateway Protocols

### Border Gateway Protocol (BGP)
>* It uses Path Vector  routing protocol
>* All BGP messages are sent as unicast (1:1)
>* BGP routers may only belong to a __single__ AS

## Interior Gateway Protols
### Open Shortest Path First (OSPF)
>* Link-state routing protocol
>* falls under interiour protocols