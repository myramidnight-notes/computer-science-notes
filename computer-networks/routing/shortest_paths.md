# Calculating the Shortest paths

## Dijkstra's Algorithm
* [Networks Shortest Path](https://www.youtube.com/watch?v=5lgLJwQXKm4)
>It is actually very simple, with the use of a tree diagram.
>1. We find the paths we can reah from the starting location
>1. Write down the node name and the distance to it (_B3_ would mean it was 3 units to reach node B)
>1. Then we move onto the next node, and continue calculating the distances, we add the distance to our current location to the distance of the next node. So going from _B3_ to _D_ would have the distance of 2, then we would add _D5_ to the tree, it was 5 units to reach D from A along this route.
>1. Then we just continue until we've compared all paths, and it is very clear what path is shortest since we can see the accumilated distance to reach the points.