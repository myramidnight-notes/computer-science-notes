# Subnets

## Subnet masks
The __host portions__ of the address can be seperated, by lining up the IP address and subnet mask together:
```
11000000.10101000.01111011.10000100 - IP address (192.168.123.132)
11111111.11111111.11111111.00000000 - Subnet mask (255.255.255.0)
```
In this example:
* The first __24 bits__ (the number of set bits in the subnet mask) are identified as the __network address__
* The last __8 bits__ (number of remaining zeros in subnet mask) are identified as __host address__.
```
11000000.10101000.01111011.00000000 - Network address (192.168.123.0)
00000000.00000000.00000000.10000100 - Host address (000.000.000.132)
```

### Total Host addresses
With the subnet mask we can see how many bits we have available to create host addresses, so it tells us the maximum number of hosts we can create.

The formula for calculating number of hosts per subnet is: 
* __2<sup>h</sup> - 2__ (`h` being number of host bits)

> Reason for subtracting 2 addresses is because the _network address_ (0) and _broadcast address_ (255) for that particular subnet/host. 

## CIDR (Classless Inter-Domain Routing)
This is a simplified notation of subnet masks. 
> Instead of `255.255.255.0` we can write `24`, which is the total number of set bits in the subnet mask. Both translating to:
>    ```
>    11111111.11111111.11111111.00000000
>    ```

So the previous example of IP address and subnet mask in the _CIDR_ notation would be: 
> `192.168.123.132/24`
* The `/` separtes the _IP address_ from the _CIDR_


## Subnet Classes (IPv4)
The first number in an IP address lets you identify the class of network.

Each class of networks relates to a specific range of _IP addresses_ (A,B and C being used most often), and the default subnet mask is the first number in their range. 

* `127` is not included in these ranges, they denote a _loopback_ address

| Class  | Range 
| --     |--
|Class A | 0-126
|Class B | 128-191
|Class C | 192-233
|Class D | 224-239
|Class E | 240-255

The higher the class, the more hosts they can support, so __Class A__ are best suited for increadibly large networks, __Class B__ better suited for serving smaller networks and __Class C__ being for very small networks.

The remaining classes are mostly reserved for experimental purposes, such as __Class D__ being almost exclusivly reserved for multicasting applications.

Uses for __E class__ were never actually developed, as a result most network implementations disregard this class altogether, this range is sometimes classified as _illegal_ or _undefined_.  The exception is the IP address `255.255.255.255` which can be used as a broadcast address on certain networks.

## VLSM (Variable Length Sub-Masks)
* [VLSM on IPv4 Tutorial](https://www.tutorialspoint.com/ipv4/ipv4_vlsm.htm)
> By using VLSM, the administrator can subnet the IP subnet in such a way that the least number of IP addresses are wasted. 

1. Find out the host needs for each network
    >How many hosts do they need?
1. Order them in decending order (High to low)
1. add +2 to each of them to see minimum IP addresses required
    > It is to take into account the network and broadcast address
1.Create the submask for each network that allows you to have enough addresses.
1. What is the bit-value for the highest host-bit (left-most bit) of this mask? add this value to the current address, that will give you the network address of the next network. 
1. Subtract 1 from this new address to get the __broadcast__ ip for the previous network.