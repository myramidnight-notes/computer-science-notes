[...back](../README.md)
# Gagnagreining

## Python
__Python__ will be used in the course to process the data.

### Useful python libraries
* NumPy
    > Library for high-performance matrix computing. Fast multidimensional arrays. Convenient array shaping methods.
* Pandas
    > Highly optimized data manipulation library for Python. _DataFrame_ object for data manipulation. Tools for reading and writing data between in-memory data structures and different file formats. Reshaping, sinsertion, deletion, merging, joining...
* SciPy
    > for scientific and technical computing. Contains modules for optimization, linear algebra... 
* Scikit-learn
    > Machine learning library for python. Features Classification algs,Regression algs and Clustering algorithms.
* Statsmodel
    > Module builds on NumPy and SciPy that allows users to explore data, estimate statistical models and performe statistical tests. Compliments SCyPy-s stat module and integrates with Pandas.
* Matplotlib
    > Plotting library for Python and NumPy. Provides an object-oriented API for embedding plots into applications.

## Jupyter Notebook
> Web application that allows you to create and share documents with live code, equations, visualization and explanatory texts. Great for data cleaning, data transformation, numerical simulation, statistical modeling, machine learning...

## Subject index

* ### [Data science](data-science.md)
    * [Python for data science](python/README.md)
* ### [Linear algebra](linear-algebra.md)
* ### [Hypothesis testing](hypothesis-testing.md)
    * #### [Inferential Statistics](inferential-statistics.md) 
    * Probability
* ### [Data visualization](visualization.md)
    > How e present the data. _A picture is worth a thousand words_
* ### [Data preparation](data-preparation.md)
    > Preparing the data for processing
* ### [Model evaluation](model-evaluation.md)
    > Evaluating how effective the model is
* ### [Linear and logistic regression](regression.md)
    > Regressin the estimates to a standard line
* ### [Ethics in Data Science](ethics-in-data.md)
    > Discrimination, biases, lack of transparency and privacy