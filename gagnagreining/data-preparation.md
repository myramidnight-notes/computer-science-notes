[...back](./README.md)
# Data Preparation
## Data collection
## Sampling
## Wrangling
> Also known as _data munging_, __Data wrangling__ is the process of cleaning up and unifying messy and complex datasets for easy access and analysis, for more convenient consumption of the data than when it was raw.
## Data Cleaning / Preprocessing
> __Data cleaning__ generally refers to: Fixing up formats, filling in missing values, correcting erroneous values, standardizing categories
>
> Data is noisy/messy and can be inconsistent or incomplete, contain duplicates or in a format that makes merging data difficult. This is a very time consuming part of data visualization (~80%).

### Dealing with outliers
> An outlier is data that falls outside the expected range, usually due to some errors when the data was collected (human input or faulty hardware perhaps). 

* For invalid outliers: treat as missing value
* For valid outliers: truncation/winserising/capping
    > This will move the outliers into the desired range, so they are no longer outliers.

### Transforming data
> A logarithmic transformation is sometimes adopted to optain a more symmetric and normal distribution, which is desirable in regression models.