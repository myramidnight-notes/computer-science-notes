[...back](./README.md)
# Data science
> Data scienece is about using data to solve problems. Data fluency and data literacy are vital skills, specially at the rate data is being crated today.
>
> Raw data is just a wall of information, it is important to be able to gleam useful information from it and precent it in a way that we can understand. The presentation of identical data can paint completely different pictures.

### What does data science involve?
>* Data managment
>* Data mining
>* Machine learning
>* Busness intelligence
>* Statistics
>* Ethics and privacy 
>    * Security
>* Visualization of data
>    * Story telling
>* Human cognition
>* Perception
>* Decision making
>
>![Data science process](images/data-science-process.png)

# Data

1. Collection
1. Cleaning
1. Exploratory data analysis
1. Model building
1. Model deployment


## Data preparation
Where does the data come from? It will probably be quite messy and neads to be cleaned up. 
* Data collection
* Sampling
    > We rarely have the whole population, so we need to select our samples. Select the time relevant data (we don't want outdated data). We want samples of all the relevant topics.
* Wrangling
    > Change the shape and size of the data, the columns and such. To make it fit our needs.
* Data Cleaning / Preprocessing
    > The outliers and such. We just want the relevant data, not the noise. 


## Data import/output
* CSV (comma seperated values)
* Exel files
* JSON files
* HTML files (html tables)
### Other formats
* SQL
* HDF
* CLIPBOARD
* PICKLE

## Data Wrangling
Also known as _data mungling_, cleaning up and unifying messy and complex data sets for easy access and processing.

## Variables and Observations
* Observations: contains all values measured on the same unit across attributes.
* Variables is a characteristics, number, or quality that can be measured or counted (also called feature)

### Long and Wide data formats
* #### Long data format (fewer columns, many rows)
    * long data format have one observation and one measurement 
    * great for plotting summary information for each group and each variable aggregated together
    ![long data format](images/data-format-long.png)
* #### Wide data formats (many columns, fewer rows)
    * Wide data format has every measurment in a single observation in a single row 
    * Ideal for things like scatterplots of one measurement against another.
    ![wide data format](images/data-format-wide.png)

## Messy and Tidy data
![Messy and tidy data](images/data-set-messy.png)
We will want to remove information from the column names and move them to be values. The rows might increase, but the data will be tidier.

* `pandas.melt` function is useful to massage a DataFrame into a format where one or more columns are identifier variables, while all other columns are "unpivoted" to the row axis.
![Data melt](images/data-melt.png)

### Problems
* Information in column names/headers
* Multiple Variables stored in one column 
    > Create multiple variables from a column name
* Variables stored in both rows and columns
    > Moving misplaced variables to new columns
* One type in multiple tables/files
    > A variable could be in the table/file name.

<hr>

## Data cleaning/preprocessing
* Fixing up formats
    > When data is saved or translated from one format to another some data may not be translated correctly. An example are date-stamps that can have different formats.
* Filling in missing values
    > Frequently gaps may appear in data sets where data wasn't collected
* Correcting erroneous values
    > Some columns have expected range of values and sometimes values present in a dataset fal outside of the expected range. Values could be incorrect (impossible), might be caused by human error. They tend to appear as outliers in the data.
* Standardizing categories
    > Categories could perhaps have multiple representations, such as `USA`,`United States`, `US`, `America` or  `United States of America`. We would need to pick one to be standardized representation entry. 

### Why clean our data?
* Data is noisy/dirty
* Inconsistent data
* Incomplete data
* Data integration and data merging problems
    > Such as values being in both USD or Euros
* Duplicate data
    > Salary vs. processional Income, Age vs. Birthyear
* Sucess of preprocessing steps is crucial to success of following steps
    * Garbage in, Garbage out (GIGO)
    * Very time consuming (80% rule)

### Missing values
Reasons:
* Non-applicable
* Not disclosed
* Error when merging data

Solutions:
* __Keep__
    * The fact that a variable is missing can be important information
    * Add an additional category for missing values
    * Add an additional missing value indicator variable (either one per variable or one for the entier observation)
* __Delete__
    * When too many missing values, removing the variable or observation might be an option
    * Horizontally versus vertically missing values (we might delete the column or row, if it is missing alot of values).
* __Replace__
    * Estimate missing value using imputation procedures (if we could estimate what the value would have been)

> Be consistent when treating missing values during model development!

#### Replacing missing values
* For continuous variables
    * Replace with median/mean (median more robust to outliers)
    * If missing values only occur during model development, can also replace with median/mean of all instances of the same class
* For ordinal/nominal variables
    * Replace with modal value (most frequent category)
    * If missing values onlly occur during model development, replace with modal value of all instances of same clas

#### Dealing with outliers
> In statistics, an __outlier__ is an observation point that is distant from other observations.
* Extreem or unusual observations
    * Example: due to recording, data entry errors or noise
* Types of outliers
    * Valid observation: salary of boss
    * Invalid observation: age = -2003
* Outliers can be hidden in one-dimensional views of the data (multimensional nature of data)
* Univariate outliers versus multivariate outliers
* Detection vs. treatment

Outlier treatment
* For invalid outliers
    * Treat as missing values (keep, delete, replace)
* For valid outliers: truncation/winsorising/capping
    * Truncation based on z-scores:
    * Truncation based on IQR (inter quartile range)
