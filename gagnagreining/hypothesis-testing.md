[...back](./README.md)
# Hypothesis testing
It is a decision making process for evaluating claims about a population, assessing the plausability of a hypothesis by using a random sample of the population being analyzed. The test will provide evidence concertning that plausability.

>There are three ways to do testing
>1. The traditional method
>1. The p-value method
>1. The confidence interval method

## Types of hypothesis
* ### Statistical hypothesis
    > A conjecture about a population parameter. The conjecture may or may not be true.
* ### Null hypothesis
    >A __null hypothesis__ is the proposision that there is __no difference__ between a parameter and a specific value, or that there is no difference between between two parameters.
* ### Alternative hypothesis
    > a statistical hypothesis that states the existence of a difference between a parameter and a specific value, or states that there is a difference between two parameters.

## The P-value
> How do we compute the probability that the null hypothesis was false?

* A __p-value__ is a measure of the probability that an observed difference could have occured just by random chance.
* The lower the p-value, the greater the statistical sagnificance of the observed difference
* p-value can be used as an alternative to or an addition to spre-selected __confidence levels__ for hypothesis testing.

* The __p-value__ is the area under the curve that begins from the test statistic and is shaded in the direction of the alternative hypothesis

>### Finding the p-value
> P-values are usually found using p-value tables or spreadsheets/statistical software. These calculations are based n the assumed or known probability distribution of the specific statistic being tested. 
>
>P-values are calculated from the deviation __between the observed value and a chosen reference value__, given the probability distribution of the statistic, with greater difference between the two values corresponding to a lower p-value.

## Solving a hypothesis test

* ### Level of sagnificance
    > Given a statistical test, the probability of observing the same results (p-value) is computed given that the __null hypothesis is true__. 
    >* It is the maximum probability of commiting a type 1 error (false positive). 
* ### Critical region (Fail to reject null hypothesis)
    >![P-value](images/p-value-critical-region.png)
    >
    >The rahnge of test values that indicates that there is a sagnificant difference and that the null hypothesis should be rejected
* ### Critical value
    > seperates the critical region from the non-critical region. The symbol for the critical value is `C.V.`
* ### One and Two-tailed test
    > Indicates that the null hypothesis should be rejected when the test value is in the critical region on one side of the mean. A one tailed test is either a __right-tailed test__ or a __left-tailed test__
    >
    >![Standard Normal Model](images/p-value-standard-normal-model.png)
    >* __Left tailed__ tests for decrease in value
    >* __Right tailed__ tests for increase in value
    >* __Two tailed__ tests for any change in value
    >    * When doing a two-tailed test, then the size of the critical region is shared between the two tails (half the size on either side than when doing a one-tailed test)

## Z-score (standard score)
It is based on the __normal distribution__ of the data, and allows you to easily compute the probability given a lot of samples and __the population standard deviation is known__.

The __z-score__ simply tells you _how many standard deviations from the mean_ your result is.

## T-score (Student's t distribution)
For cases where the sample sizes are small and we __don't know the standard deviation of the population__, then the __t-distribution__ can help find the answers.

There are as many different _t distributions_ as there are __degrees of freedom__ (sample size - 1).

## A/B testing
This is a 2-sample t-test, where a hypothesis test compares the means of the two different samples. Very often used in app/web development to test the impact of changes.

## Chi squared test of independence
The chi-squared test of independence is used to determine if there is a sagnificant relationship between two nominal (such as categorical) variables.
* Are the observed counts so diffent from the expected counts that we can conclude a relationship between the two variables?

## The hypothesis testing process
1. State the null and alternative hypothesis
1. Compute from the observations the observed value `t` of the test statistic `T`
1. Calculate the p-value. This is the probability, under the null hypothesis, of sampling a test statistic at least as extreme as that which was observed.
1. Reject the null hypothesis, in favor of the alternative hypotheis, if and __only if the p-value is less than the sagnificance level__ (the selected probability) threshold.

## Summing up the results
>![Hypothesis test results](images/hypo-test-result.png)
### Table of error types
> ![Table of errors](images/hypo-table-of-errors.png)