[...back](./README.md)
# Inferential statistics
__Inferential statistics__ is the process of _deducing properties of an underlying distribution_ by analysis of data. We gain information from the data.

Inverential statistical analysis __infers properties about a population__, including testing hypotheses and deriving estimates. These properties are made based on a sample from the population.

>### Methods
>* Confidence intervals
>* Hypothesis testing
>* Predictions - ML

## The law of large numbers
The __law of large numbers__ describes the result of performing the same experiment a large number of times. The average of the results is close to the random variable's expected value.

![Law of large numbers](images/hypo-law-large-numbers.png)

## Central limit theorem
When we add up a large number of values from almost any distribution, the distribution of their means converges to the __normal distribution__

Thus statistical methods that work for normal distributions can be applied to many problems involving other types of distributions.

![Normal distribution methods](images/normal-distribution-methods.png)

## Confidence Intervals
>It gives you a range, where we are confident that the unknown value will fall into the given range.
* __Confidence interval (CI)__ is a type of estimate of a population parameter computed from the statistics of a sample.
* Proposes a range of plausable values for an unknown parameter (such as the mean)
* The interval has an associated __confidence level__ that the true parameter is in the proposed range.
* We're estimating the probability that the true population parameters lies within a range around a sample mean.

### Computing confidence intervals

## Sagnificance levels (or _alpha_)
> Given a statistical test, the probability of observing the same results (the p-value) is computed __given that the null hypothesis is true__.

The __sagnificance level__ is decided upon a priori and __if it's lower__ than the p-value, the null hypothesis __can be rejected__.

In academic research a common value for the sagnificance level is `5%`.

## Computing probability

## Critical Regions