[...back](./README.md)
# Linear algebra
Algebra allows us to work with unknown values and figure out what the unknown variables are based on given information. Such as `x + x = 10` will give us the conclusion that `x = 5`, or perhaps `x` is another equation that is equal to `5`. 

Linear equations are easy to solve and practically every area of modern science contains models where equations are approximated by linear equations (using the __Tailor expansion__ arguments).

Linear algebra is specially vital in data science, where data is stored in arrays and matrices and manipulated with LA methods. Machine learning algorithms that learn from the data use linear functions and methods to make estimates on new data based on what it has learned from existing data.

## Vectors
A vector can be considered as a __directed line segment in N-dimensions__. It is typically in 3 dimensions.

## Dot product

The __geometric definition__ of a dot product is denoted by a dot (`.`) between two vectors. The dot product of vactors `a` and `b` results in a scalar given by the following relation, where cos takes the angle between the two vectors.
> ![dot product](images/algs-dot-product-geo.png),

The __Algebraic definition__ of the dot product of `a` and `b` is defined as:
> ![dot product algebraic](images/algs-dot-product.png)