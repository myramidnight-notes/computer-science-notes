[...back](./README.md)
![Data Science Process](images/data-science-process2.png)

# Machine learning
1. Supervised learning
1. Unsupervised learning
1. Reinforced learning

## Supervised learning
* Goal is to learn the __mapping/rules__ between a set of __inputs__ and __outputs__.
* __Training data__ is used to build a model
* __Overtraining a model will cause overfitting__ to the training data and the model will be unable to adapt to new, previously unseen inputs
* When the output could be a category from a finite set said to be classification
* When the output could be a real-world scalar this is known as __regression__

### Regression
> Regression is useful when predicting number based on problems like tempature, probability, prices, and so on... 

## Unsupervised learning
* Only input data is provided in the example data
* No labelled example outputs to aim for
* Common forms of unsupervised learning:
    * __Clustering__
        > The act of creating groups with different characteristics, finding the various subgroups within a dataset.
    * __Dimensional reduction__
    * __Anomaly detection__
## Reinforced learning
* Occasional positive and negative feedback is used to reinforce behaviours
    * Good behaviours are rewarded and become more common
    * Bad behaviours are punished and become less common