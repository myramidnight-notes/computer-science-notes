# Machine Learning
__Machine learning__ is a type of algorithm that creates models that automatically improves itself based on experience, rather than a program that is manually created by a programmer. 

In this case, the programmer might design the behaviour of how the machine will learn, but any results will depend on the data that the machine uses when __training__.

The models created through _machine learning_ have learned to identify specific patterns in the data it's provided, and are usually tailored to process specific type of input and generates processed output.

## Types of Machine Learning
* ### Reinforced learning
    > Training by reinforcement of good behaviour
* ### Unsupervised machine learning
    > Training on unlabeled data
* ### Supervised machine learning
    > Training on labeled data
    * [__Neural Networks__](neural-networks/README.md)
