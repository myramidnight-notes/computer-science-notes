#### References
* [CNN vs RNN vs ANN](https://www.analyticsvidhya.com/blog/2020/02/cnn-vs-rnn-vs-mlp-analyzing-3-types-of-neural-networks-in-deep-learning/)

# [Artificial Neural Networks (ANN)](neural-ann.md)
__Artificial Neural networks__ (or just simply __neural networks__) are a type of machine learning algorithm, specifically __supervised deep learning__. It is inspired by the way biological neural networks in the human brain process information.

![Deep learning](images/machine-learning-deep-learning.jpeg)

## Types of Neural Networks
* ### Feedforward neural network
    > This is the first and simplest type of neural networks, where information moves only from the input layer directly through any hidden layers to the output layer without any cycles or loops.
    * Group method of Data Handling (GMDH)
    * Probabilistic (PNN)
    * Time Delay (TDNN)
    * [__Convolutional (CNN or ConvNet)__](neural-cnn.md)
    * Deep stacking network (DSN)
* ### [Recurrent Neural Network (RNN)](neural-rnn.md)
    > In this network the data can move both forwards and backwards through the layers before coming to a conclusion/result. RNN can be used as a general sequence processors.
    * Fully recurrent (FRNN)
    * __Hopfield__ 
    * Boltzmann machine
    * self-organizing map (SOM)
    * Learning vector quantization (LVQ)
    * Simple recurrent
    * Resevoir computing
    * __Long short-term memory (LSTM)__
    * Bi-directonal (BRNN) 
    * Hierarchial
    * Genetic Scale
* ### Regulatory feedback networks
* ### Radical basis function networks (RBF)
* ### Modular neural networks
    > Similar how the human brain is sectioned into smaller networks, the modular neural networks contain several small networks that cooperate or compete to solve problems
    * Comittee of machines (CoM)
    * Associative (ASNN)

## Concepts
### Perceptrons
> In machine learning, the __perceptron__ is an algorithm for supervised learning of __binary classifiers__. 
>
>It is a type of __linear classifier__ called __threshold function__: a function that maps it's input `x` (a weighted vector/connection) to an ouptut value `f(x)` (_a single binary value_).
>* [Perceptrons on Wikipedia](https://en.wikipedia.org/wiki/Perceptron)
### Multilayer perceptron (MLP)
> A multilayer perceptron is a class of _Feedforward ANN_.
>*  sometimes used to refer _loosely_ to __any__ Feedforward ANN.
>*  sometimes _strictly_ referring to networks composed of multiple layers of perceptrons.