# Artificial intelligence
Here we have a table of the combinations of types of information (numeric and symbolic) and the type of processing (algorithmic and heuristic). Heuristic basically means __problem solving__ or __self discovery__.
|  | Numeric | Symbolic
| -- | -- | ---
| __Algorithmic__ | Traditional scientific calculation | Data processing
| __Heuristic__ | computation-intensive application with heuristic control | Artificial intelligence

A neural network can be trained quite extensivly, which allows us to step into the realm of artificial intelligence. Instead of only knowing how to identify cats on a image, it would learn to identify just about anything on a image, which could extend to the ability to read texts from images. Then we would have a machine that can read. Taking it even further would be having it understanding what it's reading, identifying questions and statements, perhaps learn how to answer those questions.

The possibilities are endless beacuse the network is allowed to grow and make new connections, much like the human brain would.

But this does not mean that we need to allow the system to learn indefinitly, because that can easily step into the realm of __overfitting__, which tends to be just as bad as _underfitting_. When we're happy with the level of intelligence the machine is showing, then we have a functional product that needs no further enhancing. 