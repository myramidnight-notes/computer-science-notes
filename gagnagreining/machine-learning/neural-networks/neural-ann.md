# Artifical Neural networks (ANN) 
__Artificial Neural networks__ (or just simply __neural networks__) are a type of machine learning algorithm, specifically __supervised deep learning__. It is inspired by the way biological neural networks in the human brain process information.

![Deep learning](images/machine-learning-deep-learning.jpeg)

## The layers of Artificial Neural Networks
In a _ANN_, the input data passes through multiple layers before arriving to a result. 

1. Input layer
1. hidden layers
1. output layer

>>__ANN__ is based on collection of connected __nodes__ (artificial neurons). Each connection, like the synapses in a biological brain, can send a signal to other nodes. 

>>These inputs go through __hidden layers__ of these _neurons_, and the output of each _neuron_ would be a product of some non-linear function of the sum of it's inputs. 

>>These inputs travel through connections with other _neurons_, and these connections have varying __weights__ applied to them, a real number that gives them importance. These weights are adjusted as the _ANN_ learns from trial and error, based on if the final output reached a correct conclusion. 

### Node processing
![Activation](images/ann-node-activation.gif)

## Training neural networks
Initially the _ANN_ will have no clue what it's processing, and through the labels attached to the data, it will learn to identify it __through trial and error__, adjusting the weights of it's connections between nodes for a improved output.

It essentially goes through the process of improving it's educated guesses (__predictions__), which hopefully by the end of the training will be the correct conclusion.

>Like a child seeing a tree for the first time, having no clue what it might be, and then it's told that it's looking at a tree. By repeativly experiencing something, we can identify it with more confidence each time. 
>
> What type of tree is it? It can be difficult to know, because each and every tree is unique, growing in different ways, but each still has it's characteristics. 

### Accuracy of the results
The accuracy of the neural network depends heavily on the data provided for the training. It needs to properly varied and labeled properly. 

The _ANN_ need to have seen the subject it's processing from all possible angles in order to better identify it correctly. 

> Just think of having to identify a airplane, it tends to have wheels and a windshield, so it could as well be a car when viewed from certain angles or when we don't have the full image.

Consider the task of learning to perform actions, learning to perform certain tasks correctly requires us to perform them in various ways in order to do them properly, to see what works and what doesn't. 