* [gentle introduction to neural networks](https://medium.com/cracking-the-data-science-interview/a-gentle-introduction-to-neural-networks-for-machine-learning-d5f3f8987786)
* [Advanced neural network teqhinques](https://course.elementsofai.com/5/3)
# Convolutional Neural Networks (CNN)
There is a elegant solution ot the problem of too many edge-weights existing with the growing number of neurons and connections, a special kind of neural network that has special layers. These layers are the so-called __convolutional layers__, and networks that include them are called __convolutional neural networks (CNN)__.

Their key property is that they can detect image features such as bright or dark (or specific colored) spots, edges in various orientations, patterns and so on. 

These allow us to detect more abstract features such as a ears, eyes or mouth, or any other possible features or symbols, which would normally be a very hard thing to train a neural network to detect, where the input image is processed as a whole, because these features can appear in different positions, orientation or size in the images.

A CNN can be used to generate images that are similar to the input data that it was trained on. 

### The trick with CNN
>CNNs use a clever trick to reduce the amount of training data required to detect objects in different conditions. The trick basically amounts to using the same input weights for many neurons, so that all of the neurons are activated by the same pattern, but with different input pixels (cutting the image down into pieces, processing them separately). 
>
>This trick allows specific neurons to trigger when they detect for example an ear in a image, regardless of where it was positioned in the original input image. 
>
>![Convolutional Neural Networks](images/neural-cnn.png)



>>Convolutional Neural Networks are quite different from most other networks, and are primarily used for image processing but can also be used for other types of input such as audio. 
>>
>>A typical use-case for CNNs is where you feed the network images and the network classifies the data.
>>
>>1. CNNs tend to start with an input "scanner" wich is not intended to parse all the training data at once. Essentially cutting the input into smaller parts.
>>1. This cut down input is then fd through convolutional layers instead of normal layers, where not all nodes are connected to all other nodes, but instead each node only concerns itself with close neighbboring cells.
>>1. These convolutional layers also tend to srhink as they become deeper, dividing the each piece down even further.
>>

## Computer generated images
In order to create programs that could generate realistic looking images (of animals, faces, objects..), an idea was proposed of combining two neural networks. One would identify something in the image, such as a face, and then the next network would work on replacing that face, generating a realistic image that never actually existed.

This idea was taken to the extreem with the [__DeepDream__](https://en.wikipedia.org/wiki/DeepDream) (codenamed "Inception" after a film by the same name) program that was created with convolutional neural networks. It would try to find literally any feature in a image that might possibly be the subject they were trained to identify (such as dogs), even if just partially, and then it would enhance those features until you could see the dog it found, producing dream-like or hallucionogenic images.  

### Turning Jellyfish into Dogs
1. Original image
    >![Jellyfish](images/neural-cnn-deepdream1.jpg)
    
1. We're starting to see the dogs
    >![Jellyfish](images/neural-cnn-deepdream2.jpg)
1. Now after 50 iterations of passing the image through the DeepDream
    >![Jellyfish](images/neural-cnn-deepdream3.jpg)