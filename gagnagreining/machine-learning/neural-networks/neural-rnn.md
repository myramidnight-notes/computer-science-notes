* [Sequence modeling with deep learning](https://medium.com/@ODSC/sequence-modelling-with-deep-learning-138dc50c82d2)
* [Recurrent neural networks (ibm)](https://www.ibm.com/cloud/learn/recurrent-neural-networks)
* [Finding structure in time.pdf by Jeffrey L. Elman](https://crl.ucsd.edu/~elman/Papers/fsit.pdf)
# Recurrent neural networks (RNN)
A __recurrent neural network (RNN)__ is a type of _ANN_ which uses sequencal data or timem series data. These algorithms are commonly used for ordinal or temporal problems, such as language translation, natural language processing, speach recognition and image captioning.

They are incorporated into popular applications such as _Siri_, voice search and Google Translate.

They need to be trained on data to learn, just like feedforward and convolutional neural netowrks (CNN), but recurrant neural networks are distinguished by their "__memory__" as they take information from prior inputs to influence the current input and output.

While traditional deep neural networks assume that inputs and outputs are indipendent of each other, the output of recurrent neural networks depend on the prior elements within the sequence, undirectonal recurrent neural networks cannot account for these events in their prediction.

>>A __recurrent neural network (RNN)__ is a class of artificial neural networks, where connections between nodes form a __directed graph__, that allows the data to go back and forth through the layers instead of only in a single direction from input to output. 
>>
>>We have to understand the basics of __sequence modeling__ to properly understand RNNs. 
>>
>>Recurrent neural networks are basically __perceptrons__ with states, keeping track of the past. This makes RNNs very powerful when combined with the following properties:
>>* They have distributed hidden state that allows them to store a lot of information about the past efficiently
>>* They have non-linear dynamics that allows them to update their hidden state in complicated ways.
>>
>>So with enough neurons and time, RNNs can compute anything that can be computed by your computer. 
>>
>![Recurrent neural networks processing](images/neural-rnn-processing.png)
>This example shows how the network is connecting the image to a caption

## Sequence modeling with RNN
>>Here is a basic example of a single layer in a recurrent neural network
>>![Forward passes](images/neural-rnn-seq-1.png)
>>* `x` would be the input layer and `y` the output layer. `a` represents the hidden layers and `W` are the weighted connections.
>>
>> And a diagram of one of the fancier types of RNNs used in production sequence-to-sequence systems (this is a bidirectional encoder-decoder RNN with attention):
>>![Forward passes](images/neural-rnn-seq-2.png)

## Types of RNN Architectures
* ### Fully Recurrent (FRNN)
    > This network connects the output of __all neurons__ to the inputs of all other neurons.  
    >>This is the most general neural network topology because all other topologies can be represented by setting some connection weights to zero to simulate the lack connections between those neurons. 
* ### Elman and Jordan networks
    * __Elman Networks__
        > An __Elman network__ is a three-layered network with the addition of a set of context units. 
    * __Jordan network__
        > It is very similar to Elman networks. The context units are fed from the output layer instead of the hidden layer.
* ### Hopfield
    > The Hopfield network is a RNN where all connections accross layers are equall sized. It requires stationary inputs and thus not a general RNN, as it does not process sequences of patterns.
* ### Indipendently RNN (IndRNN)
    > This network addresses the gradient vanishing and exploding problems in the traditionally fully connected RNN. Each neuron in one layer only recieves it's own past state as context information (instead of full connectivity to all other neurons in this layer) and thus neurons are indipendent of each other's history. 
* ### Recursive 
* ### Long short-term memory (LSTM)
    > It is a deep learning system that avoids the vanishing gradient problem. LSTM is normally augmented by recurrent gates called "forget gaates". LSTM prevents backpropagated errors from vanishing or exploding. Instead, errors can flow backwards through unlimited numbers of virtual layers unfolded in space. That is, LSTM can learn tasks that require memories of events that happened thousands or even millions of descrete time steps earlier. 
    >* LSTM can learn to recognize context-sensitive languages unlike previous models based on hidden Markov models (HMM) and similar concepts.