# Model evaluation
> What does the model do? is it any good? 

* Compare different models 
    >which is best? makes the least number of mistakes. Perhaps the mistakes come from the data (needing improved data cleaning).
* Improve existing models
* Avoid overfitting 
    > Over-fitting can be just as bad as under-fitting,  can cause false negatives. Not very flexible, unable to catch new variables that it hasn't seen before.
    > Appropriate fitting model might get false positive, but it will be more reliable at catching what it's made for.
    > Under-fitting model is not very useful, pointless even. 

## Model fitting
A model would be taught to identify cases based on some _training data_. 
![Model fiting](images/model-fitting.png)
![Model fitting based on dataset size](images/model-fitting-datasize.png)
* A smaller dataset size is more prone to _over-fitting_ the model

## Measuring Model Performance
* Performance
    * How well does estimated model perform in predicted __new unseen__ observations?
* Decide on data set split-up
    * Split sample method, N-fold cross-validation, single sample method, bootstrapping...
* Decide on performance measure
    * Percentage Correctly Classified (PCC), Sensitivity, Spesitivity, Specificity, Area under ROC curve (AUC),...

## Types of teaching a model
* Unsupervised learning
    > 
* Supervised learning
    * Goal is to learn a __mapping__ (or __rules__) between a set of __inputs__ and __outputs__
    * The output is the target variable that we want to predict with the model
        * __Classification__: Target is discrete, belongs to a finite set of categories, such as spam, color, gender...
        * __Regression__: Target is continuous, such as age, income, grade...
    * __Training data__ is used to build a model
    * Apply the model to unseen data to predict it's target
    * __Overtraining__ a model will cause overfitting to the training data and the model will be unable to adapt to new, previously unseen inputs.

# Supervised Training
![Supervised learning](images/model-supervised-example.png)

### Binary classification
* Supervised learning
* Target has two values: true/false, yes/no, pass/fail...
* The output of the model can be either
    * the predicted class 
        * `=> 0 or 1`
    * the probability of belonging to one of the two classes 
        * `=> a number between 0 and 1`


## Splitting data
* Used to avoid over-fitting
* to measure performance

### Split Sample Method
>* For larger datasets
>* Split the data into a training and test set
>* Train the model on the training set (model knows the target)
>* Apply the training model to the test set (model does not know the target, labels removed) to predict the target
>* Compare the target predicted by the model to the true target of the test set and estimate the error/performance
>* Stratification: 
>    * Same class distribution 
>* Common splits for training and test sets are `60/40`, `70/30`, `80/20` (training set being larger). The distribution of the cases should still be relatively equal regardless of the split sizes.
>
>![Example of splitting](images/model-split-example.png)

### N-fold Cross Validation
>* Smaller dataset 
>* Split the data in `N` parts/folds
>* Train on `N-1` folds and test on remaining fold
>* Repeat `N` times and compute mean of performance
>    * Can also get standard deviation and/or confidence interval
>* Leave-one-out cross-validation
>    * `N` equals the number of observations
>    * Leave out each observation in turn
>* Stratified cross-validation
>    * Same distribution of classes in each fold
>* Practical advice
>    * Use leave-one-out cross-validation
>    * Pick one model at random (models differe only in 1 observation, so very simplar anyway), or estimate one model on total data set.
>
>![N-fold example](images/model-n-fold-example.png)

## Training the model
### Confusion matrix
> We can map the results of the model into a confusion matrix, and use it to measure the effectiveness of our model.
>
>![Confusion Matrix](images/model-confusion-matrix.png)

### Model Prediction accuracy
>* #### Recall, sensitivity
>    * __Recall__ measures the ratio of class `1` instance detected by the model. So how how. 
>    * `TP / (TP+FN)`
>* #### Specificaity
>    * __Specificity__ measures the ratio of class `0` instances detected by the model (oposite of _recall_).
>    * `TN / (TN+FP)`
>* #### Precission
>    * __Precission__ measures the ratio of __true class `1`__ instances among those that the model predicts as class `1`. 
>    * `TP/(TP+FP)`
>* #### F score
>    * The __F-score__ is the harmonic mean between recall and precission.
>    * `2*(Recall*Precision)/ (Recall+Precision)`
>* #### Classification accuracy
>    * `(TP+TN) / (TP+FP+TN+FN)`
>    * A unbalanced/biased data, we can have a high precision model even if it predicts nothing.  
>* #### Error rate
>    * `(FP+FN) / (TP+FP+TN+FN)`
>
>![Model precision](images/model-precision.png)
>![Recall and precission](images/model-eval-recall.png)

### AUC (Area under the Curve)
>![AUC example](images/model-auc.png)

#### Area under the Precission-recall curve

## Class imbalance
* When one of the classes of the target variable is underrepresented
* In some cases this can be severe
* When the minority class is less than 10% we should take action
* Use techniques to balance the data before model building
    * Undersampling, oversampling, SMOTE, ROSE, stratified sampling... 

### Undersampling
> Randomly remove observations that belong to the majority class before training a model (to fix the ratio)

### Oversampling
> Add observations to the dataset before training a model by randomly duplicating observations that belong to the minority class

### SMOTE (Synthetic Minority Over-sampling Technique)
> SMOTE synthesises new minority instances between existing/real minority instances
>
>![SMOTE example](images/model-smote.png)