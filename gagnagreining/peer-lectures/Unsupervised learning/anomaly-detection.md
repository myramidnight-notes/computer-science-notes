> Anomalies detect something that stands out, unusual for the data
* Anomaly detection is the identification of __rare__ data points, events or observations. It depends on what is considered normal for the dataset.
* If you can establish the _norm_ for the data, then it becomes easier to spot the rare points.

### Spotting anomalies
* Manual detection
* Automatic detection using statistical scans
* Machine learning
    * System uses machine learning algos. to learn the normal patterns in dataset
    * Presume that most of the data represents normal _traffic_

