Commonly used for
* Which items are bought together
* Price bundling
* Grouping similar products in an isle
* marketing campaigns
# Association
> To group things by association, if it is found with something else, then they must belong together.


### Association algs
* Apriori
    * Ues breath-first search and hash-tree to calculate the itemset
* Eclat
    * Uses Depth-first search 
* F-P growth alg

### How does it work
* Support
* Confidence
