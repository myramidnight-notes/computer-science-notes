Unlabeled data is provided to the model to learn from. It learns to group things together without knowing what those groups are.

* Unsupervised learning finds unknown patterns
* Takes place in real time
* Easier to get unlabled data
* Best to find something, when you're not sure what you're looking for.

### Challenges of unsupervased learning
* Accuracy of output can't always be trusted
* Takes time to interpret and label the results
* How and why does the system reach these results? 

## Clustering
Clustering is the grouping of similar objects
* Input: data
* Model: for example most popular k-means
* Output: number/type of cluster of each input

## partitional clustering
* Keeps trying to find the center of mass, takes a few tries to find the clusters.
* Uses the Loss function

* _k-median_ uses simple euclidean distance and in the median in step 3 of alg. It can handle the impact of outliers better than _k-means_

### Variances of k-means
* K-median
    * Uses median instead of mean when calculating the _center of mass_
    * reduces the impact of outliers
* Fuzzy C-means clustering
    * each data point has a fuzzy degree of belonging to each cluster

### Accuracy of the model
* There is not any labeled data
    * Inter and Intra cluster distance
    * Silhouette score
* There is some labeled data
    * Homogeneity (purity)

## Hierarchical Clustering
* The number of clusters is not needed sincen it's a hierarchial model
* It can find outliers (low desity regions), because the model finds the higher density regions
    * outliers belong to the _noise_ in the data.

### DBSCAN
1. Define min. instances per cluster
1. Define the max distance between instances and cluster core point
1. _draw_ circles with radius `e` around instances

## Summary
* k-means fits all points into clusteres, DBSCAN does not
    * Training data could include outliers
* k-means have no lower limit for number of points in cluster, DBSCAN has a lower limit
* k-means needs you to specify the number of clusters
    * data changes over time, might have to run the k-means again.


# Jupyter
## Hierarchial clustering
* K-means
* Agglomerative
    * it is bottom up approach, which is popular, it starts grouping similar groups together to create a hierarchy
* DBSCAN (Density based clustering)