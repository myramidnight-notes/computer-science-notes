# XGBoost and Ensembly Learning
It builds on decision trees
## Weak learners
It is a prediction model that is better than random guessing. 

## Ensemble Learning
Base learning algorithms that are run multiple times, to classify or regress

Two main ways to do this:
- Construct the model independently (1)
- Construct the model in a coupled manner (2)

### Independ learning
Given data points, bagging samples randomly (with
return). New resampled dataset:
    - Some data points may reappear and be re-fed into
    the model
    - Others may never appear and never be fed into
    the model
    If the model is unstable:
    - Small changes in input => important changes in the
    model => bagging will produce many independent
    results from each weak learner

### Coupled learning
In contrast to independent models, where we manipulate the inputs,
outputs and features, or the randomness of the model we have an
additive model:
Prediction of a class of a new datapoint by taking a weighted sum of a
set of component models.
=> The choice of one component model influences the choice
of the next result

>### Adaboost 
>We want to build a weighted sum of hypotheses

## Gradient Boosting
## XGBoost