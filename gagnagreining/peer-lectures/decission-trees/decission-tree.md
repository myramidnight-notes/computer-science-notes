# Decision tree
* Supervised machine learning
* nodes and directed edges.
* Questions will have answer nodes.

### Finding which node should be the root
* We find the node with the lowest `gini-impurity` level
* Then for each child-node (more questions) will go through the same process of finding which of the remaining nodes would be best at the top.

## Avoiding overfitting
* Grow the tree and then prune the branches that have a negative effect on the results

## Pros and cons
### Pros
* Easy to understand and interpret
### Cons
* It can be overfitted