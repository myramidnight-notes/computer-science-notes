# Random forests
* Consists of large number of individual decision trees
* RF algorithm builds multiple decission trees and merges them together to get more accurate results

* The low correlation in random forests makes them more accurate than a regular decision tree.
* Effective on 