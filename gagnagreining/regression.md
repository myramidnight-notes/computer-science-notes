# Regression 
Linear and logistic regression are among the fundamental statistical and machine learning techniques.

>> Need to sort through these notes
>>* Advisable to learn them first and then proceed proceed towards more complex methods
>>* Connsider some phenomenom of interest and that you have a number of observations. Following the assumption that (at least) one of the features depends on the others, you try to establish a relation among them.
>>    * Want to find a function that maps some features or variables to others sufficiently well
>>* The dependent features are called the dependent variables, outputs, targets or responses
>>    * Study some hours and get a result from exam
>>* The independent features are called the 
>>* The inputs can be continuous, descrete, or even catagorical data
>>* Does

* __Linear Regression__
    > Linear approach for modeling the relationship between a scalar response and one or more explanatory variables using linear predictor functions.
    * Simple linear regression
        > Straight forward approach for predicting a quantitate response `Y` on the basis of one predictor variable. __Assumes there is approximatly a linear relationship between `X` and `Y`__
    * Multiple linear regression
        > Extends the simple linear regression model, to include more predictor variables.
* __Logistic regression__
    > Statistical model that uses a logistic function to model a binary dependent variable (the probability of a certain class or event)


## Linear Regression
### Simple linear regression
> ### Estimating the coefficients
> > _Coefficient: Stuðull er í stærðfræði föst tala sem er margfeldi jöfnu/runu/röð, Dæmi: `4x`, hér er `4` stuðullinn, margfaldar allt sem er tengt við hana._
> 
> We want to find the best line where all the real values are closesest to the predicted line, which is __the least sum of squares__ (the least squares method).
>
> ![Coefficient line](images/regres-coefficient.png)

### Multiple linear regression
## Logistic regression
When the variables are binary, then linear regression is not practical, as shown in the example below, it can't quite capture the data.
![Example between linear and logistic regression](images/regres-linear-logistic.png)

* We use the sum of __likelyhood__ of something happening rather the sum of squares to create our logistical model.

## Pros and Cons of regression
> Regressing the estimates to a standard line
* Pros
    * Interpretable
    * Fast and easy
    * Help tetermine which variables matter most
    * Golden standard in many fields
* Cons
    * Limiting
    * The linear assumption
    * Large datasets
    * Overfitting
    * Dimensionality
    * Outliers