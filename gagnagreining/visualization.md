[...back](./README.md)
# Visualization
__Visualization__ is any technique for creating images, diagrams or animations to communicate a message.

_A picture is worth a thousand words_ because the way the human brain processes information. Viual information is easier to digest than raw data.

The purpose of vizualization is to give insight into the data, not to make pretty pictures. Bad vizualization can give as the wrong impression of the data.

## Message through visualization
We can give very different messages while visualizing exactly the same data, which shows the importance of choice in presentation of these visualisations.
![Messages throug visualization](images/visualization-message.png)

## Bad Data Visualization
* Incorrect graphics
    > Wrong data, wrong title...
* Complicated
    > Hard to extract useful information 
* Misleading
    * Wrong scale (such as truncating the Y axis)
    * Omitting data
    * Correlation causation
* Bad visual cues
    >Wrong use of colors or typeface

### The Lie Factor
![The lie factor](images/visualization-lie-factor.png)

## Principles to Graphical Integrity
1. The represnetation of numbers, as physically measured on the surface of the graphic itself, should be directly proportional to the numerical quantities represented
1. Clear, detailed and thorough labeling
1. Show data variation, not design variation
1. In time-series dispslays of money, standardized unit of monetary measurements are generally better than nominal units (such as adjusting for inflation)
1. Number of information-carrying dimensions depicted should not exceed the number of dimensions of the data
1. Graphics must not quote data out of context

## Good Data Visualization
Good visualization can help users __explore__ and __understand data__, and also communicate that understanding to others.

### Good data analysis should be (per _E. Tufte_)
1. Show the data
1. Provoke thought about the subject
1. Avoid distorting the data - present the data with clarity
1. Present many numbers in a small space
1. Make large datasets coherent
1. Encourage comparison and contrasts
1. Serve a clear purpose
1. Be closely integrated with statistical and verbal description of the data

## Summary
* ### Design for your audience
    >think about the message you are trying to convey to this audince with this particular visualization.
* ### Accurately represent the data
* ### Keep it clear
    * "The purpose of visualization is insight, not pictures" (Shneiderman)
* ### Avoid using effects that can hide data