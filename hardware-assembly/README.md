[...back](../README.md)

# Computer Science notes
These notes were compiled over the `Gagnaskipan` (Data structure) course at Reykjavik university.

Reference material: 
* _Computer SystemsA Programmer’s Perspective (3rd global edition) - Randal E. Bryant and David R. O’Hallaron_
* Lectures at the University of Reykjavik

>I try to keep these notes about binary neutral of any specific programming language, but all possible code references will be in `C` since that is the programming language used in the reference book mentioned above. You can view my [notes about programming in C](../lang/c/c.md) in this collection.

### Data types and binary bits
* [Data storage and types: Binary & Hexadecimals](./cs-data.md)
* [Bit manipulation](./cs-bit-manipulation.md)
* [Binary hex](./cs-binary-hex.md)

### Assembly
* [Assembly](./cs-assembly.md)
* [Assembly conditions](./cs-assembly-conditions.md)
* [Assembly operands](./cs-assembly-operands.md)
* [Assembly registers](./cs-assembly-registers.md)
