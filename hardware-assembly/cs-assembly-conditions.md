[back to CS index](../README.md)

## Condition code register
* `CF` is _carry flag_ (unsigned overflow)
* `ZF` is _zero flag_
* `SF` is _sign flag_
* `OF` is _overflow flag_ (signed overflow)

__lea__ does not do conditions.

## Comparison instructions
`cmp_` compares two variables. Behaves the same way as SUB (substract operand) but does not alter the second operand

* `cmpq %rax, %rdx` is like doing  `0x2 - 0x1 = 0x1` -> `%rdx`
* `subq %rax, %rdx` is like doing `0x2 - 0x1 = 0x1` -> ?
> The difference between the two is what they do with the results.

## Test instructions
* `test_` works like AND but does not alter the second operand.
* `testq %rax, %rax` is like doing `%rdx & %rax`


> if you test it with itself, then you are checking if it is 0 or not.

## condition codes
Same conditional suffixes, such as `sets` and `js` are both checking _negative_ conditional for either set or jump. Some of these conditional codes have alternative code as listed below.
* `__e` or `__z` is condtion __Equal__ (or __Zero__) 
* `__ne` or `__nz` is __Not equal__ (or __Not zero__)
* `__s` is __Negative__
* `__ns` is __Not negative__

 Signed conditionals
* `__g` or `__nle` is __Greater >__ (or __Not less or equal__)
* `__ge` or `__nl` is __Greater or equal >=__ (or __Not less__ )
* `__l` or `__nge` is __Less <__ (or __Not greater or equal__)
* `__le` or `__ng` is __Less or equal <=__  (or __Not greater__)

 Unsigned conditonals
* `__a` or `__nbe` is __Above >__ (or __Not below or equal__)
* `__ae` or `__nb` is __Above or equal >=__ (or __Not below__)
* `__b` or `__nae` is __Below <__  (or __Not above or equal__)
* `__be` or `__na` is __Below or equal <=__ (or __Not above__)


## Accessing condition codes
* `sete D` or `setz D` is condtion __Equal__ (or __Zero__) 
* `setne D` or `setnz D` is __Not equal__ (or __Not zero__)
* `sets D` is __Negative__
* `setns D` is __Not negative__
* ... and so on 
> Always set a single byte D to 1 or 0 (`%al` is single byte register). __Suffix is NOT a data type specifier__

## Jump instructions 
### unconditional jumps
This is what break does, jumps out of the loop. 
* `jmp Label` unconditional jump to a label (`jmp .L1`, ``)
* `jump *Operand` an indirect jump

### Conditional jumps
* `je L` or `jz L` is condtion __Equal__ (or __Zero__) 
* `jne L` or `jnz L` is __Not equal__ (or __Not zero__)
* `js L` is __Negative__
* `jns L` is __Not negative__
* ... and so on