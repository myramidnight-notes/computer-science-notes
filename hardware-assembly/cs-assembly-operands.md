[back to CS index](../README.md)

## Assembly operands
The operators will tell the registry what it is going to do with the data it holds, such as moving it between locations.

### Move (_mov__)
* `movl` behaves differently than the other `mov` operators, it replaces the upper 32 bits with zeros. Useful for clearing out the upper bits for unsigned.

>Notes (need to be sorted/rewritten)
>* Dereferencing is when the register fetches the actual value of the pointer from memory and moves it into the register. One _register_ would hold the pointer while another will hold the derefferenced value.
>* When it is done working with the value it can return the value back to memory.
>* Interactions with memory are surrounded with brackets `( )`. 
>* The assembly suffix indicates how many bytes the register is carrying
>* `mov_ {source}, {destination}` the source and destination is seperated by a comma `,`


#### Scaled indexed move
Basic format of assembly command/instruction
* `movq imm(BASE, INDEX, SCALE)` 
* Address: `offset + base + index * scale`

Registers are just values that hold pointers or offsets. Here are examples of how to read the register info.
* `(%eax)` = fetch the value of `%eax` from memory
* `0x108`  = absolute address to memory location
* `$0x108` = the `$` indicates a hardcoded value, not address, 

### Load effective address (_lea__)
People liked to use __lea__ to do simple math even though it was not made for it, as long as the _scale_ matched existing data type sizes.

* `lea_ {source}, {destination}`
* Calculates the memory address and stores to destination.
* `(BASE, INDEX, SCALE)`, Address: offset + base + index * scale

Using _lea_ to do simple math
* `leaq (%rdi, %rsi, 2), %rax` = x + 2y
* `leaq 7(%rdi, %rsi, 4), %rax` = x + 4y + 7

### Arithmetic operations