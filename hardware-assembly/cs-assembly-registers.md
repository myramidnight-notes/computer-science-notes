[back to CS index](../README.md)

# Some notes about the CPU registers 
Each _core_ in a __CPU__ has it's own set of registers, which can be compared to being different offices at the same company. It makes it more efficient at multi-tasking.

These days the CPU has it's own inbuilt memory called _cache_, which it keeps all data that it is currently handling. This is essentially temporary storage, moving any data that needs to be stored back into the memory (harddrive or RAM). RAM is the __short term memory__ of the system, middle ground for keeping data in active memory, since constantly digging through harddrives is slow.

>The computer is now less likely to encounter a _bottleneck_, since in most cases the registry has enough hands to run a program without having to constantly place data on the stack and finding it again during the same program.
## 32bit Integer registers (1A32)
```
1A32 Integer registers
|general purpose|       | high | low |          
|---------------|-------|------|-----| 
|%eax           | %ax   | %ah  | %al |  accumilate
|%ebx           | %bx   | %bh  | %bl |  base
|%ecx           | %cd   | %ch  | %cl |  counter
|%edx           | %dx   | %dh  | %dl |  data
|%esi           | %si                |  source index
|%edi           | %di                |  destination index
|%esp           | %sp                |  stack pointer
|%ebp           | %pb                |  base pointer
|---------------|--------------------|
```
## 64bit Integer registers (x86-64)
These are basically zoomed out view of the 32bit registers. All the register names start with `r` instaed of `e`
* `%eax` becomes `%rax` and so on.
* You cannot expect a register to retain the same value next time you wish to use it, unless it is a register that saves the callee (they always go back to the original position after returning the value they were holding if they are used in between). 
```
x86-64 Integer registers
|---------------------|---------------------|
| %rax    Return value| %r8      Argument #5|
| %rbx    Callee saved| %r9      Argument #6|
| %rcx     Argument #4| %r10    Callee saved|
| %rdx     Argument #3| %r11    Callee saved|
| %rsi     Argument #2| %r12    Callee saved|
| %rdi     Argument #1| %r13    Callee saved|
| %rsp   Stack pointer| %r14    Callee saved|
| %rbp    Callee saved| %r15    Callee saved|
|---------------------|---------------------|
```
> This is like a zoomed out view of the 64bit registers
## Accessing registers
If you imagine that each register is a hand, you always use the full hand to pick up something, then you can specify to look at the data based on how many fingers touch it. Some of these hands are reserved for specific type of tasks.
* In this example `%rax` would use all 5 fingers (8 bytes), thumb touching the 8th byte,
* then `%eax` only covers 4 fingers (4 bytes)
* `%ax` being 2 fingers (2 bytes)
* `%ah` or `%al` points to which one of the last 2 fingers (1 byte) 

> though `%ah` is not always supported these days.
```
Byte index  
| 7 | 6 | 5 | 4 | 3 | 2 |  1  |  0  |
|               |       | %ah | %al |
|               |       | %ax ------|
|               | %eax -------------|
| %rax -----------------------------|          
```

### Register roles
There are total of 16 different registers in __64bit systems__, and with these increased number of registers we need to rely less on the stack than with __32bit systems__ for programs using 6 arguments or less. It is still useful to try to imagine how many different pieces of data are being juggled at the same time by the register.

Here is a list of a few examples of the 64bit registers.
* `%rax` is the hyperactive hand, containing temporary data/return value. 
Used primarily to fetch things to put into other registers.
You always need to store it's value somewhere if you want to keep it. 
* `%rsp` is the stack pointer, points to the top of the stack.
* `%rdi`, `%rsi`, `%rdx`, `%rcx`, `%r8` and `%r9` store arguments (any extra arguments would be stored on the stack)
* Rest of the registers are basically __base pointers__ that save the callee (locations within the program that requested the task).
