[back to CS index](../README.md)

# Computer Architecture
## The Processor (_"örgjafi"_)
The __CPU__ (_central processing unit_) is the brain of the computer. As the name suggests, it processes all the instructions given by programs. 

```
CPU = {
  Program counter (PC)
  Registers <-> Arithmetic Logic Unit (ALU)
  Control unit
}
CPU <-> Main memory (RAM)
CPU <-> Input/Output
```
## Cache and Memory
All __CPU__ these days have it's own internal temporary memory called __the cache__ that increases the speed of which it processes data. This is because constantly reading data from _memory_ is slower than just reaching out to the _cache_. 


It will grab whatever data it is currently working on (or expects to use) from memory and stores it in the _cache_ and then returns the results it back to memory once it is done working on the data.

The _cache_ has multiple levels, and each core in the procecssor has it's own `L1` and `L2` cache (`L3` is shared between them) and this allows for greater muti-tasking.

> Cache is temporary, while the _memory_ is permenent, any data not saved to memory will be lost if the computer happens to crash.

# The Assembly
The __assembly__ is the language that gives the __CPU__ instructions on how to handle the data, compilers handle tha translation of known programming languages into assembly.


#### Compiling code to assembly
There usually exist compilers for existing programming languages that turn the program into assembly that the CPU will understand. There are a few steps when we are compiling `C` into a executable program, and we can tell the compiler to only process the files to a certain step, for example only up to the assembly stage.

>The compilation steps for `C`
>1. `hello.c` - source program (text), goes through the _preprocessor_
>1. `hello.i` - modified source program (text), goes through the _compiler_
>1. `hello.s` - assembly porgram (text), goes through the _assembler_
>1. `hello.o` - relocatable object programs (binary), goes to the _linker_
>1. `hello`   - executable program (binary)

## Integer registers
Think of the register as _lots of hands_, each holding a single value that can be used and referenced, their size changes to pick up acurate number of bytes. 

If you need to use a specific hand, then you often need to make it put down whatever it was holding in order to have a empty hand for use. Some of these hands will move back to pick up original value once it is done working for you.

#### The reference pointers
There are two registers that are dedicated to keep track of locations within the stack, __stack pointer_ and _base pointer_

Since data keeps being piled ontop of the __stack__, creating a continous binary sequence, and you can only access the very top of the stack. All locations are calculated in relation to where the top of the stack is located. 

* The __stack pointer__ `%_sp` always points to the _top_ of the stack, it keeps changing as data is added to the stack. 
* The __base pointer__ `%_bp` is for _base reference_, where the program starts in the stack and then just calculates how many bytes from the base it should move to find the desired data. 


### Data types and assembly suffix
We will need to be able to tell the assembly how many bytes it will be working with per instruction, since data types have varied byte sizes, and does this by adding a __suffix__ to the operant.
```
for x86-64 (64bit)

C data type | Assembly equivalent | Assembly suffix | bytes
------------|---------------------|-----------------|-------
char        | byte                | b               | 1
short       | word                | w               | 2
int         | double word         | l               | 4
long        | quad word           | q               | 8
char *      | quad word (pointer) | q               | 8
float       | single precision    | s               | 4
double      | double precision    | l               | 8
long double | extended precision  | t               | 10/12
```
`movq` would tell the assembly to move 8 bytes

### Assembly operations
* __Arithmetic operations__ on register or memory data
* __Transfer data__ between memory and register
* __Transfer control__

### Address
The assembly is all about giving the _CPU_ instructions of how to handle data by telling it what action to take, where to find the data to perform said actions and where to put the data when done so the registry is free to follow the next set of instructions.

The address tells the registry where the first byte of the requested data is located, and then the __assembly suffix__ decides how many bytes following that address will be picked up (size of the _hand_ so to speak).

Here is a list that shows how to address data:
```
Type        | Form                  | Name
------------|-----------------------|----------------------
Immediate   | $imm                  | Immediate (constants)
Register    | %REG                  | Register
Memory      | Imm                   | Absolute addressing
Memory      | (%REG)                | Indirect
Memory      | Imm(%REG)             | Base + displacement
Memory      | (%REG_a, %REG_b)      | Indexed
Memory      | Imm(%REG_a, %REG_b)   | Indexed
Memory      | (,%REG,s)             | Scaled indexed
Memory      | Imm(, %REG,s)         | Scaled indexed
Memory      | (%REG_a, %REG_b,s)    | Scaled indexed
Memory      | Imm(%REG_a, %REG_b,s) | Scaled indexed
```

An example of how to read the provided address.
```
operator  {source}                , {destination}
movq      Imm(BASE, INDEX, SCALE) , ... 
    
Address: offset + base + index * scale
```
>Example: `movq 0xF8(,%rax,8), %rcx` = `0xF8 + 8` = `0x100`
>
>Move from the memory location 0xF8, indexed by %rax, scaled (multiplied) by 8 bytes to %rcx

### Sources of data
The assembly interacts with data from the following sources:
* __Immediate__ (constants), marked with a dollar sign `$0x4B` are direct input values, can be moved to registry or to memory
* __Registry__ (cache), `%rax` is data that is already being held in the registry. Can be moved to another registry or memory.
* __Memory__ (hardrive, hdd), contained in parenthesis `(%rax)`, can be moved to registry. 

 ___Memory-to-memory_ transfers cannot be done with a single instruction__, the data needs to be moved to registry and then moved back to memory with  second instruction.

## Procedures
### Subroutines
They make our code easier to read, modular and reusable.

Subroutines are functions, but they techincally do not exist in the assembly. Instead it treats it as instructions to change the _program counter_ to jump to a different spot in the stack.