[back to CS index](../README.md)

# Binary and Hex
`Hexadecimals` are often used to represent data because raw `binary` can be tedious to read. This is why it is important to understand both `binary` and `hex` together.

### contents
* [Hexadecimal](#hexadecimals)
* [Unsigned](#unsigned-integers) and [Signed integers](#signed-integers)
* [One's and Two's complement](#ones-complement)
* [Masking](#masking)
* [Overflow](#overflow)
* [Floating points](#floating-points)

## Hexadecimals
> Hexadecimal is `base 16` which means it has 16 possible values, `0-9` and `A-F`.

The fact that the highest decimal value of 4 bits is exactly 16 makes the hexadecimal perfect middle ground to make binary human-readable with each digit representing 4bits. Each pair of `hex` digits will therefor represent a single `byte`. 

Hexadecimals are used as `integers` in programming and it understands right away the difference between decimal and hexadecimal when you add `0x` at the front.

```c
// decimals and hexadecimals are interchangable at 0 - 9. 
// hex 9 (written 0x9) is same as the decimal 9. 

Binary    1010  1011  1100  1101  1110  1111
Decimal   10    11    12    13    14    15
Hex       A     B     C     D     E     F
```

For example the binary `0100 1010` would simply be `0x4B` with each letter representing 4bits. Converting the same binary into decimal would give you `75`.

```
8bit byte
Binary        :     0  1  0  0  -  1  0  1  0
nibble value  :     4              10
Hexadecimal   :     4              A          = 0x4A
```
## Binary to Decimal value
When reading the value of a binary sequence, we start at the `LSb` and work our way towards the `MSb` (from right to left) The weight of each bit is calculated as the _seat number_ (starting at 0) in the power of 2, basically just saying each bit is double the weight of previous bit.

The value of a binary sequance is then just a sum of the weight of all the set bits.
```c
Hex   |          2          |         5         | //0x25
Binary|   0  | 0  | 1  | 0  | 0  | 1  | 0  | 1  |
weight|  128 | 64 | 32 | 16 | 8  | 4  | 2  | 1  |
------|------|----|----|----|----|----|----|----|
Value |   0  | 0  | 32 | 0  | 0  | 4  | 0  | 1  | = 37
//       MSb                                LSb
```
> Hexadecimals make it easy to desypher the location of each set bit, since each hex digit tells you the value in 4bit pairs. But finding the decimal value of a hex is not as simple as just sum of it's digits.

## Unsigned integers
`Unsigned` integers are positive numbers ranging from `0` and upwards. The maximum unsigned value is called `Umax` and the minimum being `Umin` 
```c
// example using 8bit unsigned
0000 0000 = 0     // Umin
1111 1111 = 255   // Umax
```

> Not all programming languages let you specify `signed` or `unsigned`, for example __python__ only has signed integers and __javascript__ only has floating points.

## Signed integers
`Signed` integers reserve the `MSb` to represent a positive (`0`) or negative (`1`) numbers. This reserved bit is referred to as the `sign bit`. 

The maximum value of signed integers are called `Tmax` and the minimum is called `Tmin`. Taking 1 bit away to be the `sign bit` you will have one less bit to represent value. 

The `Tmax` is basically half of what `Umax` is, because half the range is reserved for negative values. `0` is on the positive range and therefor the `Tmax` is 1 less than `Tmin`.

```c
//example using 8bit signed
  0111 1111 =  127    // Tmax
  0000 0000 =  0 
  1000 0000 = -128    // Tmin
```

The `one's complement` and `two's complement` are different methods to handle the __negative numbers__ of signed integers.

### One's complement
_One's complement_ simply flips all the bits of the positive number to create the binary negative. 
* decimal 5 is `0101` in binary 
* decimal -5 in `one's complement` is `1010`.

The main issue with using this encoding to handle signed integers is the fact that you will have two zero values, `0` (0000) and `-0` (1111). 

### Two's complement
_Two's complement_ is the most popular way to handle signed integers today and is basically just _one's complment_ +1. 

With this encoding there will only be one zero value, but it also means you will have one more negative value available since the `-0` is no longer available value.

Let's use 5 as an example again.

* To get -5 in `two's complement`, you first just flip the bits as if doing `one's complement` to get`1010`
* Then you just add `1` to the binary. `1010` + `1` = `1011`.
* To decode the value, simply flip the bits again and add 1.
* `1011` flipped is `0100`, then add __+1__ to get `0101` = 5 in decimal (and since we were decoding a negative, it is actually -5)

## Overflow
Overflowing is when you try to go higher than the `Tmax`/`Umax`. Any set bits that go past the `LSb` will be dropped. The effects of overflowing depends on if the integer was `signed` or `unsigned`:
* __Unsigned__ integers will simply change values, becoming lower value than intended
* __Signed__ integers will end up 'flipping' the value _negative_ to _positive_ or _positive_ to _negative_, because it will overflow into the `sign bit`, example: `Tmax + 1 = Tmin`

```c
  132 =   1000 0100   // unsigned
+ 146 =   1001 0010
      = 1 0001 0110 = 278 // what you want
          0001 0110 = 22  // what you get
```

```c
  126 = 0111 1110   // signed 
+   3 = 0000 0011
      = 1000 0001 = -127
```
### Two's complment addition 
> 2.3.2 in the book
```
            ⎧
            ⎪ x+y−2^w, 2^(w−1) ≤ x+y      Positive overflow

x+^t_w*y=   ⎨ x+y, −2^(w−1) ≤ x + y < 2^(w−1) Normal
            ⎪ x+y + 2^w, x+y < −2^(w−1)       Negative overflow
            ⎩
```
<img src="images/cs-t-overflow.PNG">

## Masking
Masks are a way to tell the program to only look at specific bits instead of the whole byte by the using binary operators.

The masking effect is created with the logical _AND_ to compare two values bit by bit if both are `true`. So the bask would have `0` in all the bits that it does not want to recieve and `1` in all the bits it wants.
```c
      0010 1110 1011 1001 // the binary to compare
      0000 0000 1111 1111 // the 'mask', 0x00FF
      0000 0000 1011 1001 // what is returned
```
Creating masks is quite simple with hexadecimal values, the mask `0x0F` would only return the later half of the byte and `0xF0` would return the first half of the byte.


> Handy way to create a mask to isolite a bit: `(1 << n) + ~0` where `n` is how many bits to shift. Adding the 1 to a `~0` will create cascade turning all the 1's into 0's past that point to the left.

```c
//         (1 << n) + ~0
1 << 2  |  0000 0100
  + ~0  |  1111 1111
     =  |  0000 0011
```

 Read about the [bit operators in C](./notes-c.md#bit-operators)

## Floating points
> Virtually all computers nowadays support the so called `IEEE floating point` (set by the _Institute of Electrical and Electronics Engineers_). 

Floating point numbers are yet another data type that handles fractions, and it does this by _floating_ the point to where it needs to be to best use the bits, hence the name.

Fractions are use alot of approximations and cannot therefor represent all numbers perfectly. It invites accumilated inaccuracy as far as programming goes. 

Example: 
>`0.1 + 0.2 != 0.3` because it is actually ` 0.30000000000000004` 

```
Can all numbers be represented in float?
5.75    101.11000   yes
63/64   .11111100   yes
1/3     .01010101   no (will be approximation)
```
### Fractional Binary Numbers
<img src="images/cs-float-fraction.PNG">

### Single and Double precision
Since the `64bit` computers came along, floating points have become a whole lot more precise with the increased bit allowance.

> It is more difficult to _break_ `double precision` (64bits) floats than `single precition` (32bits) because of the increased number of bits it has. 

The most widely used standard for representing floating point numbers: 
* __y = M * 2<sup>E</sup>__
* `M`: the mantissa is the fractional value in the range, but the value encoded also depends on whether or not the exponent field equals 0
* `E`: the exponent tells you where the point is located
> Example:  __5*2<sup>100</sup>__ would consist of `101` followed by `100` zeros.
```c
            | single precision  | Double precision
signed bit  | 1 bit             | 1 bit
exponent    | 8 bits            | 11 bits
mantissa    | 23 bits           | 52 bits
------------|-------------------|--------------------
            | 32 bits           | 64 bits
```
