[back to CS index](../README.md)

# Notes about Bit manipulation
> Binary examples are usually `1 byte` (`0000 0000` or `0x00`)
## Bit operators
* `&` AND, true only if both bits are 1
* `|` OR, true if either bit is 1
* `~` NOT, inverts the bits
* `^` XOR, true if neither bit is identical
* `!` logical NOT, "bang"

```c
// when you use the bit operators to compare binary values:
   0110 1101      0110 1101      0110 1101  
 & 0101 0101    | 0101 0101    ^ 0101 0101    ~ 0110 1101
 = 0100 0101    = 0111 1101    = 0011 1000    = 1001 0010

 ! 0010 0000    ! 0000 0000   // 0 = True, !0 = False
 =         0    =         1   // will return False for any value except 0. 
 ```
The _bang_  will just return TRUE or FALSE, only `0` can be `true` and any other value is then `false`. This makes the _bang_ a easy way to check if any bit is set or not.
```c
!45    // 0
! 0    // 1
!!0    // 0 (not not 0)
```

## Binary math
### Addition
```c
    1 + 1 = 0 (1 carries over)
    1 + 0 = 1
    0 + 1 = 1
    0 + 0 = 0

    0010 1100       0011 1111
  + 0001 0001     + 0000 0001
  = 0011 1101     = 0111 1110
```
### Substraction
```c
    1 - 1 = 0
    1 - 0 = 1
    0 - 1 = 1
    0 - 0 = 0
```
### Multiplication (left shift)
Shifting to the left `<<` is like multiplying by __2<sup>w</sup>__ (where `w` is number of bits moved).
```c
8 multiplied by 2 = 16
0000 1000 << 1    = 0001 0000
```

### Dividing (right shift)
Shifting to the right `>>` is like dividing by __2<sup>w</sup>__  (where `w` is number of bits moved). 
``` c
8 divided by 2 = 4
0000 1000 >> 1 = 0000 0100
```

### Rounding integers
Integers will always round towards zero
* Positive numbers round down (towards zero)
* Negative numbers round up (towards zero)

### Modulus (Remainder)

The remainder of `a` divided by `k` = __a mod 2<sup>k</sup>__ = `a & ((1 << k) + ~0)`

modulus `mod(a,b)` where `b` is some value to the power of two (2<sup>k</sup>).
We can get the remainder by masking by k-1 bits

```
int mod ( int a, int k)
{
  int x = (a >> k) << k;
  return a - x; 
}
```

## A few hints and tricks
```c
~ is often useful
  ~0      = 0xFFFFFFFF = -1
  ~x      = x - 1
  x - ~0  = x - 1
  ~TMin   = TMax
  ~TMax   = TMin

  -x      = ~x + 1
  -TMin   = TMin

^ is also useful
  x ^ x   = 0x0

Shifting 
  1 << 31 = TMin
  x >> 31 = 0x0 if x > 0 else 0xFFFFFFFF

Division is in the slides
  x < 0 then add bias

If else supstitute
  (x > 0) ? a : b      
  (x > 0) if a else b ->  (a & ~m) | (b & m) where m = x >> 31

```