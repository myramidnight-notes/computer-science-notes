[back to CS index](../README.md)

# Data storage 
>These notes are specifically about data, you should also check these other notes:
>* [Binary and Hexadecimals](./cs-binary-hex.md)
>* [Bit manipulation notes](./cs-bit-manipulation.md)
>* [C programing notes](../notes-c.md)

Data needs __context__ in order to be useful when decoded, to know how many bytes to read and where the _start_ and _end_ of each data object. It is just a bunch of random `1` and `0` without context.

#### Glossary 
* `bits` or `b`
* `bytes` or `B` 
* each `bit` contains a binary value of `1` or `0` (`True` or `False`)
* one `byte` is `8 bits` 
* `4 bits` is called a `nibble` (half a byte)
* Computers are `byte addressable`, meaning they manage data in __whole bytes__ (not in individual bits)
* The `MSb` (most sagnificant bit) is the left-most bit: __0__ 000-0000
* The `LSb` (least sagnifiant bit) is the right-most bit: 0000-000 __0__

### Cache
The __cache__ lives within the __CPU__ as inbuilt memory, like the shelfs within a office that hold anything that the office worker would require to be within reach. There are different levels of _cache_,  `L1` and `L2` would be within the office space, while `L3` is in the shared area between the offices. 
>Each _CPU_ core can be viewed as individual offices within the same building, in this context.
### Random Access Memory / RAM (Short term memory)
The __RAM__ is the middle ground of memory, it can keep lots of data and is faster than accessing harddrives. Think of it as a garrage that is attached to the office building, it is outside of the _cache_ but still closer by than the harddrive. 
>The _RAM_ only lives while the computer has power and is turned on.
### Harddrives (Long term memory)
This is the long term storage, it survives even without having power supplied to it, won't loose the data even though the computer looses power (unless it becomes corrupted). This could be like the warehouse store located in the next town over, you don't really go there that often. 
> Keeping the RAM stocked with whatever you might need within reason so that you don't have to go drive all the way the store for every little thing. _It takes time to go all that distance and find what you need._

### Bases
* `binary` is __base 2__ with value range of `1`- `0` (2 values)
* `hexadecimal`/`hex` is __base 16__ with values of `0` - `9` and `A` - `F`
* * Each `hex` letter represents a single `nibble` (4 bits) 

## Standarized units / Binary prefix
In 2008, IEC prefixes were incorporated into the international standard system of units. Instead of `kB`, `MB` and `GB`, we now have `KiB`, `MiB`, `GiB` and so on. 

This change in prefix is to avoid the confusion of unit value, kilobyte implies that it's value is __1000 bytes__ when it is actually __1024 bytes__. 



Dec Value|Metric    |Bin Value (base2)|JEDEC      |IEC 
--------|-----------|--------------|--------------|------------
1000    |k kilo     |1024   (2<sup>10</sup>) |KB Kilobyte   |KiB kibibyte
1000<sup>2</sup> |M mega |1024<sup>2</sup> (2<sup>20</sup>) |MB Megabyte |MiB mebibyte
1000<sup>3</sup> |G giga |1024<sup>3</sup> (2<sup>30</sup>) |GB Gigabyte |GiB gibibyte
1000<sup>4</sup> |T tera |1024<sup>4</sup> (2<sup>40</sup>) |TB Terabyte |TiB tebibyte
1000<sup>5</sup> |P peta |1024<sup>5</sup> (2<sup>50</sup>) |  |PiB pebibyte
1000<sup>6</sup> |E exa  |1024<sup>6</sup> (2<sup>60</sup>) |  |EiB exbibyte
1000<sup>7</sup> |Z zetta|1024<sup>7</sup> (2<sup>70</sup>) |  |ZiB zebibyte
1000<sup>8</sup> |Y yotta|1024<sup>8</sup> (2<sup>80</sup>) |  |YiB yobibyte

>* 1 bit  = b
>* 1 byte = B
>
>iOS and most other linux systems report sizes in metric (kB = 1000 bytes), therefor inaccurate sizes.

## Data types and sizes
Data types can vary in size, taking `1byte`, `2byte`, `4byte` or `8byte` (_you might notice that the byte size doubles with each step_). 

You are required to declare the data type when programming with `C` so it will know how many bytes to use, higher level programming languages tend to take care of the memory allocation for you, or simply default to a single type. For example `javascript` only uses `floats`.

So regardless if you actually need to micro manage memory or not with your chosen programming language, it is very good to be aware of the memory your program is using because nobody likes a program that wastes resources.

```c
//These are c declarations for data types.
Data type                         | System
Signed        | Unsigned          | 32bit   | 64bit 
--------------|-------------------|---------|--------
[signed] char | unsigned char     | 1       | 1
short         | unsigned short    | 2       | 2
int           | unsigned          | 4       | 4
long          | unsigned long     | 4       | 8
int32_t       | uint32_t          | 4       | 4
int64_t       | uint64_t          | 8       | 8
char* (all pointers)              | 4       | 8 
float                             | 4       | 4
double                            | 8       | 8
```
> Most data types are encoded as __signed__ unless you use the prefix `unsigned`. 

Most programs are `32bit` unless specified and can run on either type of system, though programs that declare themselves to be `64bit` are designed to be run on a `64bit system` and will not work on a `32bit system` because of the limitations of the system.

When it comes to typical sizes, the program will just go with what type of system you are running. To avoid relying on the typical sizes that might change between systems, you can use data types that are fixed sizes such as `int32_t` or `ìnt64_t`.


```
Symbol    |Meaning
----------|---------------------------------
B2T       | Binary to two's complement
B2U       | Binary to unsigned
U2B       | Unsigned to binary
U2T       | Unsigned to two's complement
T2B       | Two's complement to binary
T2U       | Two's complement to unsigned
TMin      | Minimum two's complement value
TMax      | Maximum two's complement value
UMax      | Maximum unsigned value
+t        | Two's complement addition
+u        | Unsigned addition
*t        | Two's complement multiplication
*u        | Unsigned multiplication
-t        | Two's complement negation
-u        | Unsigned negation
```
> __Two's complement__ is the most common encoding of `signed` integers. 


## Addressing and Byte ordering
Programs need to know the address of the many bytes it uses in memory in order to access their values. Virtually all machines store the data as a long sequence of bytes ranging from the smallest `address` of `0x100`, for example if we have a _integer_ then the 4 bytes would be stored at the addresses `0x100`, `0x101`, `0x102` and `0x103`.

The order of these addresses will always stay the same, starting at `0x100`, but the order of bytes that are stored at these addresses depend on the type of _processor_ and the arrangements are referred to as `big endian` and `little endian`. 

### Little and Big _endian_
It is the _processor_ that decides if the system is `little endian` or `big endian`, which decides how the data is actually stored in memory. The bytes still stay consistant (the pair of hex digits), it is only the order of these bytes that is effected.


`Big endian` keeps the data in the same order as we would use it, keeping the `MSB` addressed as `0x100` to the far left.  

* In `big endian`, the most sagnificant byte (`MSB`) comes first. 
>`... | 01 | 23 | 45 | 67 | ... `  _01_ is here at address of _0x100_
* In `little endian`, the least sagnificant byte (`LSB`) comes first 
> `... | 67 | 45 | 23 | 01 | ...` _67_ is here at address of _0x100_

The order of bytes in memory is for the most part invisible to the programmer because programs compiled for either class of machine give identical results. 

One case where this ordering would become an issue is when the data is transfered from _little endian_ to _big endian_ and vise versa. 
Thankfully we don't need to think much about this difference in ordering of data because it automatically gets converted over the internet since the data would tell the other system wether the data is coming from a _big endian_ or _little endian_ machine. 

>The second case is when the data is _disassembled_ from memory storage and how that data is interpreted. 
>
>_from the book:_
> >For now, wesimply note that this line states that the hexadecimal byte sequence `01 05 43 0b20 00` is the byte-level representation of an instruction that adds a word of data to the value stored at an address computed by adding `0x200b43` to the current value of the program counter, the address of the next instruction to be executed. 
> >
> >If we take the final 4 bytes of the sequence `43 0b 20 00` and write them in reverse order, we have `00 20 0b 43`. Dropping the leading `0`, we have the value `0x200b43`, the  numeric  value  written  on  the  right.  
> >
> >Having  bytes  appear  in  reverse  order is a common occurrence when reading machine-level program representations generated for little-endian machines such as this one. 
> >
> >The natural way to write a _byte sequence_ is to have the lowest-numbered byte on the left and the highest on the right, but this is contrary to the normal way of writing numbers with the __most significant__ digit on the left and the least on the right


### Pointers
Pointers are basically directions that tell the computer where in the stack of data it can find the actual value that is being requested. 

>Pointers in `C` are written as `char *pointer` ("pointer" here just being the variable name). 

Good way to describe it is to imagine the pointer as a memo-note with just your locker number written on it, this helps you locate your locker when you need to fetch contents from it. You need to be careful when trying to edit pointers, because usually you just wish to edit the value/contents but you could accidentally be changing the pointer/memo-note itself and direct it somewhere else (and now you can't find the correct locker anymore!).

## Casting between data types
Even though everything is just stored as binary and some data types might have the same number of bytes in storage, casting between types can have unexpected and undesired effects to the value stored. It is all about how the binary is interpreted.

```c
//example using 'short' data type which is is 2 bytes
unsigned short u_num = 37888  ; // binary: 1001 0100 0000 0000
short t_num = (int) u_num     ; // binary: 1001 0100 0000 0000

printf(" u_num = %d and t_num = %d ", u_num, t_num);
//will print "u_num = 37888 and t_num = -27648"
```

Casting between data types of different byte can also mutate the value. 
### Expanding (casting to larger data types)
#### Unsigned
```c
//2 bytes being cast to 4 bytes
unsigned short my_num = 37888 ;  
  //my_num binary:  1001 0100 0000 0000
unsigned int bigger_data = (unsigned int) my_num ;
  //bigger_data = 0000 0000 0000 0000 1001 0100 0000 0000
```
`bigger_data = my_num` goes from being `4bytes` to `8bytes` and will add leading _zeros_ when unsigned (also called _zero extention_)

#### Signed
It works similar to unsigned, but instead of doing a _zero extention_ when filling the bytes when casting to larger data types, it will fill with the `sign bit` to keep negative numbers negative.


### Truncating (casting to smaller data types)

Casting to smaller data types will result in it loosing it's original `sign bit` when the leading bytes are dropped to fit the new data type.
```c
//2 bytes cast to 1 byte
unsigned short u_num = 37888  ; 
  // binary: 1001 0100 0000 0000
unsigned char smaller_data = (unsigned char) my_num ; 
  //smaller_data = 0000 0000
```
`smaller_data = my_num` goes from being `4bytes` to `2bytes` and becomes `0000 0000` because all the extra leading bytes will be dropped (left side).
