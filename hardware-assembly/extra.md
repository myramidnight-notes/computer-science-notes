# Extra things

### Digital Logic
>Just interesting things I've come across.
>
>Since computers operate in binary at the core, we might wonder how they actually manage such complex operations with only two bits (or current being on or off)
>
>The magic behind that is called logic! 
>
>And we can play with how that logic works using a breadboard, wires and lights. The status of the light would indicate our output (being 1 or 0, on or off).
>
>I came across this super simple and neat simulation of such logic oprations: 
>* [Digital Logic sim](https://sebastian.itch.io/digital-logic-sim)