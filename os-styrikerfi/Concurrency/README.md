4: Concurrency

* ### [__Process interactions__](process-interactions.md)
    * Process competition
    * A software solution to the CS problem
    * Process cooperation
* ### [__Semaphores__](semaphores.md)
    > Signal other processes that they can proceed
    * Basic principles of semaphores
    * The CS problem using semaphores
    * __The bounded-buffer problem__
        * The bounded-buffer problem using semaphores
* ### [__Implementation of semaphores__](semaphores-implementation.md)
    * Hardware support for synchronization
    * Binary semaphores
    * Implementing P and V operations on general semaphores
* ### [__Monitors__](monitors.md)
    > A monitor guarantees that functions are mutually exclusive, restricting access while also maintaining queues of processes that wish to gain access.
    * Basic principles of monitors
    * A monitor implementation of the bounded-buffer problem
    * Monitors with priority waits
    * Implementation of a monitor
* ### [__Classic synchronization problems__](synchronization.md)
    * The readers-writers problem
    * A monitor solution to the readers-writers problem
    * The dining-philosophers problem
    * Approaches to preventing deadlock
    * A monitor solution to the dining philosophers problem
    * The elevator algorithm