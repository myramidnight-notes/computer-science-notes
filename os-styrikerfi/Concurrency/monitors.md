# Monitors
> `P` and `V` operations on general semaphores are versatile primitives capable of solving a variety of synchronization problems. But `P` and `V` operations are low-level primitives that are prone to difficult-to-diagnose programming errors. 
## Basic principles of monitors
* __Monitor__ is a high-level synchronization primative implemented using `P` and `V` 
    * Follows principles of abstract data-types
    * encapulates data along with functions through which data may be accessed
* __Condition variables__ are _named queues_ on which processes can wait for a condition.

### Implementation requirements
* guarantee that the functions are __mutually exclusive__, so that only a single process can be active within the monitor at any time.
* Provides condition variables with the following methods
   * `c.wait` blocks process on queue associated with condition variable `c`
   * `c.signal` reactivates a process at the head of the queue associated with `c`
       > if there was a process waiting in the signaled queue, then the process that sent the signal will be blocked in a `urgent` queue in the meanwhile.

### Example of a monitor
This example monitor contains two functions, `f()` and `g()`, and has the queues `x_is_positive` (for processes sent there by `x_is_positive.wait`) and `urgent` (for processes that sent a `signal` to another queue and are waiting to re-enter the monitor).

> ![Monitor example](./images/monitor-queue-wait.png)
> 1. `p1` waiting in a queue because the condition `x < 1` was true at the time
>1. `p2` just gave `x_is_positive` queue a signal that `p1` may be reactivated
>1. since only one process can be within the monitor, then `p2` becomes _blocked_ and moves to a __high-priority__ queue named `urgent`
>1. Now `p1` is allowed to enter the monitor to continue processing
>1. Finally `p2` would then proceed once `p1` has exited the function

### Bounded-buffer using a monitor
>* __Consumers__ will use the `remove(data)` function in the monitor
>    * If a __consumer__ arrives while `full_slots = 0`, then it needs to wait in the `notempty` queue
>* __Producers__ will use the `deposit(data)` function in the monitor
>    * If a __producer__ arrives while `full_slots = n` (all are full), then it nees to wait in the `notfull` queue
>* The ideal situation is when `full_slots` is __neither empty or full__, because then both _consumers_ and _producers_ can proceed without ending up in a waiting queue. 
>
>![Bounded-Buffer monitor](./images/monitor-bounded-buffer.png)

### Monitors with priority waits
>* Normally a queue associated with a condition variable is processed in FIFO order
>* Some applications require additional control over the order of process reactivation
>* __Priority wait__: `c.wait(p)`
>    * `c` is the conditional variable
>    * `p` is a integer specifying priority
>        > all processes blocked on `c` are sorted in ascending order by `p`.
>
>![Priority wait](./images/monitor-priority-wait.png)

## Implementation of a monitor
A monitor is usually created in a high-level programming language, and then translated to a low-level/machine language using a __compilor__.

The compilor uses `P` (decraments) and `V` (incraments) to implement a monitor
>* semaphore `mutex` 
>    > enforces mutual exclusion, guarding entrance to the monitor and it's functions
>* semaphore `c` 
>    > allows processes to block themselves on condition `c`. Each _conditional variable_ will have such a semaphore
>* semaphore `urg` 
>    > (short for `urgent`) blocks process executing signal operations
>* counter `c_cnt` 
>    > For each _conditional variable_, there will be a counter that keeps track of the number of processes blocked on that particular queue `c`.
>* counter `urg_cnt` 
>    > keeps track of the number of processes blocked on `urg` 

### Code example (`C++`)
>* Function body
>    ```c++
>    P(mutex)
>    function body               //body of the function
>    if (urg_cnt > 0) V(urg)     //Checks if urg is empty
>        else V(mutex)
>    ```
>* `c.wait`
>    ```c++
>    c_cnt = c_cnt + 1
>    if (urg_cnt > 0) V(urg)     //Checks if urg is empty
>        else V(mutex)
>    P(c)                        //wakeup a process
>    c_cnt = c_cnt - 1           //updates the counter
>    ```
>* `c.signal`
>    ```c++
>    if (c_cnt > 0)              //if no process is waiting
>        urg_cnt = urg_cnt + 1   //updates urg counter
>        V(c)                    //enables a process to enter
>        P(urg)                  //blocks itself on the urg queue
>        urg_cnt = urg_cnt - 1   //update urg counter
>    ```
>
>### Implementation example using bounded buffer
>![Bounded Buffer Monitor](./images/monitor-implementation-bounded-buffer.png)