# Process interactions
>How do multiple processes interact? 

## Process competition
Concurrent processes (or threads) may have access to shared data. Shared data needs to be protected from __simultaneus change and access__ by multiple processes, to prevent leaving variables in an inconsistent state and we __loose the integrity of the shared data__.
* ### __Critical section__: 
    >CS is a segment of code that cannot be entered by a process while another process is executing a corresponding segment of that code.

>When multiple processes are allowed to simultaneusly change data, each will have their own copy of the data, being unaware of any changes to the source data which might have been important. They would then try to place the changed data back into storage, overwriting anything that might have changed during it's processing.
>
>![Concurrent processes competing](./images/concur-competition.png)

## Software solution to CS (Critical Section) problems
Requirements for software solutions to CS problems.
1. Guarantee __mutual exclusion__
    > Only one process may be executing within the CS
1. Prevent __lockout__
    > Process not attempting to enter CS must not prevent others from entering
1. Prevent __starvation__
    > Process must not enter CS repeatedly (monopilize the resource) while other processes are waiting
1. Prevent __deadlock__
    > Multiple processes trying to enter CS must not block each other indefinetly

### Example of a solution
>![Solution example](./images/concur-sw-solution.png)
>* The `will_wait` is a shared variable between the active processes, and if each process can check if the other is active (with the `c1` and `c2` variables). If both are active and the overlapping var tells the process to wait, it will wait. This example meets all the requirements.
>
>![Example1](./images/concur-example-1.png)
>* This example can result in a __lockout__, since the process will be stuck if the other process decides to drop out of the loop.
>
>![Example1](./images/concur-example-2.png)
>* This example can result in a __deadlock__ because both `c1` and `c2` will be set to `1`, preventing both from proceeding.

### Shortcomings of these software solutions 
>* Only works for 2 processes
>* Ineficcent, because while one process is in CS, the other keeps waiting
>* they only address competition, but not cooperation.

## Process Cooperation
>In addition to the CS problem, where processes compete for access to a shared resource, many applications require the cooperation of processes to solve a common goal. 
>
>A typical scenario is when one process produces data needed by another process. The producer process must be able to inform the waiting process whenever a new data item is available. 
>
>In turn, the producer must wait for an acknowledgement from the consumer, indicating that the consumer is ready to accept the next data item. 
>
>![Cooperation](./images/concur-cooperation.png)