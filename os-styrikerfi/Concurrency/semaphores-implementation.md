# Implementation of semaphores
### Hardware support for synchronization
Most contemporary computers offer specialized machine instructions that support synchronization 
>* __Test-and-set__ (TS) instructions
>    > `TS(R, x)` where `R` is register and `x` is a memory location.
>    >* copy `x` into `R`
>    >* set `x` to `0` 
>* __Lock__
>    > synchronization barrier that only ets one process through at a time. 
>    >
>    >TS implements lock.
>
>### Example of locking with TS instructions
>![Test and set example](./images/concur-sema-test-set.png)
>* The processes keep waiting while `x` stays at zero, until `p3` comes in and increments `x`, allowing process `p1` to proceed. 
>* Whenever a process copies the value from `x`, it replaces it with zero. 

## Implementing Binary semaphores
As the name implies, a __binary semaphores__ can only take the values `0` and `1`. 

Then we do not need to _increment_ the semaphore, we just set it to `1` when a process can gain access. And a decrement would simply be the act of changing the semaphore to `0` after the process has copied the value (like taking the access token).

* Adjustments to the `V(s)` and `P(s)` for binary semaphores
    * `Vb(sb)` will set `bs=1`
    * `Pb(sb)` will do `TS(R, bs)` while `R==0`
* __Busy-waiting__ loop keeps checking if `bs` has changed
    > Repeatedly executes a wait loop consumes. It consumes CPU resources and should be avoided. 

### Synchronization problems using Binary semaphores
* Critical section problems 
    > They are easily solved with binary semaphores
* Bounded buffer problem
    > Binary semaphores are a good solution, but can only be used with a __single buffer.__

## Implementing General semaphores
We can implement a __general semaphore__ using the __binary semaphores__ in the `V(s)` and `P(s)` operators. 
* We give `s` a dual purpose, by allowing it to go below zero
    * When `s >= 0`, then the value of `s` indcates the state of the semaphore, just as before.
    * When `s < 0`, then the negative number indicates how many processes are waiting.
* We will be calling a __binary semaphore__ from within `V(s)` and `P(s)` to manage access to the __Critical section__, since the _binary semaphore_ only allows a single process to enter at a time.
* To avoid __busy-waiting__ within `P(s)` when `s<0`, the process will block itself and place the process on a waiting list associated with `s`.
    * Once a `V(s)` operation increments the `s`, it reactivates a process from that waiting list if any were blocked.

