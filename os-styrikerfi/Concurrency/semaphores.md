# Semaphores
Traditionally __semaphore__ is when you are creating long-distance visual signals using things such as fire, lights, flags, or moving your arms. It basically means "__apparatus for signaling__" (from french literally meaning "_a bearer of signals_").

> A __semaphore__ (in programming) is a variable or abstract data type used to control access to a common resource by multiple processes to avoid critical section problems in a concurrent system such as a multitasking OS. A trivial semaphore is a plain variable that is changed (for example, incremented or decremented, or toggled) depending on programmer-defined conditions.
>* [wikipedia](https://en.wikipedia.org/wiki/Semaphore_(disambiguation))
## Basic principles of semaphores
The shortcomings of _software solutions for CS problems_, which are their limitation to 2 processes, inefficency, and inablity to solve cooperative processes in CS.

The semaphores solve these shortcomings with general-purpose primatives.
* Semaphore `s` will be a _none-negative_ integer variable that is only accessable by two special operators:
    * `V(s)` 
       >increments `s` by 1
    * `P(s)`
       > if `s > 0`, decrement `s` by 1, otherwise wait until `s > 0` (meaning any process calling `P(s)` will be blocked when `s=0` until `s` is incremented)

* __The implementation must guarantee__
    * If multiple processes try to access `P(s)` or `V(s)` simultaneously, then they are sequentially processed in an arbitrary (random) order
    * if multiple processes are blocked on `P(s)`, one resumes arbitrarily when `s > 0`


## The critical section problem using semaphores
The initial state of the semaphore decides how many processes can enter the critical section simultaniously. Usually it's just 1 at a time. 

![Critical Section problem with Semaphores](./images/concur-sema-cs-problem.png)
> In this example, the `s` is called `mutex`, which technically holds the access tokens to the critical section. Process `pi` just called `V(mutex)`, so the `mutex` is now 1. `p1` and `pn` are blocked and recieve the signal that the critical section is now accessable.
1. Each process that is calling `P(s)` is asking for access to the __critical section__. 
1. Only as many processes can enter the _critical section_ simultaniously as the initial `s` value. 
    * Rest of the processes that called `P(s)` will be blocked and are randomly (depends on the implementation) picked to proceed whenever the `s` increases.
1. Once a process finishes with the _critical section_, it will call `V(s)` to increment the variable, signaling any blocked processes that another process can now proceed to the _critical section_.

## The bounded-buffer problem
This is a classic synchronization problem that illustrates process cooperation.
* __Producer__ process shares a __buffer__ with a __consumer__ process
    * __buffer__ has a fixed number of slots
    * __producer__ fills empty slots with data in increasing order
    * consumer follows the producer by removing the data in the same order
* Buffer is called __circular__ because processes continue with the first slot (_modulo n_), reusing the same space.

The solution must guarantee the following
* __consumers do not__ overtake producers and __access empty slots__
* __producers do not__ overtake consumers and __overwrite full slots__

### Operation of the bounded buffer
>![Bounded Buffer](./images/concur-bounded-buffer.png)
>* There will be two pointers to the buffer queue: `next_out` for the consumer and `next_in` for the producer. 
>    * These pointers chase each other around the buffer, and if the `next_out` catches up to the `next_in`, then the __consumer__ gets blocked in order to prevent reading empty buffers. 
>* The producer can proceed to fill more buffer slots even though the consumer hasn't caught up, which means the consumer will not be idling blocked in the meanwhile. 
>    * If the producer catches up tot he consumer, then it cannot fill more buffers until the consumer has emptied them.
>
> When the pointers _catch up_ with each other, then they are pointing to the same buffer slot. It depends on the buffer status which pointer caught up with who. If the buffer slot is full, then the producer will be blocked. If the buffer slot is empty, then it is the consumer that gets blocked.

### Bounded buffer with with semaphores
>![Bounded Buffer with Semaphore](./images/concur-bounded-buffer-sema.png)
> *  We will have two semaphores, one for the empty slots (`e`) in the buffer, and another for the full slots in the buffer (`f`).

