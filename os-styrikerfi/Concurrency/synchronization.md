# Classic synchronization problems
* The readers-writers problem
* The dining-philosophers problem
    * Approaches to prevent deadlock
* The elevator algorithm
## The readers-writers problem
Extension of _Critical Section_ problem: two types of processes compete for a resource
* __readers__ are allowed to enter concurrently
    > Readers are not changing the data, so there is little point in limiting the number of reading-processes from entering the _critical section_.
* only __one writer__ is allowed to enter at a time
    > To avoid overwriting data accidentally, only a single writer can enter at a time. 

The main challange is to guarantee maximum __concurrency__ of readers and __prevent starvation__ of both types of processes.

>### Specific rules:
>* reader permitted to join others only when there is __no writer waiting__ (no starving writers).
>    > Once a _writer_ arrives in at the CS, no new readers may enter the CS until the writers have finished.
>* The last reader exiting enables writer (no starving writers)
>    > All readers that were already in the CS when the writer arrives are allowed to finish what they were doing. The __last reader__ will then signal the writer to enter
>* when writer exits, __all__ readers that have arrived may enter (no starving readers).
>    > Once the writer is finished with the CS, then __all readers__ that have arrived are allowed to enter together. __This includes the readers that might be in between other writers__ that arrived while the reader was in the CS, this prevents _reader starvation_ and letting the readers skip ahead of the writers like that does not cause _writer starvation_ because the readers can be active concurrently.   
>
>![Reader Writer example](./images/sync-reader-writer.png)
>
> Once `w1` finishes, then `r5`, `r6`, `r7` and `r8` are allowed to enter the CS together, but any readers that arrive afterwards will have to wait until `w2` has finished. This alternation between readers and writers will prevent starvation for either type of process.

### A monitor solution for Readers-Writers problem
>* Monitor has 4 __functions__
>    * `start_read` is called by reader to get permission to read
>    * `end_read` is called by reader when finished reader
>    * `start_write` is called by writer to get permission to write
>    * `end_write` is called by a writer when finished writing
>* __Counters__ for Critical Section: 
>    * `reading` counts number of readers inside CS
>    * `writing` is a binary counter, either there is a writer in CS or not
>* __Condition variables__: `ok_to_read`, `ok_to_write` to block the processes
>* To avoid separate counters of blocked processes:
>    * The `count(c)` method returns number of processes blocked on `c`
>
>![Semaphore solution for readers-writers](./images/sync-reader-writer-sema.png)
>* In this example, `r3`,`r4` and `w2` arrived after `w1`. When `w1` finishes, then `r3` and `r4` can enter the CS, and finally `w2` will go into the CS after the last reader leaves the CS.

## The dining-philosophers problem
This problem models situations where multiple processes compete for shared resources but __each process needs more than one resource at a time__
* __5 philosophers__ (concurring processes) are sitting at a __circular table__ and they have __5 forks__ (resources)
* Every two neighboring philosophers share one fork (`mod n`)
* Each philosopher alternates asynchronously between two phases: 
    * `thinking`, which requires no resources
    * `eating`, which requires both adjacent forks to be in their hands
* Challenge:
    * prevent __deadlock__
        > A deadlock can occur whenever there are two or more philosophers dining. 
    * guarantee __concurrency__: any two nonadjacent philosophers can always eat

>![Dining philosophers](./images/sync-philosophers.png)
>1. `p2` has grabbed both forks and is currently eating.
>1. `p1` and `p3` are trying to get the forks they share with `p2` and are therefor unable to eat.
>1. `p0` and `p4` have the option to eat, even if only one of them will be able to eat. It depensd on who is first to grab their shared fork (`f0`).

### Approaches to prevent deadlock
> There are many solutions for preventing deadlock, but most of those fail the concurrency requirement