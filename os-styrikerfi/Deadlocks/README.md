[...back](../README.md)
# Deadlock
* [__A system model for deadlocks__](deadlock-model.md)
    * Resource allocation graphs
    * Deadlock with reusable resources
    * State transitions
    * Deadlock states and safe states
* [__Deadlock detection__](deadlock-detection.md)
    * Graph reduction
    * Continuous deadlock detection
    * Deadlock detection with single-unit resources
* [__Dynamic deadlock avoidance__](deadlock-avoidance.md)
    * Resource claim graphs
    * The banker's algorithm
    * Deadlock avoidance with single-unit resources
* [__Static deadlock prevention__](deadlock-static.md)
    * Conditions for a deadlock
    * Eliminating hold-and-wait
    * Eliminating circular wait