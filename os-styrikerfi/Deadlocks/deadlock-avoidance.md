# Dynamic deadlock avoidance

### Maximum claim
>A __Maximum claim__ is a set of all resources the process may ever request
>* each potential future claim: __dashed edge__
>* __specified ahead of time and unchanging__, because the process already knows what resources it will require through it's lifetime.

## Resource claim graph
If we include the __maximum claim__ then it will result in a __resource claim graph__, which is an extension of a general resource allocation graph.
* __Resource claim graph__ contains
    * all current allocations of resources to process
    * all current requests (solid edges)
    * all potential future requests (dashed edges)

![Resource claim graph](./images/deadlock-resource-claim-graph.png)

## The banker's algorithm
If we know the __maximum claims__ in a system, then we can use the _banker's algorithm_ that will guarantee that no deadlock will ever occur.

This algorithm emulates a banking strategy, where it grants loans only if all loans can be satisfied in some order.

* __The banker's algorithm__
    * given a request in state `s`, __temporarily__ grant the request 
    * perform graph reduction on the new temporary state `s'`
        * treat all dached edges as solid edges of for this evaluation.
    * if `s'` is completely reducible then accept `s'`, else revert back to previous state `s`.

> __Theorem__: If we do not execute any _acquisition_ operations that do not result in a completeley reducible claim graph (postponing them), then __any system state is safe__.
>
>![Bankers algorithm](./images/deadlock-bankers-alg.png)
>* We can remove `p1` in this reduction example, which will unblock `p3`. This graph can be completely reduced, and therefor the new state can be accepted. 

## Avoidance with single-unit resources
* __Single-unit resources__
    * process `p` can be blocked only if it is waiting for another blocked process `p'`
        * similarily `p'` must be waiting for another blocked process `p''`, etc.
    * to make the chain irreducible, the processes must be blocked in a __directed cycle__
* __Simpified banker's algorithm__:
    * given a request in state `s`, temporarily grand request
    * starting with requesting process `p`, determine if new state `s'` contains a directed cycle
    * if no cycles exist, then accept `s'`, else revert to previous state `s`.
* __Checking for safe states__ is also simplified:
    * no edges are ever added to or removed from claim graph, they only change directions, or changed between solid and dashed.
    * thus __directed cycle__ (a deadlock) can only be formed if graph __already contains a undirected cycle__
    * if initial claim graph contains no undirected cycles, then __all__ states are safe.