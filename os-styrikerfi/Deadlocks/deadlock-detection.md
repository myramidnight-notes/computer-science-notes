# Deadlock detection
## Graph reduction
* __Graph reduction algorithm__:
    > Repeat until no unblocked process remains in the graph:
    >1. select an __unblocked__ process `p` (in any order). 
    >       * This would unblock other processes if possible, allowing us to select them.
    >1. remove `p`, including all requests and allocation edges connected to `p`

* __Completely reducable__ graph
    * Is a allocation graph where __all the processes have been deleted__ after running the graph reduction algorithm.

### Theories about deadlock states
1. `s` is a deadlock state only if the resource graph of `s` is __not__ completely reducable.
1. All reduction sequences lead to the __same__ final graph
    * __Any sequence__ of reductions finds a deadlock state (or proves there is no deadlock state). 

## Continuous deadlock detection
* If a current state `s` is not deadlocked, then the next state `s'` can only be deadlocked if :
    * the current operation is a request
    * and process `p` that issued the request is deadlocked in the new state `s'`.

### __Continuous deadlock detection__ (with graph reduction):
>* if current operation is a request and requesting process `p` is blocked in `s'` then __execute graph reduction algorithm__ until either:
>    * until `p` (the requesting process) is removed from graph
>    * or until __no unblocked process remains__ in the graph
>* if `p` is removed, then `s'` is __not__ a deadlock state. Then it is a valid state.

## Deadlock detection with single-unit resources
A __single-unit resource__ can only be allocated with a single outgoing edge.

If a system contains only __single-unit resources__, then the graph can be simplified by collapsing the resources into the processes that is __holding the resource__ (is allocated to):
>* Resource `r` along with the connecting edge can be collapsed into `p`
>    * then all incoming requests for `r` will point to `p`.

### Wait-for graph
* Resource allocation graph containing only processes
* each process can have __multiple incoming__ edges, indicating other processes waiting for them to release a resource.
* but only a __single outgoing__ request edge, because a process can only request one resource at a time.
* This will leave us only with edges that show waiting processes, pointing to the process that is blocking them (waiting for that process to release the resource)

> __Theorem__: A cicle in the wait-for graph is necessary and sufficient condition for deadlock. This makes it easier to detect deadlocks by finding the cicles in the graph.
>
>![Deadlock wait-for graph](./images/deadlock-wait-for.png)