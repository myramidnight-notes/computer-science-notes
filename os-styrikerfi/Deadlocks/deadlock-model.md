# A system model for deadlocks
Graphs are always a nice way to visualize the state of a system.

## Resource allocation graphs
How to reperesent the processes and resources with a graph.
* Each resource can hold multiple units, so technically multiple processes can be allocated at the same time.
* Edges/Arrows pointing to a process, indicates that the _resource_ has been allocated to that process. 
* Edges/Arrows pointing to a resource, indicate requests for that resource.

## Deadlocks with reusable resources
* __Reusable resource__
    * contains a __fixed__ number (one or more) of identical units
    * each unit may be requested and used by a process on a __non-shared__ basis
    * example: printers, memmory blocks, files...
* __Consumable resource__
    * They contain a variable number of units
    * Example: message
* __Deadlock__ involves _at least_ 2 processes and 2 resources, where each process holds one resource and is blocked indefinitely by another.
    * The order of request determines if a deadlock can occur or be prevented
    * The example below would show resources that hold a single unit each. 
    > ![Deadlock order](./images/deadlock-order.png)

## State transitions
* Resource allocatoin graph shows the __current system state__. 
### Operations to change state:
>* __Resource request__ `(req r, m)` creates `m` new edges from `p` to `r`. Shows how many units of that resource the process is requesting.
>    * `p` has no outstanding requests (not blocked)
>    * Number of request edges + number of allocation edges does not exceed total number of units (within the resources) 
>* __Resource acquisition__ `(aqr r, m)` Reverses direction of `m` request edges. Shows how many units the process managed to allocate.
>    * `r` must contain at least `m` free units (no partial allocation)
>* __Resource release__ `(rel r, m)` deletes `m` allocation edges between `p` and `r`. Releasing the resource/units.
>    * `p` has no outstanding requests (not blocked)
>    * Number of `m` units to be released must not exceed number of units `p` is holding
>
>![Resource allocation graph](./images/deadlock-resource-allocation.png)

## Deadlock states and safe states
* `p` is __deadlocked__ in state `s`:
    * `p` is blocked in `s` and remains blocked regardless of what transitions occur in the future.
* __deadlock state__: contains _at least_ 2 deadlocked processes
* __safe state__: no sequence of transitions leads to a deadlock state
>If all sequences of requests/releases by all the processes are known, then a complete __state transition graph__ can be constructed.  It can then be __analyzed__ for the presence of deadlock states and safe states 

### Deadlock state in a state transition graph
>![Deadlock states](./images/deadlock-transition-states-graph.png)
>* This graph shows all the possible states of those two processes and resources, but `s33` indicates a deadlocked state, where there are no edges leading out from that state (forever deadlocked).
>* All the __edges represent actions__ by the processes.
>    * The direction of the arrows indicate transition between states by actions
>       * In this graph, horizontal edges are actions by `p1` and vertical edges are `p2`
>    * No outgoing edges in a direction indicate that a __process is blocked__, based on which direction the outgoing edge would be. 
>       * no outgoing horizontal edges mean `p1` is blocked. 
>    * If there is no outgoing edge in either direction, then we have a __deadlock state__.