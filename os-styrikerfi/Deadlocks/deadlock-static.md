# Static deadlock prevention
### Conditions for a deadlock
Two conditions must hold true for a deadlock to occur in system with reusable resources, and the system has to have at least 2 processes and 2 resources.

It is structurally __impossible__ for a deadlock to occur if either condition is eliminated
1. __Hold and wait__
    > Two processes must each be holding one resource and requesting another
    >* If a process can only either hold or request a resource, then deadlock is prevented.
1. __Circular wait__
    > a process must issue a resource request that closes a directed cycle.
    >* if we can prevent a cycle from happening, deadlock is prevented.

## Eliminating hold-and-wait
Three possible solutions to eliminating hold-and-wait 
1. Process must request all resources at the __same time__ (gaining all or none of them at once)
    * __simple solution but poor resource utilization__, the resources are being held unneccessarily because the process might not be able to use them all at once.
1. Process must __release__ all resources __prior to__ making new requests
    * Then the process can either hold or request resources, not both.
    * better resource utilization but requests/releases are unnecessarily repeated
1. Process must __release__ all resources only if requested resources is __not available__
    * Then the process will have to drop all resources if it gets blocked, and mak enew requests for the resources.
    * best resource utilization, but process must have ability to test resource (to be able to check if the resource is available)

![Eliminating wait-and-hold](./images/deadlock-wait-hold-solutions.png)

## Eliminating circular wait
A cycle in a resource graph can only be closed when 2 or more processes request the same resource but in different order. A circular wait is prevented if all processes are required to request all resources in the same order.

One approach is to assign a sequential ordering, `seq`, to all existing resources, such that `seq(ri) != seq(rj)` for all `i != j`. All processes are then required to request resources in only increasing sequential order.

> A process `p` already holding a resource `ri` with the sequence number `seq(ri)` may only request resources `rj` where `seq(ri) < seq(rj)`. If all processes follow this rule, then circular wait is prevented. 
>
>That is, __a process cannot request a resource with a lower sequence number than any of the resources it's currently holding__. It would have to request the resource with the lowest sequence number first, in order to be allowed to hold it.

### Ordered resources policy
1. All the processes are given a order, and resources can only request resources that are higher in the order than what they have already aquired. 
1. This will prevent resources and processes from forming a cycle, thus eliminating the possibililty to form a deadlock
1. The order can be organized by the order in which the processes do their requests, and then assign the resources a number that allows all the resources to request the resources in the correct order.
    * if we cannot create such an order, then we cannot prevent deadlock.

![Ordered resources policy](./images/deadlock-ordered-policy.png)