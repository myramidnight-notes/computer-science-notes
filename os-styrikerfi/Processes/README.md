
## 2: Processes
* ### [The concept of Processes](os-process-state.md)
    * Creation/Desctrucion of processes
    * Process state and transitions
    * Context switch
* ### [Why use processes?](os-virtual-cpu.md)
    * Virtual CPU
    * Modular structuring
* ### [Process Control Block (PCB)](os-process-control-block.md)
    > Keeping track of the process, resources and children
    * Contents of the PCB
    * PCB structure
    * Managing processes
* ### [Resources](os-resources.md)
    * Hardware resources
    * Software resources
    * Resource control block (RCB)
* ### [Threads](os-threads.md)
    > Instead of multiple identical PCB, we can have multiple threads that share a single PCB
    * Kernel-level threads
    * User-level threads
