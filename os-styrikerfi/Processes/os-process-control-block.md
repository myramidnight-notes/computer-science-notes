# The Process Control Block (PCB)
The PCB is an instance of of a process. The OS assigns every process a unique identifier (a pointer to the PCB or index to the PCB array).

## Contents of the PCB
The contents and implementation of the PCB varies between operating systems, but they are generally built the same.

|PCB field | Explanation |
|---|---|
| CPU state | Copy of the CPU state at the time the process was stopped 
| Process state | Running, ready, blocked...
| Memory | Pointer to memory area or to a hierarchy of memory pages assigned to the process
| Scheduling | Info used by scheduler: process's CPU time, system real time, priority...
| Accounting | Info for accounting and billing purposes: amount of CPU time or memory used
| Open files | Files currently opened by the process
| Other resources | Any resources that the process has requested and successfully acquired
| Parent | The process that created this process
| Children | List of processess created by this process

## Organizing the PCBs
Two ways of implementation, the tradeoffs being flexibility vs. overhead
![PCB in memory](./images/process-memory.png)
* Array of structures 
    > Each item in the array would be a PCB structure, marked as empty (green) or in use (orange). This would be the quickest way to implement, but it also takes the most space and more limited number of available PCB.  
    >
    > This method makes it very easy to add and remove processes, but also wastes alot of space in memory for each unused process slot.
* Array of pointers
    > Each item in the array would be a pointer to the actual PCB structure. It would be more work to update the PCBs in this array, because they would have to follow the pointer to clear the space.
    >
    > This method would take less space in memory, but large overhead for the complexity to update the array.

## PCB data structure
The PCB will initially be a empty structure, some of the fields might be set right away, such as the `CPU_state`, `information` and `parent`.
![implementation](./images/process-implementation.png)

### Avoiding linked lists
To simplify the PCB data structure, we should avoid using linked lists to keep track of things like children processes. We can replace it by using 4 pointers instead of 2: `parent`, `first child`, `older sibling` and `younger sibling`, basically taking on the functionality of a linked list as part of the structure.

![linked lists in PCB](./images/process-linked-lists.png)

This makes the data structure simpler and faster, not having to go through linked lists to connect the children processes.

## Managing processes
The operating system mantains the PCBs on various lists to keep track on what process should be running.

* __Waiting list__ for each resource
    > It keeps track of all the processes that have requested the associated resource and are blocked. This would be a FIFO priority.
    > 
    >When the resource becomes available, then the next process on the waiting list will be given access to the resource and moved to the __ready list__.
* __Ready list__ 
    > A list of all processes that are ready to be run, waiting their turn for access to the CPU. This list would be a priority list, where the __running__ process would always be set on the highest non-empty priority. 
    * The _ready list_ could be a simple First-in-first-out queue.
    * It could also be a hierarchy of lists ordered by priority
        * It would always be running a process of the highest priority. If a new process is added to the list on a higher priority than the currently running one, then the current one will be interupted and the higher priority process will start running.