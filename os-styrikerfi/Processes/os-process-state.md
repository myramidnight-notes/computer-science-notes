# The process concept
A __process__ is represented by __process control blocks__ (PCB) that keep track of the current state of a running program, such as the current instruction being run. When a program is interupted, the CPU will update the PCB with the current state of the program before loading a state from another PCB when switching to another program.

![Process representation](./images/process-representation.png)

## Process state and transitions
### Basic process states
* __running__
    > CPU is running the program
* __ready__
    > process has access to the resources it needs, but is waiting for the CPU to run the program. 
* __blocked__
    > process is waiting for a resource to become available before continuing.

![Basic states](./images/process-basic-states.png)

### Additional process states
* __new__
    > To keep track of newly created processes. This allows the OS to regulate the number of processes competing for the CPU.
* __terminated__
    > a process that has been ended, but the PCB has not been deleted yet. This allows the OS to examine the final state of the process when it was terminated after a fatal error.
* __suspended__
    > a process that has been paused even if the CPU and resources are available. This could be in the case of debugging or to regulate performance.

![additional states map](./images/process-additional-states.png)

## Context switch
The __context switch__ is the transfer of control from one process to another, interuptions that switch between active processes.

When the switch gives a process access to the CPU, the CPU will update the currently running PCB with the state (current instructions and such). Then it will replace the CPU state with whatever was saved in the next process PCB. This will keep track of how far along each process was so we can do multi-programming or time-sharing.

### Who can trigger a context switch?
* __the process itself__
    > if the process requests a currently unavailable resource, it will become blocked and switch to the next process.
* __the OS__
    > since the OS controls access to the CPU, it might interupt the process at any point to switch to another process. Such as in the case of time-sharing.

### [Process Control Block (PCB)](os-process-control-block.md)
> the PCB is only up to date when the process state is blocked or ready. Only after the process is done running or is interupted will the PCB be updated with current state of the process. 
>
>Could imagine the PCB as a _bookmark_ and the process is the book. You would take the bookmark out while reading the book and only replace it back into the current location when you wish to stop reading. This only makes the PCB relevant while you aren't actively reading.

## Creation and Destruction of Processes
There would be a hierarchy of processes, the root of the system would run the `OS` and have a initial process that is parent of all users in the system. And each user in the system would be a parent process to all the programs that that user might run. 

![Process hierarchy](./images/process-hierarchy.png)
### Create a process
> We would have a function that creates a new PCB with initial states for certain arguments. Some values will be _null_ by default (a new process would have no children or files that it uses).
> ```
>create(state, mem, sched, acc){}
>```

### Destroying a process
>1. To prevent any lingering child processes getting lost, the destruction of the parent should destroy all children recursively, and then itself.
>1. Remove any pointers to the destroyed processes from any lists (waiting or ready list). 
>1. Release all resources, memory and files that it might have been using. When everything is in order, then the PCB can be destroyed.
>1. Finally the scheduler is called to select the next process to run.