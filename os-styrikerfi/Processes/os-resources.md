# Resources
* Hardware resources
    > Keyboards, terminals, printers, disks, devices...
* Software resources
    > anything that can be requested and released later. I/O buffers, locks on database tables, messages, timers...
* Resource control block (RCB)
    > data structure that represents a resource
    >
    >* Fields: description, state (availability), waiting list

## Requesting a resource
A process can request a resource by making a _system call_ to the kernel. 
* If the resource is available, then it is allocated to the process that requested it. 
* If the resource is unavailable, then the process becomes blocked and is added to the resource waiting list. 

## Releasing a resource
When a resource is no longer being used by a process, it will be released.
* If there is nothing on the resource waiting list, then mark it as available/free.
* If there is a process on the waiting list
    1. then the next process is removed from the waiting list 
    1. That process will get the resource added to it's PCB 
    1. The process is marked as _ready_ (to get access to the CPU)
    1. The _scheduler_ is called to select the next process to run