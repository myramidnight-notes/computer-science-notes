# The thread concept
A process follows a single path of execution through a program. Many applications have parts that could run concurrently, but creating a spearate process for each short-lived activity is too inefficent. Threads solve this. 

A thread is a instance of execution within a process without a separate PCB.

>### Typical uses of threads
>* browser: one thread can be drawing the image while another waits for I/O
>* word processor: spell checkr runs at the same time as the user is typing
>* email or page server: each request is a new thread to achieve concurrency

## Multi-Threaded process
> A single-threaded process has a single _program counter_ running over the coad. While a multi-threaded process will have a separate _program counter_ for each thread, each taking turns moving over the same code and moving indipendantly
>
> When multiple CPUs are available, then these threads can proceed in parallel, not having to take turns at moving.

## The thread control block (TCB)
Each thread needs to keep track of the runtime information and execution stack, which is unique for each thread. The _thread control block_ holds this dynamically changing information.

* All the threads of a single process are all sharing the same code, global data, resources and open files. 
* Each individual thread needs to track the CPU state (registers, flags), thread state (running, ready, blocked) and the call stack (the functions being run)

Multiple threads are much more efficient than managing multiple processes because of their smaller size.

>![Structure of threads in PCB](./images/threads-structure.png)
>
> Threads will replace the CPU state and process state of a single PCB, and they will still take alot less space than as many separate PCB

## Thread levels

### User-level threads (ULT)
>Threads can be implemented within a _user application_ with the use of a _thread library_ (it provides functions to create, destroy, schedule...). 
>
>__Advantages of ULT__ 
>* It is faster to manage, more ULT can be created than KLTs. 
>* Applications using ULTs are portable between different operating systems (not hardcoded for a specific OS). 
>
>__Disadvantages of ULT__ 
>* is that the _OS kernel_ is not aware of them and treats it as a single-threaded process. No parallelism, if any of the threads are blocked, then the whole process is blocked. 
>* _They can also not take advantage of multiple CPUs_

### Kernel-level threads (KLT)
>Threads can also be implemented within the OS kernel. In those cases the threads are not directly accessable by the application, requiring use of kernel calls to interact with the threads, which has is expensive. So the disadvantage of this implementation is the overhead.

>![Thread levels](./images/threads-level.png)
>* For user-level, the thread library manages the threads 
>* For kernel-level threads, the program code needs to invoke the kernel calls to run the _thread functions_.

### Combining ULT and KLT
> The kernel supports a small number of KLTs, and then the application can implement many ULTs, which are mapped on KLTs as needed at runtime.
> 
> ![Combined kernels](./images/threads-combined.png)
> Threads that require little CPU time might be grouped together to be mapped on a single KLT. It would keep running as long as any of the three ULT are running.