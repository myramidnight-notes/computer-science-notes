# Why use processes?
Having multiple concurrent processes provide indipendence from the hardware. The programs being run will not have to pay attention to the number of CPUs available because the processes will keep track of things. 

## Multi-user support
>Each user could be represented as a single or more processes, allowing multiple users to use a single computer without being aware of each other. This could be swapping between users on the same computer or multiple users accessing the same computer at the same time. 
>
>The computer might seem to run slower with increasing number of users, but it would still be able to function as if it were multiple computers.

## Virtual CPUs


>>Structuring an application as processes allows independence from the:
>>
>>*    Number of CPUs: A __physical CPU__ is a real hardware instance of a CPU. Multiple processes may run on one physical CPU using a technique known as __time sharing__. Each process is given a __virtual CPU:__ A CPU that the process assumes is available only to itself.
>>*    Type of CPU: A virtual CPU can be just an abstraction of the physical CPU or it can be software that emulates the behavior of a different CPU.

![Virtua CPU](./images/process-virtual-cpu.PNG)
* Each process can have a separate physical CPU (image _a_)
* All the processes can __time-share__ the same CPU, which repeatedly switches among the processes. (image _b_)
    * The _time-sharing_ gives the illusion of each process having their separate CPU, which is actually a __virtual CPU__.
* _Virtual CPUs_ can emulate the behaviour of different types of CPU than we might have physically. This allows us run programs that might have been created on a different type of CPU than we have, without changing or recompiling the code. (image _c_)

### Dedicated CPUs
>If the number of processes are equal or less than the number of CPUs available, then each could get a dedicated physical CPU to run their programs in true paralel. 
>
>But if there are fewer CPUs than active processes, then virtual CPUs will allow each process to behave as if they have dedicated physical CPUs, while in reality the OS is managing the access of the many processes to the available CPUs.
### Portability: Emulate CPU types
>Another benefit of virtual CPUs is the ability to bypass having to restructure a program to fit the physical CPU. The virtual CPU could emulate the type of CPU that the program would have been programmed for. 
>
>This makes the program portable, able to function on hardware other than it was originally designed on.

## Modular structuring
> A large program could be structured to make use of multiple cooperating processes, where each process would have a limited scope of functionality, to decrease the idle time in the same manner as multi-programming, just within the same program. Different processes could have utilize different CPUs, if available.
>
> It is comparable to the construction of a house, where a single process could be a worker arranging the bricks from a pallet onto the house. It would be delayed once the pallet is empty, because the worker would need to go to the factory to request more bricks, and perhaps they would have to wait for the bricks to be created first, causing even more delay. 
>
>Having a extra worker keeping track of the factory would speed things up, so there would always be a pallet ready once the previous one is empty, so the first worker only has to focus on building the house, while the second worker only focuses on supplying the full pallets. Idle time is minimized and the house is constructed faster.