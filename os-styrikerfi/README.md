[...back](../README.md)
>>#### Original texts
>> Double quotes like this one within these particular notes indicate texts unchanged from the source. I had planned to adjust things a bit and reword them as I understood them, but sometimes the originals are plenty good or I didn't have time to go further with it.
# Operating Systems (Stýrikerfi)
> _Lubomir Bic_ was the teacher of this course (even if everything were prerecorded lectures, and he's in a different country) with his interactive zyBook. The animations in the book are very useful.
>
>But I'm still not happy that we were forced to pay subscription for it, when the book was kind of useless when you had this lecture slides (where he went over every single animation as well), the _participation_ section of that interactive book was required to get graded.
>
>Besides for that, it seemed like a good introduction to Operating systems.

### [1: Operating Systems](os-basics/README.md)
* __Role of operating systems__
    * Bridge between hardware capabilities and user needs
    * Extended Machine
    * __Resource Manager__
* __The OS structure__
    * __Hierarchial organization/Structure__
    * __The Shell__
* Evolution and scope of Operating Systems

### [2: Processes](processes/README.md)
* __The concept of processes__
* __Why processes?__
* __Process Control Block (PCB)__
* __Resources__
* __Threads__

### [3: Scheduling](Scheduling/README.md)
> Scheduling the processes 
* __The Principals of Scheduling__
* __Scheduling Algorithms__
    * Batch scheduling
    * Interactive scheduling
    * Real-time scheduling
    * Combined approaches

### [4: Concurrency](Concurrency/README.md)
> Working concurrently with shared resources
* __Process interactions__
* __Semaphores and their implementation__
* __Monitors__
* __Classic synchronization problems__

### [5: Deadlocks](Deadlocks/README.md)
> How to detect and avoid deadlocks (the battle for resources)
* __A system model for deadlocks__
* __Deadlock detection__
* __Dynamic deadlock avoidance__
* __Static deadlock prevention__

### [6: Memory Managment](memory-managment/README.md)
> Without memory, there are no thoughts...
* __Requirements for efficient memory management__
* __Managing insufficient memory__
* __Paging__
* __Segmentation and paging__

### [7: Virtual Memory](virtual-memory/README.md)
* __Demand paging__
* __Page replacement with fixed number of frames__
* __Page replacement with variable number of frames__
* __Time and space efficiency of virtual memory__

### [8: File System](file-system/README.md)
* __Files__
* __File directories__
* __Operations on files__
* __Disk block allocation__

### [9: Input/Output](input-output/README.md)
> The disk hardware and interactions
* __The hardware-software interface__
* __Data buffering and caching__
* __Disk scheduling__
* __Error handling__

### [10: Protection and Security](security/README.md)
> Security is always important
* __Security goals and threats__
* __User authentication__
* __Access control__
* __Secure communication__

## Just some extra notes:
Should take look at the notes about binary, located in the `hardware-assembly` directory under computer science notes.
* `cs-data`, `cs-binary-hex`, `cs-bit-manipulation`
### Sizes:
| SI | IEC | bit sizes
| --- | --- | ---
| kbit | Kibit | 2<sup>10</sup>
| Mbit | Mibit | 2<sup>20</sup>
| Gbit | Gibit | 2<sup>30</sup> 

... and so on. Tibit, Pibit, Eibit, Zibit, Yibit