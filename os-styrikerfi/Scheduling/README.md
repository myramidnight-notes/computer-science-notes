
# 3: Scheduling

## [The Scheduler](os-schedule.md)
> The scheduler handles finding the highest priority processes to run on the CPU
>### [The Principals of Scheduling](os-schedule.md)
>* Short-term scheduling
>* Long-term scheduling
>* Scheduling Priority

## Scheduling Algorithms
There are many ways to implement a scheduler.

### [Scheduling of Batch Processes](os-schedule-batch.md)
>A batch process performs a long-running and generally repetitive task that does not require any intervention from the user. Ex: Payroll, insurance claims processing, weather prediction, scientific calculations.
>* __FIFO (First-in-first-out)__
>* __SJF (Shortest Job First)__
>* __SRT (Shortest Remaining Time)__
>* __Estimating total CPU time__

### [Scheduling of interactive processes](os-schedule-interactive.md)
>An interactive process communicates with the user in the form of a dialog by receiving commands or data from the keyboard or a pointing device and responding by generating output on the user's terminal or another output device. 
>* __RR (Round-robin)__
>* __ML (Multi-level)__
>* __MLF (Multi-level feedback)__

### [Scheduling of real-time processes](os-schedule-realtime.md)
>A real-time process is characterized by continual input, which must be processed fast enough to generate nearly instantaneous output. Each arriving input item is subject to a deadline. 
>* __RM (Rate monotonic)__
>* __EDF (Earliest Deadline First)__

### [Combined approaches](os-schedule-combined.md)
* __2-tier approach__
    > Creates two tiers, where __real-time processes__  are in the 1st tier, and __batch/interactive processes__ are combined together in the 2nd tier.
* __2-tier with floating priorities__ 
    > Allows interactive processes to rise in priority based on their type
