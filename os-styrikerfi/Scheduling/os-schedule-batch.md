
# Scheduling of batch processes
Bach process is a long-running repetative task that requires no intervention from the user. Examples include: payroll, insurance claims, weather predictions, scientific calculations...

>In the early days of computing, batch processing referred to submitting multiple computations, known as jobs, to be executed as a "batch" without manual intervention. In modern computing, any long-running computation that does not require interaction with a user is called a batch job or batch process. 

## Algorithms for Batch processes
### FIFO (First-in-first-out)
Schedules processes strictly according to the process arrival time. The earlier the arrival, the higher the priority. Theoretically, multiple processes could have the same arrival time, in which case the arbitration rule can pick a process at random. In practice, all requests are processed sequentially and thus all processes have different arrival times.

FIFO is non-preemptive. 
> Also called __FCFS__ (First-come-first-served).
>* Schedules processes are strictly selected according to __arrival time__ (earlier arrival means higher priority)
>* There needs to be a rule to handle giving priority to processes that arrive at the same time.
>* This is a __non-preemptive__ scheduling (no interuptions).

### SJF (Shortest Job First)
The SJF (Shortest Job First) algorithm, also known as SJN (Shortest Job Next), schedules processes according to the total CPU time requirements. The shorter the required CPU time, the higher the priority. If multiple processes have the same CPU time requirement, then the arbitration rule can select a process based on the arrival times.

SJF is non-preemptive. 
> Also called __SJN__ (Shortest job next)
>* Schedules processes according to __total CPU time__ requirements
>   * if CPU times are equal, then use _arrival times_ to break the tie
>* Shorter jobs will have higher priority 
>* In _short-term_ scheduling, job would reffer to the __CPU burst__
>* SJF is __non-preemptive__. So a running process will be allowed to finish even if shorter jobs arrive in the meanwhile.

### SRT (Shortest Remaining Time)
Schedules processes according to the remaining CPU time needed to complete the work. The shorter the remaining CPU time, the higher the priority. If multiple processes have the same remaining time requirement, then the arbitration rule can select a process based on the arrival times.

SRT is the preemptive version of SJN.

>* Schedules processes according to the __remaining CPU time__ needed.
>   * Shorter the remaining time, higher the priority
>* SRT is the __preemtive__ version of SJN. 
>   * It will move to other processes if they have shorter times remaining
>
> The remaining time of a process is the difference between:
>* __total__ CPU time required 
>* and the __attained__ CPU time. 


## Estimating total CPU time
### For long-term scheduling 
>The total CPU time is measured between the process creation and process destruction. The time can be estimated from previous runs of the same program or may be supplied by the programmer.

### For short-term scheduling
>The total CPU time (__CPU burst__) changes for each arrival/departure period. The total CPU time is not known in advance but can be estimated based on past behavior. 
>
>The most common approach uses exponential averaging, which combines the last estimate of the total CPU time, __S<sub>n</sub>__, with the last actually observed total CPU time, __T<sub>n</sub>__, to predict the next total CPU time, __S<sub>n+1</sub>__:
>
>> __S<sub>n+1</sub> = &alpha;T<sub>n</sub> + (1 - &alpha;)S<sub>n</sub>__
>
>__&alpha;__ controls the relative weight of the last observation (__T<sub>n</sub>__) versus the history of past predictions (__S<sub>n<sub>__). 
>
>### Large vs small &alpha;
>![Predicted alpha time](./images/schedule-alpha.png)
>* __Large &alpha;__: prediction is based mostly on the last observed value, T<sub>n</sub>
>   * advantage: predictions adapt quickly to changes
>* __Small &alpha;__: prediction is based mostly on weighted history, S<sub>n</sub>
>   * advantage: predictions ignore outliers

## Batch performance
>* Main objective: maximize __throughput__ (number of processes per unit of time)
>* Order of processing cannot reduce CPU times but can reduce waiting times of individual processes
>* __Turnaround time__: time between arrival and departure
>   * CPU time + waiting time (keep in mind arrival time)
>* __Average turnaround time__ (ATT): mean of all individual turnaround times
>* Another objective: __fairness__
>   * __Starvation__: indefinite postponement of a process while others are running 
>       * both SJF and SRT can lead to starvation (processes requiring long CPU times could be postponed indefinetly if shorter jobs keep arriving).

>![Batch performance](./images/schedule-batch-performance.png)