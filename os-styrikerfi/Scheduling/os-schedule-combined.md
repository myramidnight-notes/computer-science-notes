# Combined scheduling
Since there are many types of processes, it would be best to handle them based on their type. The __2-tier__ scheduling can do that. 
## 2-tier approach
In this combined approach, we would have two tiers.
1. __Real-time processes__
    * run at highest priority level
    * can use __FIFO__ for the shortest processes, or __RR (round robin)__ for the slightly longer processes.
1. __Interactive and batch processes__
    * scheduled together using __MLF (multi-level feedback)__
    * Interactive processes usually getting higher priority over batch processes, due to them tending to be shorter. And since batch tends to use the CPU very heavily gets put to lower priorities.

![2 tier approach](./images/schedule-2-tier.png)

### Improving 2-tier with floating priorities
>This improvement would allow interactive processes to rise in priority based on their type. 
>1. First tier will be handled with __ML (multi-level)__ scheduling.
>1. Second tier will be a __MLF with floating priorities__
>   * All the 2nd tier processes would be assigned a __base priority__, which might not be the highest priority initially.
>   * Depending on how the process behaves after initial rotation, it might rise in priority. Such as if the request came from a keyboard input, which requires a fast response time, will raise it even higher in priority.
>
>![2 tier floating](./images/schedule-2-tier-floating.png)