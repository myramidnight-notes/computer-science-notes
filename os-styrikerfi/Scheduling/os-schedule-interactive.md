# Scheduling of Interactive Processes
Interactive process will communicate with the user in form of a dialog by receiving commands from the keyboard or a pointing device and responding by generating output on the user's terminal/output device.

The primary goal in scheduling interactive processes is to respond promptly to each input. Consequently, interactive processes must time-share the CPU using preemptive scheduling to give each process a chance to make process in a timely manner.

## Algorithms or interactive processes
### RR (Round-robin) 
>The round-robin (RR) algorithm uses a single queue of processes. The priority is determined solely by a process's position within the queue. The process at the head of the queue has the highest priority and is allowed to run for Q time units. When Q ends, the process is moved to the tail of the queue and the next process now at the head of the queue is allowed to run for Q time units. 
>
> __Time quantum__ is the short interval (~10-100ms) that the process can use the CPU each turn.
>* uses a single queue of processes
>* priority is determined solely by the possition of the process in the queue
>* Process at the head of the queue has highest priority and is allowed to run for a set time duration.
>* When a process finishes it's round on the CPU, it will be moved to the back of the queue and the next process will start. This continues until all processes are done.

### ML (Multi-level)
> While _RR_ treats all processes equally, the ML can use external priorities to divide the processes into priority groups. 
>* Maintains a separate queue for each prioriity level
>* each queue can make use of RR or FIFO to schedule the processes
>* queues are serviced in descending order. That means that the lower level queues are only serviced when the higher levels become empty.
>
>This is a preemptive scheduling, so if processes arrive at higher level queue, then it can interupt the currently running process to jump to to the higher level queue. This can lead to __starvation__ of the lower level queues if higher priority processes keep arriving.

### MLF (Multi-level feedback)
>Under the multilevel feedback (MLF) algorithm a newly arriving process enters the highest-priority queue, N, and is allowed to run for Q time units. When Q is exceeded, the process is moved to the next lower priority queue, N-1, and is allowed to run for 2Q time units. The quantum size is doubled with each decreasing priority level. Thus at priority level L, the maximum time allowed is 2<sup>(N-L)</sup>Q time units. 
>
> MLF is similar to ML, but it addresses the problems of starvation and fairness.
>* Each new arrival enters the highest-priority queue, where it is allowed to run for Q time units
>* Whenever a process finishes it's round, it will be moved to a lower level queue, where it will be given double the CPU time.
>* The maximum __CPU time is doubled__ for each decreasing priority level. 
>   * This moves longer processes to lower level priorities
>   * A _infinite_ time level is rarely used, because if a process would reach that level, then it would most likely be an error (infinite loop or such, neverending process)
> 
> This scheduling will give all processes a time at the CPU and favors shorting running processes.

## Performance of interactive scheduling
> The __response time__ of a process is the elapsed time from the submission of a request (pressing the Enter key or clicking a mouse button) until the response begins to arrive. 
>
>The primary performance goal of interactive scheduling is to guarantee adequate response time.
>
>* For a __lone process__ (not sharing the CPU), the response time depends on the type of request. Examples would be a spell-check of document or render a video clip.
>* When __multiple processes__ are time-sharing the CPU, the response time will increase with the number of processes, the length of the _time quantum_ (CPU time per turn) and the time that it takes to perform a _context switch_. The time quantum could be increased to improve useful CPU time depending on the situation, but it would also increase the response time.
>   * The overhead of context switch still persist even if only a single process is left in the queue, so it adds up.
>
>![Interactive performance](./images/schedule-interactive-performance.png)