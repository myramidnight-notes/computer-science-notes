# Real-Time scheduling algs.
A __real-time process__ is characterized by continual input, which  must be processed fast enough to generated nearly instantaneous output. For example a streaming of audio/video, control of moving device (robots, cars, drones...)

A __period__ is a time interval within which each input must be processed (each period having a implicit deadline for current item).
* You can imagine a video _buffer_ where it will have to pause a video to load the next segment. That would be an example of a process failing to fetch the video segment within it's given period.

> In the examples below, the orange are just to show where the process periods line up, while the green shows the processes how they would run as scheduled. 

## RM (Rate monotonic)
>* Schedules processes according to the __length of the period__
>    * the shorter the period, the higher the priority
>* RM is __preemptive__ (can be interupted)
>
>![RM schedule example](./images/schedule-rm.png)

>>At time _12_, there are two processes competing for the CPU, and since _p2_ has lower priority, it gets postponed until after _p1_ is done with the CPU.
>

## EDF (Earliest deadline first)
>* schedules processes according to the __shortest remaining time until deadline__
>    * shorter remaining time until deadline has higher priority
>    * Deadlines are when the next period should end
>* EDF is __preemptive__
>
>![Earliest Deadline First example](./images/schedule-edf.png)
>> The red triangle indicates the deadlines for each period. The process with the deadline closer to the current time (red line) has higher priority.

## Performance of Real-time processing
* The most important goal of __real-time scheduling__ is to meet all the deadlines. A schedule is __feasible__ if the deadlines of all processes are met.
* The __fraction__ of the CPU time used by a process is __T<sub>i</sub>/D<sub>i</sub>__, where __D<sub>i</sub>__ is the period and __T<sub>i</sub>__ is the required CPU time.
* __CPU utilization__ is the sum of individual CPU fractions of all the processes. Let's call the sum `U`.
    * The formula for CPU Utilization:   ![CPU utilization](./images/schedule-cpu-utilization.png)
    * If `U = 1`, then the CPU is utilized 100%
    * if `U > 1`, then no schedule is feasible
* `EDF` is a optimal algorithm. 
    * feasible schedule is always guaranteed if `U <= 1`.
    * The period needs to be at least `2n` ms for __EDF__ to produce a feasible schedule for `n` processes, else their deadlines will fail.
* `RM` is not guaranteed to produce a feasible schedule.
    * it is likely to produce a feasible schedule if `U <= ~ 0.7`
    * The period must be greater than `2n/0.7` ms for __RM__ to be likely to produce a feasible schedule

### Advantage of EDF over RM
> What happens in RM when the period of a postponed process (lower priority) runs out before the higher priority processes is done with the CPU? Then the schedule fails. EDF addresses this issue by prioratizing the shorter remaining time to deadline.
>
>![Advantage example](./images/schedule-real-time-rm-edf.png)