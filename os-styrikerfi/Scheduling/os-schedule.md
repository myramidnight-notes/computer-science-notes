# The Scheduler
## Calling the scheduler
The scheduler is a function that is called at the end of the following operations to find the next process on the _Ready list_ with the highest priority to give it access to the CPU.
>* After Request/Release resource 
>* After Create/Destroy process 

## Principles of scheduling
### Scheduling levels
>* __Long-term scheduling__
>    * Moves a new process to the __ready__ state to compete for the CPU
>    * Also handles moving processes to and from the suspended list
>* __Short-term scheduling__
>    * Happens more frequently
>    * switching between the __ready__ and __running__ states
>    * Also handles the waiting lists (__blocked__)
>
>![Scheduling principals](./images/schedule-short-long-term.png)

### _Preemtive_ vs. _non-preemtive_ scheduling
>* __Non-preemtive scheduling__ is where a running process can continue until it terminates or gets blocked on a resource
>* __Preemptive scheduling__ can stop a currently running process and choose another process
>    * When a new process enters the _Ready-list_
>    * When a previously blocked/suspended process re-enters the _Ready-list_
>    * When the OS is handling interupts to let other processes run

## Scheduling priority

1. The __scheduler__ finds the highest priority process on the _Ready list_
1. Compare it's priority with the currently running process.
    * If currently running process has lower priority than what was on the ready list, then run the higher priority process
        > This can happen if a currently running process created a higher prioirty process than itself, or if a resource was released.
    * If the currently running process is blocked, then switch over to the highest priority item on the _Ready list_.
        > This can happen when the scheduler is called from a resource request.
1. If the scheduler swaps processes, then the state of the currently running process will be saved in the _PCB_ and then load the state from the next process to the CPU.
### Priority of short-term scheduling
> __Priority__ of a process/thread is a numerical value that indicates it's importance. A rule needs to be in place to decide which process runs when their priarity ties.
>
>It could be a _constant_ value assigned at the creation of the process, or it could be a dynamic value that changes based on some conditions/parameters

| Parameter | Description |
| --- | --- |
|Arrival 	|The point in time when the process enters the _Ready-list_.
|Departure 	|The point in time when the process leaves the _Ready-list_ by entering the blocked or suspended state, or by terminating all work.
|Attained CPU time 	|The amount of CPU time used by the process since arrival.
|Real time in system 	|The amount of actual time the process has spent in the system since arrival.
|Total CPU time 	|The amount of CPU time the process will consume between arrival and departure. For short-term scheduling, total CPU time is sometimes called the CPU burst.
|External priority 	|A numeric priority value assigned to the process explicitly at the time of creation.
|Deadline 	|A point in time by which the work of the process must be completed.
|Period 	|A time interval during which a periodically repeating computation must be completed. The end of each period is the implicit deadline for the current computation.
|Other considerations 	|The resource requirements of a process, such as the amount of memory used, or the current load on the system.

### Priority of long-term scheduling
> _Long-term_ priority uses the same parameters as the _short-term_ priority.
>
> However the _long-term_ scheduling occurs much less frequentlyi, so so decisions are made at a higher granularity of time
>* __arrival__ would be time since process creation (not arrival to _ready-list_)
>* __departure__ would be the time of process destruction (not departure from _ready-list_)
>* __attained CPU time__ and __total CPU time__ also have different meanings in _long-term_.

### Difference in short-term and long-term priority parameters 
>![Difference in priority parameters](./images/schedule-priority-difference.png)
>* __Short-term:__ The timers are reset when the process re-enters the _ready-list_.

### Estimating total CPU time
![Estimate total CPU time](./images/os-estimate-total-cpu-time.png)