# File System
* ### [Files](files.md)
    * The file concept
    * File types
* ### [File directories](file-directory.md)
    * A tree-structured directory hierarchy
    * Operations on directories
    * A directed acyclic directory structure
    * A general graph structure with symbolic link
* ### [Implementation of file directories](file-dir-implement.md)
    * Contents of file directories
    * Internal structure of file directories
* ### [Operations on files](file-dir-operations.md)
    * Create/destroy a file
    * Open a file
    * Read a file
    * Write a file
    * Seek to a position
    * Close a file
* ### [Disk block allocation](disk-block-allocation.md)
    * Contiguous allocation
    * Linked allocation
    * Clustered allocation
    * Using a file allocation table
    * Indexed allocation
    * Free space management