[...back](./README.md)
# Disk block allocation
>>A disk block is a fixed sequence of bytes on the disk, which can only be accessed as a single unit using low-level read-block and write-block operations. The FS views the disk as a sequence of blocks, numbered consecutively 0 through D - 1, where D is the total number of blocks on the disk.
>>
>>A file is stored on the disk in units of disk blocks. Thus a file may be viewed as a series of 1 or more blocks, numbered 0 through f - 1, where f is the number of blocks constituting the file. The way disk blocks are allocated to file blocks has a major impact on the efficiency of most file operations.
## Contiguous allocation
>>
>>In a __contiguous block allocation scheme__, every file is mapped into a contiguous sequence of disk blocks. The FCB points to the first disk block. The file length, also maintained in the FCB, determines how many blocks are occupied by the file.
>>
>>__The main advantages are:__
>>
>>    * Fast sequential access because neighboring blocks do not require any seek operations.
>>    * Fast direct access because a target block number can be computed using the file position and the block length.
>>
>>__The main drawbacks are:__
>>
>>    * Disk fragmentation, in that over time, the disk consists of variable-length sequences of free and occupied blocks and requires search algorithms to find free areas of appropriate size for a given file.
>>    * Inability to expand a file if the block following the last file block is not free. The entire file must be copied into a larger area.
>>    * Difficulty in deciding how much space to allocate to a new file to allow for potential expansion.

With __contiguous allocation__, the allocated disk blocks need to be adjacent to each other, so if we run out of space within the current hole for expanding the file, then we need to move the whole thing to a larger hole in order to allow the file to continue growing. 

We can directly access a desired block based on starting location of the initial block and the offset within the file length, and it will not require finding the blocks scattered on disk, because they will all be adjacent to each other, which makes for a very fast sequential access.

>![Disk allocation](./images/fs-allocation-contiguous.png)
>This example shows a file that has reached it's limit within the current space, and needs to be relocated in order for it to grow in size.

## Linked allocation

>>With a linked block allocation scheme, the blocks containing a file may be scattered throughout the disk. The FCB points to the first block and each block points to the logically next block.
>>The main advantage is:
>>
>>    * Easy expandability of a file by linking additional blocks to the last block.
>>
>>__The main drawbacks are:__
>>
>>    * Slower sequential access than with a contiguous allocation, since each block access requires a seek operation to a different position on the disk.
>>    * Inability to perform direct access since the location of the target block cannot be computed. To access any block requires following the pointers and reading all blocks preceding the target block.
>>    * Decreased reliability of the disk. If a disk block becomes physically damaged, the rest of the file cannot be found.
>>    * Considerable waste of disk space since every disk block must include a pointer to the next block.

The __linked allocation__ allows us to make better use of the disk space overall, where each block has a pointer to the next block in the chain, regardless of where it's located. There is no need for relocating the entire file for us to expand it, we simply find the next available block and link it.

But with this approach, we loose the ability to directly access specific blocks of a open file, because we will need to sequentially trace the chain of blocks. We might need a __seek__ operation in order to find the next block when it's located on different tracks on the disk.
![Linked allocation](./images/fs-allocation-linked.png)
* Every single block will contain a pointer to the next block in the link chain
    * this actually increases the size of each block, making it less efficient on the disk space.
* To access a block, you will have to traverse the chain until you find the desired block
    * there is no direct access, only sequential access

## Clustered allocation

>>A __clustered block allocation scheme__ links together sequences of contiguous blocks. The last block of any cluster points to the beginning of the logically next cluster. The last block also records the number of blocks belonging to the next cluster to facilitate direct access within each cluster. The number of blocks in the first cluster is recorded in the FCB along with the pointer.
>>
>>The approach is a compromise between the contiguous and linked allocations. The main advantages are:
>>
>>    * Easy expandability of a file. If the block following the last file block is free, than the last cluster is extended as with the contiguous allocation. If the block following the last file block is occupied, then a new cluster is started and the last block points to the first block of the new cluster.
>>    * A reduced number of pointers since only the last block of a cluster contains a pointer.
>>    * Improved sequential access over the purely linked allocation since accessing blocks within a cluster does not require any seek operations.
>>
>>__The main drawbacks are:__
>>
>>    * Slower sequential access than with contiguous allocation since each cluster requires a seek operation to a different position on the disk.
>>    * Inability to perform direct access since the location of the target block cannot be computed. To access any block requires following the pointers until the cluster containing the target block is reached.

The __clustered block allocation__ is better than a basic __linked allocation__ because it will contain clusteres of continuous blocks (allowing for direct access within the cluster), and makes it easier to jump over entries by jumping over clusters, rather than having to go through every single entry.

![Clustered allocation](./images/fs-allocation-clustered.png)
* Every cluster has a pointer to the next cluster
    * There are overall fewer pointers to cover all the blocks, compared to a __linked allocation__.


## Using a file allocation table

>>A variation of the linked organization is to allow file blocks to reside anywhere on the disk but to segregate the links in a dedicated area on the disk, rather than maintaining the links as part of each disk block.
>>
>>A __File allocation table (FAT)__ is an array where each entry corresponds to a disk block. The FAT keeps track of which disk blocks belong to a file by linking the blocks in a chain of indices. A file's FCB contains the index of the first file block, which in turn contains the index of the next block, etc.
>>
>>The FAT provides all the advantages of a linked allocation, including the ability to easily expand a file. Since the FAT resides in a small number of designated disk blocks, the blocks can be cached in main memory. Consequently:
>>
>>    * Sequential access is more efficient since no seek operations are necessary to follow pointers.
>>    * Direct access is also possible, because the location of the desired block can be found efficiently by scanning the chain of indices in the FAT.

The __file allocation table__ 
![FAT allocation](./images/fs-allocation-fat.png)


## Indexed allocation

>>With an indexed block allocation scheme, file blocks may reside anywhere on the disk. An index table is provided for each file, which keeps track of the blocks belonging to the file.
>>
>>__The main advantages are:__
>>
>>    * Fast sequential as well as direct access.
>>    * Ability to efficiently expand a file by simply adding a new block number to the index table.
>>
>>__The main drawback is:__
>>
>>    * Necessity to decide a priori how big the index table should be. A small table limits the maximum size of any file. A large table wastes disk space, since most file will have no need for the large size.
>>
>>A variation of the simple indexed allocation, pioneered by the Unix OS, is an index table that can expand as necessary. The FCB contains the block numbers of the first n file blocks, which can be accessed directly as with the simple index table. If more than n blocks are needed, the next entry of the FCB points to a second level index table of size m. Thus a file can have n + m blocks but the additional m blocks incur an access penalty, because the second-level index must be accessed first. If still more blocks are needed, the expansion can continue with additional multi-level indices, each providing additional blocks but at an increasing cost of access.

![Allocation index](./images/fs-allocation-index.png)