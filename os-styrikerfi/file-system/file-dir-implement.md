[...back](./README.md)
# Implementation of file directories
## Contents of file directories
>>In addition to a file's name, data, and metadata, the FS must keep track of various attributes necessary for accessing the file and for providing relevant information to the user. The typical file attributes include:
>>
>>    * __Size__: The current size in bytes or words.
>>    * __Type__: Information to differentiate directories, regular files, executable files, and other types of files supported by the system.
>>    * __Location__: Information necessary to locate the file's physical blocks on disk.
>>    * __Protection__: Information about who can access the file and the permitted type of access (Ex: read only or execute only).
>>    * __Use__: The date and time of file creation, last access, or last modification.
>>
>>Where to keep file attributes is an important design issue. One approach is to keep all attributes together with the file name inside each file directory. The main drawback is that file directories can become very long, which slows down the search. Another approach is to keep only the file name inside the directory and provide a pointer to a separate data structure to store the file attributes.
>>
>>A __File control block (FCB)__ is a data structure associated with a filename that contains all relevant attributes of the file. FCBs are stored apart from file directories and are pointed to by the corresponding directory entries. 

There is usually a handful of extra attributes that we store about files in a system other than the essential file header and data. 
* When was the file created?
* When was the file modifed last?
* Who has access to it? What type of access?
* How large is the file? 
* Is it an executable file?
* Where is the file physically located on disk?
* Who or when was the file accessed last time?
* ... and other such attributes to keep track of

### The File Control Block (FCB)
A __FCB__ allows us to store relvant attributes separately from the file system object (such as a _file_ or _directory_), and the associated directory or file will have a pointer to this __FCB__. 
>In _Unix_ and other OSs, an FCB is called a __i-node__ (_index node_). 

#### Methods of storing attributes
* __Without a FCB__, where all attributes are contained with the file name
   > Drawback is that it makes the file directories very large
* __Mixed approach__, keeping critical attributes with the file name, and the rest of the attributes in a FCB
* __Everything in a FCB__, only keeping a pointer to the FCB with the file name. 

Larger directories makes it slower to search within them, so storing attributes with the file-name makes these searches even slower. 


>![File control block](./images/fs-fcb.png)
> Here is an example of the three implementation

## Internal structure of file directories

>>The FS must be able to efficiently (1) search directories for a file name, (2) find and allocate an entry when a new file is created, (3) free an entry when a file is deleted, and (4) change the file name, which may shorten or lengthen the name length. Several approaches exist to organizing directory entries:
>>
>>    1. The directory is an array with fixed-length entries. Each entry contains a valid file name or is free.
>>        * Finding, freeing, or allocating entries is simple because all have the same size.
>>        * Main drawback: All file names have a maximum length.
>>    1. The directory is an array with variable-length entries. Each entry contains a length field and either a valid file name or is free.
>>        * Searching for a file name or a hole is accomplished by following the length fields.
>>        * Deleting a file requires coalescing the newly free entry with any free neighboring entries to prevent fragmentation.
>>        * Changing the file name length requires freeing the current entry and allocating a new entry to reflect the new name length.
>>    1. The directory is an array with variable-length entries. Each entry contains a valid file name, followed by a possibly empty hole. Each entry also contains two pointers, one pointing to the end of the entry and the second to the end of the file name.
>>        * Finding, freeing, or allocating entries is analogous to implementation 2.
>>        * The main advantage is that, in most cases, changing the file name length only requires adjusting the second pointer. A new entry must be allocated only when the new length exceeds the end of the hole.
## Organization of a directory
### Managing flexible file names
![Flexible file names](./images/fs-flexible-names.png)

All of these implementations have a pointer to the File Control Block (FCB) where it stores the attributes for the given entry.
1. __Fixed length entries__
    * It has a boolean value keeping track of if the entry is occupied or not: `1` represents a occupied entry, `0` represents a free entry.
    * This creates a upper limit to the name size, but it removes the need to relocate the entry every time the name size would change.
1. __Variable length entries__
    * It also has a boolean value indicating if the entry is occupied or free.
    * `L` is keeping track of the name size, where the next entry begins.
    * Changing file-name sizes need to be handled
        * If a name is lengthened, then the entry needs to be moved to a hole large enough
        * If a name is shortened, the hole it creates needs to be merged with a neighboring hole.
    * If a file is moved or deleted, the remaining hole needs to be merged with an adjacent hole.
1. __Variable length entries (combined with a hole)__
    * Each entry would contain two pointers, keeping track of the end of the name `L`, and end of the hole `K` (where next entry begins).
    * This implementation would allow for flexible name sizes, with a possibly empty hole at the end. 
        * This reduces the need to relocate files whenever the name size changes
        * This hole can grow in size if the name is shortened or the following entry is deleted.
    * the entry only needs to be relocated if the file name size exceeds the end of the hole.´
    * If entries are removed, then the space is merged with the entry before it (increasing it's hole size)
    * Entries can be added into any hole large enough
        * we simply move the `K` pointer to resize the remaining hole
