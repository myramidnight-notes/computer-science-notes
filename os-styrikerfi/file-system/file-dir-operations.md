# Operations on files

>>The FS provides a set of high-level operations to create, destroy, read, write, and otherwise manipulate files. Jointly, the set of all file operations constitutes the user interface to the FS.
>>### Create/destroy a file
>>
>>The __create__ file operation causes the creation of new named file. As part of the create operation, the FS allocates and initializes a new File control block (FCB). The FS also allocates a free directory entry, which contains the file's name and a pointer to the FCB.
>>
>>The __destroy__ file operation removes an existing file from the FS. The operation reverses the process of creation by freeing up the file's FCB and directory entry. If the file is not empty, then the location information in the FCB points to blocks on the disk, where the file's contents are stored. The FS marks the file's disk blocks as free.

### Create file
>![Create file](./images/fs-create.png)
>* Before you can create a file, you need to specify it's name
>* it doesn't require anything else, because we expect a newly created file to always be of size 0 with no content initially, meaning it wont need to allocate any blocks on disk until the file has been modified. 
>* To avoid duplicate names, the create operation will need to search the current directory for any matching names

### Destroy file
* When using the _destroy operation_, we need to search for the file name, because it has to exist in order for it to be destroyed.
* It needs to free all disk blocks being allocated to the destroyed file
* Free the directory entry associated to the destroyed file, because it won't exist anymore.

## Open a file

>>In order to read, write, or otherwise manipulate a file, the corresponding operations need to retrieve information about the file's length and location on the disk. To avoid accessing the information repeatedly and to keep track of the file's current state, the FS maintains all relevant information in an open file table kept in main memory.
>>
>>The __open file table (OFT)__ is a data structure that keeps track of all files currently in use to facilitate efficient access to and manipulation of the files.
>>
>>The __open file__ operation prepares a file for efficient access and manipulation by retrieving relevant file information from the FCB and storing the information in an entry of the OFT. Subsequent accesses to the file then occur via an index into the OFT.
>>
>>The steps performed by the open operation include:
>>
>>    1. Verify that the invoking process has the right to access the file and perform the requested operation.
>>    1. Find and allocate a free entry in the OFT.
>>    1. Allocate read/write buffers as necessary for the given type of file access.
>>    1. Copy relevant information from the FCB to the OFT entry, including the file length and location on the disk.
>>    1. Initialize other information in the OFT entry, such as the initial position of a sequentially accessed file.
>>    1. Return the index of the allocated OFT entry to the calling process for subsequent access to the file.

![Open files table](./images/fs-open-files-table.png)
1. When we open a file by name, we will need to search the directory for the file (it needs to exist first) 
1. Once we find the file, we can point to its FCB from the Open File Table (OFT), along with some essential information from the FCB such as position, length and who has access to it.
1. Then the inital block of the file data will be moved to the read/write buffer within the OFT

## Read a file

>>A __read file__ operation copies data from an open file to a specified area in main memory. The data may be accessed either directly, one record at a time, or sequentially, by specifying the number of bytes to be read next. A generic sequential read instruction has the form:
>>
>>read i, m, n
>>
>>where i is an OFT index corresponding to an open file, n specifies the number of bytes to be read, and m is a location in memory (ex: an array) where the data is to be stored.
>>
>>The contents of a file are kept on disk, which can only be accessed one block at a time. To avoid the overhead of reading an entire disk block for each read operation, the FS maintains a current disk block in a r/w buffer and copies the specified numbers of bytes from the buffer to the memory area m. Only when the end of the buffer is reached does the FS access the disk to get the next disk block, which replaces the previous disk block in the buffer.

![Read file](./images/fs-read-file.png)

## Write a file

>>A __write file__ operation copies data from an area in main memory to a specified open file. A generic sequential write operation has the form:
>>
>>write i, m, n
>>
>>where i is an OFT index corresponding to an open file, n specifies the number of bytes to write, and m is the starting location in memory (ex: the name of an array) from which the data is to be copied.
>>
>>Analogous to the read operation, the write operation also uses the r/w buffer in the OFT. The operation starts copying the specified number of bytes from the memory area to the r/w buffer. When the desired number of bytes are copied, the operation terminates. If the end of the buffer is reached before all bytes are copied, the operation allocates a new disk block and enters the block number into the FCB. The operation then stores the current r/w buffer in the new block and continues copying the remaining bytes from memory to the now empty buffer.

![Write file](./images/fs-write-file.png)

1. If we read from point `m` to `n`, then the buffer will contain everything from `m` to `m+n`
1. If we read again to the same buffer, starting from `m+n` and read the next `n` bytes, then the buffer will still contain what we previously read to it.

## Seek to a position

>>A __seek__ operation moves the current position of an open file to a new specified position. A generic seek operation has the form:
>>
>>seek i, k
>>
>>where i is an OFT index corresponding to an open file and k specifies the new position within the file.
>>
>>The operation sets the current position in the OFT entry to k. In addition, if k is not within the block currently held in the r/w buffer, the operation must save the r/w buffer to disk and load the new target block.
>>
>>The block containing position k is determined by dividing k by the block size. The offset within the buffer, corresponding to the position k, is determined by taking k modulo the buffer size.

The __seek__ operation simply moves the current position of an open file to the specified position.
* `seek i, k` or `seek <open file index>, <position in file>`
>![Seeking](./images/fs-seek.png)
>1. We can __seek__ a specific position in the _open file_, to begin reading to the buffer from there.
>1. In this example, we are seeking position 1400, but we can only contain 512 in the buffer
>1. We can figure out that we're looking for something in block `2` because `1400 / 512 = 2.73..` 
>1. Then we find out how far into the block we're looking: `1400 - (2*512) = 367`

> if we had the string `abcdfg` in the open file, and read 3 characters, then the buffer would hold the chars `abc`. If we would then seek position 0 in the file and read 3 characters, then the buffer would contain `abcabc`, because we're not moving the position within the buffer backwards (it just continues forward until buffer is full)
## Close a file
>>The close file operation reverses the effects of the open operation by saving the current state of the file in the FCB and freeing the OFT entry.
>>
>>The steps performed by the close operation include:
>>
>>    1. If the current content of the r/w buffer is modified, then save the buffer in the corresponding block on the disk.
>>    1. Update the file length in the FCB.
>>    1. Free the OFT entry.
>>    1. Return the status of the close operation to the calling process.

It's simply the reverse of the `open` operation. It will need to update the file on disk with whatever changes happened while it was open

* The `close` operation needs to update the FCB about the current read position, and update the data on disk if anything has been modified.
* It does not have to do anything about those details witihin the OFT, because any information there is only relevant when the file is open, and this action is closing it.