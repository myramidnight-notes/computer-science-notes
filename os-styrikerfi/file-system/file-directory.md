# File directory
>>A file directory (or folder) is a special-purpose file that records information about other files and possibly other directories. The directory consists of a set of entries, each containing the name of a file followed by other information necessary to access the file.
>>
>>Several approaches exist to organizing the individual file directories into a global directory structure.

## A tree-structured directory hierarchy

>>A tree-structured directory hierarchy is a collection of directories organized such that (1) every directory points to zero or more files or directories at the next lower level, and (2) every file and directory except the root is pointed to by exactly one parent directory at the next higher level. The root of a tree-structured directory hierarchy is the highest level directory, which does not have a parent directory.
>>
>>Every file and every directory has a unique ID, assigned by the FS at the time of creation. This ID is used only by the FS for internal management purposes and is not visible to the users. Instead, users assign arbitrary ASCII names to files and directories, which become part of path names.
>>
>>An absolute path name of a file, uniquely identified by an internal ID, f, is the concatenation of the directory and file names leading from the root to the file f. The individual names are separated by an agreed-upon delimiter, typically a forward slash or a backslash.
>>
>>To avoid using long path names, the FS allows the user to designate one directory as the current working directory. A relative path name is a concatenation of file names starting with the current directory.
>>
>>To be able to navigate the hierarchy both up and down, the FS provides a special convention, generally "..", to refer to a parent directory.

![File directory tree](./images/fs-file-directory.png)
* The __Root__ is the highest directory in the hierarchy, usually denoted as `/` 
* A __aboslute path__ is the path all the way from the root to the desired file/directory
* A __working directory__ (or __current directory__) can be declaired, which allows us to use shorter paths, instead of having to declare the absolute path
    * We can access parent directories with `..` (which is the typical implementation for going up a directory)
    * The path preceeding the current directory can simply be attached to any other relative paths to complete the path
    * Any path depends on the current working directory path, they will break if used from a incorrect directory (they might work from sibling directories, if they accessing parent directories in the path)
* Each name along the path are seperated by a symbol, usually a slash `/` or backslash `\` 

## Operations on directories
We will have to be able to interact with the directories, and the access to these operations usually depend on the access level of the user (if they have the rights to change anything)

| Operation | Description |
| --- | --- |
|Change | Change current working directory to a new directory specified by a path name 
|Create | Create a new named directory and new entry in the current working directory, which points to the new directory 
|Delete | Delete directory `d` by a path name. Delete also all files and directories reachable using any path name starting from `d`
|Move | Move a file or directory to a different directory specified by a path name
|Rename | Change the name of a file or directory specified by a path name
|List | List the names of files and directories contained within a directory specified by name
|Find | Find a file or directory specified by name


## A directed acyclic directory structure

>>The main drawback of a tree-structured directory hierarchy is that file sharing is asymmetric. Only one directory can be the parent of any file or another directory. Other users must refer to the file by first navigating up to a common directory and then down to the desired location.
>>
>>A __directed acyclic directory hierarchy__ organizes directories such that any directory at a given level may point to zero or more files or other directories at lower levels but also permits any file or directory to have more than one parent directory.
>>
>>To permit the creation of multiple parents, the FS provides an additional operation of the form "link path1 path2", which creates a new link from the directory identified by the path name path1 to the directory or file identified by path2.
>>
>>Since any file, including any directory, can now be pointed to by more than one directory, the semantics of file deletion must be extended. A __reference count__ is a non-negative integer associated with a file f, which indicates how many directories are pointing to the file. The file is deleted only when the reference count is 1, indicating that f has only a single parent directory. Otherwise only the pointer to f is deleted from the parent directory and the reference count of f is decremented.

In a __directed acyclic directory hierarchy__, has the following features:
* directories at a given level may point to zero or more files/directories at lower levels, 
* it also permits any file or directory to have more than one parent directory (directories that point directly to it).

>![Acyclic directory](./images/fs-dir-acyclic.png)
>
> Here is a directory tree containing file `f8`, which has 3 parent directories because all three are directly pointing at it

## A general graph structure with symbolic links
>>If a directory is allowed to point to a file or another directory at a higher level, then a cycle can form in the hierarchy. A cycle, if undetected, can lead to an infinite loop in algorithms that search the hierarchy for a given file. File deletion also becomes more difficult. A reference count is not sufficient to prevent the creation of unreachable subgraphs in the directory hierarchy, which can only be removed using a garbage collection algorithm.
>>
>>A compromise solution is to allow only a single parent directory for any file or directory but to provide symbolic links to support file sharing. A __symbolic link__ (or __shortcut__) is a directory entry that points to a file or directory just like a regular entry but is treated differently with respect to deletion. A delete operation only removes the link but not the file itself.

![Symbolic links](./images/fs-dir-symbol-links.png)