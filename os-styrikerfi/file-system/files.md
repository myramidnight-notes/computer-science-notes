# Files
## The file concept
>>Users have the need to maintain information in persistent storage over extended periods of time. The most common secondary storage devices are magnetic disks and tapes, which are supplied by many different manufacturers. The complexity and wide variety of device-specific interfaces makes the management of data directly on the devices impractical for most users.
>>
>>A __file system (FS) __is an integral part of every OS, whose function is to implement the concept of files.
>>
>>A __file__ is a named collection of information managed on secondary storage by the FS. The user can view a file as a single abstract unit of data storage and access the file's contents using high-level operations provided by the FS's interface, without knowing the specific characteristics of the underlying storage devices. Depending on the FS, a file can be viewed as an unstructured stream of bytes or a series of records.
>>
>>A __record__ is a structure of related data items, possibly of different data types, identified within a file by a record number or a unique key field. Ex: A student ID would uniquely distinguish all student records in a file.
>>
>>An __access method__ is a set of operations provided by the OS as part of the user interface to access files. The most common access method is sequential. The FS maintains the current position within the file and each read/write operation accesses the next n bytes or the next record of the file. Some OSs (Ex: most IBM systems) support also direct access methods, where a record can be accessed directly by specifying a record number or a key value.

We have a need to maintain information, storing it somewhere for the long-term. There are many types of secondary storage to keep this information.
* __Magnetic storage devices__, such as hard disk drives and tapes
    > These are the most common and cheapest ways to store information
* __Optical storage devices__, such as CDs, DVDs and Blu-ray disks
    > These are common for reusable read-only information
* __Solid state storage devices__, such as solid state drives (SSDs) and USB memory sticks.
    > Also called flash drives, they are popular for their speed of writing, but are more expensive than their magnetic counterpart.

## File system (FS)
It implements the concept of files

__Files__ are collections of data that are stored on secondary storage by he file system.

### Access methods
* __Byte-stream oriented__
    * read/write `n` bytes
* __Sequential record oriented__
    * read/write next record
* __Direct record oriented__
    * read/write record `k` 
    * read/write next record

### Example
>![File concept](./images/fs-file-concept.png)
>Example files:
>* `file a` being a __byte-stream__, 
>* `file b` being a __seuential__ record that allows for __direct__ access
>* `file c` allowing for __sequential__ access, each section being preceeded by an integer that tells how large the section is, but since each section can be of varying length, we cannot directly access a specific location because we can't reliable predict the section size.
>
> The files would appear very transparent to the users as logical files, while in reality each file could be scattered through the hard disk.

## File types
>>Applications use files to store information on the disk and to pass information from one application to another. The producer and the consumer of a file must agree on the format of the information within the file. Ex: A compiler expects a file consisting of ASCII characters organized as lines of source code. The output of the compiler is a file consisting of executable binary code, data, relocation information, and various tables and flags, which must be understood by the linker.
>>
>>Every file consist of data and metadata. Data is the portion of the file visible to the file's user. __Metadata__ is information about the format and organization of a file's data and is generally stored in a __file header__. A file header is a portion of the file preceding the actual data and is visible to only the FS itself.
>>
>>The __magic number__ is a short sequence of characters at the start of the file header, which identifies the file type. The file type, in turn, determines which programs are allowed to access and interpret the file. Ex: The OS will allow only properly compiled and linked binary files to be loaded into memory for execution.
>>
>>Another way to represent file types is to use __file extensions__. A file extension is a sequence of one or more characters following the file name. A file extension, unlike a magic number, is not hidden within the file header and thus can conveniently be examined by the user. The drawback is that a file extension can easily be changed without changing the file's type. Thus file extensions support only a weak form of file typing by providing convenient hints about a file's type but do not rigorously enforce which operations are valid for a given file type.

Each file consists of data and metadata. 
* __Metadata__ contains information about the format and structure of the file's data. This is generally stored in a __file header__ that preceeds the actual data.
* __Magic number__ is a short sequence of characters at the start of the _file header_ that identifies the file type, only visible to the _file system_ itself (hidden to the user).

### File extentions
__File extention__ is another way to identify the file type, visible to the user. Typically a 3-4 characters at the end of the _file name_ (`file-name.<file extention>`). Issue with file extentions is the fact that they are modifiable by the user.

Some systems tend to hide these file extentions by default, so that the user might not accidentally change them, but it makes it harder to identify different types of files that share a identical name (one solution would have a file type icon instead)

* __Weakly typed__ file system would rely on the file extention to tell the type of file.
* __Strongly typed__ file system would trust the __magic number__ instead of the file extension, because renaming a file extention does not effect this number.

### Example
![File types](./images/fs-file-type.png)
