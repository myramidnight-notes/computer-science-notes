# Input / Output
* ### [The hardware-software interface](hardware-interface.md)
    * __The I/O hierarchy__
    * __Programmed I/O with polling__
    * __Programmed I/O with interrupts__
    * __I/O with direct memory access__
    * __Polling vs interrupts__
* ### [Data buffering and caching](buffer-caching.md)
    * __Using a single buffer__
    * __Buffer swapping__
    * __A circular buffer__
    * __Disk block caching__
* ### [Disk scheduling](disk-scheduling.md)
    * __Magnetic disk organization__
    * __Disk access optimization__
    * __SSTF scheduling algorithm__
    * __Scan and C-scan scheduling algorithms__
* ### [Error handling](error-handling.md)
    * __Error detection and correction__
    * __Bad block detection and handling__
    * __Stable storage__
    * __RAID__