# Disk scheduling

## Magnetic disk organization

>>Disks are the most common mass storage devices. A disk stores information on one or more rotating magnetic surfaces.
>>
>>A tra__ck is one of many concentric rings on a magnetic disk surface. To access information on a track, a read/write head, mounted on a movable arm, is mechanically positioned over the track. Information is written onto or read from the track as the disk rotates under the r/w head.
>>
>>A sector is a portion of a track and is the smallest unit of data that can be read or written with a single r/w operation.
>>
>>The time to access any data on a disk consists of 3 components:
>>
>>    * The __seek time__ is the time to move the r/w head from the current position to the track containing the desired data. The time is directly proportional to the physical distance the r/w head needs to traverse.
>>    * The __rotational delay (rotational latency)__ is the time to wait for the desired data item to pass under the r/w head. On average, the rotational delay is one half of the time of one disk revolution. Thus the time depends on the disk's rotational speed.
>>    * The __data transfer time__ is the time to transfer the desired number of bits to or from the disk, and is directly proportional to the disk's rotational speed.
>>
>>The most important characteristics of a disk are the disk size in bytes and the data transfer rate. __Peak transfer rate__ is the rate at which the data is streamed to or from the disk once the read/write head is at the beginning of the sector to be transferred. The peak transfer rate depends directly on the rotational speed of the disk and the number of sectors per track. __Sustained data rate__ is the rate at which the disk can transfer data continuously. The sustained data rate includes the seek times over multiple tracks and other overhead in accessing the data over time. 
>>
>>| Example |
>>|--- |
>>| A disk rotates at 7,200 rpm (rotations per minute), or 7,200/60 = 120 rotations/sec. Thus 1 revolution takes 1/120 = 0.00833 sec.  The peak data rate is the ratio of the data transferred over a unit of time. Since 0.1 MB are transferred in 0.00833 seconds, the peak data rate is the ratio 0.1/0.00833 MB/sec.

![Disk organization](./images/disk-organization.png)
* The disk is accessed with a read/write head, we want to minimize it's movements for efficiency. It moves the head between tracks.
* The disk is seperated into multiple circular tracks, their size of course depends on their locaation from the center.
* Each track is segmented into __sections__, all tracks having same number of equal sized sections, but their size depends on the size of the track.
* The disk spins in one direction, the read/write head only looks at what's right under it at the time.

## Disk access optimization

>>The seek time and the rotational delay involve mechanical motion of the r/w head and the physical disk rotation. Both times are in the millisecond range and thus constitute most of the access time to a disk block.
>>
>>Rotational delay is easy to optimize. Given a sequence of requests for multiple blocks on a track, the requests are sorted in ascending order. Consequently, all blocks on the same track can be accessed within a single revolution of the disk.
>>
>>Optimizing the seek time is a more difficult problem because the r/w head sweeps back and forth across different tracks. Thus at any point in time, the algorithm must decide in which direction the r/w head should move next. The main objective is to minimize the movement of the r/w head while also guaranteeing some measure of fairness in servicing all requests.

![Disk optimization](./images/disk-optimization.png)
* We could reorder the requests so that they are all (hopefully) picked up in order on the current rotation of the disk.
* We could seek out the closest track each time, to minimize travel distance, but this could lead to starvation of tracks further away.

## SSTF scheduling algorithm

>>The simplest approach to disk scheduling is to service all requests in FIFO order, which is the order of arrivals. For each request to access a sector on track t, the r/w head moves to the track t. The number of tracks traversed by the r/w head using FIFO scheduling can significantly be reduced when requests are reordered prior to moving the r/w head.
>>
>>The __Shortest seek time first (SSTF)__ scheduling algorithm considers the list of all pending requests and always selects the request that requires the shortest travel distance from the current position.
>>
>>The algorithm minimizes the travel distance of the r/w head and thus maximizes the disk's performance but the main drawback is a lack of fairness in treating the different requests. Since new requests for tracks close to the current position are always served before more distant tracks, the service time of any request is unpredictable. With a continuous stream of new arrivals in the vicinity of the current position, requests for distant tracks could be postponed indefinitely, leading to starvation.

![Disk scheduling](./images/disk-scheduling.png)
* __FIFO__ would just travel to the tracks in order they arrive
* __SSTF__ would travel in the order of closest tracks
    * This could lead to starvation of tracks further away.

## Scan and C-scan scheduling algorithms

>>The __Scan scheduling algorithm__ mimics the behavior of an elevator in a building. The r/w head maintains a current direction of travel and services all request sequentially in the current direction. When the outermost request is reached, the direction is reversed and the algorithm services all requests in the opposite direction.
>>
>>The performance of the scan algorithm lies between the performance of the FIFO and the SSTF algorithms but starvation is eliminated, since each request is guaranteed to be served within one sweep of the r/w head. However, the access time to different tracks is not uniform. Since the r/w head sweeps back and forth, tracks closer to the midrange have a greater chance to be serviced quickly than tracks at either extreme of the disk.
>>
>>The __C-Scan scheduling algorithm__ is a variant of the Scan algorithm that services requests in only one direction. When the outermost request is reached, the r/w head sweeps back to the opposite end of the disk and starts servicing requests again in the same direction.
>>
>>Unlike Scan, C-Scan guarantees a uniform access time to all tracks, because the r/w head services all requests in only ascending order. Thus a position in the midrange of the disk conveys no advantage over any other position.

![Scheduling 2](./images/disk-scheduling2.png)
* __Scan scheduling__ will only travel in one direction at a time
* __C-Scan scheduling__ will only travel in one direction, going back to the start (or the closest task to the start) before moving again in the same direction as before.