# Error Handling


## Error detection and correction

>>Data on a disk can become inconsistent for two reasons:
>>
>>    * A system crash during a write operation can leave a disk block in a partially updated state.
>>    * A spontaneously occurring bad block, caused by aging or physical damage to the recording medium, makes the reading or re-writing of the block impossible.
>>
>>Data can also be corrupted during transmission between systems or I/O devices.
>>
>>A __parity bit__ is a bit added to a string of bits to ensure that the total number of 1's in the string is even or odd. Even parity sets the parity bit such that the number of 1's is even. Similarly, odd parity sets the parity bit such that the number of 1's is odd. Even and odd parity are duals of each other and either can be used as the simplest form of error detecting code, able to detect a single erroneous bit in a string.
>>
>>An __error correcting code (ECC)__ includes multiple parity bits in a string to permit the detection and automatic correction of some number of erroneous bits.The number of bits that can be detected and corrected depends on the number of parity bits and the encoding scheme employed.
>>
>>A popular ECC is the Hamming code, which can correct a single bit error. The encoding inserts even parity bits into a string of arbitrary length at positions 1, 2, 4, 8, ..., __2<sup>i</sup>__
>>
>>A single error in a string can automatically be corrected by adding the positions of all incorrect parity bits. The sum identifies the erroneous bit. 

The pairity bit method allows us to check if all the bits in the binary sequence are intact and without error, and it also allows us to find and correct a single bit error.

### Pairity bit coverage
>![Positions of pairity bits](./images/io-err-hamming.png)
>
>This table shows what bits each pairity `p` bits covers. The pairity bit's seat number also plays a role in how many bits it alternates between (always including their own position, which will only be set if needed)
>
>#### Positions of pairity bits
>It is actually quite easy to figure out where to place pairity bits, we place them at every position __2<sup>i</sup>.
>* 1, 2, 4, 8, ..., __2<sup>i</sup>__

### Odd or Even pairity counter
>The pairity bit itself is undecided until we've counted all the set bits (the bits set to __1__) that the pairity bit covers. Depending on if that total of set bits is odd or even, we set the pairity bit accordingly.
>
>We can have two ways to implement the pairity check. 
>* a __even pairity__, then we want the number of set bits that the pairity covers to sum up to a even number, so the pairty bit itself essentially just indicates if you needed to add 1 bit to make it even or not.
>* __odd pairity__ works the same, except that we aim for a add total of set bits.

### Checking for errors using the pairity bits
>In order to check if there are any errors in the binary segment, 
>1. we will count all the set bits that a pairty bit covers, and in the case of a _even pairity_, 
>1. if the total of set bits is _even_, then all the bits for that pairity bit are correct. 
>1. we continue checking all the pairity bits, and take notice of the seat number of the pairity bits where the total number of set bits was not even.
>1. When we're done, we can sum up the seat-numbers of the pairity bits that were incorrect, and that will point as directly at the bit that needs to be corrected.

### Example of a error handling with pairity bits
>s![Pairity bit](./images/io-err-pairity.png)
>* The orignal bite string is `0100`, and with the pairity bits, it becomes `1001100`. 
>* The pairity bits (colored squares on image) would give us information based on all the bits it covers.
>    * The pairity bit is set based on if we are doing a _odd pairity_ or _even pairity_.
>    * The example is doing a __even pairity__, where the pairity bit is set if we need 1 more bit to make the total set bits a even number.
>        * This allows us to narrow down where the incorrect bits might reside, based on if the paired bit is correct or not, for the bits they point to.
>        * Here we can see that the 2nd bit is correct, it covers a even number of set bits, but 1st and 4th pairity bits do not match, they should be covering a odd number of set bits, but they are actually even. 
>    * The sum of the seats of the incorrect pairity bits is a pointer to the seat where the incorrect bit is sitting, and flipping it should correct the situation.

## Bad block detection and handling

>>Magnetic disks and other mass storage devices organize data into sectors that can be read or written with a single I/O operation. Thus from the OS's point of view, a storage device is a linear sequence of logical blocks b[0] ... b[D-1], where D is the total number of blocks on the device. The blocks are mapped to sectors, preferably in an adjacent ascending order to facilitate fast sequential access.
>>
>>A __bad block (bad sector)__ is a storage block that is no longer reliable for storing and retrieving data due to physical damage. An ECC is associated with each block, which allows the detection and automatic correction of some number of corrupted bits in the block.
>>
>>Whenever a block is written to the disk, the controller computes and stores the ECC along with the block. Whenever a block is read from the disk, the controller computes the ECC and compares the value with the stored ECC. A bad block is detected when the two values disagree.
>>
>>A transient r/w error may be corrected by repeating the same r/w operation. If the error persists, the block is marked as permanently damaged. To maintain the logical sequence of blocks b[0] ... b[D-1] without gaps, the disk provides some number of spare sectors on each track. The damaged block, b[i], can be remapped to a spare sector in one of two ways:
>>
>>    * __Sector forwarding__ is a technique where a bad disk block b[i] is mapped to one of the spare sectors. The solution is fast and simple but the drawback is that the logical blocks are no longer mapped to consecutive sectors, resulting in additional revolutions of the disk.
>>    * __Sector slipping__ is a technique where all blocks following a bad block b[i] are shifted to the right. The last block is mapped to a spare sector and b[i] is mapped to the sector occupied previously by block b[i+1]. The approach requires more work initially but maintains the sequential order of all blocks.

![Bad block](./images/io-err-badblock.png)

Bad blocks are usually physical damage to the disk (imagine scratches, or damage due to aging hardware), but since the rest of the disk is still functional, we can implement ways to just work around those bad blocks.
* __Sector forwarding__ just moves the data that was supposed to be in the bad block into a spare sector. This means that the disk needs to run extra rotation for each relocated sector to be able to read them all in the correct order.
* __Sector slipping__ will shift all the sectors following the bad block by one sector, and this way the order of the sectors remains intact.

## Stable storage
>>When a disk block becomes inconsistent due to a media failure or a system crash, the data stored in the affected block is lost. Some application (Ex: banking databases) cannot tolerate any loss of data.
>>
>>Stable storage is an approach to data management that uses redundancy, along with a strict protocol for reading, writing, and error recovery, to guarantee that all data remains consistent in the presence of media and crash failures.
>>
>>A stable storage uses two independent disks, A and B, which contain identical copies of all data. ECCs are associated with all blocks to detect inconsistent blocks. Since the two disks fail independently, one of the disks always contains a valid copy of any block, which can be used to restore the corresponding block on the other disk.
>>
>>A stable read guarantees to return a valid copy of any block.
>>
>>A stable write guarantees that every block is updated atomically. After the write, the block contains either the old or the new value but never a partially updated value.
>>
>>### The stable storage protocol.
>>* Stable read 	
>>    * Read block b from disk A and from disk B.
>>    * If the two copies are different, go to Media recovery
>>* Stable write 	
>>    * Write block b to disk A.
>>    * If successful, write block b to disk B.
>>    * If either write fails, go to Media recovery.
>>* Media recovery 	
>>    * Remap the bad block to a spare block.
>>    * Copy the still correct value from the other disk to the spare block.
>>* Crash recovery 	
>>    * If a crash occurred while writing to disk A, copy the still valid block from disk B back to disk A.
>>    * If the crash occurred while writing to disk B, copy the new correct block from disk A to disk B

![Stable disk](./images/io-err-stable.png)


## RAID
>>One approach to increasing the rate at which data can be extracted from or written to mass storage devices is to use multiple independent disks working in parallel.
>>
>>Striping is a technique where a sequence of data blocks, b[i], is distributed over n disks, such that disk[i] contains block b[i] modulo n. Ex: With 3 disks, blocks 0, 3, 7, ... are mapped to disk 0, blocks 1, 4, 8, ... are mapped to disk 1, and blocks 2, 5, 9, ... are mapped to disk 2. As a result, 3 blocks can be accessed concurrently, resulting in a 3-fold speedup of the storage.
>>
>>Striping can also be applied at finer levels. Individual bytes or even bits can be distributed over multiple disks in a modulo n fashion, which increases access time to individual words.
>>
>>A __RAID (Redundant Array of Independent Disks)__ is a set of disks viewed by the OS as a single mass storage device.
>>
>>Since the probability of media failures increases rapidly with the number of disks, redundancy is used to decrease the probability of data loss. Several classes of RAIDs exist, distinguished by the amount, type, and location of the redundant data provided.
>>
>>    * The amount of redundant data can range from a single parity bit per block to a full duplication of the entire block.
>>    * The type of redundant data determines how many bit-errors can be detected and/or automatically corrected.
>>    * The redundant data can be segregated on separate disk units or distributed throughout all disk units.

![RAID](./images/io-err-raid.png)
* [RAID - wikipedia](https://en.wikipedia.org/wiki/RAID)
* Stripping improves the performance of a RAID, because stripping allows concurrent access to multiple data items.
* Adding pairity or ECC to the RAID improves it's reliability. Parity can detect erroneous bits, ECCs can both detect and correct some number of erroneous errors.