# Hardware-software interface

## The I/O hierarchy
>>A general-purpose computer system contains I/O devices for human-computer interaction, for secondary storage, and for networking. Devices are attached to the system using controllers. A __device controller (device adapter)__ is an electronic circuit capable of operating a specific I/O device using binary signals. The interface to a device controller is a set of hardware registers and flags, which may be set by and/or examined by device drivers.
>>
>>A __device driver__ is a device-specific program that implements I/O operations, requested by user applications or the OS, by interacting with the device controller. Since devices vary greatly in speed, latency, and the size of data being transmitted, the ability to perform random vs sequential access, and other aspects, device drivers are supplied by the device manufacturers.
>>
>>To be able to incorporate new devices into a system without modifications to the OS, the I/O system supports a small set of generic API instructions. The typical subdivision includes instructions for:
>>
>>    * Block-oriented devices (magnetic disks, CD ROMs, FLASH disks)
>>    * Character-oriented devices (keyboards, pointing devices, cameras, media players, printers)
>>    * Network devices (modems, Ethernet adapters)
>>
>>The driver of any new device must implement the generic I/O instructions of an API for the specific device. 

![IO hierarchy](./images/io-hierarchy.png)

* The I/O software (orange blocks) are always directly realted to the drivers 

## Interface to device controller

>>A generic interface to a device controller consists of a set of registers:
>>| Register | Use |
>>| --- | --- |
>>|Opcode 	|The register specifies the type of operation requested. Ex: read or write. Storing a new value into the register starts the I/O operation.
>>|Operands 	|One or more operand registers are used to describe the parameters of the requested operation. The values are written by the CPU prior to starting the operation.
>>|Busy 	|The register (a 1-bit flag) is set by the controller to indicate whether the device is busy or idle.
>>|Status 	|The register is set by the controller to indicate the success or failure of the last I/O operation.
>>|Data buffer 	|The data buffer holds the data to be transferred between the device and main memory. Depending on the device type, the buffer may hold a single character or a block of data.


### Programmed I/O with polling
>>__Programmed I/O__ is a style of I/O programming where the CPU, running the device driver, performs the copying of all data between the I/O device controller and main memory.
>>
>>__Polling__ is a technique to determine whether a device is busy or idle by reading a flag set and reset by the device controller.
>>
>>To perform programmed input with polling, the CPU first issues an input request to the device controller by writing a new value into the opcode register. Then the CPU repeatedly polls the busy flag until the operation completes. If the operation was successful, the CPU copies the data from the controller buffer to main memory.
>>
>>Programmed output with polling is analogous. When the device is not busy, the CPU copies the data from main memory to the controller buffer and issues an output request. The CPU then polls the busy flag until the operation completes. If the operation was successful, the CPU may proceed with the next output operation.

* Polling -> keeps asking if device is busy
* If status was successful, then we can copy data from buffer to wherever the driver want's the data.

![Programmed I/O Polling](./images/io-polling.png)
1. Driver checks if device is busy (by talking to it's controller)
1. If available, it can set the operands and write the Opcode
1. Device sets to busy and starts running the code
1. The driver keeps checking in a loop if the device is still busy
1. The device fills the buffer with the data
1. When it's done, then it changes the status and is no longer busy
    1. If status was successful, then driver can move the data from buffer to memory.

## Programmed I/O with interrupts
The interface is essentially the same as with polling, but instead of repeat checking if busy, the process is simply blocked until the device is done.

>>When interrupts are used for I/O processing, the interface to the controller remains the same, but the controller is equipped with the ability to issue interrupts to the CPU. The operand and opcode registers are used to describe and start an I/O operation. The status register indicates the success or failure of the last operation. The controller buffer holds the data transferred to or from the device.
>>
>>The busy flag is still present but is used only initially to determine whether the device is available to accept a new I/O request. Then, after starting the I/O operation, the current process blocks itself, instead of repeatedly polling the busy flag to detect the termination of the data transfer. The controller issues an interrupt when the operation has terminated, which reactivates the blocked process. The process examines the status of the operation and, depending on the outcome, proceeds with the next I/O operation or takes appropriate corrective actions.

![I/O with interupts](./images/io-interupts.png)

1. Driver writes the operands and opcode into a available controller
1. The device is set to busy, and blocks the driver process
1. Once the device is done filling the buffer and set status, it stops being busy.
1. The device signals the driver that it can continue
1. Driver reads the status, and if successful, copies the data from buffer to memory.

### I/O with direct memory access
This is essentially like the I/O with interupts, but instead of having the process move the data from buffer every time it fills, the device has direct access to main memory, and can empty its buffer directly to the memory and only has to signal the process (interupt the CPU) when the whole process is done.

* DMA controller is a hardware device capable of reading and writing to main memory

>>With programmed I/O, the CPU needs to transfer all data between the controller buffer and the main memory. The resulting overhead is acceptable with slow, character-oriented devices, but to liberate the CPU from the frequent disruptions caused by fast devices, direct memory access may be used.
>>
>>A __direct memory access (DMA)__ controller is a hardware component that allows devices to access main memory directly, without the involvement of the CPU. Using DMA, the CPU only initiates a data transfer, which can consists of a line of characters, a block of data, or even multiple blocks of data, as part of a single I/O operation. The process executing the device driver then blocks itself, which frees the CPU to serve other processes in the meantime. The device controller in collaboration with the DMA controller executes the I/O operation by transferring the data directly between the device and main memory. When the operation terminates, the device controller issues an interrupt to reactivate the blocked process.

![Direct memory access](./images/io-direct-memory.png)

## Polling vs interrupts
* Polling is best suited for dedicated systems (single process) and fast devices.

>>Polling and interrupts both result in overhead during I/O processing but the sources of the overhead are different. Executing a single poll requires only a few machine instructions. On the other hand, blocking and later reactivating a process using an interrupt constitutes significant CPU overhead.
>>
>>Polling is a good choice in dedicated systems, where only a single process is running. The CPU can busy-wait by executing a polling loop because no other computation is available to use the CPU in the meantime. Polling is also suitable for devices that complete an I/O operation within a few microseconds. Ex: non-volatile solid-state (flash) memories. With such very fast devices, a context switch using interrupts would be more time consuming than a short polling loop.
>>
>>In a general purpose multi-process environment, interrupts are a better choice with most devices. Blocking a process after issuing an I/O instruction and later processing the interrupt to reactivate the process represents constant, predictable overhead for each I/O instruction. The device is restarted immediately after completing the data transfer and CPU time is never wasted on long busy-loops when a device is slow in responding.

![Polling vs interupt](./images/io-poll-vs-interupt.png)
* Gray arrows are polling
* If the device takes long, then the application times out to allow other process to use the CPU.