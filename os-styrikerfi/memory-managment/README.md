[...back](../README.md)
# Memory Managment
>When we talk about __memory__ in this chapter, we will be refering to the __RAM__ (random access memory), and __disk__ is the harddrive (long-term storage), just so things are a bit less confusing.

* ### [Requirements for efficient memory management](memory-requirements.md)
    * Logical vs physical memory
    * Program transformations
    * Relocation and address binding
    * Implementing dynamic relocation using relocation registers
    * Free space management
* ### [Managing insufficient memory](memory-insufficient.md)
    * Memory fragmentation and the 50% rule
    * Swapping and memory compaction
    * Dynamic linking and sharing
* ### [Paging](memory-paging.md)
    * Principles of paging
    * Logical and physical addresses
    * Address translation
    * Internal fragmentation and bound check
* ### [Segmentation and paging](memory-segments.md)
    * Principles of segmentation
    * Address translation
    * Segmentation with paging
    * The translation lookaside buffer

    
## Extra notes
> Use the programmers mode on the calculator, it lets you set/see the binary
#### Calculating the max address
* The max number of pages for segment: 
    * divide the segment size with page size and round it up.
    * Subtract 1 to get correct page number of max page.
* The max word on last page: `(((maxNumPages - 1) * pageSize) - segmentSize)-1` 
    * we need to subtract one, because the indexes start from zero.

>Only subtract 1 if you are actually finding the indexes, but not if the question is how many pages, or how many words offset, and such. 
* If you are finding the page of an address, then you divide the address with the page size. Don't forget the word offset.

#### Logical addresses
* (seg, page, word)

#### Virtual Address translation
> Translating from physical address
* Just assume all segment and page numbers given are indexes! So you don't have to be offsetting things to look at the contents of the wrong frames... 
* Frame numbers would be the indexes, rather than the actual address
* Dont forget the frame sizes, when doing page offsets in address