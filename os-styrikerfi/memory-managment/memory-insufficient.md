# Managing insufficient memory

## Memory fragmentation and the 50% rule
* ### External memory fragmentation
    >It is the loss of usable memory space due to holes between allocated blocks of various sizes
    * In __Stable state__, the ratio between the number of holes and number of occupied blocks is __fixed__ and cannot be reduced by different allocation strategies
        * On average every release of a block is followed by a allocation of a new block.
* ### __The 50% rule__
    > if the probability of finding an exact match for a request approaches 0, then _1/3_ of all memory partitions are holes and _2/3_ are occupied blocks.
    * Formally: __n = 0.5 m__
        * __n__ is the number of holes
        * __m__ is the number of occupied blocks
    * It only referes to the number of holes, but __not__ the amount of space
    * If the average hole size is equal to the average occupied block size, then _1/3_ of memory space is wasted. 
    * A smaller average hole size implies better memory utilization, since less memory is wasted on holes.

>![Allocation of memory](./images/mem-allocation.png)
>* If we release block `A`, which is surrounded by holes, then we are technically removing one of the holes by merging them.
>* If we release block `B` or `C`, where there is allocated memory on one side, and empty space on the other, then the number of holes does not change.
>* If we release block `D`, then we are technically creating a new hole
>
> Combining all these releases, then we see that the number of holes didn't actually change.  
>
>Same goes for when we __allocate__ memory, since it is very unlikely that it'll ever find a perfectly fitting hole, so it only decreases the size of existing holes.


## Swapping and memory compaction

>> When no hole large enough for a request is available, the process can either be delayed until more memory is released or two possible actions may be taken to create more space:
>> 
>> Swapping is the temporary removal of a module from memory. The module is saved on a disk and later moved back to memory. Dynamic relocation is necessary so that the module can be placed into a different location without modification.
>> 
>> Memory compaction is the systematic shifting of modules in memory, generally in one direction, to consolidate multiple disjoint holes into one larger hole.

>![Memory compacting](./images/mem-compacting.png)
>* __Swapping__ will temporarily move allocated memory out of the way (from RAM to Disk) in order to create space large enough for the new request.
>    * There is no guarantee that the temporarily displaced memory will be placed back in it's previous location (probably very unlikely), so we will need to have __dynamic relocation__ so that the associated program can still find what they allocated.
>    * Moving things to and from _disk_ is slower than moving things within the memory
>* __Compaction__ will move allocated memory to remove the empty spaces between them (combining them into a larger hole).
>   * It might seem inefficient to always move all the existing  


##  Linking and sharing
### Linking
__Linking__ is the act of resolving external references among _object modules_ (connecting them together into a single program), and this can be done _statically_ before loading or _dynamically_ while the program is already running.

>#### Static linking
>This requires all modules referenced by the program to be loaded to memory while the program is running, regardless if the program will use the module or not.
>#### Dynamic linking
> This improves memory utilization by postponing any loading of modules/libraries until the program actually calls them for the first time (meaning they do not need to load them again later).
### Sharing
__Sharing__ extends the efficiency of the system by allowing different programs to share the same library functions, instead of creating copies of the function in memory for each program that requests it.

Even though the programs would be sharing functions, they would still each have their own _static_ and _dynamic_ data in memory even though they are sharing a portion of their code.

__Sharing is possible with both statically and dynamically linked programs__

>#### Example of Dynamic linking and sharing: 
>![Memory sharing/linking](./images/mem-sharing.png)
>
>For example, _Microsoft Windows_ provides hundreds of libraries (called Dynamic-linked libraries or DLLs), each containing hundreds of support functions. Normally only a small number of those libraries are referenced, so instead of loading them all to memory prior to the program execution,
>* we simply leave a placeholder __stub__ in the code that points to the library function. 
>* Only when the program reaches the point where it needs to use that function will it actually load it to memory (if it's not already there).
>* The __stub__ is then replaced with the actual reference to the function in memory (so it doesn't have to repeate the process of finding it).


## Address translation

>>With paging, a logical address is broken into two components: a page number p and an offset w within the page. Similarly, a physical address is broken into two components: a frame number f and an offset w within the frame. The notations (p, w) and (f, w) denote the concatenation of the two address components.
>>
>>Ex: If p is 4 bits and w is 5 bits, then (2, 6) denotes the 9-bit address 0010 00110, which is the concatenation of the two components, 2 and 6, in binary.
>>
>>The OS must translate logical addresses of the form (p, w) into corresponding physical addresses (f, w): 


## Internal fragmentation and bound check

>>Paging avoids external fragmentation by having all pages and page frames the same size so that any page fits into any frame without creating holes between frames. However, the size of a program is generally not an exact multiple of the page size and thus some space remains unused at the end of the last page.
>>
>>Internal fragmentation is the loss of usable memory space due to the mismatch between the page size and the size of a program, which creates a hole at the end of the program's last page.
>>
>>Most programs also do not need all pages of the entire logical address space and must be prevented from accessing any page not belonging to the program. Some systems implement a valid-bit in each page table entry and prevent access to pages not belonging to the program. But the valid bit does not protect the unused fragment on the last partially filled page. A better approach is to compare every logical address to the program size to prevent access to any memory locations not belonging to the program.
