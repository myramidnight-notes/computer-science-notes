# Paging

## Principles of paging

>>The main disadvantage of dividing memory into variable partitions is external fragmentation, which requires the searching of memory for holes large enough to satisfy each request. In addition, swapping or compaction is necessary when no hole large enough is available.
>>
>>Paging divides both the logical address space of a process and the physical memory (RAM) into contiguous, equal-sized partitions such that any logical partition can be mapped into any physical partition. Searching variable-sized holes becomes unnecessary.
>>
>>A page is a fixed-size contiguous block of a logical address space identified by a single number, the page number.
>>
>>A page frame is a fixed-size contiguous block of physical memory identified by a single number, the page frame number.
>>
>>A page frame is the smallest unit of data for memory management and may contain a copy of any page.
>>
>>A page table is an array that keeps track of which pages of a given logical address space reside in which page frames. Each page table entry corresponds to one page and contains the starting address of the frame containing the page.

![Pages](./images/mem-pages.png)
## Logical and physical addresses

* ### Address size
    > The size of the address space is __2<sup>n</sup>__
    * __n__ is the number of bits necessary to form addresses for the entire space.
    * The __page size__: 2<sup>k</sup>
        * __k__ is the number of bits needed to address _every word_ on a page.
    * The __number of pages__: 2<sup>n-k</sup>
* ### Partitioning of logical address
    > The partitioning of the __logical__ address space divides the address into 2 components:
    >* The higher order __n - k__ bits specify the page number `p`
    >* The lower order __k__ bits specify the offset `w` within the page
* ### Partitioning of physical memory
    > Physical memory is partitioned in an analogous manner.
    >* __m__ bits in the __physical__ address, __2<sup>m</sup>__ total physical addresses can be formed.
    >* The frame size must be equal to the page size
    >* The remaining __m-k__ bits determine the number of frames: __2<sup>m-k</sup>__

>![Pages physical addresses](./images/mem-pages-address.png)
>* `p` represents the page number, and `w` represents each word within the page
>* This picture shows two possible ways of dividing a logical address into pages. We can have a large pace, requiring more bits to track each word on that page, or we can have more pages, using fewer bits to track each word.
>* Regardless of how we split up the addresses into pages, the sizes of the pages need to have a identically sized frame within the physical memory.
>* A page table will keep track of the mapping between logical addresses and physical addresses of these pages/frames.
