
>> (double-quote-indent, haven't gotten a chance to "process" these in my own words)
# Requirements for efficient memory managment
## Logical vs physical memory

* ### Pysical memory
    > A computer's __physical memory (RAM)__ is a hardware structure consisting of a linear sequence of words that hold a program __during execution__. 
    * A __word__ is a fixed size unit of data, typically consisting of 4 bytes.
        * 8 bits = 1 byte. 4 bytes = 32 bits, hence the 32 bit system.
        * A 64 bit system of course has 64bit words then
* ### Physical addresses
    > A __physical address__ is an integer in the range of _[0 : n-1]__ that indicates a word in physical memory of the size __n__.
    * The address is composed of a fixed number of bits.
    * Address size: __n= 2<sup>k</sup>__
        * __n__ would be the memory size
        * __k__ would be the number of bits required to access all the memory
* ### Logical address space
    > A program will never know beforehand where it'll be stored in physical memory, so the concept of __logical address space__ was developed, which is a abstraction of the physical memory, consisting of a sequence of imaginary memory locations in the range of _[0:m-1]_
    * __m__ is the size of the logical address
    * The logical addresses are mapped onto the physical memory
    * This can give us the illusion of data being stored in order, while it's actually spread around the physical memory.
* ### Logical addresses
    >A logical address is an integer in the range [0 : m-1] that identifies a word in a logical address space. Prior to execution, a logical address space is mapped to a portion of physical memory and the program is copied into the corresponding locations.

![Logical space](./images/mem-logical-space.png)
* When we run a program, we never know where things will be stored in physical memory beforehand (it all depends on the hardware, how much memory is currently being used... ). 
* A program will make use of a __logical address space__ that is abstraction of the physical memory (virtual memory) which is then mapped 

## Program transformations
A program goes through quite a few transformations in order to go from __code__ to __execution__.
* __Source module__
    > This is a __program__ writen in a _symbolic/programming language_, such as `C` or `Python`. It needs to be compiled into __machine code__ in order for the machine to be able to _execute_ it (run the program).
* __Object module__
    > For each _source module_, there will be a _object module_, which have been translated by a _compilor_ into the __machine code language__ that the __processor__ can understand.
* __Load module__
    >A __linker__ will take all the _object modules_ that compose the program and combines them into a single __load module__
    >
    > The __load module__ is then ready to be loaded into the __main memory__ for _execution_.

>![Transformations](./images/mem-transformations.png)
>* There exist __system libraries__ in the form of _object modules_ that may be included in the __load module__ by the _linker_, _loader_ or during _execution_.

## Relocation and address binding
>>Since the physical addresses of the program components are not known until load time, the compiler/assembler and the linker assume logical address spaces starting with address 0. During each step of the program transformation, the logical addresses of instructions and data may be changing. Only during the final step of program loading are all logical address bound to actual physical address in memory.
>>
>>Program relocation is the act of moving a program component from one address space to another. The relocation may be between two logical address spaces or from a logical address space to a physical address space.
>>
>>Static relocation binds all logical addresses to physical addresses prior to execution.
>>
>>Dynamic relocation postpones the binding of a logical address to a physical address until the addressed item is accessed during execution.

>![Static and Dynamic relocation](./images/mem-static-relocation.png)
>* __Static relocation__
>    > In __static relocation__, all memory locations are known in the code when the module is in the memory, "hard-coded" so to speak.
>* __Dynamic relocation__
>    > In __dynamic relocation__, the memory locations are not changed within the module when it is loaded, but instead the difference between __physical address__ and __logical address__ is adjusted whenever the program tries to access the memory.    

## Implementing dynamic relocation using relocation registers
The __relocation register__ contains a pointer to the starting address of a program or component in memory. The value of this register is then simply added to any address being referenced within the program/component, to adjust the offset of the actual location within the memory.

### 3 main components of a program
> Most programs consist of 3 main components
>1. Code
>1. Static data
>1. Dynamic data (variables and mutable data)

* #### Simple memory management
    >>A simple memory management scheme treats the 3 components as one unit, which must be moved into one contiguous area of memory. Dynamic relocation can be implemented using a single relocation register, which is loaded with the program's starting address. The register content is then added automatically by the CPU to every logical address of the program to generate the physical address.
* #### Flexible memory managment
    >>A more flexible management scheme treats the 3 components as separate modules, each of which may reside in a different area of memory. Three relocation registers, each loaded with the starting address of one of the modules, then accomplish dynamic relocation by being added to all logical addresses at runtime.

We can have a __relocation register__ (`RR`) within the CPU that manages this address relocation, so that the program can simply stick to using __logical addresses__, and whenever the `RR` is called, it will return the actual __physical address__ that is being accessed.
>![Relocation Register](./images/mem-relocation-reg.png)
>* __Relocation registers:__
>    * __CR__ for code relocation
>    * __DR__ for static data relocation
>    * __FR__ for dynamic data relocation


## Free space managemen

>>As programs are moved into and out of memory at runtime, the memory space becomes a checkerboard of free and occupied areas of different sizes. The OS keeps track of all free spaces, referred to as holes, using a linked list and must find a hole of appropriate size whenever a new program component is to be loaded into memory.
>>
>>The OS must also coalesce any neighboring holes resulting from the removal of programs from memory to prevent the memory from becoming a fragmented collection of increasingly smaller holes.


### Some search strategies:
>![Space managment](./images/mem-space-managment.png)
>
>* __First-fit__ 
>   * It will search from the beginning of the list for the first empty space that can fit the request in order to allocate memory.
>    * This will lead to alot of small gaps eventually
>* __Next-fit__ 
>   * It is similar to _first-fit_, but instead of going from the beginning of the list, there will be a memory pointer keeping track of where it allocated memory previously (green arrow in the image).
>    * This has the same issue as _first-fit_ since their behaviour is so similar, and the memory is essentially a circle, going back to the beginning when we reach the end.
>* __Best-fit__ 
>   * It will compare all of the available spaces, and find the __smallest possible space__ that can fit the request.
>   * This will lead to alot of tiny gaps, since any request rarely finds a perfect fit.
>* __Worst-fit__ 
>   * It will simply locate the largest empty space, and allocate the memory there.
>   * This might prevent any large requests in the future to fit anywhere.

>>Simulation of different strategies have not produced a clear winner. The choice depends on the average request size relative to the memory size, and the variance of request sizes.
