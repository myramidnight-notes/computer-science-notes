# Segmentation and paging

## Principles of segmentation

>>With paging, all components of a program, including code, static data, the stack, and other data structures, are combined into one contiguous address space divided into fixed-size pages. Since the components are of variable sizes, the boundaries between the logical components do not coincide with page boundaries. The misalignment makes the sharing and the protection of logical components difficult.
>>
>>Segmentation addresses the problem by providing multiple logical address spaces for each process, where each segment can have a different size.
>>
>>A segment is a variable-size block of a logical address space identified by a single number, the segment number.
>>
>>With pure segmentation (no paging), a segment occupies a contiguous area of physical memory and is the smallest unit of data for memory management.
>>
>>A segment table is an array that keeps track of which segment resides in which area of physical memory. Each entry corresponds to one segment and contains the starting address of the segment.

![Principals of segmentation](./images/seg-principals.png)


## Address translation
>>Similar to paging, segmentation breaks each logical address into two components: a segment number s and an offset w within the segment. With an n-bit physical address, __2<sup>n</sup>__ addresses can be formed. If k bits are used for the offset w, then __2<sup>n-k</sup>__ segment numbers can be formed.
>>
>>A segment table is analogous to a page table but with two important differences:
>>
>>*    With paging, all pages have the same size, __2<sup>k</sup>__. Since segments have variable sizes, __2<sup>k</sup>__ cannot be the actual segment size but only the maximum possible segment size. To prevent access to locations beyond the segment, the segment size must be kept in the segment table entry and checked during address translation.
>>*    With paging, the starting address of a frame can be determined from the frame number f, since all frames have a fixed size and thus all start with the address (f, 0). With segmentation, no frames exist and thus a segment table entry must contain the complete starting address of the segment, rather than just the segment number s.
>>
>>>The OS then translates logical addresses of the form (s, w) into corresponding physical addresses:
>>>
>>>    1. Given a logical address (s, w), access the segment table entry at offset s and get the segment's starting address, sa, and size, sze.
>>>    1. If w ≥ sze then reject the address as illegal.
>>>    1. Otherwise, add w to sa to form the physical address, sa + w, corresponding to the logical address (s, w).

![Segment address translation](./images/seg-address-translate.png)


## Segmentation with paging

>>The main advantage of segmentation is the ability to create multiple variable-size address spaces. The main advantage of paging is the ability to place any page into any frame in memory. To gain the advantages of both, segmentation can be combined with paging.
>>
>>The logical address space consists of multiple variable-size segments but each segment is divided into fixed-size pages. A logical address is then divided into 3 components: a segment number s, a page number p, and an offset w within the page. A physical address is divided into 2 components as before: a frame number f and an offset w within the frame.
>>
>>Every process has one segment table where each entry points to a page table corresponding to one of the segments and each page table entry points to a page frame.
>>
>>>The OS then translates logical addresses of the form (s, p, w) into corresponding physical addresses (f, w):
>>>
>>>1.    Given a logical address (s, p, w), access the segment table at offset s to find the page table and the size, sze, of segment s.
>>>1.    If (p, w) ≥ sze then reject the address as illegal. Otherwise, access the page table at offset p and read the frame number, f, of the frame containing page p.
>>>1.    Combine f with the offset w to form the physical address, (f, w).

![Addressing with segments and pages](./images/seg-address-translate-pages.png)


## Mapping the logical address space to physical memory

>>Each page of the logical address space is mapped to one page frame. Segment and page tables are also mapped to page frames. Depending on the size of each table entry, a table can occupy one or more frames.
>>
>>The number of entries in the segment table determines how many segments can be represented. The number of entries in a page table determines how many pages a segment can consist of.
>>
>>If all tables occupy a single frame and all table entries occupy one word, then the size of the 3 address components (s, p, w) is identical. Ex: if the frame size is 1024 words, then the size of s, the size of p, and the size of w is 10 bits each. Consequently, the local address space can have up to 1024 segments, and each segment can have up to 1024 pages of 1024 words each.
>>
>>If a table entry is larger than one word or if a table occupies more than one frame, then the size of the corresponding address component (s or p) is larger.

![Mapping in physical memory](./images/seg-address-physical-mem.png)


## The translation lookaside buffer

>>Paging and segmentation both double the number of memory accesses because the address translation must first access the segment or page table. When segmentation is combined with paging, the number of memory accesses triples: First the segment table is accessed, then a page table, and finally the page holding the addressed word.
>>
>>A __translation lookaside buffer__ (TLB) is a fast associative memory buffer that maintains recent translations of logical addresses to frames in physical memory for faster retrieval.
>>
>>When a logical address (s, p, w) has been translated to a physical address (f, w), the TLB saves the frame number f together with the address component (s, p). When another address in the same segment and page, (s, p, w'), is to be translated, the TLB retrieves the frame number f associated with (s, p) without any memory access. The new offset w' is then added to f to form the physical address.
>>
>>The TLB can store only a small number of translations. When all entries are full, a new translation must replace an existing one. The __principle of locality__ states that locations accessed recently are more likely to be accessed again than locations accessed in the distant past. The TLB uses the principle of locality to always replace the least recently accessed entry.
>>
>>The TLB's __hit ratio__ is the fraction of memory accesses that find a match in the TLB. The higher the TLB hit ratio, the lower the overhead of address translation.

![lookaside buffer](./images/seg-lookaside-buffer.png)