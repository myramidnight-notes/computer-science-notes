[...back](../README.md)
# Operating Systems
* [__Role of operating systems__](os-basics.md)
    * Bridge between hardware capabilities and user needs
    * Extended Machine
    * Resource Manager
        * time-sharing and multitaksing
* [__The OS Structure__](os-structure.md)
    * A hierarchical organization
    * The shell 
    * System calls and supervisor calls
    * Interrupts and traps
* [__Evolution and scope of Operating Systems__](os-evolution-scope.md)
    * The five generations of computer systems 
    * Types of OSs