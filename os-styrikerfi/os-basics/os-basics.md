[...back](README.md)
# Operating Systems (Stýrikerfi)

## Bridge between hardware capabilities and user needs
The hardware and CPU just execute simple machine instructions, but the user wants to do high level actions. It is the job of the _operating system_ to bridge this gap, to translate what the user wants to do into something the machine can execute.

Computers can be made to do complex things, and the user wants to make use of it without having to be exposed to the complexity that goes on behind the scenes that allows us to do complex things with very low-level commands (that basically just understands `true`/`false`, which binary is at the core, `1`/`0`).
    
### Mismatch between hardware capabilities and user needs.
|Hardware component 	|Hardware capabilities 	|User needs
|--- |--- |---
|CPU 	|Machine instructions perform operations on contents of registers and memory locations. 	|The user thinks in terms of arrays, lists, and other high-level data structures, accessed and manipulated by corresponding high-level operations.
|Main memory 	|Physical memory is a linear sequence of addressable bytes or words that hold programs and data. 	|The user must manage a heterogeneous collection of entities of various types and sizes, including source and executable programs, library functions, and dynamically allocated data structures, each accessed by different operations.
|Secondary storage 	|Disk and other secondary storage devices are multi-dimensional structures, which require complex sequences of low-level operations to store and access data organized in discrete blocks. 	|The user needs to access and manipulate programs and data sets of various sizes as individual named entities without any knowledge of the disk organization.
|I/O devices 	|I/O devices are operated by reading and writing registers of the device controllers. 	|The user needs simple, uniform interfaces to access different devices without detailed knowledge of the access and communication protocols. 

## OS as Extended Machine
__Abstraction__ is used alot to make the given commands seem less complex than they actually are, hiding unimportand or overly complex details. This is an attempt at making the computer commands __human understandable__, because human's have a difficult time thinking in _binary_ and _system assembly_.

__Virtualization__ is also used to give the illusion that the hardware is structured in a way that is easier to comprehend and access, such as splitting up a harddrive into multiple drives when it's still just a single harddrive. Or that files are stored in specific folders while it's actually spread around a linear binary memory.

![Extended Machine](./images/os-extended-machine.png)

## The OS as a resource manager

An application executes code stored in main memory, reads data from an input device, and outputs results to a printer or disk. Performance of the application depends on the efficient use of various devices. In addition, performance is affected by the presence of other concurrent applications. One of the main tasks of an OS is to optimize the use of all computational resources to ensure good overall performance, while satisfying the requirements of specific applications, including guaranteeing acceptable response times for interactive processes, meeting deadlines of time-critical tasks, and allowing safe communication among different tasks.

A program typically alternates between phases of input, computation, and output. Consequently, the CPU is underutilized during the I/O phases, while the I/O devices are idle during the compute phase. The overall performance of all applications can be improved by using parallelism. The OS strives to keep the CPU, main memory, and all storage and I/O devices busy at all times by overlapping independent operations whenever possible.

![Resource manager](./images/resource-manager.png)
* ### Multiprogramming 
    >Multiprogramming is a technique that keeps several programs active in memory and switches execution among the different programs to maximize the use of the CPU and other resources. Whenever a program enters an I/O-bound phase where little CPU time is needed, other programs can resume and utilize the CPU in the meantime. Similarly, whenever a device completes an operation, the OS activates computations that can utilize the now available device.
* ### Time-sharing (multitasking)
    >Time-sharing is an extension of multiprogramming where the CPU is switched periodically among all active computations to guarantee acceptable response times to each user. Time-sharing employs the concept of virtualization by creating the illusion of having a separate virtual CPU for each computation.
