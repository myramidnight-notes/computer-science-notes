[...back](README.md)
# Evolution and scope of Operating Systems
## The 5 generations of computer systems
* First digital computers came in the _1940's_ with no OS.
* Various innovations in technology and the growth of user needs pushed the evolution of the OS
* _Moore's_ law: number of transistors in a circuit doubles about every two years
* Each generation of computer systems last _between 10-15 years_, each associated with a major shift in hardware technology.

![OS evolution](./images/os-evolution.png)

## Types of Operating Systems
* Computers are now imnipresent in all aspects of life
* Different types OSs are required for different purposes and enviorments
* The requirements on OS generally coincide with the size and complexity of the OS

![Types of OS](./images/os-types.png)