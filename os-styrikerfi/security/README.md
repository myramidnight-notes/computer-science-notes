# Protection and Security
* ### [Security goals and threats](security-goals-threats.md)
    * Types of security violations 
    * Insider attacks 
    * Exploiting human weaknesses 
    * Exploiting systems weaknesses 
    * Confining mobile code 
* ### [User authentication](user-authentication.md)
    * Protecting individual passwords 
    * Protecting password files 
    * Using biometrics 
* ### [Access control](access-control.md)
    * The access matrix 
    * Operations on the access matrix to enforce security 
    * Implementation of the access matrix 
* ### [Secure communication](secure-coms.md)
    * Principles of cryptography 
    * Secret-key cryptography 
    * Public-key cryptography 
    * Authentication 
    * Digital signatures