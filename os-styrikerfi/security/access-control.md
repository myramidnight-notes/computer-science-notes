# Access control 

## The access matrix

>>A computer system consists of a set of hardware and software objects, all of which must be protected from misuse. Each object has a unique name, O<sub>i</sub>, and a set of possible operations, op<sub>i</sub>, that may be performed on the object. An access right, r<sub>i</sub>, represents the ability by a process to perform operations opi on O<sub>i</sub>. Ex: The most common rights are r, w, and x, corresponding to the operations read, write, and execute. The owner right, o, designates the creator of an object.
>>
>>A protection domain is a set of pairs `<object, rights>`, where rights specify what operations a process may perform on the object. Ex: `<file1, rx>` means that file1 may be read or executed but not modified.
>>
>>An access matrix is a representation of protection domains, where each row corresponds to one domain D<sub>i</sub>, each column corresponds to one object O<sub>i</sub>, and the intersection of the row and the column records the rights that a process in domain D<sub>i</sub> has with respect to the object __O<sub>i</sub>__).
>>
>>Every process is associated with one domain at a time but the domain may change as the process executes, which dynamically changes the set of objects the process may access.
>>
>>#### Example: Typical domain changes in Unix.
>>
>>*    Every executable file f carries a flag, setuid, which can be set by the owner. When the file is called from a domain D and setuid is set, then the domain changes from D to the domain D' associated with the file f. The domain change allows the caller to temporarily access objects in D'. When execution of f terminates, the domain reverts back to D.
>>*    When a process executes a kernel call, the execution transfers to a system function associated with a domain of the OS, where the process has the ability to execute privileged instructions and access additional objects unavailable to processes at the user level.

![Access matrix](./images/access-matrix.png)
* Each _column_ represents a object (such as a file or script)
* Each row represents the domain (imagine a user account with different permissions)
* If the process `p` creates a new object in the current domain, then that domain becomes the owner of that object (indicated by the `o` right)
    * the owner automatically has full rights, and can decide what if a different domain has rights to that object, and what rights those should be.
* The process itself could switch domains, because the process itself isn't attached to any specific domain. 
    * these switches could be caused by executing kernel calls, where the process might need additional rights to a object.


## Operations on the access matrix to enforce security

>> Each entry A[D, O] in the access matrix A specifies what operations a process associated with a given domain D can perform on the object O. In addition to accessing objects, processes also have the need to modify the matrix itself by adding or deleting objects and/or rights.
>>
>> | Operation 	| Description
>> |---|---
>> |Create object O 	|A new object O may be created by a process in any domain D without any conditions. The process becomes the owner of O. A new column of the access matrix is created and the entry A[D, O] receives all rights rwxo.
>> |Destroy object O 	|An object O can be deleted by a process in D only by the object's owner. The column corresponding to object O is deleted from the access matrix.
>> |Add right r 	|A new right r for object O may be granted only by the object's owner. The right is entered into the entry A[D, O].
>> |Remove right r 	|An existing right r for object O may be removed from the entry A[D, O] only by the object's owner.
>> |Copy right r 	|To control the propagation of rights, a new copy right c is introduced. Unlike all other rights, the c-right does not apply to an object, but to another right r. The holder of r may copy r to another domain only if r carries the copy right c.
>> 
>> Each right or a combination of rights, along with the valid operations on the access matrix, address a different security issue:
>> 
>>*    Information disclosure: absence of the r-right in A[D, O] guarantees that a process in domain D cannot read object O.
>>*    Information modification: absence of w-right in A[D, O] guarantees that a process in domain D cannot modify object O.
>>*    Information destruction: absence of the o-right in A[D, O] guarantees that a process in domain D cannot destroy object O.
>>*    Unauthorized use: a right lacking the c-right cannot be copied into another domain. Thus a process in Di holding the x-right for an object O cannot propagate the x-right to another domain D<sub>j</sub> unless x carries the c-right (denoted as x<sup>c</sup>). Thus an authorized user of a service cannot pass the authorization to another user.
>>*    Revocation of access: The owner of an object must be able to revoke any right granted previously to other domains. The Remove right operation on the access matrix accomplishes the goal.

![Access matrix operations](./images/access-matrix-operations.png)
* Here we have the `c` copy-right, which allows other domains than the owner of the object to copy permissions of that object over to other domains (imagine an admin giving another user access/permissions to use the given object)
    * Only the permission that have the copy-right attached to the permission.
    * We could copy the permission without including the copy-right on the permission, which prevents it from being propagated from the target domain (essentially cannot forward the right to other domains anymore).


## Implementation of the access matrix

>>The access matrix is a large but sparse matrix and serves only as a conceptual underpinning of the actual implementation. To reduce the matrix size, the entries are segregated either along columns or along rows. Using columns reduces the matrix to a set of access lists. Using rows reduces the matrix to a set of capability lists.
>>
>>An __access list (AL)__ associated with an object O contains entries of the form (D, rights), where rights specify what operations a process in domain D may apply to object O.
>>
>>A __capability list (CL)__ associated with a domain D contents entries of the form (O, rights), where rights specify what operations a process in domain D may apply to object O.
>>
>>While the two implementations are dual to each other, each has important advantages and drawbacks. With ALs, each request requires a search of the potentially long AL for a match. With CLs, the mere possession of a capability is sufficient as an authorization to perform the operations. Thus a capability is also referred to as a ticket that authorizes the holder to perform the specified operations.
>>
>>The main drawback of CLs is the difficulty to revoke a right. Since capabilities for a given object are not segregated on a single list but are dispersed throughout the CLs of individual processes, revocation of a right or a deletion of the capability result in an extensive search.
>>
>>Frequently, both ALs and CLs are used in the same system.
>>
>>>#### Example 10.3.2: Combining ALs and CLs.
>>>Before a file can be accessed, the open command checks if the process has the necessary rights to perform the requested operation. The information about who can access the file and what operations are permitted is stored in an access list associated with each file. Once the file is open, the process is given a pointer or an index into an open file table, commonly referred to as a file handle, which may be used to access the file contents. The handle is a capability, used efficiently without any further access verifications.

![Access lists and Capability lists](./images/access-lists.png)


## Extra notes, FTP:
I am guessing that if you have used `ftp` (file transfer protocol) or played around with creating user accounts on a `vps` (virtual private server), then it might a bit easier to see the connections with the topic of access control.

* The domains would be user accounts (or groups)
* The columns would be the files and directories
    * it's nice that files usually inherit permissions from it's parent directory
* If a file/directory didn't have the correct permissions, such as write, then sometimes programs wouldn't be able to do certain things.

> One example would be a program's inability to write to a output file, to discover that either your antivirus or settings might have made it so that any _script_ type of file would not get any _write_ permissions, and only running a program in adminstrative mode would bypass such permissions. 

### Change Mode (`chmod`) for managing permissions
* Each object would have 3 sets of permissions: for the _owner_, the _group_ and _all users_.
* These permissions would be to __read__, __write__ and __execute__
    * the extra `d` at the front of the permission chain indicates if the object is a file or directory.
    * These permissions are actually set as a binary string, and if the bit is set in the corresponding spots decides if you have the permission.
        * That is where the `777` or `775` and such numbered permissions come from. Each digit represents permissions for in the _owner_, _group_ and _all users_. The digit is the decimal value of the binary string for the permissions combination:
        * For example, only having the _read_ and _execute_ perms would be `101` binary, which is `5` in decimal, having all 3 permissions would be the binary string `111`, which equals `7` in decimal. We have different decimal values for the various combinations of those 3 permissions.

