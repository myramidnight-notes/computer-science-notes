# Secure communication

## Principles of cryptography

>>Computers in a network communicate with one another and with I/O devices using communication lines or wireless signals. Some means of communication, such as communication on a public network, are vulnerable to attacks.
>>
>>A __man-in-the-middle attack (MITM)__ is an attack where the attacker secretly listens to, and possibly alters, the communication between two systems.
>>
* #### Cryptography
    >>__Cryptography__ is a technique that allows a sender to transform plaintext into a ciphertext, which the receiver can transform back to the original plaintext. 
* #### Encryption
    >>__Encryption__ is the act of transforming plaintext into ciphertext. 
* #### Decryption
    >>__Decryption__ is the act of transforming ciphertext into the original plaintext. 
* #### Encryption/decryption
    >>__Encryption/decryption__ keys are parameters used by the encryption/decryption functions, respectively. Plaintext encrypted using an encryption key cannot be decrypted without the corresponding decryption key.

### Cryptography
The 3 main goals of _cryptography:
*  __Secrecy__: 
    >>An attacker must not be able to derive the plaintext from a transmitted ciphertext.
*  __Authenticity__: 
    >> An attacker must not be able to fabricate a fake ciphertext, destroy the integrity of a transmitted ciphertext, including changing the identification of the sender, or resend a copy of a previously copied ciphertext.
*  __Non-repudiation__: 
    >>The creator of a ciphertext must not be able to deny having created the ciphertext.

![Authenticity](./images/coms-authenticity.png)
* What all the letters mean:
    * `P` is the plaintext being sent
    * `E` is the encrypter
    * `D` is the decrypter
    * `K` is the keys used for the encrypter/decripter
    * `C` is the encrypted __cyphertext__
* The _red_ lines indicate an attacker trying to do various things with whatever you're sending. We do not want the attacker to be able to do the following things:
    * __extract__ the contents of the file
    * __modify__ the file
    * __replay__ the file (that is sending a copy of the file to the reciever at a later time)
        * this would violite the __authenticity__ of the file
    * create a __fake__ that the reciever can decrypt (thinking it's authentic)

> A __captured__ file is useless unless the attacker can decrypt it, so the capture alone does not violate any security


## Secret-key cryptography

>>__Secret-key cryptography (symmetric cryptography)__ uses the same secret key for both encryption and decryption.
>>
>>The encryption algorithm uses a variety of permutations and combinations of the plaintext and the key to produce the ciphertext. The decryption algorithm reverses the steps to produce the original plaintext.
>>
>>The common secret key is established between the sender and the receiver using a trusted service.
>>
>>Secret-key cryptography guarantees secrecy in that only the holder of the secret key is able to decrypt a ciphertext. Authenticity is guaranteed to only a limited extent, in that an attacker is unable to successfully modify a ciphertext or generate a fake ciphertext that could be decrypted into a valid plaintext. But the approach does not guarantee the sender's authenticity and also does not prevent the resending of a previously captured ciphertext.
>>
>>>#### Example 10.4.1: DES.
>>>The data encryption standard (DES) is one of the best-known secret-key cryptosystems adopted in 1977 as a standard for government and business applications. DES divides the plaintext into blocks of 64 bits, which are encrypted using a 56-bit key in a series of 16 substitutions, permutations, and combinations of the text with the key using the exclusive OR function. With the steadily increasing speed of computers, the DES became vulnerable to brute-force attacks. Rather than developing new transformation algorithms to work with a longer key, the widely accepted solution is to apply the same DES algorithm successively multiple times. Triple-DES uses a sequence of three DES encryptions, each using a separate 56-bit key, to encrypt a block of plaintext P into the corresponding ciphertext C = DES(DES(DES(P, K3), K2), K1).

![Secret keys](./images/coms-secret-key.png)
* Again, `P` is the original plaintext, `K` is the key, `E` is encryptor, `D` is decryptor, `C` is the encrypted cyphertext
* A seceret-key encryption does not guarantee authenticity of the sender, since a capured message can be replayed to the receiver at a later time (so the attacker has become the sender)


## Public-key cryptography

>>Public-key cryptography (asymmetric cryptography) uses different keys for encryption and decryption. The encryption key is made public while the decryption key is kept private. The decryption key cannot be derived from the encryption key, and vice versa.
>>
>>Public-key cryptography is computationally more expensive than secret-key cryptography but one of the main advantages is that no secret key needs to be exchanged through secure channels between the sender and the receiver. Any sender can use the public key of a receiver to send encrypted messages that can only be decrypted by the receiver holding the private key.
>>>#### Example 10.4.2: RSA.
>>>RSA, named after the inventors Rivest, Shamir, and Adelman, is one of the best known public-key cryptosystems. The encryption function has the form: C = E(P) = P<sup>e</sup> mod n where the two integers e and n jointly are the public encryption key. The decryption function has the form: P = D(C) = C<sup>d</sup> mod n where the two integers d and n jointly define the private decryption key. The three integers e, d, and n must be chosen such that d cannot be derived from e and n. The algorithm is based on the fact that factoring n, which is the product of 2 very large prime numbers, is computationally infeasible.

![Public key](./images/coms-public-key.png)
* The attacker has no way of deriving `d` from the `e` and `n`, so they cannot decypher the message being sent.
* Again, this does not guarantee the authenticity of the sender, if the attacker does __replay__.


## Authentication

>>Public-key cryptography can be used for message and sender authentication by reversing the roles of the public and private keys. But a less computationally intensive approach is generally taken.
>>
>>A __message authentication code (MAC)__, is a short bit string attached to a message and used to confirm that the message came from the stated sender and has not been modified in transit. Thus the MAC protects both the message's data integrity as well as the sender's authenticity.
>>
>>A MAC is generated by an algorithm that uses a secret key shared by the sender and the receiver. The algorithm applies a one-way hash function to the plaintext message, which compresses the message body into a unique MAC. The sender attaches the MAC to the message and sends both to the receiver. The receiver uses the same algorithm to generate another MAC from the received message. If the received MAC and the generated MAC are identical, the message is authentic.
>>
>>To prevent an attacker from replaying a captured message at a later time, a timestamp or a sequence number can be attached to, and encrypted along with, the plaintext when generating the MAC. The receiver can check the timestamp or sequence number to detect any duplicate messages.

![Message authentication](./images/coms-message-auth.png)


## Digital signatures

>>Non-repudiation cannot be guaranteed in a secret-key cryptosystem because both the sender and the receiver of a message use the same key and thus a given ciphertext could be produced by either party. A public-key cryptosystem can be used to guarantee non-repudiation. The basic concept is to reverse the roles of the sender's public and private keys: The private key is used for encryption while the public key is used for decryption. If a receiver is able to decrypt a ciphertext using the sender's public key K<sub>S</sub><sup>pub</sup>, then the sender is unable to deny having produced the plaintext, since only the sender is in possession of the private key K<sub>S</sub><sup>pri</sup> necessary to produce the ciphertext.
>>
>>Unfortunately, public-key cryptography is computationally very intensive and thus unsuitable for the encryption of large documents. A __digital signature__ is a bit string that uses public-key cryptography to undeniably link a document to the producer and guarantees that the document has not been altered in any way.
>>
>>To generate a digital signature for a document P, the sender first generates a digest d of P using a one-way hash function. The sender encrypts the digest using the private key K<sub>S</sub><sup>pri</sup>. The result is the digital signature, which is sent to the receiver along with the unencrypted document P. The receiver decrypts the signature using the sender's public key K<sub>S</sub><sup>pub</sup>, which yields the original digest d. The receiver also produces a new digest from the unencrypted message P. If the two digests match, then an undeniable link between the sender and the document has been established.

![Digital signature](./images/coms-digi-signature.png)