# Security goals and threats


## Types of security violations
>>Computer security is the freedom from theft of or damage to hardware, software, or information, and from disruption or misdirection of services.
>>
>>Protection is the set of mechanisms and policies that guarantee computer security, including the confidentiality, integrity, availability, and authenticity of all data and services.
>>
>>Computer security may be compromised by a multitude of attacks, which can target computer hardware, system or user software, or the communication lines.

|Security violation 	|Definition 	|Typical consequences
| --- | --- | ---
|Information __disclosure__ 	|An unauthorized release or dissemination of information, which may be the result of theft or the deliberate release of the information by an authorized user. 	|Violation of confidentiality and/or privacy of users.
|Information __modification__ 	|An unauthorized modification of data or programs, which may be performed by a legitimate user or by an intruder. 	|Loss of information and/or the ability to carry out subsequent security violations.
|Information __destruction__ 	|A deliberate or accidental deletion of information or damage to hardware. 	|Loss of information or access to services.
|Unauthorized __use__ 	|A circumvention of the system's user authentication services to make unauthorized use of a service. 	|Loss of revenue to the service provider/owner.
|__Denial__ of service 	|Preventing a legitimate user from employing a service in a timely manner. 	|Financial loss, unavailability of a mission-critical or life-critical system.
|User __deception__ 	|Causing a legitimate user to receive and believe false information purported to be true. 	|Loss of information, leading to other possible violations of security.

* _Information __disclosure___ could happen both intentionally or unintentionally, but it's still a security risk, or risk to privacy.
* _Information __destruction___ is an extreem case of _information __modification___, and can happen without _information __disclosure___.
* ___Denial__ of service_ is the act of preventing a user from gaining access to a service or information that they othervise should have access to. 
* _User __deception___ is a broad category, but it includes tricking users into releasing vital information, such as passwords.
* Erasing portions of a disk without authorization can lead to __denial of service__, because the information would be missing. 
    * This is why we would want to restrict access to certain things, specially if you might not trust the user to know what they're doing and accidentally cause damage by removing or adding something in the wrong places.


## Insider attacks

>>Security can easily be compromised by a legitimate user. Possible motivations are financial gain, revenge against colleagues or an employer, or simply malice.
>>
>>A __logic bomb__ is unauthorized code inserted into the system and executed at a specified time to perform some destructive action. The main use of a logic bomb is for blackmail or an act of revenge.
>>
>>A __back door (trapdoor)__ is a mechanism that bypasses user authentication. Ex: A systems programmer could modify the login utility to accept a specific user without requiring a password, thus permitting unauthorized access to the system at a later time.
>>
>>__Information leaking__ is the disclosure of confidential or secret information by a legitimate user to an unauthorized user. Ex: A user with a high security clearance could down-protect a sensitive file to make the information readable by users without security clearance.
>>

### Login spoofing

__Login spoofing__ is a form of __user deception__ where you trick them into providing valid login authentication by precenting them with a very convinsing dupicate of the real login screen. Once the user has provided the information, the program would display the real login page and "run away" with the stolen authentication details, while the user might just consider the occurence a glitch in the system, failing to login with the first attempt.
![Spoofing](./images/security-spoofing.png)

1. User would have attempted to login into a __genuine__ login screen
1. The spoof program would replace the screen with a convinsing __fake__ login screen, giving the impression that the login failed due to a simple glitch or connection issue.
    * If they would decide to leave the computer open at this point, then the next user to attempt to login would be using the fake login screen.
1. When the user inputs their authentication information, the spoof successfully steals that information 
1. The spoof replaces the _fake_ login screen with the __genuine__ login, leaving the user just thinking that there might have been a glitch or connection issue causing the failed login attemts.
    * Since this final screen is a genuine one, the user would succeed into logging in this final time.


>This can happen if the system has been __compromised__ by a virus or unauthorized access/use. Public computers can especially be at risk in places where people expect to be able to gain access to a specific service or system.

>>A possible solution to prevent login spoofing is to display the login screen only after the user has typed in a special character sequence that automatically invokes the OS and which a user program cannot intercept (Ex: CTRL-ALT-DEL).

### Phishing and scams
> __Phishing__ is a similar technique to _login spoofing_, where the user is precented with a fake website, which convinces the user that they are on the genuine site. This also can be an issue on public computers, if someone intentionally leave a fake website open in attempt to have users accidentally giving away their information.
>
>#### __Preventive measures__:
>* look at the domain/path of the website in question and/or of the email that sent the login link. These tend to be the most obvious clues, but sometimes the people behind the scam have gone to great lengths to be convincing (using very similar domain names for example, that could fool at a quick glance). 
>* rule of thumb: always be paranoid of emails telling you to login with provided link to prove that you're actually who you say you are. These days confirmation emails don't request you to input any information.


## Exploiting _human_ weaknesses

>>Many intrusions into a system exploit weaknesses in human behaviors by devising various forms of deception or by relying on the carelessness of a user.
>>
>>A __Trojan horse__ is a program that appears to provide a useful service but also contains a hidden function intended to violate computer security. The name derives from Greek mythology, where a wooden horse was presented to the besieged city of Troy as a gift. After being taken inside the city, soldiers hidden inside the horse caused the city's destruction. In an analogous manner, a Trojan horse program is voluntarily accepted and executed by an unsuspecting user, with potentially disastrous consequences.
>>
>>A virus is a piece of executable code that embeds itself into legitimate programs and copies itself to other programs and systems with the intention of causing harm. A Trojan horse attack is a common way for a virus to enter into a system as part of a service program downloaded by a user from the Internet. Other ways of spreading a virus include file transfers between computers, sharing of removable storage media, and clicking on executable email attachments.
>>
>>A __virus__ is executed whenever the containing host program executes. The first action a virus takes is to infect other programs by inserting itself in front of the legitimate code. Antivirus software maintain steadily growing databases of known viruses and periodically scan all programs for the presence of any virus.
>>
>>To avoid detection, a virus may employ code compression to maintain the original program length, which is checked by the antivirus software. A more sophisticated virus also employs encryption and various forms of self-mutations that do not change the virus' functionality but prevent the virus' identification in the database.

### Trojan horses
Trojan horse programs take advantage of the carelessness of the user to gain access to the system. Just like the Greek myth, These trojan horses are volunteerily installed by the user for their useful service, but will also contain something hidden that violates the computer's security.

Often enough these tend to be imitators of valid programs or services, that pretends to be the original, providing the expected service along with the additional hidden content. 

* It might contain a virus that will be executed along side the program
* It might take advantage of the data it has access to, such as your contacts or private information, without your concent, to spread itself around or steal your information.

>An example could be programs that would lock up the computer, essentially holding the computer hostage until the user would comply to the demands on screen.

### Virus
![Security virus](./images/security-virus.png)
1. The virus attaches itself at the front of a program, to ensure that the virus is executed first
1. To preserve the original size of the program (else the anti-virus programs will notice the change), it will compress the program size and pad out the unused space, to appear to be it's original size.
1. For additional disguise against the anti-virus, it might incude a encryptor and decryptor for the virus. So that when the program is executed, it starts by decrypting the virus.
    * The decryptor cannot be encrypted, so it would be the only exposed part of the virus.
1. The virus then infects other programs and repeats the progress.

#### Dissquising the decryptor code
> The decryptor must remain unencrypted. It can contain code that doesn't alter or change anything that the anti-virus would notice as odd behaviour.
>* Adding and immetiately Subtracting the same number from register would have no effect on the register, so it can be used by the decryptor.
>* Logical AND/OR do not change anything, only compare, so they can be used by the decryptor.


## Exploiting _systems_ weaknesses

>>Many intrusions into a system exploit a particular weakness of the OS, which can be the result of a programming error, an unanticipated set of circumstances, or careless programming.
>>
>>A __buffer overflow attack__ is an intrusion technique that exploits the fact that many programs do not check for array overflow, allowing an attacker to overwrite portions of memory beyond the legitimate scope of an input buffer. When a legitimate systems function is called, a new frame is allocated on the stack, which holds the function's return address and an input buffer. If the input size supplied by the caller exceeds the buffer size, the return address is overwritten, which causes the function to transfer control to an arbitrary location chosen by the attacker.
>>
>>A __worm__ is an unauthorized program, which exploits one or more systems weaknesses to spawn copies of itself on other systems via computer networks. The typical objective of a worm is to cause harm to the system by destroying information or causing denial of service. A buffer overflow attack is a common approach for a worm to enter a system.

### Buffer overflow and Worms
Worms are similar to viruses, where their purpose is to multiply and spread, but instead of having the user executing the virus manually, it would get the system to execute it's code through manipulation of system weaknesses, such as a __buffer overflow__.

![Buffer overflow](./images/security-buffer-overflow.png)
* If a system does not have something that prevents the buffer from overflowing (the program could just continue writing things to the stack, overwriting existing information on the stack), then a the program could essentially overwrite the return address with whatever it wants by overflowing the buffer, and that new return address could point to a worm.
* This essentially means that it can hijack the system by overwriting the return address


## Confining mobile code

>>Users face the dilemma of choosing between the convenience of running code written by others (ex: applets, mobile agents) and the danger of viruses and other malware that can sneak in as part of the imported code. One effective method to guard against unauthorized activities is interpretation. The imported code is not executed directly and thus all actions can be verified by the interpreter.
>>
>>Another approach is sandboxing. A __sandbox__ is a small area of memory within which a program may execute and which guarantees that the program cannot access and jump to any location outside of the designated area. A sandbox consists of two areas, one for the executable code and another for data. The implementation checks each address generated by the code. Branch instructions are limited to addresses within the code section, while load, store, and other data access instructions are limited to addresses within the data segment.

![Sandbox](./images/security-sandbox.png)