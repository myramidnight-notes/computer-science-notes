# User authentication
>> __User authentication__ is the act of verifying the identity of a person who wishes to connect to a computer or a network. User authentication can be based on:
>>
>>    * The knowledge of some information (ex: a password).
>>    * The possession of some artifact (ex: a smart card).
>>    * A physical characteristic or behavior of the person (ex: a fingerprint or a signature).

## Protecting individual passwords

>>Passwords are still the most common way of user authentication and are the target of many attacks on security. One of the easiest approaches to steal a password is guessing. To minimize the chances of a successful guess, users are encouraged or required to use certain combinations of lower-case and upper-case letters, numerals, and special characters. A password may also have an expiration date or a limit on how many times the same password may be used.
>>
>>A __one-time password__ can be used only once and thus becomes useless when stolen. A smart card is able to generate an unlimited stream of one-time passwords.
>>
>>A __one-way hash function__ converts a variable-length input string into a fixed-length output string in a manner that cannot be inverted. If H is a one-way hash function and m = H(n) then no function __H<sup>-1</sup>__ exists to compute n = __H<sup>-1</sup>(m)__. A one-way hash function H can be used to generate a sequence of time-limited passwords. Starting with an initial password, pw, the function H is applied successively to each new password, thus generating the series: H(pw), H(H(pw)), H(H(H(pw))), etc. The passwords are then used in reverse order. Since H cannot be reversed, capturing H(H(H(pw))) does not enable an attacker to derive the next password H(H(pw)). Similarly, H(pw) cannot be derived from H(H(pw)), and pw cannot be derived from H(pw).
>>

### Challenge-response authentication
>>__Challenge-response__ is a method of user authentication that has the form of a dialogue between the user and the system. The system generates a random number r, called the challenge, which the user combines with the password, pw, and encrypts both using a one-way hash function. The result, H(r, pw) is sent back to the system. The system applies the same function H to r and pw. The password is valid if the values match. Since H cannot be inverted, the password cannot be derived from H(r, pw) by an attacker. Furthermore, H(r, pw) is different for each login attempt and thus cannot be reused if captured by an attacker.
>>

![Hashed password](./images/auth-chall-response.png)


## Protecting password files

>>The OS keeps all user passwords in a password file, where each entry contains a valid login name and the corresponding password. The password file is an extremely critical resource since unauthorized access would compromise the passwords of all users.
>>
>>Given that potentially many systems programmers and administrators have access to system files, passwords are not kept in the original form. Instead, each password pw is encrypted using a one-way hash function H and only the encrypted form H(pw) is stored in the file. At the time of login, the user presents the password pw, which the system encrypts using H and compares the result H(pw) to the stored value. Since H cannot be reversed, gaining access to the password file does not reveal any of the passwords. However, the file is still subject to password guessing.
>>
>>A password guessing program can build a large database of common passwords using dictionaries, augmented with common extensions of numbers or special characters. Each password guess, pw, is encrypted using the known function H and the result H(pw) is compared to all entries in the password file. If any match is found, then the program has gained a valid pair (login name, password), which can be used to enter the system.
>>
>>To make the attack computationally difficult, each password is augmented with a random value, s, known as "__salt__". Each entry in the password file then stores the triple (login name, s, H(pw, s)). To see if a guessed password, p, is a valid password of some user, the attacker has to generate H(p, s) for every possible value of s and then compare the result to every entry in the password file.

![Salting passwords](./images/auth-salting.png)

## Using biometrics
>> Biometrics are user authentication methods that measure, record, and analyze unique physical or behavioral characteristics of a person. Each method has advantages and drawbacks in terms of accuracy, cost, convenience to the user, and a number of other attributes. Many studies have been carried out to compare different methods with varying degrees of agreement. The colors in the table, ranging from green (high to very high) to red (low to very low) capture the general consensus of different studies. 

![Biometric tradeoffs](./images/auth-biometric-tradeoff.png)

>>Unlike passwords, biometrics do not return a single true/false value to indicate a match or mismatch. A biometric approach returns real numbers in the range [0:1], which follow a normal probability distribution. Genuine attempts tend to cluster around the value 1, which represents a perfect match, while imposter attempts tend to cluster around the value 0, which represents a perfect mismatch.
>>
>>A threshold value t between 0 and 1 must be chosen to decide whether an attempt is accepted or rejected. Since the distributions for genuine and imposter attempts can overlap, the choice of t represents a trade-off between the number of imposter attempts accepted vs the number of genuine attempts rejected.

![Biometric threshold](./images/auth-biometric-threshold.png)
* It is ideal if neither clusters (genuine and imposter attempts) do not overlap, but there is usually some overlap.
* If we place the threshold where no imposters are accepted, then we will be rejecting a large number of genuine attempts as well.
* Same goes for placing the threshold where all genuine attempts are accepted, because then a large number of imposters will be accepted as well.
* The best threshold would be somewhere in the middle of the overlapping area where, so that some genuine attempts are rejected, as well as some imposter attempts are accepted. 