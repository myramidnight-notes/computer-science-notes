# Virtual memory
* ### [Principles of virtual memory](vm-principles.md)
    * Demand paging
    * Page replacement
    * Paging of tables

* ### [Page replacement with fixed numbers of frames](vm-fixed-frames.md)
    * The optimal page replacement algorithm
    * The FIFO page replacement algorithm
    * The least-recently-used page replacement algorithm

* ### [Approximations of the LRU algorithm](vm-aprox-alg.md)
    * The aging page replacement algorithm
    * The second-chance page replacement algorithm
    * The third-chance page replacement algorithm

* ### [Page replacement with variable numbers of frames](vm-page-var-frames.md)
    * The working set concept
    * The working set page replacement algorithm
    * The page-fault-frequency replacement algorithm

* ### [Time and space efficiency of virtual memory](vm-time-space.md)
    * Overhead of demand paging
    * Load control
    * The choice of page size


## Extra notes
* __page != frame__
    > When we are replacing pages, we replace them within the frames, so we have to pay attention to if they are asking what page was replaced, or what frame.