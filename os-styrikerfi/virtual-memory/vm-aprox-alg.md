# Approximations of the LRU algorithm

The __LRU__ (_least-recently-used_) algorithm requires the queue of pages to be modified at each memory reference, which makes the algorithm too inefficient in practice. Several algorithms exist that approximate LRU and can be implemented efficiently.
The aging page replacement algorithm

* ### Reference bit
    > A __referenced bit (r-bit)__ is a bit associated with a page and is set automatically by the hardware whenever the page is referenced by any instruction.
* ### Aging register
    > An __aging register__ is associated with a page and is shifted periodically to the right by 1 bit. Unless the most significant bit is set to 1, the page is aging in the sense that the associated register value is steadily decreasing.
* ### Aging page replacement algorithm
    > The __aging page replacement algorithm__ does not maintain pages sorted in the exact LRU order, but groups together pages referenced during a period of d consecutive references. Each period is represented by 1 bit in a periodically shifting aging register.

## The aging algorithm
> The algorithm is invoked periodically after every d reference, where d is a parameter determined experimentally:
>
>1. Shift all aging registers to the right by 1, discarding the least significant bit.
>1. Copy the r-bits into the most significant bits of the aging registers.
>1. Reset all r-bits to 0.
>
>When a page fault occurs:
>
>* Select the page with the smallest aging register value for replacement.
>* If multiple pages have the same smallest value then select one at random.
>
>
>![Aging algorithm](./images/vm-algs-aging.png)

1. All the bits set in the `r` column indicate that the page has been referenced
    * Then the `r` column is shifted into the `aging` section, to update the age value
1. All the bits are shifted to the right whenever references are made, pushing the values of the `r` column into the aging from the left.
1. The value of the __aging__ indicates how recently the page has been referenced: the higher the number, the more recent. 
    * The page with the lowest value in the _aging_ will be a candidate for replacement.

## The 2nd chance algorithm

>>The second-chance page replacement algorithm
>>
>>The second-chance page replacement algorithm is a coarse-grain approximation of LRU. The algorithm uses the r-bit to divide all pages into only two categories: recently referenced and not recently referenced. A page is selected from the not-recently referenced category.
>>
>>Similar to FIFO, the algorithm maintains a pointer that cycles through the pages when searching for a page to be replaced. Because of the circular nature, the algorithm is also known as the clock algorithm.
>>
>>At a page fault, the algorithm repeats the following steps until a page is selected for replacement:
>>
>>1. If the pointer is at a page p with r = 0 then select p and advance the pointer to the next page.
>>1. Otherwise, reset r to 0, advance the pointer to the next page, and continue the search.

![2nd chance](./images/vm-algs-aging-2.png)
* This table shows the state of the resident pages table at any given time
* `3|0` would indcate that the frame contains _page 3_, and has zero as their `r` bit.


| Action | Rules of second changes |
| --- |---
| Load |A newly loaded page will have __1__ as their `r` bit by default, as they were just referenced.
| Reference |When a page is referenced, turn the `r` bit to __1__
| Second chance |If a page has __1__ as their `r` bit when attempting to replace, then change the `r` bit to __0__ without replacing it, continue with the next frame
| Replace |If a page has __0__ as their `r` bit when attempting to replace, then replace it.


## The 3rd chance algorithm

>>The third-chance page replacement algorithm
>>
>>The third-chance algorithm is a refinement of the second-chance algorithm. The algorithm takes advantage of both the r-bit and m-bit associated with every page. Since replacing a modified page is more costly than replacing an unmodified page, the algorithm gives modified pages an additional chance to remain resident.
>>
>>The third-chance page replacement algorithm, also known as the not-recently-used page replacement algorithm (NRU), is a coarse-grain approximation of LRU, which divides pages into 4 categories based on the 4 possible combination of the r- bit and the m-bit.
>>
>>At a page fault, the algorithm repeats the following steps until a page is selected for replacement:
>>
>>1. If the pointer is at a page p with r = 0 and m = 0 then select p and advance the pointer to the next page.
>>1. Otherwise, reset the bits according to the following table, advance the pointer to the next page, and continue the search.
>

### Bit changes and 2nd/3rd chances.
* The layout is `p|rm`
    * `p` is the page number loaded in the frame
    * `r` is the reference bit, if it's been recently referenced
    * `m` is the modified bit, if it's been modified recently

The table below shows how the `rm` bits are updated whenever we attempt to replace a page. Unless their value is __00__, then we don't replace the page, simply update the bits and move on.
>| Action | Bit change | description 
>| --- | --- | ---
>|Second chance|__11__ &rarr; __01__ 	| Recently referenced and modified
>|Third chance|__01__ &rarr; __00__ 	| Recently modified
>|Third chance|__10__ &rarr; __00__ 	| Recently referenced, unmodified
>|last chance |__00__| Page has no changes left, it will be replaced if needed

These reference and modified bits are only keeping track of if it's a candidate for replacement. So a __3rd bit__ is needed to keep track of the modified status of the page so we can remember to write it to disk upon replacement if needed.

### Example of 3rd chances in action
>![3rd chance](./images/vm-algs-lru.png)
>In this example, we started with our pointer at __frame 0__. 
>The red slashes are changes in the ref/mod bits
>1. At time __1__, nothing changes since _page 0_ is already loaded
>1. Next we get a page fault, with the pointer still at _frame 0_. 
>    * we give _frame 0_ a 2nd chance
>    * we replace _frame 1_
>1. Here we are referencing a already loaded page, no replacements.
>1. No _page faults_ at time __4__ either
>1. Now we need to load _page 1_, with pointer starting at _frame 2_.
>    * since _frame 2_ has been modified, it gets a 2nd chance (__01__ &#8594; __00__)
>    * _frame 3_ has neither been referenced or modified, so we replace it.