# Page replacement with fixed number of frames
>>Memory consists of a fixed number of frames. One approach to sharing the frames among multiple processes is to let the processes compete with one another. When a page fault occurs and no free frame is available, the algorithm chooses one of the resident pages for replacement, regardless of which process the page belongs to.
>>
>>A reference string is the sequence of page numbers referenced by an executing program during a given time interval. Reference strings are used to compare different page replacement algorithms by counting the number of page faults generated.
>>### The optimal page replacement algorithm
>>
>>The optimal page replacement algorithm selects the page that will not be referenced for the longest time in the future.
>>
>>The algorithm is guaranteed to generate the smallest number of page faults for any reference string. However, the algorithm is unrealizable because the replacement decisions depend on future references, which are unknown at runtime. But the algorithm provides a useful lower bound on the number of page faults for comparison with other algorithms.

![Optimal replacement algorithm](./images/vm-page-reference.png)


## The FIFO page replacement algorithm
This algorithm would simply have a pointer to which frame is next in line for replacement if any page needs to be loaded.

>>The FIFO page replacement algorithm selects the page that has been resident in memory for the longest time.
>>
>>The algorithm is easy to implement, requiring only a single pointer to designate the oldest page in memory. When the page is replaced, the pointer is advanced to the next frame modulo n.
>>
>>The FIFO algorithm takes advantage of the principle of locality. Except for branch instructions, which constitute only a small percentage of all instructions, execution is sequential. As execution advances sequentially through the code, the likelihood of referencing a page used in the distant past diminishes with time. Similarly, many large data structures are processed in sequential order.


![Optimal replacement algorithm](./images/vm-page-replace-fifo.png)
* The pointer shows the oldest resident page in the table, which will be the one to be replaced if needed.

## The least-recently-used page replacement algorithm
For this algorithm, we have a queue that keeps track of which page was access last, moving it to the back of the queue whenever it's accessed, and then the algorithm will replace the page which is at the head of that queue if another page needs to be loaded. 

![Last recently used](./images/vm-page-replace-lru.png)

* The most recently used page numbers are added to the __end__ of the queue, because we remove from the __head__, which are the least recently used page entries.

>>FIFO exploits the principle of locality to some degree but fails to recognize that a program frequently returns to pages referenced in the distant past. FIFO replaces the oldest resident pages even if the pages have been referenced again recently and thus are likely to be referenced again in the near future.
>>
>>The least-recently-used page replacement algorithm (LRU) selects the page that has not been referenced for the longest time.
>>
>>The implementation requires a queue of length n, where n is the number of memory frames. The queue contains the numbers of all resident pages. 
>>* Whenever a resident page p is referenced, p is moved to the end of the queue. Thus the queue is always sorted from most recently used page to least recently used page.
>>* When a non-resident page p is referenced, p is moved to the end of the queue and the least recently referenced page q at the head of the queue is removed. Page p then replaces q in memory.

