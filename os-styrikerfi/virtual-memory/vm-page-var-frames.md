
# Page replacement with variable numbers of frames
## The working set concept
>>
>>A program always needs a certain set of pages resident in memory to run efficiently. When not enough page frames are available, the page fault rate becomes too high and the process spends much of the time waiting for pages to be moved between the disk and the memory.
>>
>>The optimal working set of a process is the set of pages that will be needed in the immediate future and thus should be resident. The size of the working set varies with the program's behavior. When execution is highly localized in long tight loops and static data structures, the set contains a small number of repeated page numbers. When execution involves many branches or highly dynamic data structures, the set contains many different page numbers.
>>
>>To determine the optimal working set at time t, a window of size d is superimposed on the reference string. Pages visible in the window belong to the set. The size and composition of the set change automatically as the window slides forward with each memory reference.
>>
>>The window size is determined by the typical behavior of a newly started program. During the first few memory operations, the number of pages referenced increases rapidly but the rate of increase diminishes with time. Thus d must be chosen to cover the period of rapid growth but not past the point where adding more pages is of only marginal benefit.

>![Working set](./images/vm-working-set.png)
> The optional __working set__ of a process is the set of pages that it will need in the immediet future, and should therefor be present in the memory. But finding the best working set at any time will rely on the algorithm being able to predict what pages will be requested (by viewing the _request string_ (RS) that dictates the upcoming page requests).
>
> The red frame represents the window of size `d` which indicates how many pages can be loaded into memory, and all the different pages referenced within the window will form a __working set__. The _size of the working set_ depends on how many different pages are within the window at any given time.
>
>To find the best _window size `d`_, we want to find where the _working set size_ is of a relatively stable size. 

## The working set page replacement algorithm
This algorithm will be focusing on the past requests in the __RS__ (_request string_), only keeping the previously referenced pages loaded while they stay within the frame of past requests.
>>The optimal working set is unrealizable in practice because the algorithm requires advance knowledge of the RS. The working set page replacement algorithm relies on the principle of temporal locality by assuming that recently referenced pages will be similar to pages referenced in the immediate future.
>>
>>The __working set__ (WS) of a process at time t is the set of pages referenced during the past d memory operations preceding t.
>>
>>The __working set page replacement algorithm__ uses a trailing window of size d superimposed on the RS to determine the size and composition of the working set at time t. Analogous to the forward-looking window of the optimal working set, pages visible in the trailing window belong to the working set.
>>
>>At each memory reference, the algorithm follows the steps:
>>
>>1. Advance the sliding window by 1 to include the current reference.
>>1. Keep resident only the pages that appear in the window.

![Working set page replacement](./images/vm-ws-page-replace.png)


## The page-fault-frequency replacement algorithm
>>The working set page replacement algorithm is very effective in keeping the page fault frequency low but is difficult to implement in practice because the list of pages in the current working set changes with every memory reference.
>>
>>The __page-fault-frequency replacement algorithm__ takes a direct approach to controlling the page fault rate by adjusting the current resident set based on how frequently consecutive page faults occur.
>>
>>A constant d, determined experimentally, is used to decide if the page fault frequency is too high and thus the resident set should be increased. If the page fault frequency is acceptable then the resident set can be reduced by removing some of the pages not referenced recently.
>>
>>When a page fault occurs:
>>
>>1. Add the currently referenced page causing the page fault to the resident set.
>>1. When the time since the previous page fault is greater than d, remove all pages from the resident set that have not been referenced since the previous page fault.
>>
>>The algorithm can be implemented efficiently since any action is taken only at a page fault.

With the __page-fault-frequency algorithm__, the window size `d` is increased when there are too many _page faults_ happening, and decrease the window size if the _page faults_ are further appart (shrinking the set)

This algorithm is easier to implement, because it does not have to focus on what the future or past page requests are, but instead views the history of __how frequent__ the _page faults_ have been.

![Page fault alg](./images/vm-ws-page-fault.png)