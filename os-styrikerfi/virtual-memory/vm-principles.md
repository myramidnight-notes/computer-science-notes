# Principles of virtual memory
We are familar with the fact that our physical memory (RAM) is usually alot smaller than the space we have on disks/harddrives. The memory is also not a good place to expect data to survive in the long-term, since it only exists for as long as the system is running (while it has power). 
>Good example for how temporary the memory is, would be information stored on active servers that are not activly storing the data on disk, any new information will be lost once the server is killed.

We still want to be able to use all the data we have stored on the disks, even if they cannot all fit into the physical memory. This is where __Virtual Memory__ (VM) comes into play. 

It keeps track of where all the things on disk are stored, keeping track of all the logical addresses (now refered to as __virtual addresses__ when we are using VM). 

* __Paged VM__ creates a single large contiguous (_borders-touching_) address space per process. 
* __Paged VM with segmentation__ creates multiple large address spaces per process, each of which are paged. 

__Demand paging__ makes the implementation of VM possible, by moving pages to main memory only as they're needed.

## Demand paging
__Demand paging__ is principle of loading a page into memory only when we need to use it, rather than at the start of the program execution.

![Demand pages](./images/vm-demand-pages.png)
### Present bit (is the page loaded?)
>A __present bit__ is a indicator of wether the referenced page is currently in _physical memory_ or if it needs to be loaded. 
>* `1` would be _true_ and `0` be _false_, whether the page can be found in memory or not.
>* If the page is _present_, then we simply have to follow the attached physical address to locate what we're looking for.

### Page fault (load page trigger)
>A __page fault__ is an interrupt that occurs when a program attempts to  reference a page that hasn't been loaded to memory yet. Then the system has to go find the requested page on disk and move it into memory, before setting the __present bit_ to `1`

> #### Things to consider: 
>How many _page faults_ would be triggered when running a instruction on a variable: 0, 1 or 2?
>* if both the instruction and variable are on the same page as the code being run, then there is no is no _page fault_ triggered. 
>* if one of the two recides on a different page then we will of course have to load a page.
>* if both the instruction and variable are on separate pages respectively, then 2 pages will be loaded (triggered by 2 page faults). 


## Page replacement
Page replacement is the __act of overwriting a page__ in memory with a different page from disk. These replacements are bound to happen since we cannot keep all the pages from disk in physical memory.

### The modified-bit (is data in memory different than on disk?)
Interactions with the disk are time-consuming, specially compared to how fast the physical memory is. So when we change things that we have loaded to memory, it just lives in memory until we cannot keep it in memory any longer. Meaning that the data on disk isn't updated until the page needs to be replaced in memory.

> This delayed writing to disk only on replacement might sound like it would cause you to loose some process in case the system died (such as computer crashing), and that is actually what happens. But at the same time, we don't want corrupted data on the disk, which might happen if the system might crash in the middle of a operation if we had things written directly to the disk. So this technically saves the integrity of our data on disk.
>
> Of course systems are structured based on the needs of the users, so this behaviour of how often does the disk update just depends on our implementation. How valuable is the possibly lost data in those rare occasions?

The __modified-bit__ is a _binary flag_ that allows us to see at a glance if we have to bother with moving the data from memory back onto the disk, because whatever is in memory is a copy of whatever is on the disk. If there were no modifications, then we can minimize having to write the data on disk needlessly.

>![Page replacement](./images/vm-page-replace.png)
>
> This slides shows a situation where a already loaded page `k` has been modified in physical memory. __VM__ is a index of where things are on disk. __Physical memory__ is the _RAM_.
>* If `r` wanted to replace `k` in the physical memory, then we would need to load the modified data from __5__ in order to not loose the modifications.
>   * It would copy the data from __5__ into `k` on disk, this clears `k` in the page-table.
>   * we would then move data from `r` into the now free space of __5__, adding information about the loaded page into the page table for `r`.
>* However, if we wanted to replace a page in physical memory that hasn't been modified, then there is no need to update the data on disk before replacing the page in physical memory.


## Paging of tables (pages of pages)
The virtual memory can create addresses that are simply too large for the available physical memory. 

>Example: With a __64-bit__ virtual address, the potential size of VM is __2<sup>64</sup>__ = 18,446,744,073,709,551,616 words. With 12 bits for the offset w, the remaining 52 bits, when split evenly between s and p, would result in segment and page tables of __2<sup>26</sup>__ = 67,108,864 entries. An uneven split would make one of the tables even larger.

Since it's rare that programs make use of such large address spaces, alot of the memory would be wasted on such huge addresses. To reduce the overhead, we can divide a table into pages, creating a intermediate table. 

It's essentially the same principle, to trim things down to only keep in memory what we're actually using, and that goes for the unused addresses in the page table as well.

![Pages of Pages](./images/vm-page-of-pages.png)

This extends the virtual addresses to have two page pointers: `p1` for the page in the page-table, and then `p2` to the page within that page. The `w` pointer stays the same, pointing to the word within the page at the end of the tunnel.

>The same tactic can apply to segment tables as well, if that one is too large to be efficiently stored in memory, we can split that into intermediate segments