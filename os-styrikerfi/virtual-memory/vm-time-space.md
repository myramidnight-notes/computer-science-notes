# Time and Space efficiency of virtual memory

## Overhead of demand paging

>>Page fault rate is the number of page faults, f, occurring during a number of memory references, t. The page fault rate can be expressed as P = f/t, where 0 ≤ P ≤ 1.
>>P = 1 means that every memory reference results in a page fault, and P = 0 means that no page faults occur.
>>
>>A page fault results in a significant overhead, which increases the average access time to memory.
>>Effective access time is the average time to access memory in the presence of page faults. The effective access time, E, depends on the frequency of page faults:
>>```
>>E  = (1 - P) * m + P * S
>>```
>>where m is the time to access physical memory and S is the time to process a page fault. 

>![Overhead](./images/vm-time-overhead.png)
>
>Here the `S` is the time it takes to process _page faults_ `f`. We want to keep the overhead (red area) __less than 10%__ in order to keep the system practical.
>* The first equation shows how to calculate the overhead in percentage, 
>* second equation shows the page fault rate


## Load control

>> Every process typically alternates between compute-bound phases, when the CPU is heavily utilized, and I/O-bound phases, during which the process is mostly blocked while waiting for input or output to complete. To increase CPU utilization, the OS runs multiple processes concurrently.
>> 
>> Load control is the activity of determining how many processes should be running concurrently at any given time to maximize overall system performance.
>> 
>> Thrashing is an execution state during which most of the time is spent on moving pages between the memory and the disk while the CPU is mostly idle and no process is making any real progress.
>> 
>> Thrashing occurs when too many processes are sharing memory concurrently and no process has enough pages to operate without frequent page faults.
>> 
>> Page replacement algorithms that use variable numbers of frames prescribe the working set of every process. A process is allowed to run only if enough frames are available to hold the complete working set. The sum of all working sets thus imposes an automatic limit on how many processes can run at any given time.
>> 
>> Algorithms that use fixed numbers of frames do not impose any implicit limits. Consequently, a load control mechanism must be implemented to prevent thrashing.

>![Thrasing](./images/vm-time-thrashing.png)
>
> We will want try to keep the CPU utilization at a 100% when possible, which requires a balance of number of processes and the number of page-faults. If there are too many processes with too many page faults, then the __CPU utilization__ will drop very fast, eventually reaching a point of __thrasing__ where hardly any work gets done and most of the time is spent moving pages between disk and memory.
>* `L` is the time between page faults
>* `S` is the time that takes to process the faults

## The choice of page size

>>The page size has a major impact on the time and space overhead of virtual memory and thus on the overall performance of the system. The optimal page size depends on many factors, some of which favor a large size while others favor a small size. Consequently, the page size is always a compromise between the different factors.
>>
>>The most common page sizes range between 512 and 4096 but the number has been increasing in recent years with improvements in disk technology. To accommodate the needs of different programs, some systems even support multiple page sizes.

![Page size](./images/vm-page-size.png)