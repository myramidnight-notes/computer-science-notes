[...back](../README.md)
# Programming Language Concepts
>* Instructor of this course at Reykjavik University: __Tarmo Uustalu__
>* Course builds on the book __Programming Language Concepts__ by _Peter Sestoft_
>   * [Table of contents from book](https://www.itu.dk/people/sestoft/plc/sestoft-plc-2nd-toc.pdf)
>   * [slides and example code from the book](https://www.itu.dk/people/sestoft/plc/)
### Jupyter Notebooks for Code
> It might be intesting to keep full code examples in Jupyter Notebooks for each section/chapter of these notes (versions of the languages), since there are `F#` language kernels and the fact I can work on both directly within VSCode
>
> I came to the conclusion that completely converting the notes to jupyter notebooks just looked too messy for me, but they have their uses, separating the code into executable blocks, the option to include notes in between. 

## Meta- and Object-language
A __object language__ is a language we are studying (be it programming or spoken languages) and __meta language__ is the language we use to conduct our discussion, because we need to somehow explain what we're trying to say about the _object language_ in question.

For these notes and course, `F#` (f-sharp) is used as a _meta language_ to explain programming concepts in general rather than any specific programming language. But we will of course have to learn to understand `F#` in order to understand the code snippets.

# Index of concepts
1. ### [Syntax and Semantics](syntax-semantics.md)
    * Concrete and Abstract syntax
    * Dynamic and Static semantics
    * Programming language levels
    * Syntactic sugar
    * [__Interpretation and Compilation__](interpretation-compilation.md)
    * [__Compilors: Stack machines__](stack-machine.md)
1. ### [Expressions](expressions/README.md)
    * __Expressions without variables__ (basic expressions)
    * __Expressions with global variables__
    * __Expressions with local variables__ (let-bindings)
1. ### [Parsing and Lexing](parsing/README.md) 
    > Going from __concrete__ to __abstract__ syntax
1. ### [Functional languages](functions/README.md)
    * First-order functions
    * Statically vs Dynamically typed languages
    * Higher-order functions
    * Polymorphic type interface
    * [__lambda-calclulus__](lambda-calculus/README.md)
1. ### [Imperative programming](imperative-lang/README.md)
1. ### [__Garbage/Memory Managment__](garbage-managment/README.md)
    * Garbage collection
    * Memory leaks
    * Dangling pointers
## [Vocabulary](prog-vocabulary.md)
>Just a incomplete collection of words and terms used to describe programming, it even has the localized (Icelandic) translation for some of them.