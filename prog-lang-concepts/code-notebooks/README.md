# Jupyter notebooks with Code examples
These notebooks are simply the full code examples used to describe each version of the programming languages we've been implementing. Their purpose is not to explain the code, but to view and execute it.

This allows us to actually see how the language behaves, without having to load the code to `fsi` or something like that. All you need is a `F#` kernel.

### Kernels
You can even tell VSCode to use your already running Jupyter server, if you have one running, to access the kernels available on it.

>#### Installing .NET Interactive for `F#` kernel
>* Documentation: [Installing .NET Interactive as a Jupyter kernel](https://github.com/dotnet/interactive/blob/main/docs/NotebooksLocalExperience.md)

### Changes
These notebooks might change drastically, I am considering having the code simply as `.fs` files to be loaded into the notebooks, to make more space for examples without the mess (since all the code is essentially covered in the usual `.md` notes

This is still an experiment with using notebooks for these notes.

## Notebooks in collection 
If you don't want to dig through the folder.

* ### Expressive languages (Basics)
    * Basic language without variables
    * Basic language with global variables
        * Basic language with target expressions
    * Basic language with let-bindings
        * Closed expressions
        * With both named and target variables
* ### Functional languages
    * [First Class functional language](functions-1st-class.ipynb)
        * Type inference
    * [Higher order functional language](functions-higher-order.ipynb)
        * Higher order type inference with Polymorphic types