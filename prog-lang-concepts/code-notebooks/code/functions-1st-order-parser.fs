module firstClassParser

//=======================================
// THE EXPRESSIONS
type expr =
   | Var of string                                  // x
   | Let of string * expr * expr                    // let x = erhs in ebody
   | Call of string * expr                          // f e   
   | LetFun of string * string * expr * expr        // let f x = erhs in ebody
   | Num of int
   | Plus of expr * expr
   | Minus of expr * expr
   | Times of expr * expr
   | Neg of expr
   | True
   | False
   | Equal of expr * expr
   | Less of expr * expr
   | ITE of expr * expr * expr                      // if e then e1 else e2

//=======================================
// THE TOKENS
type token =
   | NAME of string                      // variable names
   | LET | EQUAL | IN
   | IF | THEN | ELSE | LESS
   | INT of int | TRUE | FALSE           // unsigned integers and bools
   | PLUS | MINUS | TIMES
   | LPAR | RPAR                         // parentheses
   | ERROR of char                       // illegal symbols


//=======================================
// THE HELPER FUNCTIONS
let isDigit c = '0' <= c && c <= '9'

let digit2Int c = int c - int '0'

let isLowercaseLetter c = 'a' <= c && c <= 'z'

let isUppercaseLetter c = 'A' <= c && c <= 'Z'

let isLetter c = isLowercaseLetter c || isUppercaseLetter c

let word2Token (s : string) : token =
    match s with
    | "let"   -> LET
    | "in"    -> IN
    | "if"    -> IF
    | "then"  -> THEN
    | "else"  -> ELSE
    | "true"  -> TRUE
    | "false" -> FALSE
    | _       -> NAME s

//=======================================
// THE TOKENIZER
let rec tokenize (cs : char list) : token list =
    match cs with
    | [] -> []
    | '+'::cs  -> PLUS :: tokenize cs
    | '-'::cs  -> MINUS :: tokenize cs
    | '*'::cs  -> TIMES :: tokenize cs
    | '='::cs  -> EQUAL :: tokenize cs
    | '<'::cs  -> LESS :: tokenize cs
    | ' '::cs  -> tokenize cs
    | '\t'::cs -> tokenize cs
    | '\n'::cs -> tokenize cs
    | '('::cs  -> LPAR :: tokenize cs
    | ')'::cs  -> RPAR :: tokenize cs
    | c::cs when isDigit c ->
        tokenizeInt cs (digit2Int c)
    | c::cs when isLowercaseLetter c ->
        tokenizeWord cs (string c)
    | c::cs -> ERROR c :: tokenize cs

and tokenizeInt cs (acc : int) =
    match cs with
    | c::cs when isDigit c ->
        tokenizeInt cs (acc * 10 + digit2Int c)
    | _ -> INT acc :: tokenize cs

and tokenizeWord cs (acc : string) =
    match cs with
    | c::cs when isLetter c || isDigit c ->
         tokenizeWord cs (acc + string c)
    | _ -> word2Token acc :: tokenize cs


//=======================================
// STRINGS TO CHARACTERS
let string2Chars (s : string) : char list =
    let rec helper cs i =
        if i = 0 then cs else let i = i - 1 in helper (s.[i] :: cs) i
    helper [] (String.length s)

//=======================================
// THE LEXER
let lex s = tokenize (string2Chars s)


//=======================================
// THE PARSER
let rec parseExpr (ts : token list) : expr * token list =
   let (e1, ts) = parseComparand ts
   match ts with
   | EQUAL :: ts ->
       let (e2, ts) = parseComparand ts
       Equal (e1, e2), ts
   | LESS :: ts ->
       let (e2, ts) = parseComparand ts
       Less (e1, e2), ts
   | _ -> e1, ts

// parseComparand ============  
and parseComparand (ts : token list) : expr * token list =
   let (e, ts) = parseSummand ts
   parseSummandList e ts

// parseSummandList ============  
and parseSummandList (e1 : expr) (ts : token list) : expr * token list =
   match ts with
   | PLUS :: ts ->
       let (e2, ts) = parseSummand ts
       parseSummandList (Plus (e1, e2)) ts
   | MINUS :: ts ->
       let (e2, ts) = parseSummand ts
       parseSummandList (Minus (e1, e2)) ts
   | _ -> e1, ts

// parseSummand ============  
and parseSummand (ts : token list) : expr * token list =
   let (e1, ts) = parseFactor ts
   parseFactorList e1 ts

// parseFactorList ============  
and parseFactorList (e1 : expr) (ts : token list) : expr * token list = 
   match ts with
   | TIMES :: ts ->
       let (e2, ts) = parseFactor ts
       parseFactorList (Times (e1, e2)) ts
   | _ -> e1, ts

// parseFactor ============  
and parseFactor (ts : token list) : expr * token list =  
   match ts with
   | NAME f :: ts ->
       match ts with
       // Here's a bit of lookahead
       | NAME _ :: _ | INT _ :: _ | TRUE :: _ | FALSE :: _ | LPAR _ :: _ ->
           let (earg, ts) = parseArg ts
           Call (f, earg), ts
       | _ -> Var f, ts     
   | _ -> parseHead ts
// parseHead ============  
and parseHead (ts : token list) : expr * token list = 
   match ts with                         
   | LET :: NAME f :: NAME x :: EQUAL :: ts ->
       let (erhs, ts) = parseExpr ts
       match ts with
       | IN :: ts ->
           let (ebody, ts) = parseExpr ts
           (LetFun (f, x, erhs, ebody), ts)
       | _ -> failwith "let without in"
   | LET :: NAME x :: EQUAL :: ts ->
       let (erhs, ts) = parseExpr ts
       match ts with
       | IN :: ts ->
           let (ebody, ts) = parseExpr ts
           Let (x, erhs, ebody), ts
       | _ -> failwith "let without in"
   | LET :: NAME x :: _ -> failwith "let without equals sign"
   | LET :: _ -> failwith "lhs of def in let not ok"
   | MINUS :: ts ->
       let (e, ts) = parseFactor ts
       Neg e, ts
   | IF :: ts ->
       let (e, ts) = parseExpr ts
       match ts with
       | THEN :: ts ->
           let (e1, ts) = parseExpr ts
           match ts with
           | ELSE :: ts ->
               let (e2, ts) = parseExpr ts
               ITE (e, e1, e2), ts
           | _ -> failwith "if-then-else without else"
       | _ -> failwith "if-then-else without then"
   | _ -> parseArg ts

// parseArg ============  
and parseArg (ts : token list) : expr * token list =
   match ts with 
   | NAME x :: ts -> Var x, ts
   | INT i :: ts -> Num i, ts
   | TRUE  :: ts -> True, ts
   | FALSE :: ts -> False, ts
   | LPAR :: ts ->
       let (e, ts) = parseExpr ts
       match ts with
       | RPAR :: ts -> e, ts
       | _ -> failwith "left paren without right paren"
   | _  -> failwith "not a factor"


//=======================================
// THE TOP LEVEL PARSER
let parse (ts : token list) : expr =
    let (e, ts)  = parseExpr ts
    if ts = [] then e else failwithf "unconsumed tokens"

//=======================================
// THE TOP LEVEL PARSER + LEXER
let lexParse (s : string) : expr = parse (lex s)