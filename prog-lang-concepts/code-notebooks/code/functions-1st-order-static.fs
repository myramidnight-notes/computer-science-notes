module firstClassStatic

//=======================================
// THE EXPRESSIONS
type expr =
    // Variable definitions
    | Var of string                             // x
    | Let of string * expr * expr               // let x = erhs in ebody    

    //Function definitions
    | Call of string * expr                     // f e
    | LetFun of string * string * expr * expr   // let(rec) f x = erhs in ebody

    // Operators
    | Num of int
    | Plus of expr * expr
    | Minus of expr * expr    
    | Times of expr * expr
    | Neg of expr
    
    // Comparators
    | True
    | False
    | Equal of expr * expr
    | Less of expr * expr
    | ITE of expr * expr * expr   

//=======================================
// THE ENVIRONMENT AND LOOKUP
type 'a envir = (string * 'a) list

let rec lookup (x : string) (env : 'a envir) : 'a =
    match env with
    | []          -> failwith (x + " not found")
    | (y, v)::env -> if x = y then v else lookup x env

//=======================================
// THE VALUES
type value =
    | I of int
    | B of bool
    | F of string * expr * value envir // Static scope function version

//=======================================
// THE EVALUATOR
let rec eval (e : expr) (env : value envir) : value =
    match e with
     | Var x  -> 
          match lookup x env with
          | I i -> I i
          | B b -> B b
          | _   -> failwith "a function used as a value"

     | Let (x, erhs, ebody) ->
          let v = eval erhs env
          let env' = (x, v) :: env  //adds
          eval ebody env' //uses an exptended enviornment
          
     // START OF STATIC SCOPE IMPLEMENTATION
     | Call (f, earg) ->
          match lookup f env with
          | F (x, ebody, env0) as clo ->
               // argument evaluated in current env
               let v = eval earg env
               // including the argument and function in extended env
               let env' = (x, v) :: (f, clo) :: env0
               // function body evaluated
               eval ebody env'
          | _   -> failwith "variable called not a function"     
     | LetFun (f, x, erhs, ebody) ->
          // adding the function name to extended enviornment
          let env' = (f, F (x, erhs, env)) :: env
          // def-time environment recorded in closure 
          eval ebody env'
     // END OF STATIC SCOPE IMPLEMENTATION

     (* 
     // START OF DYNAMIC SCOPE IMPLEMENTATION
     | Call (f, earg) ->
          match lookup f env with
          | F (x, ebody) as clo ->
               // argument evaluated in current env
               let v = eval earg env
               // including the argument and function in extended env
               let env' = (x, v) :: (f, clo) :: env
               // function body evaluated
               eval ebody env'
          | _   -> failwith "variable called not a function" 
     | LetFun (f, x, erhs, ebody) ->
          let env' = (f, F (x, erhs)) :: env
          // def-time envmnt NOT recorded in closure 
          eval ebody env'
     // END OF DYNAMIC SCOPE IMPLEMENTATION
     *)

     //Rest is the same as usual
     | Num i -> I i
     | Plus  (e1, e2) ->
          let I i1, I i2 = eval e1 env, eval e2 env in I (i1 + i2)
     | Minus  (e1, e2) ->
          let I i1, I i2 = eval e1 env, eval e2 env in I (i1 - i2)    
     | Times (e1, e2) ->
          let I i1, I i2 = eval e1 env, eval e2 env in I (i1 * i2)
     | Neg e ->
          let (I i) = eval e env in I (-i)
     | True  -> B true
     | False -> B false
     | Equal (e1, e2) ->
          let I i1, I i2 = eval e1 env, eval e2 env in B (i1 = i2)         
     | Less (e1, e2) ->
          let I i1, I i2 = eval e1 env, eval e2 env in B (i1 < i2)     
     | ITE (e, e1, e2) ->
          let (B b) = eval e env in if b then eval e1 env else eval e2 env