module higherOrderStatic

//=======================================
// THE EXPRESSIONS
type expr =
    | Var of string                             // x
    | Let of string * expr * expr               // let x = erhs in ebody    

    | Call of expr * expr                       // efun earg
                //definition of Call has been modified
    | LetFun of string * string * expr * expr   // let f x = erhs in ebody
    | Num of int
    | Plus of expr * expr
    | Minus of expr * expr    
    | Times of expr * expr
    | Neg of expr
    | True
    | False
    | Equal of expr * expr
    | Less of expr * expr
    | ITE of expr * expr * expr 

//=======================================
// THE ENVIORNMENT
type 'a envir = (string * 'a) list

//=======================================
// THE LOOKUP
let rec lookup (x : string) (env : 'a envir) : 'a =
    match env with
    | []          -> failwith (x + " not found")
    | (y, v)::env -> if x = y then v else lookup x env

//=======================================
// THE VALUES
type value =
    | I of int
    | B of bool
    | F of string * string * expr * value envir   


//=======================================
// THE EVALUATOR

let rec eval (e : expr) (env : value envir) : value =
    match e with
    | Var x  ->  lookup x env
                  // changed! we did not allow lookup to return (F _) before
                  
    | Let (x, erhs, ebody) ->
         let v = eval erhs env
         let env' = (x, v) :: env
         eval ebody env'


    | Call (efun, earg) ->
         let clo = eval efun env 
         match clo with
                 // changed! before we matched on (lookup f env) 
         | F (f, x, ebody, env0) as clo ->
             let v = eval earg env
             let env' = (x, v) :: (f, clo) :: env0
             eval ebody env'
         | _   -> failwith "expression called not a function"     
    | LetFun (f, x, erhs, ebody) ->
         let env' = (f, F (f, x, erhs, env)) :: env
         eval ebody env'

    | Num i -> I i
    | Plus  (e1, e2) ->
         match eval e1 env, eval e2 env with
         | I i1, I i2 -> I (i1 + i2)
         | _ -> failwith "argument of + not integers"
    | Minus  (e1, e2) ->
         match eval e1 env, eval e2 env with
         | I i1, I i2 -> I (i1 - i2)
         | _ -> failwith "arguments of - not integers"  
    | Times (e1, e2) ->
         match eval e1 env, eval e2 env with
         | I i1, I i2 -> I (i1 * i2)
         | _ -> failwith "arguments of * not integers" 
    | Neg e ->
         match eval e env with 
         | I i -> I (- i)
         | _ -> failwith "argument of negation not an integer"
    | True  -> B true
    | False -> B false
    | Equal (e1, e2) ->
         match eval e1 env, eval e2 env with
         | I i1, I i2 -> B (i1 = i2)
         | _ -> failwith "arguments of = not integers"      
    | Less (e1, e2) ->
         match eval e1 env, eval e2 env with
         | I i1, I i2 -> B (i1 < i2)
         | _ -> failwith "arguments of < not integers"
    | ITE (e, e1, e2) ->
         match eval e env with
         | B b -> if b then eval e1 env else eval e2 env
         | _ -> failwith "guard of if-then-else not a boolean"
