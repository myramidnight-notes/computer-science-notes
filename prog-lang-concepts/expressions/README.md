[...back](../README.md)
# Expressions
1. [__Expressions without variables__](expr-basic.md) (basic expressions)
    >* Parsing and interpretation
1. [__Expressions with global variables__ ](expr-vars.md)
    > referencing values by name, similar to algebra
    * [__Enviornment and Scope__](expr-env-scope.md)
        > The variables live in the enviornment, scope is where something is accessable.
    * [__Target expressions (numbered/location variables)__](expr-target.md)
        > uses the index/location of the stored value for reference instead of names
        >* This is generally a intermediate level/language of expressions that makes expressions við named variables simpler for the stack-machine.
        >* Stack machines with runtime enviornment
1. [__Expressions with local variables (let-bindings)__](expr-local-var.md)
    > Variables that only exist within their local enviornment, contained within their limited scope (a module or function scope).
    * [__Closed expressions__](expr-closed.md)
        > They do not need any outside information and can be evaluated with an empty enviornment.
    * __Shadowing__
        > When a variable is redefined (has same name) in inner scopes, then it's existance overshadows any previous definitions, seemingly overwriting it.
    * [__Expressions with targets and local variables__](expr-target-let.md)
        > Let's combine things together