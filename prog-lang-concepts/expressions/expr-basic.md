# Expressions
We will be using `F#` as a meta-language to explain the simple language of _expressions_, which all programming languages build on.


> Keep in mind that `F#` does not require the parenthesis when giving arguments to a function, it simply makes things clear. 

### Parsing an expression
>Expressions are written in __concrete syntax__ (plain text), and this needs to be __parsed__ into a __abstract syntax__ so it can be enterpreted as a program. 
>
>The reverse of _parsing_ would be __pretty-printing__, which takes _abstract syntax_ and converts it _concrete syntax_. It's main reason why it's refered to as pretty is because _abstract syntax_ can be very messy to look at.
>
> We are not designing a parser at the moment, but we are trying to understand how the program behaves once it has been parsed from it's expression.

#### Pretty-printing example
>```fsharp
>//Example of printing pretty 
>let rec prettyprint (e: expr) : string = 
>    match e with
>    | Num i             	-> string i
>    | Op (s, e1, e2)        -> "(" + prettyprint e1 + " " + prettyprint e2 + ")"
>```

## Expressions without variables
Let's keep things simple and just focus on  __Arithmetic expressions__  that are composed of integers and operators (such as `+` and `*`). 

### Expression as datatype
We can represent an expression as the `expr` datatype. This datatype is an __abstract syntax tree__ that represents an expression. Each term (`Num` and `Op`) will translated into the associated structures (`Num` becomes an integer, `Op` will be 3 values of specified types).
```fsharp
// Creating the expressions `Num` and `Op` 
type expr = 
    | Num of int   //now integers an be used in our expressions
    | Op of string * expr * expr //this is how we add operands to the expressions
```
> This will generate the following functions
>```fsharp
>Num : int -> expr         // Num takes an integer and turns into expression
>Op : string * expr * expr -> expr // Op takes a string and two expressions
>```

| Expression | Representation in type `expr` |
| --- | --- |
| `17` | `Num 17` 
| `3-4` | `Op("-", Num 3, Num 4)`
| `7*9+10` | `Op("+", Op("*", Num 7, Num 9), Num 10)`

### Interpreting an expression by evaluation
Expressions are nothing on their own, it is our interpretation of the expressions that give it meaning. The __evaluator__ function will be a __interpreter__ for our program, giving it meaning so it can compute something.

We can evaluate the parsed expression to an integer with a function. But before we can evaluate what `e1 + e2` is, we will first have to evaluate what `e1` and `e2` are respectively to optain two integers. For this we use __recursive functions__ that can dive deep into the provided expressions.

> We can describe this function as `eval : expr -> int`, which simply says that the `eval` function takes `expr` datatypes and outputs integers. At the moment our evaluator does not take parenthesis into account.
```fsharp
// This function is an interpreter
let rec eval (e : expr) : int =
    match e with
    | Num i -> i
    | Op ("+", e1, e2) -> eval e1 + eval e2
    | Op ("*", e1, e2) -> eval e1 * eval e2
    | Op ("-", e1, e2) -> eval e1 - eval e2
    | Op _             -> failwith "unknown primitive"
```

| expression |  Evaluating the parsed expression | Evaluation
| ---| --- | --- 
| 3 | `eval (Num 3)` | `int = 3`
| 2 * 3 - 4 | `eval (Op ("*", Num 2, Op ("-", Num 3, Num 4)))` | `int = -2`

#### Our own interpretation of expressions
>We can give operators different meanings, which is perfectly fine, because this is our interpretation. Then we get a _language_ with the same expressions but compute into different things based on the interpretation of the langage.
>```fsharp
>// Interpreting with different meaning
>let rec evalm (e : expr) : int =
>    match e with
>    | Num i -> i
>    | Op ("+", e1, e2) -> evalm e1 + evalm e2
>    | Op ("*", e1, e2) -> evalm e1 * evalm e2
>    | Op ("-", e1, e2) -> 
>         let res = evalm e1 - evalm e2
>         if res < 0 then 0 else res 
>    | Op _             -> failwith "unknown primitive"
>```
> This function now has a cutoff at zero, so we cannot go into the negative range. 