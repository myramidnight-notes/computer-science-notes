# Closed expressions
* Closed expressions are __self contained__ and do not need any outside information. 
* They can be evaluated in an empty __enviornment__.
    * any variable referenced that cannot found in that enviornment during evaluation would indicate a outside reference.

If an expression ever references a variable that is missing from it's enviornment , that means that it's referencing outside of it's scope and that means it's _not_ a __closed expression__.

## Checking for closed expressions
It can be important to be able to tell if an expression is closed, not all programming languages allow us to access variables defined outside of the current scope.  

If we run into a variable that is not contained within the list of available variables (the accessable enviornment), then either that variable was either __defined outside__ of the current scope. That would mean that the expression is __not__ closed. 


### Code example
```fsharp
// let contains x xs =  List.exists (fun y -> x = y) xs
let rec contains x xs : bool = 
    match xs with
    | []      -> false
    | y :: xs -> x = y || contains x xs

// closedin checks that the free variables are contained in a given list.
let rec closedin (e : expr) (xs : string list) : bool =
    match e with
    | Var x -> contains x xs
    | Let (x, erhs, ebody) -> 
          let xs1 = x :: xs 
          closedin erhs xs && closedin ebody xs1
    | Num i -> true      
    | Op (op, e1, e2) -> closedin e1 xs && closedin e2 xs
```
* #### `contains` checks if `x` appears in the list `xs`. 
    * Either `x` would be the head of the list (`x = y`) or it will be in the tail of the list (using the recursion to  run the tail through `contains` again). 
* #### `closedin` will check if a given expression is a _closed expression_.
    * An expression is closed if there are no free/missing variables
        > We cannot reference any variables that weren't defined before 
    * When we run into a _variable_, then we just check if it's within the given list of variables.
    * When evaulating a `Let`, 
        * The _variable_ that is being bound by the `Let` isn't included in the available variables for the orignal expression, but it is included for the scope of the new `Let`. 
            > We need to add the bound variable to the list of available variables when checking the _body_ of the `Let` (`xs1 = x::xs`)
        * We also continue to check the rest of the given expression
    * Any numbers will be `true` for the evaluation, because it's a direct value rather than a variable.
    * When evaluating an operation (`Op`), then we need to check both expressions that come as it's arguments.
