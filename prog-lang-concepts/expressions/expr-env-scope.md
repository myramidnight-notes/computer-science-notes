# Environment (scope)
When it comes to programming, the enviornment are the surroundings that the program has access to, where all the variables exist. If we expect to get a value from a variable, then it needs to exist in the enviornment that program can reach.

* An __enviornment__ is a somewhat isolated area of code
    > The enviornment is where all the variables live, and each scope will have it's own enviornment, a extention of the parent's enviornment in most cases, which allows a child scope to access the parent's enviornment variables.
    >
    > A __global variable__ would live in the __global enviornment__, essentially the root of the code structure. Within each child of this structure would be their own extention of the parents's enviornment, added ontop of the variables that the parent had access to. So the child has access to the parent's enviornment, but the parent cannot see the child's enviornment.
* The __scope__ of something is the location where it is accessable.
    > You might be most familiar with the __function scope__ in programming, which is the area within the function. Any code within that function is only accessable from within that function. 
    >* This is explained with `Let` bindings in these notes
