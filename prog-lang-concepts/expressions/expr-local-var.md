
* [Keyword of the day: let](http://dobegin.com/let-keyword/)
# Expressions with local names (let-bindings)
> * Defining local variables with `Let` binding
> * Evaluating `Let`
> * Shadowing

Sometimes we want to have named variables that only have meaning within a limited scope (not global), which sometimes are just to simplify things for the programmer.

A scope is the enviornment where a variable has meaning, and these scopes can be a __module__ or __function__, and the variables will be considered to be __local variables__ contained within that scope.

In order to make variables useful to us, we will have to be able to assign these variables to the enviornment. A scope is the environment in which the variable exists. This could be on a global-scope or within a limited function scope. 

### Our `Let` syntax
> Keep in mind that `let` of our meta-language (`F#`) is not same as the `Let` that we're creating. It's an example of a __scope__, 
>
>If a variable in a local scope has same name as existing variable, then it does not actually overwrite it, but instead overshadows it, making the previous instance inaccessable while we are inside this scope. This inner variable is then discarded when we leave that scope.
> ```
> Let [var name] = [var value] in [expression in scope]
> ```
> * `Let` just tells the evaluator that we are defining a variable
> * We tell it the name of the variable that we're binding with `Let`
> * `in` seperates the var value from the scope, where the variable will be accessable.
## Defining the local variables
A __let-binding__ comes in two parts: 
1. __name__ 
    > which is the variable within the local scope
1. __value__ 
    > It can be a datatype (integer, string, obj, list... ) or an expression. This can include nested scopes with it's own let-bindings (this would be a function scope) 


| Some let-bindings | expression 
| --- | --- 
| with expression | `let z = 17 in z + z`
| with expression as value | `let z = 5 + 6 in z + z`
| with nested scope | `let z = 17 in (let z' = 2 in z * z') + z` 
| with nested scope as value | `let z = (let y = 3 in y * 2) in z + z`
> If we look at the las example, then we have `let z = 17` which is the defenition of name and value, and then what comes after `in` is the body of the let. It does not always have a body.

### Adding `let` to the expressions
>```fsharp
>//Example of expressions including the let-binding
>type expr = 
>    | Var of string
>    | Let of string * expr * expr // let x = erhs in ebody
>    | Num of int  
>    | Op of string * expr * expr
>```
### Looking up variables
```fsharp
type envir = (string * int) list  // values of named variables 

let rec lookup (x : string) (env : envir) =
    match env with 
    | []          -> failwith (x + " not found")
    | (y, v)::env -> if x = y then v else lookup x env
```
### Evaluating the expressions
```fsharp
let rec eval (e : expr) (env : envir) : int =
    match e with
    | Var x              -> lookup x env 
    | Let (x, erhs, ebody) ->       //var, right-hand-side, body
         let xval = eval erhs env
         let env1 = (x, xval) :: env 
         eval ebody env1
                // the environment is extended by an additional  binding
    | Num i             -> i    
    | Op ("+", e1, e2) -> eval e1 env + eval e2 env
    | Op ("*", e1, e2) -> eval e1 env * eval e2 env
    | Op ("-", e1, e2) -> eval e1 env - eval e2 env
    | Op _             -> failwith "unknown primitive";;

let run e = eval e []
```
> When we are evaluating the `Let`, we will have to evaluate it's body as well. The `Let` is it's own scope, so it can both access the global enviornment as well as define it's own variables that will only exist while the `Let` is being evaluated. 
>
>It will append it's local variables to the front of the global enviornment list, which is actually a new list within the scope (these additions will not effect the global enviornment), and if the local variables have identical names to existing ones, then the _first instance_ of it will be used (the one we just appended). 
>
>This new local enviornment `env1` will be used when evaluating the body of the `Let`, and discarded when we return back to the global or parent scope.

With the local variables in play, we can actually provide the evaluator with a empty enviornment, since it will be able to add variables to the enviornment during evaluation. 

Many programming languages have a default value for undefined variables, which is generally `null` or `NONE`, in order to avoid breaking the program at runtime. It might break if those `null` values are not handled in the program though.

>In the context of how we do programming, the global scope are things defined by the system and we are generally only aware of our own locally defined variables within our code unless we target that global enviornment (they tend to have special characters/symbols at the front of the variable names, so they are not accidentally accessed). 
>
> So essentially  when we start writing a program in a file, our local scope is the file and it's local enviornment is empty until we define our own variables.

### Shadowing of variables
When we have a locally defined variable that shares the same name as existing variable in the global or parent enviornment, then the local instance of it will overshadow the other instances, because in our implementation only returns the first instance it comes across when looking for a variable in the enviornment. Any new variables added within the local scope each time is added to the front of the enviornment list so the latest instances are the valid ones.

With the recursive nature of `F#`, the local enviornment in nested scopes the new variables only exist within that scope and get discared when the program returns to the parent. This means local scope do not effect the parent scope.