# Target expressions with `Let` bindings
* Defining the expressions
* Compiling source expressions to target expressions
* Evaluating target expressions with a runtime enviornment
* Stack-machine with intermediate values

### Defining the expressions
* Target expressions with numbered instead of named variables
* The `Let` (now `TLet` just to keep things distinct) has only 2 arguments now instead of 3
```fs
type texpr =                             
    | TVar of int
    | TLet of texpr * texpr     // Let [expression] in [body]
                        // new var always gets number 0
    | TNum of int
    | TOp of string * texpr * texpr

```

### Compiling source expression to target expression
* Change named variables to target variables
* Within the scope of a `Let`, we add new variables to the front of the enviornment, shifting existing variables down by 1 (they get discarded when we leave the scope).

We would expect the source expression to be using named variables, because then it's easier for us humans to understand what variables are being referenced. But the machine will prefer target expressions because fetching values from a set location is quicker than having to search for the value associated to a name (the name means nothing to the machine).

When we reach a local variable, it is only concerned about he scope it lives in (the `TLet`) and does not keep track of the index count outside of it's own scope,. We essentially will have a new order of indexes starting at `0` for that scope, shifting all previously existing variables down by `1` for each variable in the new scope. 
```fs
type cenvir = string list

let rec getindex (cenv : 'a list) (x : 'a) : int = 
    match cenv with 
    | []      -> failwith "Variable not found"
    | y::cenv -> if x = y then 0 else 1 + getindex cenv x

let rec tcomp (e : expr) (cenv : cenvir) : texpr =
    match e with
    | Var x  -> TVar (getindex cenv x)
    | Let (x, erhs, ebody) ->         
        let cenv1 = x :: cenv 
        TLet (tcomp erhs cenv, tcomp ebody cenv1)
                         // new var gets number 0
                         // old var numbers are increased by 1
    | Num i -> TNum i
    | Op (op, e1, e2) -> TOp (op, tcomp e1 cenv, tcomp e2 cenv)

```

### Evaluating target expressions with a runtime environment
In this interpritation, variables are just index pointers, and any new variables will be added index `0`
```fs
type renvir = int list  // values of numbered variables

let rec teval (e : texpr) (renv : renvir) : int =
    match e with
    | TVar n  -> List.item n renv     // variable n is at position n
    | TLet (erhs, ebody) -> 
        let xval = teval erhs renv
        let renv1 = xval :: renv      
        teval ebody renv1 
    | TNum i -> i
    | TOp ("+", e1, e2) -> teval e1 renv + teval e2 renv
    | TOp ("*", e1, e2) -> teval e1 renv * teval e2 renv
    | TOp ("-", e1, e2) -> teval e1 renv - teval e2 renv
    | TOp _             -> failwith "unknown primitive"
```
>* `renvir` is the _runtime enviornment_, and the values of numbered variables are placed at the top of the enviornment (it is essentially a __stack__)
>* `TLet` only has 2 arguments now, omitting the variable name, and any new variable will be automatically added at index `0`.
>   * `xval` is the evaluation of the variable being bound by the `TLet`
>   * `renv1` is the runtime-enviornment for the body of the `TLet`

>We could evaluate the following expression 
>```fs
>let myexpr = TLet (TNum 17, TOp ("+", TVar 0, TVar 2))
>let myenv = [42;1011;2021;-3]
>
>// Let's evaluate myexpr with myenv
>teval myexpr myenv
>```
>> results would be `1028`, because within the body of the `TLet` we add `17` to the enviornment at index `0`, shifting all the previous variables by 1, so we have `17 + 1011` instead of `42 + 2021` while we're within the body. 
>
>By referencing to `TVar 2` from within the body of the `TLet` (when the scope only has a single variable defined), it is pointing outside of the scope.

## Stack machine with intermediate values
This stack machine has the following arguments:
1. The instructions (`inss`)
1. A stack (`stk`) for _intermediate values_ 
1. heap/stack for _variable-bound_ values (the environment, `renv`).

When we have inner scopes that redefine the enviornment (possible shadowing), then we have to be able to move things around while we compile

### Defining the instructions
```fsharp
type rinstr =
    | RLoad of int
    | RStore
    | RErase              
    | ...              // Rest are the same as the initial rinstr

type rcode = rinstr list

type stack = int list         // intermediate values
```
* `RStore` 
    > Moves top value from the _stack_ and places it at the `0` position of the _enviornment_. It shifts everything in the enviornment down by `1`
* `RErase`
    > Removes the `0` value from environment, shifting the remaining values up by `1`. This is essentially the __garbage disposal__, to reset the stack to the state it was before we entered a inner scope (for example the body of a `TLet`).

### Compile target expression to stack-machine code
```fs
let rec rcomp (e : texpr) : rcode =
    match e with
    | TVar n             -> [RLoad n]
    | TLet (erhs, ebody) -> rcomp erhs @ [RStore] @ rcomp ebody @ [RErase]
    | TNum i             -> [RNum i]
    | TOp ("+", e1, e2)  -> rcomp e1 @ rcomp e2 @ [RAdd]
    | TOp ("*", e1, e2)  -> rcomp e1 @ rcomp e2 @ [RMul]
    | TOp ("-", e1, e2)  -> rcomp e1 @ rcomp e2 @ [RSub]
    | TOp _              -> failwith "unknown primitive"

```
### Evaluating the stack-machine instructions
The _stack-machine_ interpreter will now take a 3rd argument, the enviornment (empty list initially, gets filled during the evaluation).
```fsharp
// * Interpreting (running) stack machine code

let rec reval (inss : rcode) (stk : stack) (renv : renvir) =
    match inss, stk with 
    | [], i :: _ -> i
    | [], []     -> failwith "reval: No result on stack!"
    | RLoad n :: inss,             stk ->
          reval inss (List.item n renv :: stk) renv
    | RStore  :: inss,        i :: stk -> reval inss stk (i :: renv)
    | RErase  :: inss,             stk -> reval inss stk (List.tail renv) 
    | RNum i  :: inss,             stk -> reval inss (i :: stk) renv
    | RAdd    :: inss, i2 :: i1 :: stk -> reval inss ((i1+i2) :: stk) renv
    | RSub    :: inss, i2 :: i1 :: stk -> reval inss ((i1-i2) :: stk) renv
    | RMul    :: inss, i2 :: i1 :: stk -> reval inss ((i1*i2) :: stk) renv
    | RPop    :: inss,        i :: stk -> reval inss stk renv
    | RDup    :: inss,        i :: stk -> reval inss ( i ::  i :: stk) renv
    | RSwap   :: inss, i2 :: i1 :: stk -> reval inss (i1 :: i2 :: stk) renv
    | _ -> failwith "reval: too few operands on stack"
```
>> __Note:__  We could technically do without `RPop`, `RDup` and `RSwap` when we have `RLoad`, `RStore` and `RErase`, but it would increase the list of instructions needed to perform those actions. 