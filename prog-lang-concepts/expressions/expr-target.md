
# Target expressions (Pointers)
>* Defining a targets 
>* Evaluating targets

Instead of having to search a list for a matching variable name, consider having variables that are simply targeting a specific location within the enviornment. Such as `Var 3` would point to the 4th item in the enviornment list (since we start counting at `0`).

With this kind of targeting, the lookup would be quicker because there is essentially no searching, we just check at the specified location and either find what we're looking for or not (value being `null` or out of range).

Though these kind of variables are not very descriptive of their purpose, so this would make most sense for compiled code where all variables are defined when the program is executed, and when we are aiming at being efficient and compact (don't expect it to be a very _human readble_ code after compilation).

> This would be a good representation of __pointers__, where we find values in memory by going to the indicated position. 
>
>It is also good to note that if you have _named variables_, then you also have to keep track of where their values are stored. Targeted expressions would minimize the time spent searching.

### Defining targets
If you have used any `C` language, there they use `*` to let the evaluator know that the value in question is a pointer. This is essentialy what target expressions are. 

>```fsharp
>// Example of target expression using numbers
>type texpr =                             
>    | TVar of int                       // a number rather than a name
>    | //... then the rest of the defined expressions
>```

### Evaluating targets
>And we would have to make the evaluator (we have added T at the front just to avoid conflicting with the `eval` functions from before)
>```fsharp
>let rec teval (e : texpr) (renv : renvir) : int =
>    match e with
>    | TVar n  -> List.item n renv     // variable n is at position n  
>    |  // Then the rest of the expressions and operators
>```


### Correctness of compilation
>It is very important when you have two different workflows (the syntax), then it is important to be sure that the variable names match up with the target, that they are really the same thing.
>
>If we evaluate the source expression (`eval e env`) and check if the compilor environment is equal to the target enviornment as well. 
>```fsharp
>eval e env = teval (tcomp e cenv) renv 
>      if for any n, List.item n renv = lookup (List.item n cenv) env
>      // for any item in renv, the cenv item should be found in env
>```
>* `renv` is runtime env, `cenv` is compile env and `env` is the source env

