
# Expressions with Variables
>* Defining variables
>* Enviornment for variables
>* Looking up variable values
>* Evaluating expressions with variables
>* Stack-machine with runtime enviornment
>   * Compile source to target expressions

### Defining variable expressions
We have all gotten familar with variables through algebra, where `x` and `y` could be replaced by any number. To implement this, we will need to create the expression `Var`, and extend the _evaulator_ to give meaning to those variables.
```fsharp
// Creating the expressions `Num` and `Op` 
type expr = 
    | Num of int 
    | Var of string
    | Op of string * expr * expr 
```

| Expression | Representation in type `expr` |
| --- | --- |
| `3+a` | `Op("+", Num 3, Var "a")`
| `b*9+a` | `Op("+", Op("*", Var "b", Num 9), Var "a")`


## Setting up an Enviornment for variables
We can create an _enviornment_ that gives meaning to those variables (this would technically be all declared variables accessable within current scope). We will be using a __map__ or __dictionary__ that will be an __association list__.


Variable names are used for reference, so it is important that they are unique to prevent overlapping (it could end up fetching the incorrect value if there are duplicate variable names)

```fsharp
// Creating the enviorment datatype, it defines the shape of the structure
 type envir = (string  * int) list 

let env = [("a", 3) ;  ("b", 111) ; ("c", 78)]
```

> In the context of computers and hardware, an environment could be the memory, and references to the memory-cell blocks would be used instead of variable names. Perhaps there would be a intermediate dictionary that would connect the variables within a program to their location within memory. This could be emulated.

### Looking up variable values
 We can then attempt to look up the variable within the enviorment. 
```fsharp
let rec lookup (x: string) (env: envir ) = 
   match env with
   | [] -> failwith (x + " not found")
   | (var,value)::env -> if x=var then value else lookup x env 
   //else continue down the list
```

### Evaluating expression with variables
Then we can have the evaluater lookup the enviorment to find it's integer values for the variables. 
> The following syntax explains that `env` is a `list` containing tuples of the format `(string * int)`.
>```fsharp
>let rec eval (e: expr) (env : envir) : int =
>    match e with
>    | Num i -> i
>    | Var x -> lookup env x //look up the variable
>    | Op ("+", e1, e2) -> eval e1 env + eval e2 env
>    | Op ("*", e1, e2) -> eval e1 env * eval e2 env
>    | Op ("-", e1, e2) -> eval e1 env - eval e2 env 
>    | Op _             -> failwith "unknown primitive"
>```
>>Testing by giving the evaluation an expression and an enviornment list
>>```fsharp
>>eval (Var "c") ["a", 13; "b", 17; "c", 29] //output: 29
>>```

## Stack-machine with runtime enviornment
In order to keep things simple for the __stack machine__, we will have to convert all named variables to __target expressions__, so that the machine will simply know exactly where to find the value. 

* Look at the __notes about target expressions__ to see the stack-machine implementation 


### Compile source expressions to target expressions
> Changing `Var` into `TVar`
>* Look at the notes for __target expressions__ for more...

To simplify expressions with named variables for the stack-machine, then we will have to use a intermediate compilor that changes them to target expressions.

Since the variables are already stored as a list, their __index__ in the variable list would have direct corrolation to their __location in the target list/memory__. The code example below is just converting the names into indexes.

>```fsharp
>type cenvir = string list // C is for compile time enviornment
>
>//A helper function to find the variable and it's index
>let rec getindex (cenv : cenvir) (x : string) : int = 
>    match cenv with 
>    | []      -> failwith "Variable not found"
>    | varString::cenv -> if x = varString then 0 else 1 + getindex cenv x
>
>// Target expression compilor
>let rec tcomp (e : expr) (cenv : cenvir) : texpr =
>    match e with
>    | Var x  -> TVar (getindex cenv x) //match variable by index
>    | Num i -> TNum i
>    | Op (op, e1, e2) -> TOp (op, tcomp e1 cenv, tcomp e2 cenv)
>```
>> `getindex ["a";"b";"bah"] "bah" //output: 2`