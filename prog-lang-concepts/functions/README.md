[...back](../README.md)
# Functional Programming Languages
__Functional programming__  is a _paradigm_ where the primary means of computation are done applying and composing functions. It falls under the __declarative programming__ _paradigm_.

### Some definitions
* A __pure functional language__ cannot have any side effects. A function's output is decided purely by it's input (no external influences). 
    * What are side effects? Mutable data, changing state, even printing to console counts as a side effect.
* A __first order functional language__ doesn't allow functions to be passed as values (in other words, the value of an expression cannot be a function).
* A __first class functional language__ allows us to treat functions like any other variable, which allows us to return functions as values from functions, store them in variables or pass them as arguments to other functions.
    * _first class functions_ implies the presence of _higher order functions_, but not the other way around.
* A __higher order function__ will do at least one of the following things (_not nessicerily both_):
    1. _takes one or more functions as arguments_
    1. _returns a function_
* A __recursive function__ is a function which calls itself until it doesn't.
* A __function closure__ (or sipmly _a closure_) is a technique for implementing lexically scoped name binding in a language of first-class functions. 
    * Simplest way to think about _closures_ is a function that can be stored as a variable.
    * The term __closure__ is often used as a synonym for _anonymous functions_.

## Index
* ### Closer Definitions
    * [__Static vs. Dynamic scopes__](scopes.md)
    * [__Function Closures__](closures.md)
    * [__Typing and Type inference__](type-inference.md)
    * [__Polymorphic types (with fsi)__](poly-type-inference.md)
    * [__Lambda calculus__](lambda.md)
* ### [First-order functions](functions-1st-order.md)
    > Static/Dynamic scope explained with the implementation of a first-order functional language. 
    * [__parsing a first-order functional language__](parsing-1st-order.md)
    * [__type inference/checking (first order)__](functions-1st-order-infer.md)

* ### [Higher order functions](functions-higher-order.md)
    * [__Implmementing polymorphic type inference__](functions-higher-order-poly.md)
* ### lambda-calculus (Higher Order)
    > How do we curry our functions?!
    * Untyped lambda-calculus
    * Simply typed lambda-calculus
    * Polymorphically typed lambda calculus

* ### [Extras](extras/README.md)
    > Alternative implementations and things from exersises. These might end up sem code notebooks.
    * [__First order: Multiple arguments__ (example implementation)](functions-1st-order-multi-args.md)
    * [__First order: Multi args with types__](functions-1st-order-multi-arg-type.md)
    * [__Higher order: pair types__](functions-higher-order-pairs.md)
    * [__Higher order: pair types infer__](functions-higher-order-pair-infer.md)