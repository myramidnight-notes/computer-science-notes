>[...back](./README.md)
# Closures
Also called __lexical closures__ or __function closures__
### Descriptions of _function closures_:
> "A closure is a technique for implementing lexically scoped name binding in a language with first-class functions. Operationally, a closure is a record storing a function together with an enviornment" 
>>Wikipedia

>"A function closure is a tuple consisting of the name of the function, the name of the function's parameter, the function's body expression, and the function's declaration enviornment." 
>>Programming language concepts by Peter Sestoft

> "A closure is the combination of a function bundled together (enclosed) with references to its surrounding state (the lexical environment). In other words, a closure gives you access to an outer function’s scope from an inner function." 
>>Mozilla

So essentially a __function closure__ is a function that can be stored in a variable.

### Example
Here is an example how a closure could be implemented
```
Fun(foo, x, body, env)
```
* `foo` would be the function name
* `x` would be the argument
* `body` would be the function's body expression
* `env` would be the enviornment upon declaration, which the body should be evaluated in when the function is called.

## Anonymous functions
The term _closure_ is often used as a synonym for __anonymous functions__, though strictly speaking, a anonymous function has no name, while a closure is an instance of a function.