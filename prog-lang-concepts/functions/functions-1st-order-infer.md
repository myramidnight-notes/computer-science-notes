>[...back](./README.md)
# First order: Type inference
## Defining our language
We will be modifying our __first-order functional language__  slightly so that types are __declared__ for the __input__ (_function parameter_) and __output__ (return value of the _function body_) 

This allows us to infer types without having to hope and guess that we've got correct types.
* By checking if the defined type agrees with the actual type, to see if we have a __well typed__ function. 
    * The input type has to match the argument type, and the output type has to match what is returned from the function.
#### Our types
>```fs
>type typ =
>    | Int                                // int            
>    | Bool                               // bool
>    | Fun of (typ * typ)                 // t -> t'
>```
#### Our expressions
>```fs
>type expr =
>    | Var of string     // x
>    | Let of string * expr * expr
>                        // let x = erhs in ebody
>    | Call of string * expr
>                        // f e                  
>    | LetFun of string * string * typ * typ * expr * expr
>                        // let(rec) f (x : t) : t' = erhs in ebody        
>    | Num of int
>    | Plus of expr * expr
>    | Minus of expr * expr    
>    | Times of expr * expr
>    | Neg of expr
>    | True
>    | False
>    | Equal of expr * expr
>    | Less of expr * expr
>    | ITE of expr * expr * expr
>```
>* The only differece here would be that `LetFun` not only takes two names and two expressions, but also two types.

#### The enviornment and lookup
>```fs
>type 'a envir = (string * 'a) list
>```
>```fs
>let rec lookup (x : string) (env : 'a envir) : 'a =
>    match env with
>    | []          -> failwith (x + " not found")
>    | (y, v)::env -> if x = y then v else lookup x env
>```
>* Having `'a` as acceptable type (meaning _any type_), it allows us to use the same `envir` type and `lookup` for both the __type inference__ as well as the __evaluator__ 

## Type inference
The enviornment for our inference will contain types instead of actual values, because we don't really care about the numerical results, just about the type produced by the given expression.

Type inference is quite similar to __evaluation__, except that it computes types instead of values. 

#### Our inference implementation
* We cannot know if there is a guard in place to catch possible mis-matching types from a __if-statement__, so we will simply fail the inference if the branches (_if-else_) have different types.
    * We can only be sure of the type of __if-statements__ if both branches are of the same type. 
* We will also type-check __functions__ and __function calls__, to ensure that the input and output matches the declared types when the function was defined.
* We also assume that the language has the __static scope__ rule.

```fs
// However: in function calls and letfun, we also
// check types. We infer types and compare them to the declared
// types.

let rec infer (e : expr) (env : typ envir) : typ =
    match e with
    | Var x  -> 
         match lookup x env with
         | Int  -> Int
         | Bool -> Bool
         | _    -> failwith "a function used as a value"
    | Let (x, erhs, ebody) ->
         let t = infer erhs env
         let env' = (x, t) :: env
         infer ebody env'

    | Call (f, earg) ->
         match lookup f env with
         | Fun (t, t') ->
             if infer earg env = t then t'
             else failwith "call argument not of declared type"
         | _ -> failwith "variable called not a function"    
    | LetFun (f, x, t, t', erhs, ebody) ->
                   // let(rec) f (x : t) : t' = erhs in ebody
         let env' = (x, t) :: (f, Fun (t, t')) :: env
         if infer erhs env' = t' then infer ebody env' 
         else failwith "letfun function body not of declared type"

    | Num i -> Int
    | Plus  (e1, e2) ->
         match (infer e1 env, infer e2 env) with 
         | Int, Int -> Int
         | _ -> failwith "arguments of + not integers"
    | Minus  (e1, e2) ->
         match (infer e1 env, infer e2 env) with 
         | Int, Int -> Int
         | _ -> failwith "arguments of - not integers"  
    | Times (e1, e2) ->
         match (infer e1 env, infer e2 env) with 
         | Int, Int -> Int
         | _ -> failwith "arguments of * not integers"
    | Neg e ->
         match infer e env with 
         | Int  -> Int
         | _ -> failwith "argument of negation not an integer"
    | True  -> Bool
    | False -> Bool
    | Equal (e1, e2) ->
         match (infer e1 env, infer e2 env) with 
         | Int, Int -> Bool
         | _ -> failwith "arguments of = not integers"
    | Less (e1, e2) ->
         match (infer e1 env, infer e2 env) with 
         | Int, Int -> Bool
         | _ -> failwith "arguments of < not integers"
    | ITE (e, e1, e2) ->
         match infer e env with
         | Bool -> let (t1, t2) = (infer e1 env, infer e2 env)
                   if t1 = t2 then t1
                   else failwith "if-then-else branches of different types"
         | _ -> failwith "guard of if-then-else not a boolean"     
```
>* The value/type of `Let` is the value of it's __body__ 
>   * It needs to work with extended enviornment, so that the body can work with the variable that the `Let` is binding. Same goes for the functions, it needs to be able to reference itself in the extended enviornment, in order to create recursive functions.
>* `Equal` and `Less` are both comparators that should result in a boolean
>* For the `ITE` (_if-then-else_) to work, the expression `e` needs to be a boolean value. 
>   * `e` is the __guard__ of the _if-statement_, because it protects `e1` from being accessed unless `e` is __true__.
>   * The `ITE` is only well-typed if both `e1` and `e2` are of the same type. And in our implementation only allows us to make well typed if-statements.
#### Type checker function (top-level)
> Here is a our type-checker function, which runs the given expression `e` through `infer` and asks if the resulting type matches the expected type `t`.
>```fs
>let rec check (e : expr) (env : typ envir) (t : typ) : unit =
>    if infer e env = t then ()
>    else failwith "top-level expression not of declared type"
>    
>// Why are closures not needed here like they were for eval?
>// Because we infer and check the type of the body of a function already
>// when it is defined.
>// When it is called, we can already assume that it is type-correct.
>// So we don't need to remember the types of the variables defined
>// outside the function body.
>```

## The evaluator

>>A language is said to be statically typed (better: have static type-checking) if a program is checked for type-correctness at compile-time.
>>Type-correct programs should not give errors at runtime, but this is not quite true (think division by 0, or out-of-bounds array access).
>>
>>A language is dynamically typed if it has types but no static type-checking.
>>
>>Why is static typing good?
>>
>>Lisp, Scheme, ECMAScript/Javascript, Perl, Postscript, Python, Ruby are dynamically typed.
>>
>>Java and C# are for the most part statically typed, but not fully.
>>
>>Eg dynamic typing in Java and C# reference-type array assignment, eg this code in Java compiles but gives a run-time error.
>>```
>>Integer[] arr = new Integer[16];
>>Number[] arrN = arr;
>>
>>arrN[0] = new Double(3.14);
>>```

```fs
// Here's the evaluator once again. It just ignores the type annotations.

type value =
    | I of int
    | B of bool
    | F of string * expr * value envir

let rec eval (e : expr) (env : value envir) : value =
    match e with
    | Var x  -> 
         match lookup x env with
         | I i -> I i
         | B b -> B b
         | _   -> failwith "a function used as a value"
    | Let (x, erhs, ebody) ->
         let v = eval erhs env
         let env' = (x, v) :: env
         eval ebody env'

    | Call (f, earg) ->
         match lookup f env with
         | F (x, ebody, env0) as clo ->
             let v = eval earg env
             let env' = (x, v) :: (f, clo) :: env0
             eval ebody env'
         | _   -> failwith "variable called not a function"     
    | LetFun (f, x, _, _, erhs, ebody) ->
         let env' = (f, F (x, erhs, env)) :: env
         eval ebody env'

    | Num i -> I i
    | Plus  (e1, e2) ->
         match (eval e1 env, eval e2 env) with
         | I i1, I i2 -> I (i1 + i2)
         | _ -> failwith "argument of + not integers"
    | Minus  (e1, e2) ->
         match (eval e1 env, eval e2 env) with
         | I i1, I i2 -> I (i1 - i2)
         | _ -> failwith "arguments of - not integers"  
    | Times (e1, e2) ->
         match (eval e1 env, eval e2 env) with
         | I i1, I i2 -> I (i1 * i2)
         | _ -> failwith "arguments of * not integers" 
    | Neg e ->
         match eval e env with 
         | I i -> I (- i)
         | _ -> failwith "argument of negation not an integer"
    | True  -> B true
    | False -> B false
    | Equal (e1, e2) ->
         match (eval e1 env, eval e2 env) with
         | I i1, I i2 -> B (i1 = i2)
         | _ -> failwith "arguments of = not integers"      
    | Less (e1, e2) ->
         match (eval e1 env, eval e2 env) with
         | I i1, I i2 -> B (i1 < i2)
         | _ -> failwith "arguments of < not integers"
    | ITE (e, e1, e2) ->
         match eval e env with
         | B b -> if b then eval e1 env else eval e2 env
         | _ -> failwith "guard of if-then-else not a boolean"
```