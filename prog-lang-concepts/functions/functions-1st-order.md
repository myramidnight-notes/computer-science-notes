# First order functional language
>Should not be confused with __first class functional languages__.

__First order functional language__ does not allow us to pass functions as values. 

We will explore the concept of __static__ and __dynamic__ scopes with this implementation.

## Static and Dynamic scope
We should consider how scopes work before we continue, because the type of scope we choose to implement will have different effects on how the scope will look at it's surroundings, specially how it will define the value of a referenced variable that was defined outside of it's inner scope.

* #### Dynamic scope
    > It is the older type of scoping, and takes into account the nesting of blocks from where the function was called
    >
    > Any variables used within the scope have to be defined __before the scope is entered__ (such as calling a function), it does not care if it was defined before the scope was initially defined, because it only looks at the latest definition closest to where the scope was entered.
* #### Static scope
    > It only looks at the enviornment in which the function was defined. This is the type of scoping used in nearly all modern programming languages, and is more straight forward.
    >
    > Any variables used within the scope have to be either defined __before the scope was defined__ or defined within the scope (this includes parameters for function scopes). Anything else is simply undefined.

* ### [__More details about scopes...__](scopes.md)

## Parsing 1st-order functional language
> I found the parser complex enough to be moved to it's own page.
>* [Read about parsing first-order functiona language](parsing-1st-order.md)

## Implementing functions
We will be adding __function definitions__ and __function calls__ to our expression language.

* Rules for our implementation of functions
    * functions must be named (no anonymus functions)
    * value of an expression cannot be a function
    * can only take one argument (for simplicity)

This language is __first-order__

If we want to make a function __recursive__, then it can call itself where it's being defined (the _function body_).

### Defining our expressions
```fs
type expr =
    // Variable definitions
    | Var of string                             // x
    | Let of string * expr * expr               // let x = erhs in ebody    

    //Function definitions
    | Call of string * expr                     // f e
    | LetFun of string * string * expr * expr   // let(rec) f x = erhs in ebody

    // Operators
    | Num of int
    | Plus of expr * expr
    | Minus of expr * expr    
    | Times of expr * expr
    | Neg of expr
    
    // Comparators
    | True
    | False
    | Equal of expr * expr
    | Less of expr * expr
    | ITE of expr * expr * expr    
```
* The function (`LetFun`) will be a scope, just like the `let` that we've grown used to.
    * We give the function a _name_ (accessable only within the body of the `let`)
    * A _parameter_ for the argument passed to the function (accessable only within the _function body_)
    * the _function body_ 
    * the _body_ of the `let`

### Example of the concrete syntax
>```
>let f x = x + 8 in f 5
>//let [function name] [argument name] = [function scope] in [let scope]
>```
>1. Here we define the function named `f`
>1. it has the argument `x` 
>    * `x` can be used within the __function scope/body__
>    * we can create a __recursive function__ by using `f` within the body of the function.
>    * in different implementations, `x` could be a list of arguments, serving as the enviornment for the function scope.
>1. finally we have the __scope of the let-binding__
>    * we can _call_ `f` within this scope with `f 5`, passing __5__ as an argument to `f`

### Environment and Lookup
```fs
type 'a envir = (string * 'a) list

let rec lookup (x : string) (env : 'a envir) : 'a =
    match env with
    | []          -> failwith (x + " not found")
    | (y, v)::env -> if x = y then v else lookup x env
```

### Defining values
We are introducing the __boolean__ (`true/false`) to our expression language as a possible value type, which have only been integers so far (when there was only a single type of value, there was hardly a need to define a `value` datatype).

```fs
type value =
    | I of int
    | B of bool
```
> And then finally adding _functions_ to the types of values (to let us store functions within variables), we first need to decide if it's going to be a __static__ or __dynamic__ scope.
>#### Static scope version
>```fs
>    | F of string * expr * value envir
>```
>#### Dynamic scope version
>```fs
>    | F of string * expr
>```

### Evaluating the expressions with functions
```fs
let rec eval (e : expr) (env : value envir) : value =
    match e with

    | Var x  -> 
        match lookup x env with
        | I i -> I i
        | B b -> B b
        | _   -> failwith "a function used as a value"

    | Let (x, erhs, ebody) ->
        let v = eval erhs env
        let env' = (x, v) :: env  //adds
        eval ebody env' //uses an exptended enviornment
```
>* We want to prevent variables to return functions as the value of an expression
>* For `Let` bindings: The evaluator extends the enviornment to include the variable that is being bound by the `Let` before evaluating the body of the binding. 
#### Static scope version
>```fs
>    // The following implments a Static scope:
>    | Call (f, earg) ->
>        match lookup f env with
>        | F (x, ebody, env0) as clo ->
>            // argument evaluated in current env
>            let v = eval earg env
>            // including the argument and function in extended env
>            let env' = (x, v) :: (f, clo) :: env0
>            // function body evaluated
>            eval ebody env'
>        | _   -> failwith "variable called not a function"     
>    | LetFun (f, x, erhs, ebody) ->
>        // adding the function name to extended enviornment
>        let env' = (f, F (x, erhs, env)) :: env
>        // def-time environment recorded in closure 
>        eval ebody env'
>```
>* Only within a __function call__ is allowed to use function stored in variables
>* When evaluating a bound function `LetFun`:
>    * we will include the argument and current enviornment when we add the function to the extended enviornment (for evaluating the body)

#### Dynamic scope version
>```fs
>    | Call (f, earg) ->
>        match lookup f env with
>        | F (x, ebody) as clo ->
>            // argument evaluated in current env
>            let v = eval earg env
>            // including the argument and function in extended env
>            let env' = (x, v) :: (f, clo) :: env
>            // function body evaluated
>            eval ebody env'
>        | _   -> failwith "variable called not a function" 
>    | LetFun (f, x, erhs, ebody) ->
>        let env' = (f, F (x, erhs)) :: env
>        // def-time envmnt NOT recorded in closure 
>        eval ebody env'
>```
>* Mostly the same as the __static scope__ version, __except__ that we do NOT include the current enviornment when adding the function to the extended enviornment, so it will just look at what the current enviornment is __when the function is called__.

And then the rest of the definitions for the evaluator
```fs
    | Num i -> I i
    | Plus  (e1, e2) ->
         let I i1, I i2 = eval e1 env, eval e2 env in I (i1 + i2)
    | Minus  (e1, e2) ->
         let I i1, I i2 = eval e1 env, eval e2 env in I (i1 - i2)    
    | Times (e1, e2) ->
         let I i1, I i2 = eval e1 env, eval e2 env in I (i1 * i2)
    | Neg e ->
         let (I i) = eval e env in I (-i)
    | True  -> B true
    | False -> B false
    | Equal (e1, e2) ->
         let I i1, I i2 = eval e1 env, eval e2 env in B (i1 = i2)         
    | Less (e1, e2) ->
         let I i1, I i2 = eval e1 env, eval e2 env in B (i1 < i2)     
    | ITE (e, e1, e2) ->
         let (B b) = eval e env in if b then eval e1 env else eval e2 env
```