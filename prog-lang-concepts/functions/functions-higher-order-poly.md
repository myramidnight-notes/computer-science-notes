# Higher Order: polymorphic types!

### Defining our base types
We shall start with defining our base types as usual. This is actually a single list of types, like when we had the simultaniously recursive functions, they refer to each other
```fs
type typ =
    | TVar of typevar                     // type variable           
    | Int
    | Bool
    | Fun of typ * typ                    // t -> t'

and typevar =
    (tvarkind * int) ref                  // kind and binding level  

and tvarkind =  
    | NoLink of string                    // uninstantiated type var
    | LinkTo of typ                       // type var instantiated to t
```
* `tvarkind` lets us know if the actual type of this variable has been infered. 

### Type scheme (the type enviornment)
> This is basically what our _type enviornment_ was in previous versions of our language. It has evolved to hold generalized type variables and a type (the placeholder name and actual type, if any )
>
> As we work our way through the expression, we can discover what the actual types of our placeholders. 
>
> * The `Unify` function handles updating these type variables with an actual type. Until then it's a generalized placeholder that could be of any type.

```fs
type typescheme = 
    | TypeScheme of typevar list * typ    // forall t1,...,tn . t 
```
#### Update the type variable
>> Update type variable kind or level. Each of these functions update the `kind` and `level` respectivly.
```fs
let setTvKind (tv : typevar) (kind : tvarkind) : unit =
    let _, lvl = !tv
    tv := kind, lvl
```
```fs
let setTvLevel (tv : typevar) (lvl : int) : unit =
    let kind, _ = !tv
    tv := kind, lvl
```

#### Normalize a type
>> Normalize a type; make type variable point directly to the associated type (if any).
```fs
let rec normType (t : typ) : typ = 
    match t with
    | TVar tv ->
        match !tv with 
        | LinkTo t', _ -> let tn = normType t' 
                          setTvKind tv (LinkTo tn); tn
        | _ -> t
    |  _ -> t
```
>> Make type variable `tv` equal to type `t` by linking it to `t`, but first check that `tv` does not occur in t, and reduce the level of all type variables in `t` to that of tyvar.

## `unify` and helper functions
* [Unification (computer science) on wikipedia](https://en.wikipedia.org/wiki/Unification_(computer_science))

Since we have generalized type variables that keep track of what types should be where in order for the program to work, we need unify to say "these two are the same type".

It checks if unification is possible, because if there is a mismatch, then there will be an error.

The job of `unify` is to check if a type matches the type-variable type (the placeholder). 
* If the type variable is still undecided (meaning it could still be anything), then we can unify them, effectivly deciding the type.
   * From that point on, the type variable will be of the unified type. 
* if there is a decided type and it doesn't match what we're trying to unify it with, then we raise exception.

>```fs
>// The types as strings
>let rec typeToString (t : typ) : string =
>    match t with
>    | TVar  _      -> failwith "we should not have ended up here"    
>    | Int          -> "Int"
>    | Bool         -> "Bool"
>    | Fun (t1, t2) -> "function"
>
>// The unify function
>let rec unify (t1 : typ) (t2 : typ) : unit =
>    let t1' = normType t1
>    let t2' = normType t2
>    match t1', t2' with
>    | Int,  Int  -> ()
>    | Bool, Bool -> ()
>    | Fun (t11, t12), Fun (t21, t22) -> unify t11 t21; unify t12 t22
>                                        //shorthand let binding
>    | TVar tv1, TVar tv2 -> 
>        let _, tv1level = !tv1
>        let _, tv2level = !tv2
>        if tv1 = tv2                then () 
>        else if tv1level < tv2level then linkVarToType tv1 t2'
>                                    else linkVarToType tv2 t1'
>    | TVar tv1, _ -> linkVarToType tv1 t2'
>    | _, TVar tv2 -> linkVarToType tv2 t1'
>    | _, _ -> failwith ("cannot unify " + typeToString t1' +
>                                                  " and " + typeToString t2')
>```
>* Two functions are of the same type if all argument types and output types are the same.

#### linkVarToType
>This function is used by `unify` when it wants to link a variable to a type
>```fs
>let rec linkVarToType (tv : typevar) (t : typ) : unit =
>    let _, lvl = !tv       // only calling it when _ is of form NoLink _ 
>    let tvs = freeTypeVars t
>    occursCheck tv tvs;
>    pruneLevel lvl tvs;
>    setTvKind tv (LinkTo t)
>```
>#### Extra notes on the `F#` syntax and naming choices in the code
>* `f ; g` is shorthand for 
>   ```fs
>   let () = f in g
>   ```
>* A variable named `x'` is simply a normal name, it is NOT a _any-type_ placeholder `'x` (seen when the `fsi` is printing out the type inference of expressions/code that we've run in the teriminal). This choice in naming is sometimes used for indicating altered versions of a existing variable named `x`.

#### occursCheck
>This function is used by the `linkVarToType` to check if `tv` is already on the `tvs` list
>
>```fs
>let occursCheck (tv : typevar) (tvs : typevar list) : unit = 
>    if List.contains tv tvs then failwith "type error: circularity"
>    else ()
>```
> An attempt to unify a variable `tv` with a term containing `tv` would lead to an infinite term as solution for `tv`, since `tv` would occur as a subset of itself (the _circularity_). Hence the _eliminate_ rule (`pruneLevel`) may only be applied if `tv` is not in `tvs`. 
>
>an example would be trying to update `'a` to equal `Int -> 'a`, 
>   ```
>   'a = Int -> 'a
>   'a = Int -> (Int -> 'a)
>   'a = Int -> (Int -> (Int -> 'a))
>   ```
>
>> Doing this check actually slows down the algorithm, so some systems omit it. But omitting this check amounts to solving equations over infinite trees.

#### pruneLevel
>```fs
>let pruneLevel (maxLevel : int) (tvs : typevar list) : unit = 
>    let reducelevel tv = 
>        let _, lvl = !tv
>        setTvLevel tv (min lvl maxLevel)
>    List.iter reducelevel tvs
>```
>

#### freeTypeVars
>```fs
>let rec freeTypeVars (t : typ) : typevar list = 
>    match normType t with
>    | TVar tv      -> [tv]    
>    | Int          -> []
>    | Bool         -> []
>    | Fun (t1, t2) -> union (freeTypeVars t1) (freeTypeVars t2)
>```

#### union
>```fs
>let rec union xs ys = 
>    match xs with 
>    | []    -> ys
>    | x::xs -> if List.contains x ys then union xs ys
>               else x :: union xs ys
>```

## Generate fresh type variables 
>>Generalizing the datatype, this creates a placeholder for any possible type, which will be updated when the type-variable is unified.
```fs
let tyvarno : int ref = ref 0

let newTypeVar (lvl : int) : typevar = 
    let rec mkname i res = 
            if i < 26 then char(97+i) :: res
            else mkname (i/26-1) (char(97+i%26) :: res)
    let intToName i = new System.String(Array.ofList('\'' :: mkname i []))
    tyvarno := !tyvarno + 1;
    ref (NoLink (intToName (!tyvarno)), lvl)
```

## Generalize 
>> Generalize over type variables not free in the context;  i.e., over those whose level is higher than the current level
```fs
let rec generalize (lvl : int) (t : typ) : typescheme =
    let notfreeincontext tv = 
        let _, linkLvl = !tv 
        linkLvl > lvl
    let tvs = List.filter notfreeincontext (freeTypeVars t)
    TypeScheme (tvs, t) 
```
>> Copy a type, replacing bound type variables as dictated by subst and non-bound ones by a copy of the type linked to.
```fs
let rec copyType (subst : (typevar * typ) list) (t : typ) : typ = 
    match t with
    | TVar tv ->
        let rec loop subst =          
            match subst with 
            | (tv', t') :: subst -> if tv = tv' then t' else loop subst
            | [] -> match !tv with
                    | NoLink _ , _ -> t
                    | LinkTo t', _ -> copyType subst t'
        loop subst
    | Fun (t1,t2) -> Fun (copyType subst t1, copyType subst t2)
    | Int         -> Int
    | Bool        -> Bool
```
>> Create a type from a type scheme (`tv`, `t`) by instantiating all the type scheme's parameters tvs with fresh type variables

```fs
let specialize (lvl : int) (TypeScheme (tvs, t)) : typ =
    let bindfresh tv = (tv, TVar (newTypeVar lvl))
    match tvs with
    | [] -> t
    | _  -> let subst = List.map bindfresh tvs
            copyType subst t
```

>> Pretty-print type, using names `'a`, `'b`, ... for type variables
```fs
let rec showType (t : typ) : string =
    match normType t with
    | Int          -> "Int"
    | Bool         -> "Bool"
    | TVar tv      -> 
        match !tv with
        | NoLink name, _ -> name
        | _                -> failwith "we should not have ended up here"
    | Fun (t, t') -> "(" + showType t + " -> " + showType t' + ")"
```

## Type inference function
* The `lvl` keeps track of our nested depth level, this is relevant for the `typescheme`
* The `env` is now a pair of a `typescheme` and `envir`, because we have to be able to know our types.
* We will infer every branching expression, and if there are no issues, then we 
* `unify` will verify if the _infered_ types match the _expected_ type. 
    * if both types are defined and identical, then we don't have to do anything.   
        * if they don't match, then there is inconsistency in the program, raise exception.
    * if we are trying to unify a type variable with a defined type, then the defined type wins
    * if it's provided twith two type variables, then we look at the level to see which variable is used for the unification. The lower the level, the higher importance (meaning that the type declaired closer to the root in the tree will be used).
    * anything else would be something we can't unify, raising exception.

```fs
let rec infer (e : expr) (lvl : int) (env : typescheme envir) : typ =
    match e with

    | Var x  -> specialize lvl (lookup x env)
    
    | Let (x, erhs, ebody) -> 
        let lvl' = lvl + 1
        let tx = infer erhs lvl' env
        let env' = (x, generalize lvl tx) :: env
        infer ebody lvl env' 

    | Call (efun, earg) -> 
        let tf = infer efun lvl env
        let tx = infer earg lvl env
        let tr = TVar (newTypeVar lvl)
        unify tf (Fun (tx, tr)); tr
      
    | LetFun (f, x, erhs, ebody) -> 
        let lvl' = lvl + 1
        let tf = TVar (newTypeVar lvl')
        let tx = TVar (newTypeVar lvl')
        let env' = (x, TypeScheme ([], tx)) 
                      :: (f, TypeScheme ([], tf)) :: env
        let tr = infer erhs lvl' env'
        let () = unify tf (Fun (tx, tr))
        let env'' = (f, generalize lvl tf) :: env 
        infer ebody lvl env'' 

    | Num i -> Int

    | Plus (e1, e2) -> 
        let t1 = infer e1 lvl env 
        let t2 = infer e1 lvl env
        unify Int t1; unify Int t2; Int
    | Minus (e1, e2) -> 
        let t1 = infer e1 lvl env 
        let t2 = infer e2 lvl env 
        unify Int t1; unify Int t2; Int
    | Times (e1, e2) -> 
        let t1 = infer e1 lvl env
        let t2 = infer e2 lvl env
        unify Int t1; unify Int t2; Int
    | Neg e ->
        let t = infer e lvl env
        unify Int t; Int
    | True  -> Bool
    | False -> Bool
    | Equal (e1, e2) -> 
        let t1 = infer e1 lvl env
        let t2 = infer e2 lvl env
        unify Int t1; unify Int t2; Bool
    | Less (e1, e2) -> 
        let t1 = infer e1 lvl env 
        let t2 = infer e2 lvl env 
        unify Int t1; unify Int t2; Bool   
    | ITE (e, e1, e2) ->
        let t1 = infer e1 lvl env 
        let t2 = infer e2 lvl env 
        unify Bool (infer e lvl env); unify t1 t2; t1
```
>In some cases, such as `Plus` or `Minus`, both expressions need to be of the same type in order for the operation to succeed (even if you happen to have somthing in the evaluator that would allow floats and integers to mix, that is done by changing the int into a float, so in the end this is still true, that both expressions are of the same type when the operation is done on them).
>
> So the inference doesn't really have to know if you are going to pass a float or integer, only that both have to be the same type.

#### The top-level of the infer function
Here we want to infer the _top level expression_, by giving `infer` the expression and telling it that current level is `0` and empty enviornment. 
```fs
let inferTop e = 
    tyvarno := 0; showType (infer e 0 [])
```
* `tyvarno` is a counter (that starts at `0`) for tracking the type variable names, to keep them unique, like the  `'a`, `'b` and `'c` placeholders the `fsi` uses when doing it's type inference.
* `showType` will print the inferred types.
### Some other things?
```fs
// let f x = if x < 3 then true else 1011 in fact (f x) 
// let twice f = (let ff x = f (f x) in ff) in twice
let twice =
    LetFun ("twice", "f",
               LetFun ("ff", "x", Call (Var "f", Call (Var "f", Var "x")),
                   Var "ff"),
         Var "twice")


// let (|>) x = (let xto f = f x in xto) in (|>)
let pipe =
    LetFun ("|>", "x",
               LetFun ("xto", "f", Call (Var "f", Var "x"),
                   Var "xto"),
        Var "|>")           
                   
// let add1 x = x + 1
let add1 =
    LetFun ("add1", "x", Plus (Var "x", Num 1),
        Var "add1")


// let const x y = x

let k =
    LetFun ("const", "x",
                LetFun ("constx", "y", Var "x",
                    Var "constx"),
        Var "const")                    
                   

// let const x y = let z = y in x

let k' =
    LetFun ("const", "x",
                LetFun ("constx", "y", Let ("z", Var "y", Var "x"),
                    Var "constx"),
        Var "const")


// let const x y = let z = (y < 5) in x

let k'' =
    LetFun ("const", "x",
                LetFun ("constx", "y", Let ("z", Less (Var "y", Num 5), Var "x"),
                    Var "constx"),
        Var "const")

```