# Higher order functional languages
When we were implementing _first class functions_, they were actually defined as a type of value but didn't have the same rights as other value types, such as you couldn't return functions as a value, or refer to them outside of their own scope (the `Let` binding).

In __higher order functional languages__, the functions have __equal rights__ as other values. They can be stored in variables, passed as arguments, returned as values and so on.

Higher order functions are also called __combinators__

>#### Reusable and simplified code
>The biggest advantage that this gives us is the ability to create reusable code. Chainable functions also become possible because you can pass functions as arguement to any other function.

## Higher order functions
### Map
>__Map__ is an example of higher-order functions. 
>1. It takes both a function and a collection/list as arguments
>1. Then it returns a new collection where `f` has been applied to each element in the provided collection.
>```fs
>// map :: ('a -> 'b) -> 'a list -> 'b list
>let rec map f xs = 
>    match xs with 
>    | []    -> []
>    | x::xs -> f x :: map f xs
>```

### Filter
>__Filter__ is very also an example of a higher-order function 
>1. it's provided a function (that results in a boolean) and a collection as arguments
>1. then it will run the function `p` for every element, and return a collection contain only the elements where `p` answered __true__.
>```fs
>// filter : ('a -> bool) -> 'a list -> 'a list
>let rec filter p xs =
>   match xs with 
>   | []    -> []
>   | x::xs -> if p x then x :: filter p xs else filter p xs
>```

### Pipes 
>We usually refer to `|` as __pipe__ in programming languages, and `|>` or `<|` would indicate in which direction the pipe flows. 
>
>>We have mainly seen it in `match` cases so far in the code examples or __type__ definitions, which is actually piping things down until it finds a match.
>
>Generally in programming, the pipe is used to funnel flow of output and input, so that the output of one function/expression is fed as an argument into the function that is at the end of the pipe (as long as the output of one function matches the desired input of another). 
>
>We can chain functions with these pipes, connecting them together with pipes. 
>which greatly simplifies composing programs that would othervise be full of nested functions to produce the same results.
>```fs
>// (|>) : 'a -> ('a -> 'b) -> 'b   
>let (|>) x f = f x
>
>// f : ('a -> 'a) -> ('a -> 'a)
>twice f x = f (f x)
>
>// x |> f1 |> f2 |> .. |> fn
>```

### Other examples of higher-order functions
>Just some reusable functions
>* `swapArgs` will just reverse the order of arguments
>* `parComp` will pair arguments with functions
>   * so the two functions get paired with the two expressions/arguments in order. `f` gets paired with `x`, `g` gets paried with `y`. 
>```fs
>// swapArgs : ('a -> 'b -> 'c) -> 'b -> 'a -> 'c
>let swapArgs f x y = f y x
>
>// parComp : ('a -> 'b) * ('c -> 'd) -> 'a * 'c -> 'b * 'd
>let parComp (f, g) (x, y) = (f x, g y)
>```
>Why would we have such functions? perhaps we didn't like how things were done in existing code, or want to use a external library/code in a certain way. We can always design reusable higher-order functions that suit our needs.

## Defining our higher order functional language
We have here a slightly modified version of the expression definitions from our __first order functional language__

The only difference is how the __Call__ is defined. It now takes an expression instead of string, because now functions are actually values that can be returned by an expression.

We will be using the __static scope__ for our implementation.

```fs
type expr =
    | Var of string                             // x
    | Let of string * expr * expr               // let x = erhs in ebody    

    | Call of expr * expr                       // efun earg
                //definition of Call has been modified
    | LetFun of string * string * expr * expr   // let f x = erhs in ebody
    | Num of int
    | Plus of expr * expr
    | Minus of expr * expr    
    | Times of expr * expr
    | Neg of expr
    | True
    | False
    | Equal of expr * expr
    | Less of expr * expr
    | ITE of expr * expr * expr    
```
`Call` was previously: 
```
| Call of string * expr
```

>#### Examples
>Examples of some function syntax, both in abstract and concrete.
>```fs
>let e1 = LetFun ("f1", "x", Plus (Var "x", Num 1), 
>                 Call (Var "f1", Num 12))
>// let f1 x = x + 1 in f1 12
>```
>```fs
>let e2 = LetFun("fac", "x", 
>                 ITE (Equal (Var "x", Num 0),
>                    Num 1,
>                    Times (Var "x", 
>                              Call (Var "fac", 
>                                   Minus (Var "x", Num 1)))),
>                 Call (Var "fac", Var "n"))
>// let fac x = if x = 0 then 1 else x * fac (x - 1) in fac n
>```
>```fs
>let e3 = 
>    LetFun ("tw", "g", 
>           LetFun ("gg", "x", Call (Var "g", Call (Var "g", Var "x")), 
>                  Var "gg"),
>           LetFun ("mul3", "y", Times (Num 3, Var "y"), 
>                  Call (Call (Var "tw", Var "mul3"), Num 11)))
>(* 
>   let tw g = (let gg x = g (g x) in gg) in
>   let mul3 y = 3 * y in     
>   tw mul3 11
>*)
>```
>```fs
>let e4 = 
>    LetFun ("tw", "g",
>           LetFun ("gg", "x", Call (Var "g", Call (Var "g", Var "x")), 
>                  Var "gg"),
>           LetFun ("mul3", "y", Times (Num 3, Var "y"), 
>                  Call (Var "tw", Var "mul3")))
>(* 
>   let tw g = (let gg x = g (g x) in gg) in
>   let mul3 y = 3 * y in     
>   tw mul3 
>*)
>```

### The environment
```fs
type 'a envir = (string * 'a) list

let rec lookup (x : string) (env : 'a envir) : 'a =
    match env with
    | []          -> failwith (x + " not found")
    | (y, v)::env -> if x = y then v else lookup x env

```
### Value types
Closures (function values) were anonymous in the first-order version, because it didn't need to know it's name when it would only be used where it was initially bound. 

But since we are now allowing closures to be stored and passed around as variables, we will need to be able to tell it what it's name is at any given time (for cases where the value is given a new name, `let b = a`).

To achieve this, we simply change `F` so that it has one more _string_ to keep track of.

```fs
type value =
    | I of int
    | B of bool
    | F of string * string * expr * value envir   
            //definition has been modified
```
>* Now `F` is keeping track of:
>   * the function's name
>   * the parameter name
>   * function body
>   * and the static enviornment
### Evaluator
The evaluator has changed a bit, since function values don't need any special treatment anymore. 
```fs
let rec eval (e : expr) (env : value envir) : value =
    match e with
    | Var x  ->  lookup x env //different from 1st-order
                  
    | Let (x, erhs, ebody) ->
         let v = eval erhs env
         let env' = (x, v) :: env
         eval ebody env'

    | Call (efun, earg) ->
         let clo = eval efun env 
         match clo with  //different from 1st-order
         | F (f, x, ebody, env0) as clo ->
             let v = eval earg env
             let env' = (x, v) :: (f, clo) :: env0
             eval ebody env'
         | _   -> failwith "expression called not a function"     
    | LetFun (f, x, erhs, ebody) ->
         let env' = (f, F (f, x, erhs, env)) :: env
         eval ebody env'
```
>* Since functions can now be treated like other values, `Var` doesn't need the _match case_ anymore (from the first-order version).
>* `Call` has been changed, because we don't have to specially catch the function value type with `lookup`. We can now just expect that `clo` will be a function (and raise an error if it wasn't a function).
>* We also have to remember that `LetFun` has to include the name when defining the function value.

#### And then the rest of the evaluator, same as previously
```fs
    | Num i -> I i
    | Plus  (e1, e2) ->
         match eval e1 env, eval e2 env with
         | I i1, I i2 -> I (i1 + i2)
         | _ -> failwith "argument of + not integers"
    | Minus  (e1, e2) ->
         match eval e1 env, eval e2 env with
         | I i1, I i2 -> I (i1 - i2)
         | _ -> failwith "arguments of - not integers"  
    | Times (e1, e2) ->
         match eval e1 env, eval e2 env with
         | I i1, I i2 -> I (i1 * i2)
         | _ -> failwith "arguments of * not integers" 
    | Neg e ->
         match eval e env with 
         | I i -> I (- i)
         | _ -> failwith "argument of negation not an integer"
    | True  -> B true
    | False -> B false
    | Equal (e1, e2) ->
         match eval e1 env, eval e2 env with
         | I i1, I i2 -> B (i1 = i2)
         | _ -> failwith "arguments of = not integers"      
    | Less (e1, e2) ->
         match eval e1 env, eval e2 env with
         | I i1, I i2 -> B (i1 < i2)
         | _ -> failwith "arguments of < not integers"
    | ITE (e, e1, e2) ->
         match eval e env with
         | B b -> if b then eval e1 env else eval e2 env
         | _ -> failwith "guard of if-then-else not a boolean"
```