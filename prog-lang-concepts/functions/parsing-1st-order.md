# Parsing a first-order functional language
> __First-order__ means that the functions cannot be used as values. 
## Define our types
#### Our expressions
>```fs
>type expr =
>    | Var of string                                  // x
>    | Let of string * expr * expr                    // let x = erhs in ebody
>    | Call of string * expr                          // f e   
>    | LetFun of string * string * expr * expr        // let f x = erhs in ebody
>    | Num of int
>    | Plus of expr * expr
>    | Minus of expr * expr
>    | Times of expr * expr
>    | Neg of expr
>    | True
>    | False
>    | Equal of expr * expr
>    | Less of expr * expr
>    | ITE of expr * expr * expr                      // if e then e1 else e2
>```
>We now have __functions__, __booleans__ and __if statements__ (`ITE`) to spice up our language.
#### Our tokens (for parsing)
>```fs
>type token =
>    | NAME of string                      // variable names
>    | LET | EQUAL | IN
>    | IF | THEN | ELSE | LESS
>    | INT of int | TRUE | FALSE           // unsigned integers and bools
>    | PLUS | MINUS | TIMES
>    | LPAR | RPAR                         // parentheses
>    | ERROR of char                       // illegal symbols
>```
#### Our helper functions
```fs

let isDigit c = '0' <= c && c <= '9'

let digit2Int c = int c - int '0'

let isLowercaseLetter c = 'a' <= c && c <= 'z'

let isUppercaseLetter c = 'A' <= c && c <= 'Z'

let isLetter c = isLowercaseLetter c || isUppercaseLetter c

let word2Token (s : string) : token =
    match s with
    | "let"   -> LET
    | "in"    -> IN
    | "if"    -> IF
    | "then"  -> THEN
    | "else"  -> ELSE
    | "true"  -> TRUE
    | "false" -> FALSE
    | _       -> NAME s
```

## Lexer 
```fs

let rec tokenize (cs : char list) : token list =
    match cs with
    | [] -> []
    | '+'::cs  -> PLUS :: tokenize cs
    | '-'::cs  -> MINUS :: tokenize cs
    | '*'::cs  -> TIMES :: tokenize cs
    | '='::cs  -> EQUAL :: tokenize cs
    | '<'::cs  -> LESS :: tokenize cs
    | ' '::cs  -> tokenize cs
    | '\t'::cs -> tokenize cs
    | '\n'::cs -> tokenize cs
    | '('::cs  -> LPAR :: tokenize cs
    | ')'::cs  -> RPAR :: tokenize cs
    | c::cs when isDigit c ->
        tokenizeInt cs (digit2Int c)
    | c::cs when isLowercaseLetter c ->
        tokenizeWord cs (string c)
    | c::cs -> ERROR c :: tokenize cs

and tokenizeInt cs (acc : int) =
    match cs with
    | c::cs when isDigit c ->
        tokenizeInt cs (acc * 10 + digit2Int c)
    | _ -> INT acc :: tokenize cs

and tokenizeWord cs (acc : string) =
    match cs with
    | c::cs when isLetter c || isDigit c ->
         tokenizeWord cs (acc + string c)
    | _ -> word2Token acc :: tokenize cs
```

```fs
let string2Chars (s : string) : char list =
    let rec helper cs i =
        if i = 0 then cs else let i = i - 1 in helper (s.[i] :: cs) i
    helper [] (String.length s)

let lex s = tokenize (string2Chars s)
```

## Parsing the expression
The challenge for this parser is to correctly handle factors. They can either be expressions or functional applications.

* The subtlety comes from the fact that we want to be able to write _function application_ without any special symbols.
* We also want to be able to give the argument without parentheses around it in reasonable cases (when the argument isn't an expression)
    * `f 2 + 2` must be parsed as `(f 2) + 3`, not `f (2 + 3)`
    * `f if x < y then 3 else 4` should parse as `f (if (x < y) then 3 else 4 )`
    * `f - 2`, should be `f (-2)` instead of becoming subtraction `f - 2`
    
### Grammar for our parser
>```
>e --->  c = c | c < c | c               // expressions
>
>c --->  s ss                            // comparands
>
>ss ---> + s ss | - s ss | (empty)       // lists of summands
>
>s --->  f ff                            // summands
>
>ff ---> * f ff | (empty)                // lists of factors
>
>f ---> x a | h                          // factors
>
>h ---> let x x = e in e | let x = e in e
>     | - f | if e then e else e | a     // heads
>
>a  ---> x | i | b | (e)                 // arguments
>
>x                                       // names
>
>i                                       // numerals
>
>b                                       // boolean values
>```

### The parser code
The parser has grown quite a bit from previous examples, now containing total of 8 functions that are simultaniously recursive.

* #### parseExpr
    > According to the grammar rules, any expression is composed of __comparands__  so that the first comparand in the expression is followed by `EQUAL` or `LESS`
    >
    > `e ---> c = c | c < c | c`
    >```fs
    >let rec parseExpr (ts : token list) : expr * token list =
    >    let (e1, ts) = parseComparand ts
    >    match ts with
    >    | EQUAL :: ts ->
    >        let (e2, ts) = parseComparand ts
    >        Equal (e1, e2), ts
    >    | LESS :: ts ->
    >        let (e2, ts) = parseComparand ts
    >        Less (e1, e2), ts
    >    | _ -> e1, ts
    >```
    >* We first use `parseComparand` to parse the first expression `e1`. 
    >* We run the remaining tokens through the match, to see if the expression was followed by either `EQUAL` or `LESS` operators. 
    >   * If we find a match, then we parse the 2nd expression with `parseComparand` and return the parsed results `Equal(e1, e2)` or `Less(e1, e2)` along with the remaining tokens.
    >* If we found __no match__, then we had a stand-alone expression and simply return the first expression along with the remaining tokens. 
* #### parseComparand
    >Any _comparand_ is a __summand__ followed by a list of summands 
    >
    >`c -> s ss` 
    >```fs
    >and parseComparand (ts : token list) : expr * token list =
    >    let (e, ts) = parseSummand ts
    >    parseSummandList e ts
    >```
    > * `parseComparand` will parse the _summand_ and remaining tokens and passing them to the `parseSummandList`
* #### parseSummandList
    >A _list of summands_ is either a `PLUS`/`MINUS` and a summand followed by a list of summands or the list is empty. 
    >
    > `ss ---> + s ss | - s ss | (empty)`
    >```fs
    >and parseSummandList (e1 : expr) (ts : token list) : expr * token list =
    >    match ts with
    >    | PLUS :: ts ->
    >        let (e2, ts) = parseSummand ts
    >        parseSummandList (Plus (e1, e2)) ts
    >    | MINUS :: ts ->
    >        let (e2, ts) = parseSummand ts
    >        parseSummandList (Minus (e1, e2)) ts
    >    | _ -> e1, ts
    >```
    >* Here we try to match `PLUS` and `MINUS` tokens
    >    * if we find a match, then we extract the _summand_ and pass the parsed expression to `parseSummandList`
    >* else we will return the expression and remaining tokens
* #### parseSummand
    > a _summand_ is a __factor__ followed by a list of factors.
    >
    >` s ---> f ff`
    >```fs
    >and parseSummand (ts : token list) : expr * token list =
    >    let (e1, ts) = parseFactor ts
    >    parseFactorList e1 ts
    >```
    >* we extract the factor to `e1` and pass the parsed expression with the remaining tokens to `parseFactorList`
* #### parseFactorList
    > The list of factors is either `TIMES` along with a factor followed by a list of factors, or the list is empty.
    > `ff ---> * f ff | (empty)`
    >```fs
    >and parseFactorList (e1 : expr) (ts : token list) : expr * token list = 
    >    match ts with
    >    | TIMES :: ts ->
    >        let (e2, ts) = parseFactor ts
    >        parseFactorList (Times (e1, e2)) ts
    >    | _ -> e1, ts
    >```
    >* We try to match `MINUS` token here
    >    * if we find a match, then we extract the __factor__ and pass the expression along with the remaining tokens back into `parseFactorList` (staying in this function)
    >* else we just return the expression we've parsed and the remaining tokens
* #### parseFactor
    > A _factor_ is either a __name__ followed by an __argument__ or it's a  __head__.
    >```
    >   f ---> x a | h
    >```
    >There is actually an overlap between the two possible paths to take: if we have a head, it could be an argument, which in turn could simply be a name. 
    >```fs
    >and parseFactor (ts : token list) : expr * token list =  
    >    match ts with
    >    | NAME f :: ts ->
    >        match ts with
    >        // Here's a bit of lookahead
    >        | NAME _ :: _ | INT _ :: _ | TRUE :: _ | FALSE :: _ | LPAR _ :: _ ->
    >            let (earg, ts) = parseArg ts
    >            Call (f, earg), ts
    >        | _ -> Var f, ts     
    >    | _ -> parseHead ts
    >```
    >To avoid the possible backtracking due to this overlap (because we can find both names and arguments if we go down the _head_ route as well), we will have to implement a lookahead when we run into a __NAME__, to determine if it's a __variable__ or __function call__.
    >* If we match a __NAME__
    >    * If it is followed by a __NAME__, __INT__, __TRUE__, __FALSE__ or __LPAR__
    >      >then we are parsing a __function call__. 
    >    * else we're simply parsing a __variable__
    >* Anything else would be passed to `parseHead`
    >   >It handles _let-bindings_ and _if-statements_ among other things
* #### parseHead
    > A head can be many things, a __let-binding__, `MINUS` expression, __if statement__ or __argument__. 
    >```
    >   h --->  let x x = e in e 
    >         | let x = e in e
    >         | - e 
    >         | if e then e else e 
    >         | a    
    >```
    >```fs
    >and parseHead (ts : token list) : expr * token list = 
    >    match ts with                         
    >    | LET :: NAME f :: NAME x :: EQUAL :: ts ->
    >        let (erhs, ts) = parseExpr ts
    >        match ts with
    >        | IN :: ts ->
    >            let (ebody, ts) = parseExpr ts
    >            (LetFun (f, x, erhs, ebody), ts)
    >        | _ -> failwith "let without in"
    >    | LET :: NAME x :: EQUAL :: ts ->
    >        let (erhs, ts) = parseExpr ts
    >        match ts with
    >        | IN :: ts ->
    >            let (ebody, ts) = parseExpr ts
    >            Let (x, erhs, ebody), ts
    >        | _ -> failwith "let without in"
    >    | LET :: NAME x :: _ -> failwith "let without equals sign"
    >    | LET :: _ -> failwith "lhs of def in let not ok"
    >    | MINUS :: ts ->
    >        let (e, ts) = parseFactor ts
    >        Neg e, ts
    >    | IF :: ts ->
    >        let (e, ts) = parseExpr ts
    >        match ts with
    >        | THEN :: ts ->
    >            let (e1, ts) = parseExpr ts
    >            match ts with
    >            | ELSE :: ts ->
    >                let (e2, ts) = parseExpr ts
    >                ITE (e, e1, e2), ts
    >            | _ -> failwith "if-then-else without else"
    >        | _ -> failwith "if-then-else without then"
    >    | _ -> parseArg ts
    >```
    >* We always expect a __LET__ to be followed by a __name__
    >    * if it's then followed by a __EQUAL__, then it's a variable
    >    * else if it's followed by another __NAME__ and then __EQUAL__, then it's a function
    >    * anything else would be a badly defined let-binding, raising an exception
    >* We match __MINUS__ followed by an expression
    >* We also try to match __IF__ for a _if statement_.
    >    * An __if statement__ is required to have a __THEN__ and a __ELSE__ to complete the syntax. Our implementation does not allow us to only have __IF__.
    >    * when we've defined a _if statement_, then we return the parsed expression and remaining tokens.
    >* If it was anything else, then we pass the remaining tokens to `parseArg`
* #### parseArg
    > An argument can be a __name__, __integer__, __boolean__ or an __expression__ in parentheses.
    >
    >`a ---> x | i | b | (e) `
    >```fs
    >and parseArg (ts : token list) : expr * token list =
    >    match ts with 
    >    | NAME x :: ts -> Var x, ts
    >    | INT i :: ts -> Num i, ts
    >    | TRUE  :: ts -> True, ts
    >    | FALSE :: ts -> False, ts
    >    | LPAR :: ts ->
    >        let (e, ts) = parseExpr ts
    >        match ts with
    >        | RPAR :: ts -> e, ts
    >        | _ -> failwith "left paren without right paren"
    >    | _  -> failwith "not a factor"
    >```
### Finally the top-level parser
```fs
let parse (ts : token list) : expr =
    let (e, ts)  = parseExpr ts
    if ts = [] then e else failwithf "unconsumed tokens"

```
#### Or the combined lexer and parser
```fs
let lexParse (s : string) : expr = parse (lex s)
```