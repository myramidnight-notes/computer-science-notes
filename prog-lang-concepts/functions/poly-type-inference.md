# Polymorphic Type Inference

## Understanding polymorphic type inference with `fsi`
>These are basically extra notes about how to read what `fsi` is telling you about the type value of expressions, and at the same time understanding how the types are polymorphic (how they transform).
* `'a` are placeholders for any possible type, but as long as it's the same letter, then it's the same type.
* The arrows are displaying how the function is curried (broken down for each argument) until we reach a result/output.
    * Three arrows would mean that we got 2 inputs(arguments).
* Its not always obvious how the inference came to the given conclusion.

#### Does declairing the types change things?
>Here we have `mkpair` function, that returns the arguments as a pair/tuple
>```fs
>let mkpair x y = x, y;;
>//val mkpair : x:'a -> y:'b -> 'a * 'b
>```
>```fs
>let mkpair x (y: int) = x, y;;
>//val mkpair : x:'a -> y:int -> 'a * int
>```
>```fs
>let mkpair (x: char) (y: int) = x, y;;
>//val mkpair : x:char -> y:int -> char * int
>```
>It doesn't really change how the inference is done, it just uses the defined type instead of a placeholder if you declair them.

#### Reading the nested functions
>```fs
>let foo x y = x y y ;;
>//val foo : x:('a -> 'a -> 'b) -> y:'a -> 'b
>```
> This example shows us that `x` must be a function that takes 2 arguments, because it takes `y` twice when used within `foo`. 

>```fs
>let foo x y z = x z (y z);;
>//val foo : x:('a -> 'b -> 'c) -> y:('a -> 'b) -> z:'a -> 'c
>```
>Same as the  example above, `x` and `y` must be functions in this case.
>
> `x` is taking two arguments when used within `foo`, but the parentheses says the second argument will be the output of the function `y` with the argument `z`. 
>* Now we can essentially try to understand what the value typing is telling us, seperated by the arrows.
>   * `x:('a ->- 'b > 'c)` says that `x` is a function that takes two arguments of types `'a` and `'b`, returning a `'c` type. 
>   * `y:('a -> 'b)` says that `y` is a function, that takes in type `'a` as argument and returns `'b`.
>   *  `z` is simply of type `a'`
>   * Finally because the output is `'c`. _Why?_ because `foo` just returns the function `x`, which outputs `'c`.

### Example
If we wish to create a function that has the following typing, we can read from this quite a few things already.
```
foo : ('a -> 'a) -> 'a -> 'a list -> 'a
```
This value example is not actually telling us which argument each type is representing, but we can assume certain things. That the arguments appear in the given order and they are seperated by the `->` arrows 
* `('a -> 'a)` is the 1st argument
* `'a` is the 2nd argument
* `'a list` is the 2rd argument
* the final `'a` is the result/output

Usually we would also be shown the argument names as well
```fs
let foo f x xs = <some function body example>

//foo: f: ('a -> 'a) -> x: 'a -> xs: 'a list -> 'a
```
Each argument name is a placeholder/variable for whatever input the function is given, and it could be anything. 

With a polimorphic type inference, we get generalized type variables as placeholders to represent the types when they aren't specified.

In order for it to assume anything about these arguments, they need to be treated as the type they represent.

* Now we have a general idea of what the function `foo` wants.
* Each argument is simply a variables within the function, and it has no clue what each of them will be until they are used in specific ways.
    * 1st argument needs to be used as a function by passing it arguments of it's own.
    * Since it will take the same type as the argument `x` means it will take `'a` as a argument at some point. Meaning we pass the 2nd argument to the 1st argument function (if my words make sense).
        * If it does not take the 2nd argument at any point, the infer will just assume that it could possibly take a different type, therefor using a different type varible name for it.
    * Since we know `f` will take an `'a` and output it as well, we can use that to make sure that the provided list in 3rd argument has items that the function can process, by passing it the individual items