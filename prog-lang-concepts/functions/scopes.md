>[...back](./README.md)
# Scope view
>A scope of a variable is the location where it is accessable.

### The variable definition by scope
A variable's definition is looked up in the current block or the __nearest enclosing block__. The definition of what the _nearest enclosing block_ depends on the type of scope used by the language.
>Let's have an example of possible nested scopes that contain both a function and local variables.
>```
>let y = 11 in
>    let f x = x + y in     // function defined
>        let y = 22 in
>            f 3            // function call
>```
>
>The question is: What `y` will be used when the function `f` is called?
>* The answer:
>    * `y` is __11__ in a __static scope__ 
>       > because that's the value of `y` when the function that uses it was defined.
>    * `y` is __22__ in a __dynamic scope__
>       > because that's the latest definition of `y` where the function was called. 
### Static scope
* Also called __lexical scope__
* Static scope refers to the __syntactic nesting of blocks__
* It is more __straight forward__ and __correct__ to implement a static scope rule from the point of _software engineering_.
* nearly all modern languages use a __static scope__.

### Dynamic scope
* It takes into account how function calls are nested in blocks and in each other during evaluation.
* it invented (in `Lisp`) before the _static scope_.
* It is used in `Perl`, which has both _static_ and _dynamic_ scopes.

>#### Possible issues with the dynamic scope and datatypes
>When we take into account shadowing for local variables, identically named variables could technically contain different datatypes (perhaps `integer` and `float`), and this would cause type issues if the if the closest definition of `y` happens to be different type from when the function was first defined.

## Conclusion
Even if a dynamic scope was invented first, and might be simpler to implement, it makes understanding and predicting the behaviour of your program more confusing. It becomes difficult to know or keep track of where a function is actually getting it's variable values, because they would depend on the latest declaration/shadowing of that variable name.

That is why __static scope__ (also called _lexical scope_) is what all modern programming languages use, any variables referenced that aren't provided through input simply reference the local scope as it was upon the function's declaration (so it stays consistant, regardless of when or where the function is called)