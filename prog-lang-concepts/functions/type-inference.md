>[...back](./README.md)
# Typing
A __type system__ defines how a programming language classifies values and expressions into __data types__, how it can manipulate those types and how they interact.

Each type will always have the defined architecture or shape. Such as a _variable_ always being a name and a value/expression, or a _integer_ is always a whole number. 

And since we programmers are creators, we can also define our own types that would work best for the program we are designing. 

It of course depends on how the programming language is implemented, what rules are in play, the details of how the _data types_ are structured, but generally we are always working with the same concepts when we talk about certain types.

### Untyped languages
>In contrast, an __untyped language__, such as most assembly languages, allows any operation to be performed on any data, generally sequences of bits of various lengths

### Type inference
>__Type inference__ is the ability to automatically deduce the type of an expression at compile time.

## __Type safety__ 
It is the extent in which a programming language will discourage or prevent __type errors__, such as trying to treat a integer as a float.

### Well and ill typed (Type safety)
* __Well typed__:
     >"__Well typed__ programs cannot _go wrong_", it essentially describes the __type safety__ of the language or program. 
* __Ill typed__:
     >A ill/badly typed program might result in a run-time error, which makes it a __dangerous__, because it might fail.

## Typing definitions
### Explicit and Implicit Typing 
* __Explicit__: 
     >When you state clearly all types, leaving no room for doubt in the program about what type something might be.
* __Implicit__: 
     > The types are implied, but not clearly stated.
### Static and Dynamic typing
* __Static__: 
     > Avoiding these mis-matching types that could cause run-time errors via the evaluator is called __static typing__. This means we refuce to run dangerous programs (that would cause type-errors at run-time). 
* __Dynamic__ (also called __latent typing__): 
     >Dynamic type checking is the process of varifying the type safety of a program at runtime. 
     >
     >Most type-safe languages include some form of dynamic type checking, even if they also have a static type checker.This makes them a __Dynamic Programming Language__
     >
     > Programming languages that include dynamic type checing but not static type checking are often called __dynamically-typed programming languages__
### Strong and Weak typing
* __Strong__:
     >Programming languages in which variables have specific data types are strong typed. This implies that in strong typed languages, variables are necessarily bound to a particular data type. 
     >
     >So the var `foo` that initially contains a string, cannot later contain a integer (or any other type than string). A defined variable cannot be redefined as a different type. 
     >```fs 
     >let foo = "x";;
     >
     >let foo = foo + 2;;
     >//error FS0001: The type 'int' does not match the type 'string'
     >```
     >> Python, Java and F# are examples of strongly typed languages 
* __Weak__ (also called __loosely typed__):
     >Opposed to strong typed languages, weak typed languages are those in which variables are not of a specific data type. This however does not imply that the variable doesn't have a type, instead it means that the variable is not bound to a specific data type.
     >
     >This would allow us to redefine a variable to be of a different type.
     >> PHP, C and JavaScript are examples of weak typed languages

### Polymorphic and Monomorphic types
* __Polymorphic__
     > __Parametric polymorphism__ is a way to make a programming language more expressive, while maintaining full static type-safety. We are essentially able to create generic functions that can handle values identically without relying on a specific type.
     >
     > Good examples of usage would be creating a _higher order function_ that could process lists of any type, applying a function on every item in the list, but the function itself doesn't care what types are being stored in the list. So long we gave it a function argument that could work with the provided type of list, everything would work out without the function having to know the provided types.
* __Monomorphic__
     > Monomorphic is of course the oposite of _polymorphism_. Functions would work with a single type, and will return the same type, it won't morph into another type at any point. 
     >
     >This essentially means we don't mix types, if we would allow floats and integers to work together, the output would not be identical to the input, therefor not monomorphic.