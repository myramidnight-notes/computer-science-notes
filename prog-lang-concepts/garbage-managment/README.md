[...back](../README.md)

* [Fundimentals of garbadge collecting (.NET)](https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/fundamentals)
* [Garbage collection (wikipedia)](https://en.wikipedia.org/wiki/Garbage_collection_(computer_science))
* [Dangling pointers (wikipedia)](https://en.wikipedia.org/wiki/Dangling_pointer)

# Garbage / Memory managment

### Why do we need to manage memory?
Memory is important, if we run out of available memory, then our system can't function properly and we experience lag or crashes. 

A __memory leak__ is when a application keeps allocating memory for usage without ever freeing it even though it is no longer using those allocation, until we run out of available memory.

__Dangling pointers__ can also become an issue, where they no longer point at valid objects of the appropriate type. This arises during __object destruction__, when the object that the pointer points to has been deallocated/removed, and the pointer has not been changed to point at something valid.

## Tombstones
> __Tombstones__ is a mechanism to detect _dangling pointers_, which can essentially be described as a marker for "the data is no longer there" when a memory at the location of the pointer has been deallocated.
>
>A structure that acts as a intermediary between a pointer and the heap-dynamic data in memory. The pointers (sometimes called _handles_) points only at tombstones and never at the actual memory. This prevents the use of invalid pointers, which would otherwise access the memory that might now be deallocated, and interacting with that data might lead to corruption of data that might belong to some other program at this point.
>* The downside of tombstones would be the extra space it takes, having the extra table to connect the pointers to the memory.

## Garbage Collection
A __garbage collector__ serves as a __atuomatic memory manager__, which manages the allocation and release of memory for the application. 

__Garbage__ refers to objects/data in memory that has been allocated but will not be used in any future computation by the system or program running on it, essentially wasting space.

> It might be worth mentioning that __memory__ refers to the RAM that is essentially the active brain of the computer, not the permenent storage (hard drives...).
>
> Every computer system has a finite amount of memory, so is necessary to deallocate memory, returning it to for reuse.

Garbadge collectors are implemented in all the modern programming lanaguages, though some allow you to manually allocate memory, which you yourself would be responsible for freeing.


### Benefits of garbage collectors
* The programmer/developer can focus on the functionality of their program, without the hassle of manually releasing all memory we are n o longer using.
* Reclaims items that are no longer being used/referenced, clearing their memory for future use.

### [Garbage Collector Algorithms](./garbage-collection-algs.md)
* The __Mark and Sweep__
* __Reference Counting__