[...back](./README.md)

# Garbage collection algorithms

## The _Mark and Sweep_ garbage collector
>The __mark and sweep__ algorithm is called a __tracing garbage collector__ because it traces the entire collection of objects that are directly or indirectly accessable by the program.. 
>
>It has two phases to it's operation:
>1. The Mark phase´
>    *  On object creation it's mark bit is initially set to zero(false). 
>    * During the _mark phase_, we set all reachable objects as true, starting from the root.
>1. The sweep phase
>    * The _sweep phase_ clears the heap memory for all the unreachable objects (all the ones with the marked bit set to zero).
>* [7min lecture about the mark-sweep](https://www.youtube.com/watch?v=lXj6j9hVGLQ&t=117s)
## Reference counting
>__Reference counting garbage collection__ is where each object has a count of a number of references to it. Garbage is identified by having the _reference count_ of zero.  
>
>An object's reference count is incremented when a reference to it is created, and decremented when a reference is destroyed.