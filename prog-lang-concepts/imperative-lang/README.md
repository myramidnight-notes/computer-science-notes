# Imperative languages
>>In an imperative languages, one distingquishes between expressions and statements (commands).
>>
>>Expressions are (primarily) to compute a value.
>>
>>Commands are there to produce some side-effects, such as update mutable store, print, and so on...

* Expressions are there (primarily) to compute values
* Commands (statements) are there to produce some side-effects, such as update mutable store/memory, print, and so on...

## Loops
* Syntactic validity of loops
    * We can have some checks that would automatically make a loop definition invalid, such as if the variable used for the stop condition is also used within the loop body, because then it could be changed somewhere along the way.

## Pointers
This example list of variables and pointers would be stored in order somewhere as an enviornment, so we can essentially predict where they are sitting in that enviornment/store.
* `x` is a variable pointer, which points to a value somewhere in memory
* `&x` shows you the address that the pointer holds, where it's pointing, instead of showing the value stored at the address.
* `*x` derefrences the pointer, letting you interact directly with it's value stored at the address location. 

If you recall how the variables in our enviornments were stored on a stack during evaluation, so the latest variables are near the top. So it might be good to take this into consideration when considering the pointer addresses. 
>If variable `a` was first given the address 0, it will be 3 by the time we have added variables `b`, `c` and `d`. 

### Examples of pointers
These examples are counting in logical order, rather than thinking of their actual address on the stack.
1.  Pointer to the integer, the star `*` dereferences the `p` as an integer. `p` at __0__
    ```fs
    int *p;
    ```
1. Integer, `i` at __1__ 
    ```fs
    int i;
    ```
1. array of 10 ints. `ia` at __2...11__.  
    ```fs
    int ia[10];
    ```
    * `ia` points to the first item in the array by default, so if we assign a value to the dereferenced pointer `*ia`, then it equals to saying that we want to assign value to `ia[0]`.
        ```fs
        *ia = 14;q
        *(ia+1) = 3;
        *(ia+2) = 16;
        ```
    * printing from array `ia`
        ```fs
        print ia[0]; //prints 14
        print ia[1]; //prints 3
        print ia[2]; //prints 16
        ```
1. a pointer to a integer, it hasn't been assigned yet. It sits in slot __12__
    ```fs
    int *ia2;
    ```
1. Pointer to array of 10 ints, slots __13...23__
    ```fs
    int (*iap)[10];
    ```
1. We set `p` to point to `i`, instead of pointing to whatever `i` is pointing to.
    ```fs
    p = &i;
    ```
1. `ia` is a pointer, so assigning `ia2` to be `ia` is just making them both point to the same location.
    ```fs
    ia2 = ia;
    ```
1. We previously assigned `p` to point to the same place as `i`, so now we've assigned their shared value to be __227__.
    ```fs
    *p = 227;
    ```