# Enviornment and Store

## The Store
The store is a place where we keep the actual values of our variables, and the variables acctually just carry pointers, which are addresses to the actual location of the value that the variable is associated with.

> The store is __single-threaded__ in all imperative languages, meaning at most one copy of the store needs to exist at one time. That is because we never need to look back.

### Operations on the store
* `emptyStore`
    > returns a new empty store
* `setSto store addr value`
    > returns a new store, where the address has been given value.
* `getSto store addr`
    > returns the value at the address within the store (or throws an exception)
* `initSto loc n store`
    > returns a new store, where the addresses `loc`, .... `loc`<sub>n-1</sub> has a default value, for the allocation of arrays.

## Accessing the store
We have access to both the addresses and the values stored, 

Then we can use the `lvalue` ("left hand side value") which is a index to the value in store. The `rvalue` ("right hand side value") will hold the value stored at that location in the store.

* ### Call by value
    > A copy of the argument's _rvalue_ is made at a new location, and that new location is passed into the function/procedure.
    >
    >This means that any changes done within the function on the argument passed in does not actually effect the original _rvalue_ (because we're doing operations on a copy)
* ### Call by reference
    > The argument's pointer location (_lvalue_) is passed into the function, so the varible passed into the argument and the variable used within the function are one and the same (just under different names, pointing to the same location).
    >
    >This is actually useful if we want a function to return multiple results, because all changes done to the arguments within the function will also effect them outside of the function (so we don't have to update the values with the output of the function)
* ### Call by value return
    > This one is like a hybrid of _call by value_ and _call by reference_.
    >
    > It creates a copy of the argument's _rvalue_ in a new location, but when it passes the location of that copy into the function. 
    >
    >When it comes time to return the output, the current value at that copy location is copied to the value of the argument's location (_lvalue_).