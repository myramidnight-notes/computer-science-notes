# Implementing our Imperative language
>>* "Imperative languages are upside down"
 
We want to implement a very simple imperative language

* It will only have global variables, no functions, no pointers and no interesting types.
* It's mainly a memory model, having a store instead of environment.
    >```
    >while z < 17 do { x = 3; y = x * 4 } ; print (y - 1) ; x = 1001
    >```

* This allows variables to have values that change over time.

## Our expressions and Statements: 
In imperative languages, we almost always have 2 or more syntactic categories, not only _expressions_. Expressions are no longer our main or only syntax.

* Expressions: __expr__
    > They are something we can use when we want to return values
    ```fs
    type expr = 
        | Num of int                             // n 
        | Var of string                          // x
        | Op of string * expr * expr             // e1 op e2
    ```
* Statements (commands): __stmt__
    > These are not definitions, because they themselves are not assigned a definition, but instead are commands to be performed.
    ```fs
    type stmt = 
        | Assign of string * expr                // x = e
        | Block of stmt list                     // {s1; ...; sn}
        | If of expr * stmt * stmt               // if e then s1 else s2       
        | For of string * expr * expr * stmt     // for x := e1 to e2 do s
        | While of expr * stmt                   // while e do s
        | Print of expr                          // print e
    ```
    >* We can `Assign` values to a variable, even reassign.
    >   * What these possible reassignments of values allows us to create a sequence of commands that use the same variables, but they evolve over time, having different values at different points of the program run.
    >* `Block` is a section of code, wrapped in curly braces `{` `}`
    >* `If`statements as usual
    >* `For` and `While` loops are something new
    >* `Print` will output whatever we want to print.

## The naive store (memory)
The naivity comes from how our implementation actually doesn't take into account the physical address of the values in memory, but instead just sees it as a stack/array of values. 

>>A naive store is a map from names (strings) to values (integers), in a mutable data structure (dictionaries).
>>
>>This is something we can afford so long we don't have local names.
>>
>>__We have no booleans__, instead we represent Booleans by integers.
>>Any _non-zero integer counts as True_, zero as False.

Our naive store is a place to store the values of the variables, which 

```fs
type naivestore = Map<string,int>

let emptystore : Map<string,int> = Map.empty

let getSto (sto : naivestore) x : int = sto.Item x

let setSto (sto : naivestore) (k, v) : naivestore = sto.Add (k, v)
```
* `emptyStore` to clear it
* `getStore` will get (`x`) from the store (`sto`) 
* `setStore` will change the store

## Evaluation of expressions
```fs
let rec eval e (sto : naivestore) : int =
    match e with
    | Num i -> i
    | Var x -> getSto sto x
    | Op (op, e1, e2) ->
        let i1 = eval e1 sto
        let i2 = eval e2 sto
        match op with
        | "*"  -> i1 * i2
        | "+"  -> i1 + i2
        | "-"  -> i1 - i2
        | "==" -> if i1 = i2 then 1 else 0
        | "<"  -> if i1 < i2 then 1 else 0
        | _    -> failwith "unknown primitive"
```
## Execution of statements
Statements don't return anything of interest, but they update the store with `Assign`, and that results in a new updated store.
```fs
let rec exec stmt (sto : naivestore) : naivestore =
    match stmt with
    | Assign (x, e) -> 
        setSto sto (x, eval e sto)
    | If (e1, stmt1, stmt2) -> 
        if eval e1 sto <> 0 then exec stmt1 sto
                            else exec stmt2 sto
    | Block stmts -> 
        let rec loop stmts sto = 
            match stmts with 
            | []          -> sto
            | stmt::stmts -> loop stmts (exec stmt sto)
        loop stmts sto
        
    | For (x, estart, estop, stmt) ->   
        let start = eval estart sto
        let rec loop i sto =
            let stop = eval estop sto
            if i > stop then sto 
                        else let sto'  = setSto sto (x, i)
                             let sto'' = exec stmt sto'
                             loop (getSto sto'' x + 1) sto''
        // here we actually start the loop we defined
        loop start sto

    | While (e, stmt) -> 
        let rec loop sto =
            if eval e sto = 0 then sto
                              else loop (exec stmt sto)
        loop sto
    | Print e -> 
        printf "%d\n" (eval e sto); sto
```
* A `Block` of code is just a sequence of statements, seperated by `;`, so it will _loop_ through them. 
* The `loop` functions are helper recursive functions within each branch of the program, the store keeps changing with each iteration. 
* `For` loop takes in a __control variable__ and it's __start value__ variable, stop value for that varaiable, and then the codeblock of statements (`stmt`) that would be run while the condition is true.
    * We evaluate `stop` within the body of the loop, allowing it to be checked at each iteration
    * > In some languages we get to define how the the _control variable_ will change at each iteration, but in our simple version it's hard-coded to increase by 1 at each iteration. Finally the loop ends when the __control value__ value is no longer smaller than the __stop value__.
    * What the this version does, it allows us to ignore any possible changes done to the iterator `i` within the body of the loop, to preventing odd behaviour in the control factor. 
    * There is a alternative example below of the `For` loop.
* `While` loop has a boolean guard, so that their code only runs if the guard expression is _true_, and keeps running while the expssion is _true_ (an iteration might change a variable that effects the guard expression, resulting in the end of the loop). In our case, `0` equals _false_, anything else would be _true_.
* If the __stop value__ will never be met in our loops, then the loop could run endlessly
>### Alternative versions of the `For` loop
> These were created because teacher was having trouble with making it work, the cause was indentation... 
>
>    ```fs
>        | For (x, estart, estop, stmt) ->    // this is what you usually do
>            let start = eval estart sto
>            let stop  = eval estop  sto //here we evaluate stop outside of loop
>            let rec loop i sto = 
>                if i > stop then sto 
>                            //update x with iterator i 
>                            else let sto'  = setSto sto (x, i)
>                                //execute the body of the loop
>                                let sto'' = exec stmt sto'
>                                // use the incremented iterator i for next loop round
>                                loop (i+1) sto''
>            // Here we actually start using the recursive loop
>            loop start sto
>    ```
>   * This version will pass `i` as the control variable to the next iteration of the loop, rather than using the actual control variable `x` that was passed initially. 
>   * we could ay that having the `stop` evaluated outside of the loop is more efficient, if we don't expect the stop condition to change, because then we will have less computation than doing the evaluation of `stop` at each iteration.
>   * This has the issue of the possibility of `i` being changed during the course of the function, which would effect the iteration counter. 

```fs
let run stmt = 
    exec stmt emptystore
```

## Example programs
1. 
    ```
    sum = 0; for i = 0 to 100 do sum = sum + i; print sum 
    ```
    ```fs
    let ex1 =
        Block [Assign ("sum", Num 0);
            For ("i", Num 0, Num 100, 
                    Assign ("sum", Op ("+", Var "sum", Var "i")));
            Print (Var "sum")]
    ```
    1.
        ```
        for i = 0 to 100 do sum = sum + i; print sum
        ```
        ```fs
        let ex1' =
            Block [For ("i", Num 0, Num 100, 
                        Assign ("sum", Op ("+", Var "sum", Var "i")));
                Print (Var "sum")]
        ```
1.
    ```
    i = 1; sum = 0;
    while sum < 10000 do { print sum; sum = sum + i; i = 1 + i } ;
    print i; print sum
    ```
    ```fs
    let ex2 =
        Block [Assign ("i", Num 1);
            Assign ("sum", Num 0);
            While (Op ("<", Var "sum", Num 10000),
                    Block [Print (Var "sum");
                        Assign ("sum", Op ("+", Var "sum", Var "i"));
                        Assign ("i", Op ("+", Num 1, Var "i"))]);
            Print (Var "i");
            Print (Var "sum")]

    ```

1. 
    ```
    sum = 0;
    for i = 1 to 10 do { sum = sum + i; i = i * 2 ; print i };
    print sum
    ```
    ```fs
    let ex3 =
        Block [Assign("sum", Num 0);
            For ("i", Num 1, Num 10, 
                    Block [Assign ("sum", Op ("+", Var "sum", Var "i"));
                            Assign ("i", Op ("*", Var "i", Num 2));
                            Print (Var "i")
                            ]); 
            Print (Var "sum")]
    ```
1. 
    ```
    sum = 0;
    for i = 1 to 10-sum do { sum = sum + i; i = i * 2 ; print i };
    print sum
    ```
    ```fs
    let ex4 =
        Block [Assign("sum", Num 0);
            For ("i", Num 1, Op ("-", Num 10, Var "sum"), 
                    Block [Assign ("sum", Op ("+", Var "sum", Var "i"));
                            Assign ("i", Op ("*", Var "i", Num 2));
                            Print (Var "i")
                            ]); 
            Print (Var "sum")]
    ```