
# Executing programs
## Interpretation
Interpretation is evaluating the the given expressions directly, without compiling it to a lower-level language closer to the __assembly__ (it is the language that the hardware understands). 

Interpreter CLI are useful when experimenting with syntax and running little code snippets in order to see the output right away. This is not suited for creating programs, because the code only lives while the interpreter is running.

Most programming languages havee this kind of interpreter CLI available, and usually enabled by simply running the command used to interact with the language engine. 
* The `fsi` (fsharp interactive) terminal is a good example, since we are using `F#` as a meta-language for these notes. The command is most likely `dotnet fsi` because `F#` comes bundled within the `dotnet` core.

## Compilation
Compilers are basically __translators__ between programming languages. It is not unusual to have compilers that built ontop of other compilers (intermediate languages), like translating the code closer to the __assembly__ language in stages.

Compilers work better for larger programs, that have been written as text files, and then having the compilor evaluating the program from those files.

It happens in two stages:
1. Compile an expression to stack-machine code (closer to the language that the hardware understands)
1. Evaluate the stack-machine code

### Some reasons for using compilors
* If a program will be executed often, then it would be more efficient to have the program in machine code (_assembly_), where it will not require spending so many resources to evaluating what the code is trying to do.
    * __Assembly is not very human-readable__, so having the source code in a language easier to work with and then simply use compilers to translate it later is ideal.
* The _machine code_ can vary between systems/platforms based on the processors they use. So having compilors translate the code between them allows programs to be more compatible with those different systems.
* Maybe the developers have a favored programming language, but it is not the target enviorment for the code, so the final product would be compiled into the suitable format  
    * Taking `react-native` for example, that compiles the program to native code for android and iOS, not having to spend the extra time making the same program work on different devices manually.
* Perhaps some code from obsolete languages can be salvaged with compilors, translating into something that is still valid and compatable with the system in order to execute the code. 
    * Flash animations might be a good example, since support for that platform ended recently, artists might want to salvage their work and make the files compatible with newer programs / systems.
