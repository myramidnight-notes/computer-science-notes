[...back](../README.md)
# Lambda calculus
* Developed by ogician __Alonzo Church__ in the 1930s as a mathmetical formalism to express computation with functions.
* Many functional languages are based on Lambda calculus
* The idea of functions accepting functions as arguments comes fom __Lambda calculus__.


## Index
* ### [Lambda-calculus introduction](lambda-short-example.md)
    * The basics
    * Bound and free variables
* ### [Curried functions with lambda](lambda-currying.md)
* ### [Reduction](lambda-reductions.md)
    * &alpha;-conversions
    * &beta;-reduction and normal form
* ### Useful videos
    * [Reykjavik University á Youtube](https://www.youtube.com/user/rucomputerscience/search?query=fmal)
        * [fyrirlestur um lambda: 1](https://www.youtube.com/watch?v=v1IlyzxP6Sg)
        * [fyrirlestur um lambda: 2](https://www.youtube.com/watch?v=Mg1pxUKeWCk)
        * [fyrirlestur um lambda: 3](https://www.youtube.com/watch?v=3h0-p4SDHig)
    * [Lambda Calculus, as explained by __Computerphile__](https://www.youtube.com/watch?v=eis11j_iGMs)
    * [Fundamentals of Lambda Calculus and Functional Programming (__FullStack Academy__)](https://www.youtube.com/watch?v=3VQ382QG-y4).

## Some lambda definitions
* ### Lambda term
    > A __lambda term__ can have one of three forms: 
    >* a variable by itself is a valid _lambda term_
    >* a _lambda abstraction_
    >* an application of one _lambda term_ on another.
* ### Lambda functions/abstractions
    > A __lambda abstraction__ is also called a ___lambda_ function__ or ___lambda_ expression__.
    >
    > It is a definition of a anonymous function that is capable of taking a single input and substituting it into the expression.
* ### Lambda application
    > It is essentially like a function call, where we provide it with a argument. `t s` would represent the application of the function `t` to the input `s`.
* ### Free variables
    >__Free variables__ are variables not local to the expression, and not bound by an abstraction.
    >* The term `x` is a free variable
    >* The set of free variables of `λx.t` is the set of free variables of `t`, but with `x` removed (because `x` is bound to the function).
    >* The set of free variables of `t s` is the union of the set of free variables of `t` and the free variables of `s`.
    >
    >#### Colored example
    >![Example of free variables](./images/lambda-free-var.png)
    >
    > Here is a colored example of a very simple lambda expression, where it just outputs the input, but the function is followed by a variable `x`
    >* The red `x` are bound within the scope of the function `(λx. x)`
    >* the green `x` is outside of the function scope, so it's not the same `x` as the red one.
    >* This makes the green `x` a free variable from the defined function. 
    >
    >#### F# version 
    >```fs
    >let foo x = x ;;
    >foo x;;
    >```
    >* `foo` has a parameter named `x`, and it outputs it's input 
    >* We are calling the function `foo` with `x` as the argument, but this `x` is a different variable than `x` within the function.
    >   * The _property_ `x` of `foo` will shadow any previous definitions of `x` as a variable while within the scope. 

