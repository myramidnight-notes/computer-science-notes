[...back](./README.md)

* [Fundamentals of Lambda Calculus and Functional Programming (in javascript)](https://www.youtube.com/watch?v=3VQ382QG-y4).



## Curried functions
A __curried function__ is a function that takes in multiple arguments, one at a time, with the use of nested functions. The first argument is consumed by the first function, which returns a function that takes in the next argument, which returns a function that takes the argument after that, and so on... 

The nested functions allow us to compute a function in stages, instead of having to provide all the arguments at once. We can store and pass around functions that have recieved only a portion of the total arguments.

>`F#` actually _curries_ all functions for us, so we don't have to really think about doing that. 

Chaining bound variables is essentially currying, because it will just process one argument at a time.

* `λa.λb.λc.b` would be of the typing `a => b => c => b` 
    >(that might make sense if you've gone over polymorphic type inference).
* __Shorthand__ lambda would condence all the bound variables: `λabc.b`
    * It might give the impression that we take in all 3 arguments at once, to produce `b`, but behind the scenes it would sipmly be taking one argument at a time.

> When we have been using `F#` to gain understanding of functional programming, where we don't really have to add parenthesis around the arguments. If we had the variables `a`, `b` and `c`, and we would just line them up like this: 
>```
>a b c
>```
> Then the language just assumes (because it is left-associative) that any variables or values on the right of `a` (being the first variable in the line) are actually arguments that will be consumed in order. We can use parenthesis to force how the arguments should be handled (to define to which functions they belong to).
>
> So the `F#` version of the lambda term `(λa.λb.λc.b)` would be as follows:
>```fs
>let foo a b c = b;;
>//val it : ('a -> 'b -> 'c -> 'b)
>``` 



