# Reduction
There are 3 kinds of reductions
* __alpha/&alpha;-conversion__: changing bound variables
* __beta/&beta;-reduction__: applying functions to their arguments
* __eta/&eta;-reduction__: which captures a notion of extensionality

## Alpha/&alpha;-conversion
__&alpha;-conversions__, also known as __&alpha;-renaming, allows bound variables to be changed. 

Variable names are just placeholders for real values, so the names themselves hold no meaning, so renaming them changes nothing.

So `(λx.(λy.y x))` is equivalent to `(λa.(λb.b a))`, by renaming `x -> a` and `y -> b`.

### Substitution
>An example of the substitution syntax would be `(y * z)[(5-4)/z)]`, where the contents of the square bracketes can be considered the enviornment that tells you what to replace the variable with.
>
>Substitution only effects the free occurences of the variable in question.
>
>* The substitution of: `(y * z)[(5-4)/z)]` 
>    * All the free `z` will be replaced with the expression `(5-4)`,
>    * results: `(y * (5-4))`
>
>#### F# code substitution function
>```fs
>let rec lookOrSelf env x =
>   match env with
>   | []            -> Var x
>   | (y, e)::r     -> if x=y then e else lookOrSelf r x;;
>```
>

### Variable capture
>If we would rename a bound variable to be identical to a existing free variable that resides in the expression, then we are essentially changing the meaning of the function.
>
>If we would supstitute `y` with `x` in the following lambda term, than you can see how it might effectivly change the output of the function.
>```
>(λy.y x) ---> (λx.x x)
>```
>
>Substitutions that avoid accidentally capturing free variables are called __capture avoiding substitutions__
#### Capture avoiding substitutions


## &beta;-reduction 
A __beta-reduction__ is the act of taking a function and applying it to it's argument.
>1. We have the following lambda term: 
>    ```
>     ((λa.a)λb.λc.b)(x)
>    ```
>1. ![Beta reduction](./images/beta-reduction.png)
>    * We apply `λb.λc.b` to the argument of `(λa.a)`, so it replaces every `a` within that function.
>    * This would result in `(λb.λc.b)`, because we have now reduced the function `(λa.a)`, replacing it with what it will output.
>1. ![Beta reduction step 2](./images/beta-reduction2.png)
>    * Here we apply `x` to the argument `b`, replacing all occurences of `b` within the function.
>    * We can then remove the surrounding function, replacing it with the output.
>1. ![Beta reduction step 3](./images/beta-reduction3.png)
>    * If we would pass an argument to the resulting function `(λc.x)`, then we would see that the output is simply `x`, because it does nothing with the argument `c`
>1. `((λa.a)λb.λc.b)(x)` is therefor reduced to `x`, because the final `(λc.x)` simply returns `x`. 

## &beta;-normal form
A __beta-normal-form__ is a way to say that we fully evaluated the lambda function using _&beta;-reduction_, that it cannot be reduced further. It would be normalized because it won't go on forever.

## Lambda combinators (some names for certain lambda terms)
### The Kestrel 
>The _kestrel_ simply takes two things, and returns the first one. It's all it does.
> `K := λab.a` 
>* The typing would be `a => b => a`
>* It is called __const__ in Haskel, because it will always give you the same value, regardless of what you supply it with.

### The kite
> Its like the _kestel_, but instead it always returns you the 2nd thing.
> 
> We can actually derive a _kite_ using the _kestrel_ by providing it with 3 arguments, the 1st one would be a function (one that simply returns the input as output), the 2nd argument would be ignored by the kestel, and the function that is returned from the kestel will take in the 3rd argument. 