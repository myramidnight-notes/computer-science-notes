* [Lambda Calculus, as explained at Computerphile](https://www.youtube.com/watch?v=eis11j_iGMs)
* [Lambda calculus (wikipedia)](https://en.wikipedia.org/wiki/Lambda_calculus)

# Lambda Calculus
__Lambda calculus__ (also written as &lambda;-calculus) is a formal system in mathmetical logic for expressing computation based on function abstraction and application using variable binding and substitution. It is a universal model of computation that can be used to simulate any __Turing machine__.

>#### Turing Machine?
> A __Turning machine__ is a mathmetical model of computation that defines an __abstract machine__ that manipulates symbols on a strip of tape according to a table of rules. Despite the model's simplicity, given any computer algorithm, a Turning machine capable of simulating that algorithm's logic can be constructed.
>
>#### Abstract machine?
>Also called an __abstract computer__, it's a model of a computer system (considered either as hardware or software) constructed to allow a detailed and precise analysis of how the computer system works. Such a model usually consists of input, output and operations that can be performed (the operation set), and so can be thought of as a processor. Turing machines are the best known abstract machines, but there exist many other machines.
>
>Abstraction of computing processes is used in both the computer science and computer engineering disciplines and usually assumes a discrete time paradigm 

## Lambda and functions
Functions are essentially _black boxes_ that we cannot see into, but these functions will be pure functions that have no inner state, where the output is directly tied to the input.

So we will know the input, and we will know the output. 

With __Lambda Calculus__ we can use this information and break the function down according to it's arguments, which is the essence of __currying functions__, which explains why understanding lambda is important.

In the notation of lambda, each input variable will be tied to a lambda symbol (&lambda;). So if we had a function with the input `x` and output `x+1`, then the lambda notation would be: 
* __&lambda;x. x+1__
    >This is saying it takes in the variable `x` and outputs `x+1`

### What's the point?
It might seem absurdly simple, as if the lambda isn't actually doing anything, but it's actually a great way to abstract a function, allowing us to define more complex behaviours in a simpler way.
1. __Lambda can encode ANY computation__
    * Any turing machines (_mathmetical model of computation that defines an abstract machine_) can be translated in lambda calculus, and vise versa. 
1. __It can be the basis for any functional programming language__
    * They are all fundimentally based on the lambda calculus
    * This is how we get our curried functions
1. __Lambda can be found in any pogramming language these days__

## Syntax in lambda
|Syntax | Name | Description |
| ---|--- | --- |
|`x` | Variable | A __variable__ is a _character_ or _string_ that represents a parameter or mathmetical/logical value. 
| `(λx.M)` | Abstraction | Function definition, `λx.` is binding the variable `x` as a parameter for the function, and `M` is a reference to another lambda term (imagine a function call)
| `(M N)` | Application | Applying a function to an argument. `M` and `N` are lambda terms

## Example: True False
These boolean definitions in lambda will take two inputs, `x` and `y` that are bound to the lambda, and it outputs one or the other, depending on if it's the definition for __true__ or __false__. Very basic.
* `TRUE` =  	__&lambda;x. &lambda;y. x__
* `FALSE` =   __&lambda;x. &lambda;y. y__

And in the context of `F#` programming language that we've been using as a meta language, a function call is always followed by it's arguments.

So how would we define the typical __NOT__ expression with these lambda boolean definitions we described? What we want is essentially `NOT TRUE` = `FALSE`, turning a `TRUE` into a `FALSE`

We simply break it down, by expanding definitions.
* `NOT` = &lambda;b. b `TRUE` `FALSE`
   >Here we have defined `NOT` as a lambda, where we apply `b` to both `TRUE` and `FALSE` functions we defined before (or simply, `b` will take `TRUE` and `FALSE` as arguments). It might not make sense right now, but we can go deeper! By expanding definitions
*  `NOT` `TRUE` 
    > Let's call the `NOT` function with `TRUE` as the argument, so that we can expand it to see that it actually works in lambda calculus.
*  `NOT` `TRUE` = (&lambda;b. b `FALSE` `TRUE`) `TRUE`
    > Then we expand the definition by substituting the `NOT` with our lambda definition of it.
    >
    >Let's use some lambda magic to prove that this is a valid definition of `NOT` (that it can inverse the boolean)
* `b` `FALSE` `TRUE` =  `TRUE` `FALSE` `TRUE`
    > What happened here? We replaced `b` with it's value (the `TRUE` being passed in as argument `b` so to speak). This might make even less sense now, but we can explain by expanding further.
* `TRUE` `FALSE` `TRUE` = (&lambda;x. &lambda;y. x) `FALSE` `TRUE`
    > Here we substitute `TRUE` with it's lambda definition from earlier, can you see the lambda magic now?  
    >
    >`x` = `FALSE`, `y` = `TRUE`, and according to the lambda, we will output `x`
    >
    >We have successfully turned a `TRUE` into a `FALSE`

## Extras
### The `Y` operator (encoding recursion)
>This apparently is a very famous lambda expression, which is the key encoding recursion. It was defined by _Haskell b. Curry_
>* `Y` = &lambda;f. (&lambda;x. f (x x)) (&lambda;x. f (x x))
