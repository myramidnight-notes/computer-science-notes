[...back](../README.md)
# Parsing
Being able to write in a concrete syntax lets the code be more human-readable, while it can still be converted into something that the computer would understand easier. Then we just parse the code from __concrete__ to something that the computer can process easier.
>When we talk about __parsing__, it can be one of two things: 
>* The whole process of going from _concrete_ to _abstract_ syntax, including the __lexing__
>* Just the process of going from _token list_ to _abstract syntax tree_


### Going from concrete to abstract
1. We begin have code in __concrete syntax__, which means it's just strings.
1. We use __lexing__ to convert the strings into __list of tokens__
1. Then we __parse__ that _token list_ into a __abstract syntax tree__
    * from there we can interpret or compile the code

## Index
* ### [Lexing](lexing.md) 
    > Turning the code string into a list of _tokens_
* ### Parsing
    > Turning the list of _tokens_ into the __abstract syntax tree__
* ### Implementing parsers
    * [__with mandatory parenthesis on operators__](parsing.md)
    * [__without parentheses on operators__](parsing-opt-par.md)
        * The issue with indefinite recursion
        * [__Operator associations__](op-association.md)