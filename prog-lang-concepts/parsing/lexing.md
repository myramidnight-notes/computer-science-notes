
# Lexing
__Lexing__ would be the step of interpreting the concrete strings into a list of __tokens__ that can be used to fully parse the code into __abstract syntax tree__, so that the computer can process it.


### Defining the tokens
```fs
type token =
    | NAME of string         // variable names
    | LET | EQUAL | IN
    | INT of int             // unsigned integers
    | PLUS | MINUS | TIMES
    | LPAR | RPAR            // parentheses
    | ERROR of char          // illegal symbols 
```


| Phase | code |
| --- | --- |
| concrete syntax | `let x = 17 in z + x` 
| token list | `[LET; NAME "z"; EQUAL; INT 17; IN; NAME "z"; PLUS; NAME "x"]`
| abstract syntax | `Let("z", Num 17, Plus( Var "z", Var "x"))`

## Going from concrete to tokens
### The lexer
We will define three functions that will be simultaniously recursive, where they can call each other.

`tokenize` is the base of the function, that traverses the string of code and detect what it found. Since a string is just a __list of characters__, it will go through each character and match it accordingly.

If it finds a letter or digit, then it will let the `tokenizeInt` and `tokenizeWord` handle collecting each characater that the word or number is composed of.

```fs
let rec tokenize (cs : char list) : token list =
    match cs with
    | [] -> []
    | '+'::cs  -> PLUS :: tokenize cs
    | '-'::cs  -> MINUS :: tokenize cs  
    | '*'::cs  -> TIMES :: tokenize cs
    | '='::cs  -> EQUAL :: tokenize cs
    | ' '::cs  -> tokenize cs
    | '\t'::cs -> tokenize cs
    | '\n'::cs -> tokenize cs
    | '('::cs  -> LPAR :: tokenize cs
    | ')'::cs  -> RPAR :: tokenize cs
    | c::cs when isDigit c ->               //if digit
        tokenizeInt cs (digit2Int c)
    | c::cs when isLowercaseLetter c ->     //if letter
        tokenizeWord cs (string c)
    | c::cs -> ERROR c :: tokenize cs

and tokenizeInt cs (acc : int) =
    match cs with
    | c::cs when isDigit c ->
        tokenizeInt cs (acc * 10 + digit2Int c)
    | _ -> INT acc :: tokenize cs

and tokenizeWord cs (acc : string) =
    match cs with
    | c::cs when isLetter c || isDigit c ->
         tokenizeWord cs (acc + string c)
    | _ -> word2Token acc :: tokenize cs

```
* This lexer will only consider lower-case letters to be a valid start of words.
    > Sometimes upper case letters have a specific meaning in a language
* `tokenizeInt` and `tokenizeWord` are accumilators, that collect a word or number.
### Finding the words
When the `tokenizeWord` is done collecting all the letters of a single word, it will ask the `word2Token` to check if it's a __keyword__ or a __variable name__. 

```fs
let word2Token (s : string) : token =
    match s with
    | "let" -> LET
    | "in"  -> IN
    | _     -> NAME s
```
1. We will want to identify defined __keywords__ first (in our case: `let` and `in`) 
    > __Keywords__ are reserved and have specific meanings for the language.
1. anything else would be considered a `NAME` (a variable)

### Finding the numbers
```fs
let string2Chars (s : string) : char list =
    let rec helper cs i =
        if i = 0 then cs else let i = i - 1 in helper (s.[i] :: cs) i
    helper [] (String.length s)     

let lex s = tokenize (string2Chars s)
```
