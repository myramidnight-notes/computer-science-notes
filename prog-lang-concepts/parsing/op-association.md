# Operators
In programming languages, the associativity of an operator is a property that determines how operators of the same precedence are grouped in the absence of parentheses. 

## Operation assosiations
If you think of `~` as any possible operand
* __associative__ (meaning the operations can be grouped arbitrarily)
* __left-associative__ (meaning the operations are grouped from the left)
    ```
    (x ~ y) ~ z
    ```
* __right-associative__ (meaning the operations are grouped from the right)
    ```
    x ~ ( y ~ z)
    ``` 
* __non-associative__ (meaning operations cannot be chained, often because the output type is incompatible with the input types).


