
# Parser with optional parentheses
Most modern programming languages have optional parentheses, but then the parser has to figure out operators start and where they end.
## Why not just remove the parentheses?
We cannot expect our previous implementation (with mandatory parentheses) to work correctly by simply dropping the parentheses from the grammar for operators.
### Issues with infinite left-recursion
Our previous parser example had mandatory parentheses around any operators, to give us clear indicators of where they start and end. It is simpler to implement.

If we would wish to implement __optional parentheses__ for operators, then we will have to complicate our implemenetation quite a bit. We cannot simply drop the parentheses from the grammar and expect our previous implementation to work. 

> Version of grammar from previous parser, without the mandatory parentheses
>```
>e --->
>    | x
>    | let x = e in e
>    | i
>    | e + e 
>    | e - e
>    | e * e
>    | (e)
>```

Trying to parse `e + e` then would result in a __infinite left-recursion__, causing a stack-overflow. 

>```
>e  ---> Plus (e1, e2)
>    ---> Plus (Plus (e11, e12), e2)
>    ---> Plus (Plus (Plus (e111, e112), e12), e2)
>    ---> Plus (Plus (Plus (Plus (//endless recursion...
>```

>The parser evaluates the rules in a depth-first manner, having a strict prioritization of rule invocations. With this depth-first approach, rules can get invoked in cycles, which constitutes a significant problem for parsers.
>> [The importance of left-recursion in parsing expression grammars](https://medium.com/@friedrich.schoene/the-importance-of-left-recursion-in-grammars-608f849447f6)


## Fixing the infinite left-recursion
In this new grammar, an __expression__ is defined as a __summand__ with a possible _operation_ on a expression. 

And a __summand__ would be defined as a __factor__, possibly followed by a __TIMES__ operation on a _summand_.
>```
>e --->
>    | s + e             //summand and expression
>    | s - e
>    | s
>
>s --->
>    | f * s             //factor and summand
>    | f
>```
* This removes the issue of infinite __left-recursion__
* However our operators will still be __right-associated__
    * we would get `x ~ ( y ~ z )` instead of `(x ~ y) ~ z`.
        > `~` indicates any possible operand 
    * We actually want our operands to be __left-assosiated__.

### Making the grammar left-associative
To make our grammar __left-associated__, then we can reorganize our grammar even further. Here our expressions become summands followed by a list of summands, and so on.
>```
>e --->                            // expressions
>    | s ss 
>
>ss --->                           // lists of summands
>    | + s ss | - s ss
>    | (empty)
>
>s --->                            // summands
>    | f ff
>
>ff --->                           // lists of factors
>    | * f ff
>    | (empty)
>    
>f --->                            // factors
>    | x
>    | let x = e in e
>    | i
>    | (e)          <--- expression in parentheses
>```
1. An expression is initially composed into summands
    * It includes `+` and `-` operators in it's vocabulary
1. Summands will contain factors
    * It includes `*` operator in it's vocabular
1. Factors would define the variables, local variables, values and expressions within parentheses 
    * expressions would then go to the top, being a list of summands.
    * Since it's a recursion, we return to where we left the factors, to find the closing parentheses to conclute that the syntax was completed correctly according to the grammar.
## Parser for left-association 
Here we have 5 functions that are simultaniously recursive. The current grammar says that all expressions are just a summand followed by a list of summands. 
1. `parseExpr`
    * Takes tokens and produces a expression + remaining tokens list
    * We first throw list of tokens to `parseSummand` and give `parseSummandList` the leftover tokens that would be returned.
1. `parseSummandList`
    * If the summand is a `+` or `-`, then it goes to `parseSummand` and repeats the recursion in `parseSummandList` on the leftover tokens
    * Else it returns the the _summand_ and tokens back.
1. `parseSummand`
    * This function also throws the list of tokens to `parseSummand`, but instead it gives the leftover tokens to `parseFactorList`
1. `parseFactorList`
    * This function tries to match the `*` operator, else it returns the _factor_ and token list back.
    * If it finds a `*` operand, then it parses the factor with `parseFactor` and throws the leftover tokens into the recursion of `parseFactorList`
1. `parseFactor`
    * This is essentially the parser from previous example, which tries to match the __keywords__, __variables__,__local variables__ and __values__
    * At this stage, if it does not find a expected match, then it leaves us with an __error__.
    * We can now place parentheses around any expression, defining a scope of sorts.
```fs
let rec parseExpr (ts : token list) : expr * token list =
                         // e ---> s ss
     let e1, ts = parseSummand ts
     parseSummandList e1 ts         //accumilator

and parseSummandList (e1 : expr) (ts : token list) : expr * token list =   
    // ss ---> + s ss | - s ss | (empty)
    match ts with
    | PLUS :: ts ->
        let e2, ts = parseSummand ts
        parseSummandList (Plus (e1, e2)) ts
    | MINUS :: ts ->
        let e2, ts = parseSummand ts
        parseSummandList (Minus (e1, e2)) ts
    | _ -> e1, ts

and parseSummand (ts : token list) : expr * token list =
    // s ---> f ff
     let e1, ts = parseFactor ts
     parseFactorList e1 ts

and parseFactorList (e1 : expr) (ts : token list) : expr * token list =
    // ff ---> * s ss | (empty)
    match ts with
    | TIMES :: ts ->
        let (e2, ts) = parseFactor ts
        parseFactorList (Times (e1, e2)) ts
    | _ -> e1, ts
     
and parseFactor (ts : token list) : expr * token list =
    // f ---> x | let x = e in e | i | (e) 
    match ts with 
    | NAME x :: ts -> Var x, ts
    | LET :: NAME x :: EQUAL :: ts ->
        let erhs, ts = parseExpr ts
        match ts with
        | IN :: ts ->
            let ebody, ts = parseExpr ts
            Let (x, erhs, ebody), ts
        | _ -> failwith "let without in"
    | LET :: NAME x :: _ -> failwith "let without equals sign"
    | LET :: _ -> failwith "lhs of def in let not a variable name"
    | INT i :: ts -> Num i, ts
    | LPAR :: ts ->
        let (e, ts) = parseExpr ts
        match ts with
        | RPAR :: ts -> e, ts
        | _ -> failwith "left paren without right paren"
    | _  -> failwith "not a factor"
```
>* If the `parseSummandList` or `parseFactorList` find no operands in the list of tokens, then it will simply return what the parser has accumilated so far and the leftover tokens.

### The top-level parser
```fs
let parse (ts : token list) : expr =
    let e, ts  = parseExpr ts
    if ts = [] then e else failwithf "unconsumed tokens"

```

### Combined parser with lexer
```fs
let lexParse (s : string) : expr = parse (lex s)

```