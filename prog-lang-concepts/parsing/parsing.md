# Parsing with mandatory parentheses
This is a implementation of a parser with __mandatory parentheses__ around operators (`+`, `-`, `*`). 

## Defining the grammar
We will need to define our __grammar__ for the parser. 
* The parentheses are _mandatory_ around operation applications and cannot be used elsewhere
```
e ---> 
    | x
    | let x = e in e
    | i
    | (e + e)
    | (e - e)
    | (e * e)
```
An expression according to the given grammar
```
let w = 3 in ((w+3) + (y * z))
```
## The recursive parser
This parser takes in a list of __tokens__ from the __lexer__ and returns an expression and list of unconsumed tokens.
* It expects `Let` expressions to have a specific format
    > `[LET; NAME x; EQUAL; ...(body of the let)]`
```fs
let rec parseExpr1 (ts : token list) : expr * token list =

    match ts with
    | [] -> failwith "missing expression"
    
    | NAME x :: ts -> Var x, ts               // e ---> x
    
    | LET :: NAME x :: EQUAL :: ts ->         // e ---> let x = e in e
         let erhs, ts = parseExpr1 ts
         match ts with
         | IN :: ts ->                        // defines the body
              let ebody, ts = parseExpr1 ts
              Let (x, erhs, ebody), ts
         | _ -> failwith "let without in"     // no body defined
    | LET :: NAME x :: _ -> failwith "let without equals sign"
    | LET :: _ -> failwith "lhs of def in let not a variable name"
    
    | INT i :: ts -> CstI i, ts               // e ---> i

    | LPAR :: ts ->                           // e ---> (e op e)
         let e1, ts = parseExpr1 ts
         match ts with
         | PLUS :: ts ->
              let e2, ts = parseExpr1 ts
              match ts with
              | RPAR :: ts -> Plus (e1, e2), ts
              | _ -> failwith "left paren without right paren"
         | MINUS :: ts ->
              let e2, ts = parseExpr1 ts
              match ts with
              | RPAR :: ts -> Minus (e1, e2), ts
              | _ -> failwith "left paren without right paren"
         | TIMES :: ts ->
              let e2, ts = parseExpr1 ts
              match ts with
              | RPAR :: ts -> Times (e1, e2), ts
              | _ -> failwith "left paren without right paren"
         | _  -> failwith "missing operation symbol"

    | IN :: _ -> failwith "in without let"
    | EQUAL :: _ -> failwith "equals sign without let"
    | PLUS :: _ | MINUS :: _ | TIMES :: _ ->
         failwith "operation symbol without left arg"
    | RPAR :: _ -> failwith "right paren without left paren"
    | ERROR _ :: _ -> failwith "illegal symbol"   
```
* The multiple `LET` matches are essentially _error catches_, to catch badly formed `let` expressions (missing parts of the expected syntax).
    > It expects the `LET` to have a defined variable and value, followed by `IN` to indicate where the variable ends and body begins. Our implementations do not work without all the parts.
* It will expect a operation if it runs into a `LPAR` (left parenthesis), and fails if it does not find a matching `RPAR` (right parenthesis).
    > It expectes there to be a operand within those parentheses. 


### Top-level parser
The __top level__ parser, which calls on the recursive parser `parseExpr1` and then returns an expression in __abstract syntax__ if the token list comes back empty (no leftover tokens).

```fs
let parse1 (ts : token list) : expr =
    let (e, ts)  = parseExpr1 ts
    if ts = [] then e else failwithf "unconsumed tokens"
```

## Combine the parser with the lexer
Here we have the __lexer__ nested in the __parser__, so the `lexParse1` takes a _string_ and passes it straight to the lexer and then the results will be a expression in __abstract syntax__.
```fs
let lexParse1 (s : string) : expr = parse1 (lex s)
```

## Handling leftover tokens
Leftover tokens does not equal a failed expression, but instead it might indicate that we were parsing a __sub-expression__ (where we were provided with an expression instead of a number/variable), so we are still in the middle of parsing.

Then the parser needs to finish the inner expression and then we continue with the outer expression.

## Why parentheses for operations?
We run the risk of __infinite left-recursion__ if we simply remove the parentheses from the grammar around operations, which ends in a __stack-overflow__.
> Take a look at [parsing with optional parentheses](parsing-opt-par.md#Parser-with-optional-parentheses) for better explanation