[...back](README.md)
# Programming Vocabulary
* [__Icelandic translations__](#translated-vocabulary)
* [__Programming language descriptors__](#Programming-language-descriptors)
* [__Programming Paradigms/Styles__](#Programming-Paradigms/Styles)
## Programming terms
* __expression__ 
    > code that can be evaluated to determine it's value
    >* Arithmatic expressions (math/algebra)
    >* Boolian expressions ("stærðfræðileg rökfræði")
* __syntax__ 
    > the rules of the programming language
* __semantics__
    > the way we express the programming language (the same expression could be presented in multiple ways, depending on the language)
* __variable__ / __scalar__ 
    > a name that is associated to a value that is discovered during evaluation (think of algebra, where `x` is a variable that could take on any unknown value)
* __scope__
    > it is the location where a variable is accessable
* __enviornment__
    > the list of variables accessable within the scope
* __name bindings__
    >The values in expressions can be given names, which makes them variables. A value is bound to the given name, and in some languages this value and type cannot be changed after it has been defined.
    >
    > A variable name is just a placeholder for whatever has been bound to the name, so the type of the variable still has to match the other values in an expression.
* __closure__ / __function closure__ / __function__
    > a closure is operationally a __record__ containing a function.
* __record__ / __structure__ / __compound structure__ 
    > a __data structure__ for storing a fixed number of elements/fields. Can be considered a dictionary/object with immutable keys.
* __Pure functions__
    >* They are deterministic (always returns the same output for a given input)
    >* It has no side-effects:
    >    * Nothing else in your system will change. 
* __Well typed__
    > It means that an expression or function is consistent, returning the expected/defined type. A __ill typed__ _if-statement_ for example could return a _boolean_ or _integer_, depending on the results of the comparator.
### Programming language descriptors
* __Strongly typed__ language
    > This means that all data types are defined, even before you start coding anything properly. Everything should have a specified type and it should not _mutate_ into something else. 
    >
    > If a variable/property/value is not of the expected/defined type, then the program cannot run. 
    >
* __Loosely typed__ language
    >Javascript is an example a loosely typed language. Any variable/parameter/value can be of any type, the language will take a guess at what the type is based on the value given. A variable could even change types over the course of the program running. It makes the program unreliable in a way that you have no insurance that a variable/value will be of the expected type.
* __Strict (eager/keen)__ language
    >A language is strict when it evaluates the function and arguments before the application itself.
    >
    > The oposite to strict is called __lazy__ 



## Translated vocabulary
>Not sure if these are standardized translations, but it's the words I picked up when I sat a course teaching programming in Icelandic.
* __algorithm__ ('algrím')
* __syntax__ ('málskipan')
* __expressions__ ('segðir')
* __loops__ ('lykkjur')
* __itterating__ ('ítrun', 'endurtekning')
* __sequence__ ('röð')
* __precedence__ ('forgangur')
* __statements__ ('setningar', 'yrðingar')
* __variable__ ('breyta')
* __local variable__ ('staðvær breyta')
* __operators__ ('virkjar')
* __assignment__ ('gildisveiting')
* __conversion__ ('umbreyting')
* __binary opeators__ ('tvíundarvirkjar')
* __literals__ ('lesgildi')
* __conditional__ ('skilyrðing')
* __namespace__ ('nafnsvið')
* __function__  ('fall')
* __scope__ ('gildissvið')
* __argument__ ('viðfang')
* __parameter__ ('stiki')
* __suffix__ ('viðskeyti')
* __prefix__ ('forskeyti')

## Programming Paradigms/Styles
> [Comparison of programming paradigms](https://en.wikipedia.org/wiki/Comparison_of_programming_paradigms)

Even if a programming language initially falls under or best suited for the associated paradigm, some can be flexible enough to allow the programmer to create programs using different paradigms. Such languages can sometimes be categorized as _multi-paradigm_, such as `Python`.

* __Object Oriented__: Program states are maintained with classes/objects
    > `Python`
* __Imperative__: Focuses on how to execute. Statements that directly change computed state.
    > `C`, `Java`, `JavaScript`, `Python`
* __Functional__: Program is maintained with functions. It avoids states and mutable data.
    > `JavaScript`, `F#`, `Python`

