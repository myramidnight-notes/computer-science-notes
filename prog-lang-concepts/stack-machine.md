
# Stack machines
* [Stack machine without variables](#Stack-machine-without-variables) (basic)
* [Stack machine with runtime enviornment](#Stack-machine-with-runtime-enviornment) (target variables)
* [Stack machine with intermediate values](#Stack-machine-with-intermediate-values) (local variables)

Even though _expressions_ are a very simple language, it can get very mess/complicated to do more complex equations. 

A __stack__ is a data structure that is basically a list where you can only add/remove items from one end of it. It is easiest to imagine a stack of plates in a stand that does not allow you to extract plates from the sides, which only leaves you with access to the top plate.

We would be adding a extra step to our interprations:
1. Compiling our expressions to a stack machine code
1. Interpreting the stack machine code

| Example of stack machine compilation | result 
| --- | --- 
| Expression | `2 * (3 - 4)`
| Stack Machine Code | `2 3 4 - *`
| Stack Machine instructions | `[RNum 2; RNum 3; RNum 4; RSub; RMul]`

## Stack machine without variables 
### Instructions and Code
A computer functions as a list of instructions at the very core, executing orders in the order they arrive. This stack machine can be a simplicied representation of how __assembly__ works.

A stack machine will have a fixed number of instructions it can perform. 
```fsharp
// Our stack instructions (rinstr for runtime instructions)
type rinstr =
  | RNum of int //pushes integer to the stack
  | RAdd
  | RSub
  | RMul
  | RPop        //Removes an integer from the stack
  | RDup        //Duplicates whatever is at the top of stack
  | RSwap       //Swaps the top two integers on the stack

type code = rinstr list //datatype that is list of instructions
```
* `RNum` 
    > It will add numbers to the stack
* `RAdd` 
    > It will remove the top two numbers from the stack, add them together, and place the results back on the stack
* `RSub` 
    > It will remove the top two numbers from the stack, Subtract them from each other and place the results back on the stack 
* `RMul`
    > It will remove the top two numbers from the stack, multiply them together and place the results back on the stack.
* `RPop`
    > It will remove the top/latest number from the stack
* `RDup`
    > It will duplicate the top number of the stack and add it to the stack
* `RSwap`
    > Will take the top two numbers from the stack, and place them back on the stack in reverse order (swapping them)


The Stack machine code will be what is called a __options list__, which will have _options_ sometimes paired with a value. These options are not actually strings, and will be translated into instructions in our case. 
>`let myInstr = [RNum 7; RNum 9; RMul; RNum 10; RAdd]`

### Stack machine evaluator
It is up to the machine evaluator to give those instructions meaning.

```fsharp
// Our stack type
type stack = int list 

// Our evaluator
let rec reval (inss : code) (stk : stack) : int =
    match inss, stk with    //instructions and stack
    | ([], i :: _) -> i     //return the solution and return it
    | ([], [])     -> failwith "reval: no result on stack!"
    | (RNum i  :: inss,             stk) -> reval inss (i :: stk)
    | (RAdd    :: inss, i2 :: i1 :: stk) -> reval inss ((i1+i2) :: stk)
    | (RSub    :: inss, i2 :: i1 :: stk) -> reval inss ((i1-i2) :: stk)
    | (RMul    :: inss, i2 :: i1 :: stk) -> reval inss ((i1*i2) :: stk)
    | (RPop    :: inss,        i :: stk) -> reval inss stk
    | (RDup    :: inss,        i :: stk) -> reval inss ( i ::  i :: stk)
    | (RSwap   :: inss, i2 :: i1 :: stk) -> reval inss (i1 :: i2 :: stk)
    | _ -> failwith "reval: too few operands on stack"

//Test for our evaluator
let rpn1 = reval [RNum 10; RNum 17; RDup; RMul; RAdd] []
```
* It will initially take an empty stack, which it will use to place the integers as they are processed.
* If we write a well balanced code, then the stack should only have a single item left when it's done computing all the instructions, which will be the solution of the expression
* Since datatypes in `F#` is usually immutable, each evaluation will return a new stack.

### Compile expression into stack machine code
```fsharp
let rec rcomp (e : expr) : code =
    match e with
    | Num i            -> [RNum i]
    | Op ("+", e1, e2) -> rcomp e1 @ rcomp e2 @ [RAdd]
    | Op ("*", e1, e2) -> rcomp e1 @ rcomp e2 @ [RMul]
    | Op ("-", e1, e2) -> rcomp e1 @ rcomp e2 @ [RSub]
    | Op _             -> failwith "unknown primitive"
```
> This does not evaluate the expression, since compilers do not execute code. What you will get is the list of commands needed for the Stack machine evaluator.
>
>| Expression | in Abstract Syntax | Compiled instructions |
>| --- | ---  | ---
>| `5+5` | `Op("+", Num 5, Num 5)` | `[RNum 5; RNum 5; RAdd]`
>| `3-4` | `Op("-", Num 3, Num 4)` | `[RNum 3; RNum 4; RSub]`
>| `7*9+10` | `Op("+", Op("*", Num 7, Num 9), Num 10)` | `[RNum 7; RNum 9; RMul; RNum 10; RAdd]`



## Stack machine with runtime enviornment
This is essentially the basic stack-machine code example, with a added `RLoad` to find the values from the enviornment and copy them to the stack. 

The `renv` will be our runtime environment (imagine it being the computer memory)

> This implementation uses __targets__ for the enviornment instead of _named variables_. This represents __pointers__ to memory where the value would be stored.
>
> We could compile the target expressions to stack machine code, and have it change named variables into pointers instead (index)


### The machine compilor
```fsharp
//The stack-machine compiler with added Rload
let rec rcomp (e : texpr) : rcode =
    match e with
    | TVar n            -> [RLoad n]  //Load the target
    | TNum i            -> [RNum i]
    | TOp ("+", e1, e2) -> rcomp e1 @ rcomp e2 @ [RAdd]
    | TOp ("*", e1, e2) -> rcomp e1 @ rcomp e2 @ [RMul]
    | TOp ("-", e1, e2) -> rcomp e1 @ rcomp e2 @ [RSub]
    | TOp _             -> failwith "unknown primitive"
```

### Evaluating the instructions with targets
```fs
type rinstr =
  | RLoad of int            // load from environment
  | ...                    // rest are same as before... 
```
* `RLoad i`
    > loads variable from enviornment at location `n`
```fs
let rec reval (inss : rcode) (stk : stack) (renv: renvir) =
    match inss, stk  with 
    | [], i :: _ -> i
    | [], []     -> failwith "reval: no result on stack!"
    | RLoad n :: inss,             stk ->   // the new loading instruction
         reval inss (List.item n renv :: stk) renv   
    | RNum i :: inss,             stk -> reval inss (i :: stk) renv
    | RAdd    :: inss, i2 :: i1 :: stk -> reval inss ((i1+i2) :: stk) renv
    | RSub    :: inss, i2 :: i1 :: stk -> reval inss ((i1-i2) :: stk) renv
    | RMul    :: inss, i2 :: i1 :: stk -> reval inss ((i1*i2) :: stk) renv
    | RPop    :: inss,        i :: stk -> reval inss stk renv
    | RDup    :: inss,        i :: stk -> reval inss ( i ::  i :: stk) renv
    | RSwap   :: inss, i2 :: i1 :: stk -> reval inss (i1 :: i2 :: stk) renv
    | _ -> failwith "reval: too few operands on stack"
```

## Stack machine with intermediate values
A stack machine that has variables and local scopes
>This stack machine has the following arguments:
>1. The instructions (`inss`)
>1. A stack (`stk`) for _intermediate values_ 
>1. heap/stack for _variable-bound_ values (the environment, `renv`).

When we have inner scopes that redefine the enviornment (possible shadowing), then we have to be able to move things around while we compile

### Defining the instructions
```fsharp
type rinstr =
    | RLoad of int
    | RStore
    | RErase              
    | ...              // Rest are the same as the initial rinstr

type rcode = rinstr list

type stack = int list         // intermediate values
```
* `RStore` 
    > Moves top value from the _stack_ and places it at the `0` position of the _enviornment_. It shifts everything in the enviornment down by `1`
* `RErase`
    > Removes the `0` value from environment, shifting the remaining values up by `1`. This is essentially the __garbage disposal__, to reset the stack to the state it was before we entered a inner scope (for example the body of a `TLet`).

### Compile target expression to stack-machine code
```fs
let rec rcomp (e : texpr) : rcode =
    match e with
    | TVar n             -> [RLoad n]
    | TLet (erhs, ebody) -> rcomp erhs @ [RStore] @ rcomp ebody @ [RErase]
    | TNum i             -> [RNum i]
    | TOp ("+", e1, e2)  -> rcomp e1 @ rcomp e2 @ [RAdd]
    | TOp ("*", e1, e2)  -> rcomp e1 @ rcomp e2 @ [RMul]
    | TOp ("-", e1, e2)  -> rcomp e1 @ rcomp e2 @ [RSub]
    | TOp _              -> failwith "unknown primitive"

```
### Evaluating the stack-machine instructions
The _stack-machine_ interpreter will now take a 3rd argument, the enviornment (empty list initially, gets filled during the evaluation).
```fsharp
// * Interpreting (running) stack machine code

let rec reval (inss : rcode) (stk : stack) (renv : renvir) =
    match inss, stk with 
    | [], i :: _ -> i
    | [], []     -> failwith "reval: No result on stack!"
    | RLoad n :: inss,             stk ->
          reval inss (List.item n renv :: stk) renv
    | RStore  :: inss,        i :: stk -> reval inss stk (i :: renv)
    | RErase  :: inss,             stk -> reval inss stk (List.tail renv) 
    | RNum i  :: inss,             stk -> reval inss (i :: stk) renv
    | RAdd    :: inss, i2 :: i1 :: stk -> reval inss ((i1+i2) :: stk) renv
    | RSub    :: inss, i2 :: i1 :: stk -> reval inss ((i1-i2) :: stk) renv
    | RMul    :: inss, i2 :: i1 :: stk -> reval inss ((i1*i2) :: stk) renv
    | RPop    :: inss,        i :: stk -> reval inss stk renv
    | RDup    :: inss,        i :: stk -> reval inss ( i ::  i :: stk) renv
    | RSwap   :: inss, i2 :: i1 :: stk -> reval inss (i1 :: i2 :: stk) renv
    | _ -> failwith "reval: too few operands on stack"
```
> __Note:__  We could technically do without `RPop`, `RDup` and `RSwap` when we have `RLoad`, `RStore` and `RErase`, it would however increase the list of instructions needed to perform those actions. 