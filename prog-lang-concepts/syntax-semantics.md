[...back](README.md)
# Programming languages
## Syntax and Semantics
The distinction between _syntax_ and _semantics_ are not clear-cut.
### __Syntax__ deals with form
* __Concrete syntax__ (as text)
    > The program represented as text with whitespace, parenthesis, curly braces and so on. This essentially  means that the code is just a generic text that does nothing until it is __parsed__ into a _abstract syntax_.
* __Abstract syntax__ (Abstract Syntax Trees)
    > The program represented as a tree structure. All the whitespaces, parentheses and so on have been abstracted to simplify the processing/interpretation/compilation of the program in parts.
### __Semantics__ deals with meaning
* __Dynamic semantics__
    > The meaning/effect of a program at run-time
* __Static semantics__
    > The compile-time correctness of a program, if variables are declared, if the program is well-typed and so on...

## Parsing, Interpreting and Compiling
* __Parsing__
    > Parsing is turning _concrete syntax_ into a _abstract syntax_ for the machine to interpret. 
    * __Prettyprinting__
        > This is the reverse of parsing, it takes the code and converts it to _concrete syntax_
* __Interpreting__ 
    > it means to execute a program _directly_, not by first translating it to a more low-level language. Deriving meaning from the code. 
* __Compiling__
    > Copiles an expression to stack machine code (assembly) and evaluates that through interpretation. Compilors are also used to translate between different languages (assembly is essentially a different language).
    >
    > Often compilors are built on other compilors, so the original expression would go through a few compilations before reaching a conclusion (to have a executable program)
## High and Low level programming languages
In this context, the level of a programming language would describe how abstracted the language, because the language of the hardware is not very _human understandable_. The higher the level, the more the language engine will be doing for you, such as automatic memory managment.

A higher level language could be fairly low level compared to another, so it again depends on the context. `C` could be considered a relatively high-level, while being rather low level compared to `javascript` or `F#`.

>It seems to be best to introduce people to programming through higher level languages (such as `python` or `javascript`)
>
>When it comes to teaching the concepts, algorithms and hardware, then lower level languages seem to be preffered (such as `C` or `Java`). 
>>The reason for that is that it is easier to focus on how things work (such as memory, pointers, algorithms and types) when the language is more explicit and the language engine isn't managing as much for you. It also makes you appreciate how the higher level languages are actually handling so much for you to make programming simpler.

### The language of expressions
We will be making an example of compiling between levels of languages. It always depends on the situation or purpose of the code, how far down we want/need to compile things before it can be evaluated. 
* Evaluating source expressions directly
    > Dynamic and higher level languages can usually evaluate the code directly, without needing to compile to other languages before evaluation.
* Compiling source expressions down to target expressions
    > It gets confusing for us poor humans to keep track of variables in the form of pointers/targets. So we generally use named variables and just compile it to target expressions if we need to.
* Compiling target expressions to stack-machine code
    > It is simpler for the stack-machine to evaluate target expressions where the location of each value is defined, rather than having to work with names that have to be looked up for their values. 

## Syntactic sugar
The term __syntactic sugar__ comes up sometimes, and it refers to how the syntax of a programming language is designed to make it easier to read, improve the _human readability_ of the syntax, which does not really improve the practicality of the code for the computer, it is purely for us humans.

>This can include indentation, namming conventions, the choice of implementation and presentation, optional line endings. __The higher the programming language, the more sugar it tends to provide.__

It is sometimes used in the context of how people might precent their code differently, and if we would remove the _syntactic sugar_ from the program, their functionality and expressions would remain the same. 

You could say that __compilors__ tend to _desugar_ the syntax, because it is aimed at functionality rather than human readability.

>#### [Some sugar quotes](https://en.wikipedia.org/wiki/Syntactic_sugar)
>* _"Syntactic sugar causes cancer of the semi-colons"_
>* _"Syntactic sugar causes semantic cavities"_
