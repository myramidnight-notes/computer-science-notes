# Programming in C++
Course taught at Reykjavik university
1. Week 1
    * Difference between C++ and python
        * Compiled vs. interpreted (Þýtt vs. túlkað)
        * Strongly typed (Sterkt tagbundið)
    * Variables, assignments, control statements, loops
    * C++ syntax
    * Compiling a C++ program on the terminal
    * g++
    * Debug tools
    * bit-operations
1. Week 2
    * Arrays
    * Stack memory
    * Files
    * strings
    * programs with arguments
1. Week 3
    * Functions, return value, typed parameters
        * Call-by-value
        * Call-by-reference
    * Structs and classes
        * private vs. public
        * constructors
        * operator overloading
1. Week 4
    * Heap memory
    * Pointers and memory allocation (Bendar og minnisúthlutun)
        * new and delete
        * pointers and arrays
        * pointers to class instances
        * pointers as instance variables
            * class destructors
    * Dynamic arrays
1. Week 5
    * NULL-terminated strings and operations on char*
        * deleting char arrays and char* arrays (char**)
    * Linked lists
    * Trees
1. Week 6
    * Pointers and typecasting
    * static keyword
        * class variables vs. instance variables
        * class functions vs. instance functions
    * Singleton
    * Templates and STL introduction
1. Week 7
    * Templates
        * shared pointer implementation example
    * STL
        * vector, set, map (multimap, unordered_map)
        * queue, stakc, priority_queue
        * shared_ptr
1. Week 8
    * Inheritance
        * "protected" keyword
        * "virtual" keyword
            * pure virtual functions
        * abstract classes (interface)
    * Enumerations
1. Week 9
    * Using the debugger properly
    * Building libraries without main()
1. Week 10
    * Python modules in C++
    * Function pointers
Week 11
    * Threads
    * Network programming in C++
1. Week 12
    * Summary and assignment work