> * [Continuous Delivery](https://continuousdelivery.com/)
# Continuous Integration and Quality assurance
> Course: _Hagnýt Gæðastjórnun og prófanir_ (Efficient Quality assurance and testing?)

material: 
* [Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation](https://www.amazon.com/Continuous-Delivery-Deployment-Automation-Addison-Wesley/dp/0321601912)
* Course lectures and slides
* Good old google

My notes about continuous integration after this course

> We focus on build, deploy, test and release process

![](images/pipeline-branching.png)


## The plan
1. ### [The problem of delivering software](problem-with-delivering-software/README.md)
    > chapter 1
    * Antipatterns
    * Release life cycle
    * Principles of software delivery
1. ### [Configuration managment](configuration-managment/README.md)
    > Chapter 2
1. ### [Continuous Integration](continuous-integration/README.md)
1. ### [Build and deployment scripting](build-deploy-scripting/README.md), advanced version control
    > Chapter 6, (11), 14
1. ### The anatomy of build pipeline
    > Chapter 5
1. ### Implementing a test strategy
    > Chapter 4
1. ### [the commit stage](stage-commit/README.md)
    > Chapter 7
1. ### Automated acceptance testing
    > Chapter 8
1. ### Testing nonfunctional requirements
    > Chapter 9
1. ### Security testing (guest lecture on security)
1. ### [Deploying and releasing applications](stage-deploy/README.md), managing data
    > Chapter 10, 12
1. ### Mobile app deployment, accessability, usability
    > Chapter 11
1. ### Managing continuous delivery, change managment
    > Chapter 15