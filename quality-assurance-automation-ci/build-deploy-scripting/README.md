> Chapter 6
# Build and Deployment scripting
Pretty much every modern platform has a way to run the build from the command line, since the command line is the most basic way to communicate with the computer.

>It is actually neat to have test reports with timestamps, so the tests would only run if the timestamps on the files are newer than the latest test. 


## Build tools
### Shell scripts
You can actually store bash commands in a `.sh` script, it is actually impressive what you can do with it, such as adding conditional statements and variables. More than I thought I could do before taking the course.

Essentially storing complex commands in a file.

### Make (Makefile)
`Make` is a Unix utility tool that is designed to start execution of a makefile. The _makefile_ is a special file that contains shell commands, a recipe of commands you could run with `make`.

They are most commonly used to make it simple to compile applications from source code. 

The _makefile_ will contain _recipes_ for commands you can run

>It allows you to skip having to remember all the configurations you had to include with the compile command, and just specify it once in the _makefile_ and use a command to run them.

### Just ([Justfile](https://github.com/casey/just)): Just a command runner
`Just` is a handy way to save and run project-specific commands. It's syntax is inspired by `make`, so it will share obvious similarities, allowing you to specify _recipes_ of commands or to run code snippets. 

You can actually make `just` run code in various programming languages, but you need to use a _bash_ command line in order to use `just`.

Could use it to run multiple commands or scripts, so you could have a task set up as a single `just` recipe and just run it.

We should strive to make our work a bit easier after all.

>It wasn't mentioned in the book it came out 2010/2011, and `just` appeared a few years later. They would probably have been all over it

## Principles and practices of build and deployment scripting

### Create a script for each stage in your deployment pipeline
Once your script gets sufficently long, you can divide it up into separate scripts for each stage in your pipeline
* You will want a commit script 
    >It will do everything required to compile/build the application, package it, run the commit test suite and static analysis of the code (linting and type checking)
* You need a functional test suite
    >The unit tests
* You need functional acceptance test script
    >It would call your deployment tool to deploy the application to the appropriate environment, prepares any data, and finally runs the acceptance tests
* You should have a script that runs any nonfunctional tests 
    >such as stress tests, capacity tests, security tests

### Use an appropriate technology to deploy your application
Deployment of your application will be done by both the developers (on local machines at least) and by testers and operations staff (who put it into production)

That is why it is important to pickt he right tools for the job that will involve everyone. You don't want to waste time later in the project trying to get people to use a different tool (and having to redo all the configurations to make the Continuous integration work with the new tools).

### Use the same scripts to deploy to every environment
It is essential to use the same process to deploy to every environment in which your application will run (for testing and production at least).