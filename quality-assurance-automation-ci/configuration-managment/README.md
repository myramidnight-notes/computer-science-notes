> Chapter 2
# Configuration management
Configuration managment is often used as a synonym for version control
> Configuration management refers to the process by which all artifacts relevant to your project, and the relationships between them, are stored, retrieved, uniquely identified, and modified.

### What does a good configuration managment do for you?
* Allows you to reproduce any of your environments, including the version of the operating system (for containers), the network configuration, the software stack, the applications deployed into it, and their configuration
* Allows you to easily make an incremental change to any of these individual items and deploy the change to any, and all, of your environments
* Allows you to easily see each change that occured to a particular environment and trace it back to see exactly what the change was, who made it, and when they made it
* Allows you to satisfy all of the compliance regulations that you are subjected to
* Makes it easy for every member of the team to get the information they need, and to make the changes they need to make it. 

If your configuration managment strategy gets in the way of efficient delivery, leading to increased cycle time and reduced feedback, then it can put all kinds of barriers in the way of collaboration between teams.

## Index
* ### [Using Version Control](version-control.md)
    * Keep everything in version control
    * Check in regularily
    * Use meaningful commit messages
* ### [Managing Dependencies](managing-dependencies.md)
    * Managing external libraries
    * Managing components
* ### [Managing software configuration](software-configuration.md)
    * Configuration and Flexability
    * Types of configuration
    * Managing application configuration
    * Testing system configuration
    * Managing configuration across applications
    * Principles of managing application configuration
* ### [Managing your Environments](managing-environments.md)   
    * Tools to manage environments
    * Managing the change process

## Exceptions about adding everything to the version control
Even if the rule of thumb is to _add absolutely everything to version control_, there are a few exceptions that are a good practice, which can usually be configured to ignore certain files.

The more specific version of that rule is: __Keep everything you need to build, deploy, test and release your application in version control__

* Do not add passwords into version control 
    > Meaning, don't keep them in any file that will be stored in the version control. You never want to expose your passwords, regardless if the repository is private or public, everybody with access to the repository will be able to see it (removing them later does not help, because they will still exist in the history of the version control)
    >
    >The trick is to use __environment variables__ instead, so the code can just reference those variables or have a script that extracts the variables from the environment and writes them into the code when it builds the application
    >
    >You could also use encryption (could use encryption even if you use environment variables)
* Leave the compiled versions of applications out of your version control
    > They tend to be big, and usually not all systems can run them (different systems compile differently). Keeping them goes against the rule that you should be able to deploy the same binaries to every environment
    >
    > Just recreate them with the compilers from version control.
* Leave out external libraries out of the version control
    > They tend to bloat your project repository, and it is generally quicker to just download them through the package managers and have them unpack it, than having to get them through the repository.
    >
    >Though it depends on your project and programming language/framework if you should keep the external libraries in version control or not. 
    >>For the example working with nodeJS or python, we leave the external libraries out, and only __keep a list of required packages and their exact versions__ so that the package manager can just download them.
* Local virtual environment
    >These are usually just for yourself to run the project locally, and has nothing to do with the deployment environment, so files for a local virtual environment are usually excluded, because it will not be used by anybody else but yourself, and should not be included in the version control
* IDE configurations
    >These are usually configurations specific to your editor of choice for the current project and have nothing to do with the project itself as a whole, and since these will just be your personal configurations, they have no place in the version control.