# Managing Dependencies
The most common external dependencies within your application are the third-party libraries it uses and the relationships between components or modules under development by other teams within your organization.
* __Third-party libraries__ are typically never changed by your application's development team, and are updated infrequently.
* __Components and modules__ that are typically under active development by other teams change quite frequently.

## Managing external libraries

### To include or not include external libraries in version control
* We usually do not add external libraries to our version control, but instead include a list of required third-party packages that need to be installed (including their exact versions, to be able to reproduce the application perfectly). 
    * This might not be desirable, since it forces any new team members to download a lot of things before they begin working on the project. 
    * These libraries often include a lot of small files, which will can take a while to check-in (a lot of small files can be slower to process than one large file)
    * It might be quicker to download and unpack external libraries, than having to push and pull them through the repository, because of the number of files involved.
* But this really depends on the type of project, what language it uses, because there are always trade-offs.

## Managing components
It is a good practice to split up all but the smallest applications into components. 
* This limits the scope of changes to your application, reducing regression bugs and merge conflicts.
* It also encourages reuse and enables much more efficient development process on large projects

Projects typically start off as a monolithic build, and for small applications it might stay as a monolith. 

For larger projects, you would consider splitting up the components' builds into separate pipelines.
