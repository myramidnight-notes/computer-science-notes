# Managing your Environments
The configuration of the environment that your application runs in is as important as the configuration of the application.

The key to managing environments is to make their creation a fully automated process. It should always be cheaper to create a new environment than repair an old one
* It removes the problem of having random pieces of infrastructure around whose configuration is only understood by somebody who might not be part of the project anymore and unreachable.
* Fixing an environment can take many hours. It is always better to be able to rebuild it in a predictable amount of time to get back to a known good state
* It is essential to be able to create copies of production environments for testing purposes (such as accaptence and capacity tests). In terms of software configuration, testing environments should be exact replicas of the production ones, so configuration problems can be found early.

#### Environment information you should be concerned about
>* The various operating systems in your environment, including their versions, patch levels and configuration settings
>* Additional software packages that are needed on each environment to support the application, including their versions and configuration
>* The network topology required for your application to work
>* External services that your application depends upon, including their versions and configuration
>* Any data or other state that is present in the environment (such as production databases)

#### Effective principles for configuration managment strategy
>1. Keep binary files independent from configuration information
>1. Keep all configuration information in one place
>
>These fundementals should be applied to every part of your system to pave the way to the point where creating new environments, upgrading parts of your system, and rolling out new configurations without making your system unavailable becomes a simple, automated process

### Evaluating third-party products and services
It is important to apply these principles to the third-party software stack that we will depend on. __Good software has installers that can be run from the command line without any user intervention__. If they do not meet these criteria, then you should find alternatives.

So you should ask the following questions when evaluating third-party products and services before including them in the project
1. Can we deploy it?
1. Can we version its configuration effectively?
1. How will it fit into our automated deployment strategy?

### Treat your environment as you would your code
> Treat your environment the same way you treat your code: changing it incrementally and checking the changes into version control. Every change should be tested to ensure that it doesn't break any of the applications that run in the new version of the environment.
>
>This usually does not refer to virtual venv that you set up on your local machine when running a project locally, but the deployment environment.

## Managing the Change process
It should not be possible for _anybody_ to make a change to the environment without going through your organization's change managment process.

* Even a tiny change could break the environment so that the application won't run
* Any change must be tested before it goes into production