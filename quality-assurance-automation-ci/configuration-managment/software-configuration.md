
# Managing software configuration
Configuration is one of the three key parts that comprise an application, along with its binaries and its data.

* It is important to ensure that configuration is managed consistently across components, applications and technologies
* You should treat your configuration the same way you treat your code:
    * Make it subject to proper managment and testing

## Configuration and Flexability
Everyone wants flexible software, but it usually comes at a cost.

The desire to achieve flexibility may lead to the common antipattern of "__ultimate configurability__" which is too often stated as a requirement for software projects. It is unhelpful, and at worst, this requirement can kill a project.

The more configurability you intend to offer users, the fewer constraints you can afford to place on the configuraration of the ssytem, so the more sophisticated the programming environment needs to become.

### Pitfalls on the road to highly configurable software
* Analysis paralysis
    > Where the problem seems so big and so intractable that the team spends all the time thinking about how to solve it and none of their time to actually solving anything.
* Becoming so complex to configure that it looses flexibility
    > The system becomes so complex to configure that many of the benefits of its flexibility are lost, to the extent where the effort involved in its configuration is comparable to the cost of custom development (could have been a project of its own)

Configurable software is not always the cheaper solution it appears to be. It's almost always better to focus on delivering the high-value functionality with little configurability and then add configuration options later when necessary.

Essentially keep the __YAGNI__ principle in mind, "You ain't gonna need it", don't implement configurations and features that you don't need, until you need them.

## Types of configuration
* Configurations used at __Build time__
    > not recommended
* Configuration used at __Packaging time__
    > not reccomended
* Configuration used at __Deployment time__
    > this is where you might add in usernames and passwords that were left out of the version control for security reasons
* Configuration used at __Startup/Run time__

Generally it is considered __bad practice to inject configuration information at build or packaging time__. This follows from the principle that you should be able to deploy the same binaries to every environment so you can ensure that the thing you release is the same thing you tested.

> You should be able to deploy the same binaries to every environment

### Try supplying all configuration through the same mechanism
> You should try to supply all configuration information for all the applications and environments in your organizeation through the same mechanism.
>
>This isn't always possible, but when it is, it means there is a single source of configuration to change, manage, version-control and override (if necessary). 
>
>In organizations where this practice is not followed, people regularily spend hours tracking down the source of some particular setting in one of their environments.

## Managing your application Configuration
* How do you represent your configuration information?
* How do your deployment scripts access it?
* How does it vary between environments, applications and versions of applications?

You will need to keep track of exact versions of everything (external libraries specially). Many package manager know how to handle a file listing required packages and download the exact versions specified, which makes it easy to list the requirements and avoid having to save those external libraries in the version control.

### Documentation of application configuration
There are platforms/frameworks that can help with managing documentation for the project, wikis are a popular choice (there are many types available).

#### Auto generated documentation if possible
>Depending on the project, there might even be a way to auto-generate documentation about usage of your application (creating APIs with GraphQL can generate such documentation for example)

### Principles of managing application configuration
* Manage the configuration just as carefully as your code
* Configuration options should be in the same repository as your code
* Not all all configuration options are created equal
    * Consider where they make sense
    * Different mechanisms usually make sense
    * Choose sensible defaults carefully
* Manage available options with code
    * Keep the values elsewhere
        > Configuration values have a lifecycle completely different from that of code
* Use clear naming conventions
    > Clever is the enemy of clear, so just keep things clear and obvious rather than trying to think of clever names.
* Keep it modular

#### Other useful principles that can apply
>* DRY: Don't repeat yourself 
>   > though it might conflict with modularity
>* YAGNI: You ain't gonna need it. 
>   > Be minimalist. Don't implement configurations and features that you don't currently need. 
>* KISS: Keep it simple, stupid. 
>   > Avoid over-engineering. Simple things are easier to understand and it increases readability of the code. As mentioned before "_clever is the enemy of clear_", and clever is rarely simple, so trying to do something in a clever way usually results in complicating things needlessly.