
# Using Version Control
The aim of a version control system is twofold
1. __Retain__ and __provide access to every version of every file__ that has ever been stored in it
1. Allow teams that may be distributed across space and time to collaborate

## Types of version control systems
There most popular version control tool today is unquestionably __Git__, but it is not the only system, and certainly not the first.
### Older version control systems
* SCCS (Source Code Control System) 
    > It was the first popular version control system, which dates back to the 1970s
* RCS (Revision Control System) 
    > It superseded SCCS, firt released in 1982. 
* CVS (Concurrent Versioning System).
    > It superseded RCS, and is apparently still a popular versioning control system, being in second place, after _Git_

### Newer version control systems
* Subversion (SVN)
* Mercurial
* Git
* [and more...](https://www.softwaretestinghelp.com/version-control-software/)

### Source, Revision and Version control
All three are essentially the same thing these days, but as we've seen above, they were essentially built ontop of each other.
1. Source control deals with source files, changes in the source code (individual files).
1. Revison control are essentially snapshots of all the files in the repository at a given time. Giving us control over the revisions of the repository.
1. Version control gives us branches! To be able to keep track of multiple versions of the same repository, and merge those branches together. 

## Terms used in version control
These are essential terms used when handling version control
* __Repository__ (or _repo_ for short)
    >Each project will have it's own repository, where all stored versions are kept
* Adding to __stage__
    > Adding all changes that you might want to commit need to be added to a staging area, giving us control over what goes into the version control.
* __Commit__
    > This will take all the staged changes and store them as a indidual version of the project. It is good practice to commit after each completed task, when things work. 
    >
    >It is essentially a __checkpoint__, a place that you can start from again if needed.
* __Push__ to remote repository
    > This will gather all the commits you have made since the last time you pushed, and pushes them to a remote repository where it will be accessable to the rest of the team.
    >
    >This is essentially the same as _checking in_, as talked about in the book, allowing the rest of the team to see your progress.
* __Pull__ from remote repository
    > This will _pull_ all the commits made to the remote repository, so that you can be up-to-date. 
* __Branch__ 
    > Each _branch_ of a repository will house alternative versions of the project. The central branch is usually called _master_ or _main_, which is expected to contain the latest stable version of the project.
    >
    >Developers will create their own branches from other branches (usually the central branch) to work on, to keep their work separate until it is ready to be merged with the rest of the tree again (to combine the work).
* __Merging__ branches
    > When work on a branch is finished, and ready to be part of the central version, then the branch will be _merged_ to the central branch, combining them.

## Good practices for version control
### Keep absolutely everything in version control
> If you are working on any project involving digital files, then using version control can make a huge difference. The projects would not have to be exclusive to software, but even composing a book could benefit from version control.

It is important to store all information that would be required to recreate the testing and production environment that your application runs on.

Version control allows you to have a stored version of the project, so any changes and saves you make can be reverted, regardless of your editors ability to undo changes. It gives you more freedom to do changes without worry of how long your _undo history_ might be, or loosing track of what  changes you might need to undo in order to go back to a previous version of your project.

* It gives security of being able to roll back any changes that might have broken the application, 
* Ability of comparing versions if you're trying to find something that might have been removed or added in particular.
* Gives more freedom to do experiments that could improve the application or simply be scrapped.

#### Exception to keeping everything in version control
> It is not reccomended to keep the binary output of a application compilation in version control for a few reasons:
>1. They are big 
>1. With automated build system, these compilations can just be re-created with the compiler from version control.
>1. And finally, it breaks the idea of being able to identify a single version of your repository in each application version. Applications could compile differently based on their environment/system they are going to be running in.

### Check in regularily to the trunk
> To keep your branch up-to-date on the tree? I think it means that you should always have your things on the remote repository by the end of the day at least, and if things work (not breaking the application), they should be merged to the master, so other team members can make use of it.

Checking in (basically pushing your code to the shared repository) is important for teamwork. 
1. Once you check your changes into version control, they become visible to everyone on the team that has access to the repository.
1. Checking in allows other team members to see your work

Checking in is a form of __publication__, so it is important to make sure that your work is ready for the level of publicity that a check-in implies
* This is specially important if the developer is in the middle of working on a complex part of the system, they might not want to push their code until it's finished with the changes (to ensure that it works).

#### Best practice to avoid breaking the application on check-in
> 1. __Run your commit test suite before the check-in__. They are quick (less than 10 minutes) but relatively comprehensive set of tests which validate that you haven't introduced any obvious regressions.
> 1. __Introduce changes incrementally__. Aim to commit changes to the version control system at the conclusion of each separate incremental change or refactoring. In other words, don't try to do many different changes at once, do one task at a time.
>
>If this technique is used correctly, you should be checking in at least once a day, or more. 

### Use meaningful commit messages
Every time you commit your work, it expects you to add a little message to it, which should be a short description of what was changed with the given commit. 

This message should allow anyone looking over the history of the project to easily see what has happened.

Typical scenario where you wish you had good commit messages: 
1. You find a bug that is down to a rather obscure line of code
1. You use your version control system to find out who put in that line of code and when
1. That persion is off on holiday or has gone home for the night, and left a commit message that said "`fixed obscure bug`" (which is not very descriptive of what was changed)
1. You change the obscure line of code to fix the bug
1. Something else breaks
1. You spend hours trying to get the application working again

In these situations, a commit message explaining what the person was doing when they commited the change can save you hours of debugging. The more often this happens, the more you wish you had used good commit messages.

>One style is a multiparagraph commit message, in which the first paragraph is a quick summary of what gets shown on line-per-commit displays (that just show the first line of the message for each commit), like it was a __headline__ of an article. 

Usually these messages are just a single descriptive sentance describing what was changed, and if you can't describe it in a single sentance, then it is verly likely that you are doing too many changes at once (practice commiting changes in increments).