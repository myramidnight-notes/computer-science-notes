# Continuous Integration (CI)
Without continuous integration, your software is broken until somebody proves it works, usually during a testing or integration stage.

By making the integration automatic and continuous (every time you push to the repository), then the software is proven to work with every new change, and you know the moment it breaks.

Teams that use _continuous integration_ are effectivly are able to deliver software much faster with fewer bugs.

* Continuous Integration will have continuous testing
    > Every time something changes, it is tested, and we will know if it passed or failed.
* Bugs caught early are cheaper to fix
    > before they get buried under more changes which might make fixing it more difficult to fix.

### What do you need for Continuous Integration?
1. Version control
    > Everything in your project must be checked into a single version control repository. The code, tests, database scripts, build and deployment scripts, and anything else needed to create, install, runn and test your application.
1. Automated build
    >You must be able to start your build from the command line, so you can run the build process in an automated way from your continuous integration environment
1. Agreement of the team
    > Continuous integration is a practice, not a tool. Everyone needs to be using it for it to work, so the whole development team needs to have the degree of commitment and discipline to use it.

## Basic Continuous Integration system
You do not need a specific continuous integration software in order to use CI, since it is a practice, not a tool. Though there exist tools that make CI easier.


1. Check to see if the build is already running. If so, wait for it to finish.
    > If it fails, you will need to work with the rest of the team to make it green before you check in.
1. Once the build has finished and the tests have passed, then update the code in your development environment to be up-to-date with the control repository (the remote repository)
1. Run the build scripts and test on your development machine to make sure everything still works correctly on your computer (or use your CI tool's personal build feature)
    > If something breaks after updating, then you just have to fix that so it is compatible with the control version (master branch) before even considering merging your work with the rest.
1. Once your local build passes, check your code into version control
1. Wait for your CI tool to run the build with your changes
1. If it fails, stop what you are doing and fix the problem immediately on your development machine and repeat the testing
1. If the build passes, rejoice and move on to your next task.

### Check in regularily
The most important practice for continuous integration to work is frequent check-ins to the main repository. 
* You should be checking in your code at least daily, or couple times a day
* Regular check-ins makes your changes smaller, thus less likely to break the build
* You will have a recent known good version of the software to revert to when you make a mistake or go down the wrong path.
* It helps to ensure that changes altering a lot of files are less likely to cause conflicts with other people's work (merge conflicts)
* It allows developers to be more explorative, trying out new ideas and discarding them by reverting back to the last commited version
* It forces you to take regular breaks to stretch your muscles to help avoid carpal tunnel syndrom or RSI
* It also prevents you from loosing too much work if something catastrophic happens (such as deleting something by mistake).

### Working with multiple branches
Many projects make use of branches in version control to manage large teams.

The book says it's impossible to truly do continuous integration while using branches, but perhaps this is a outdated perspective, but they do have a point.

>Working on a branch means your work is not being integrated with the work of other developers (changes have to be merged to the central branch in order to integrate it with the whole project)

The central branch (main/master) usually holds the latest stable version of

But working on branches is actually a good practice to keep your work separate while you are developing it, until you can prove that it works without breaking the application. We don't want to complicate things for other team members by breaking the central branch while doing experiments after all.

In order to make branches work with continuous integration is to make them short-lived, merging them to the central branch whenever possible (so long as it doesn't break the application). 

#### Branching best practices
>* Try to keep things simple
>* Have well defined code branching policies
>* Give codelines an owner
>* uses branches for releases or milestones
>* Protect your mainline (the master/main branch)
>* Merge down and copy up