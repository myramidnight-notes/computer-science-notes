> Chapter 1
# The problem of delivering software
The central pattern of the book would be _deployment pipeline_. 
> The __Deployment pipeline__ is essentially an automated implementation of your application's build, deploy, test and release process. It has it's foundations in the process of __continuous integration__

![](images/deployment-pipeline.png)

The aim of the deployment pipeline is threefold
1. Make every part of the process of building, deploying, testing and releasing software visible to everybody involved, aiding collaboration.
1. Improve feedback so that problems are identified and resolved
1. Enable teams to deploy and release any version of their software to any enviornment at will through a fully automated process

## Risks and antipatterns
There is quite a lot that could go wrong in this process. If any step is not perfectly executed, the application won't run properly. 

 We will be looking into how to avoid these risks, how to reduce the stress on release days and how to ensure that each release is predictably reliable.

### Antipatterns:
1. [__Manual Deployment__](antipatterns/manual-deployment.md)
    * There needs to be extensive, detailed deployment documentation
    * It requires deployment experts to do repeatative and boring work
    * Releases take a while to perform
    * Releases are unpredictable in their outcome (prone to human error)
1. [__Deploying to a production-like env only after development is complete__](antipatterns/production-env-last.md)
    * All testing has only been for development env so far
    * The first time operations person sees the software, is the day it is released into production (last)
    * Dev team passes their files and documentation to operations to perform, which is untested for their env at the time of release.
    * Little or no collaboration between dev and ops teams

    > This will leave testing for production environment for the very end as well, and leaving tests until the end normally means that there is no time to actually fix the bugs, or that only small proportion of them can be fixed. 
1. [__Manual configuration managment of production env__](antipatterns/manual-configuration.md)
    * Servers have different versions of operation systems (unintentionally)
    * configuration of system is carried out directly on production systems

### improvements
Software release can be (and should be) a low-risk, frequent, cheap, rapid and predictable process. These patterns have been developed over many years and have shown that they make a huge difference in many projects.

If something can be automated, it should totally be automated. It allows us to work faster, see results of our work sooner (see if it works or not), get feedback quicker so the issues can be solved, all in a efficient, fast and reliable manner.

The first time you do automation, it will be painfull, but it will become easier, and the benefits to the project and to yourself are almost incalculably large
> In software, when something is painful, the way to reduce the pain is to do it more frequently, not less. So we should integrate frequently. When this practice is followed, then the software is always in a working state.

__Every change to the software should trigger a feedback__, by running it through the deployment pipeline that would give us that feedback quickly, showing us what failed or worked, because it gets more expensive to fix something later. Instant feedback allows us to solve such issues as soon as we notice them.

## Benefits
* Repeatable, reliable, predictable release process
* Reduces cycle time
* Cost saving
* Enpowering teams
    * Testers can select older versions
    * Support staff can deploy a older version to reproduce issues
    * Operations staff can select a known good build
    * Releases can be performed at a push of a button
        > No longer requiring specialized staff to handle the deployment
* Reducing errors
    >Specially those caused by human error or poor configuration managment
* Lowers stress
    * Frequent (small delta), fast, rellback
    >The ability to rollback to a working version if something goes wrong can reduce a lot of worry and stress, specially around final release date, when things have already been tested for production 
* Deployment flexability
   * Should be a simple task to start your application in a new env. 
   * Systems developed in this manner generally can run on a single machine
* Practice makes perfect
    > Use same deployment methods for all environments


## The software release life cycle
![](images/release-candidates.png)
1. Pre-alpha 
    > Development releases, nightly builds. It refers to all activities performed during the software project before formal testing.
1. Alpha versions
    > pre-release, early version that is part of a dedicated testing process
1. Beta version
    > pre-release of software that is given out to a large group of users to try under real conditions. They are fairly close to look and feel like the final product.
1. Release candiates 
    > A release candidate (also known as "going silver") is a _beta version with the potential to be a stable product_. It is like a _sneak preview_ of the final release with the added advantage that serious bugs may yet surface and be fixed before the general public uses it.
1. Gold / Final release
    > The final stage of software development. It is the version released to the end-users after further enhancements and bug fixes.

## Principles of Software Delivery
* Create a repeatable, reliable process for releasing software
* Automate almost everything
* Keep everything in version control
* If it hurts, do it more frequently, and bring the pain forward
* Build Quality In
* Continuous Improvement
> [Read more](software-delivery-principles.md)