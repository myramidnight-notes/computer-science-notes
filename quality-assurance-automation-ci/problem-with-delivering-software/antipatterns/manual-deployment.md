
# Antipattern: Deploying software manually
Manual deployment is prone to __human error__ (besides having to have people specialized in the process of deployment, which is repetative work)

__Signs of this antipattern__:
* There needs to be a __extensive and detailed documentation__ describing the steps to be taken for deployment and the ways in which the steps may go wrong
    > Maintaining the documentation is a complex and time-consuming task involving collaboration between several people, so the documentation could be incomplete or out-of-date at any given time.
    >
    >Documentation also has to make assumptions about the level of knowledge the reader has and in reality is usually written as a reminder for the person performing the deployment, which might not be as helpful to others not with their level of knowledge.
* Reliance on manual testing to confirm that the application is running correctly
    > The only way to test manual deployment process is to do it manually, which is time-consuming and expensive (you are paying people to do this after all)
* Frequent corrections to the release process during the course of a release
* Environments in a cluser that differ in their configuration
* Releases that are unpredictable in their outcome, that often have to be rolled back or run into unforseen problems
    >There is no guarantee in manual deployment that all the steps of the documentation has been followed, and that is assuming the documentation would be up to date.
* Sitting bleary-eyed in front of a monitor at 2 A.M. the day after the release day, trying to figure out how to make it work
    > Manual deployment is boring and repetative work that still requires a significant degree of expertise. It will increase the risks of human errors for various reasons (sleep deprivation, forgetting steps, typos, misunderstanding the documentation..)

Manual deployment depends on the _deployment expert_. If they are away from work or quit, then there will be trouble. 

### Solution and goal: Make it automated
We should over time _work towards being fully automated_, to allow computers to do the repetative work they are well equipped to do, so your team can work on higher-value activities, rather than having them do boring and repetative work of manual deployment.

* A set of __automated deployment scripts__ serves as documentation, and it will always be up-to-date and complete or the deployment will not work.
* Automated deployments __encourage collaboration__, because everything is explicit in a script. 
* Automated deployment process is cheap and easy to test
* The __outcome of a automated deployment will be reliable__, in the sense that you will always get the same results when running it, free of any human errors
