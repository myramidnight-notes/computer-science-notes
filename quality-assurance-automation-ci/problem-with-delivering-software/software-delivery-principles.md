

# Principles of software delivery

## The principles
1. ### Create a repeatable, reliable process for releasing software
    * It should be as simple as pressing a button
    * The repeatability and reliability derive from two principles:
        * Automate almost everything
        * Keep everything you need to build, deploy, test and release your application in version control (such as _git_)
    * Deploying software ultimately involves three things
        * Provisioning and managing the environment in which your application will run (hardware, software, infrastructure, external services...)
        * Installing the correct version of your application into it
        * Configuring your application, including any data or state it requires

    The deployment of your application can be implemented using a fully automated process from version control. Hardware can of course not be included in version control, but we can avoid that issue with the use of virtualization such as docker to allow us to run the application in a specific enviornment.
 1. ### Automate almost everything
    The list of things that cannot be automated is much smaller than many people think. Your process should be automated up to the point where it needs specific human direction or decision making.
    * Acceptance tests can be automated. 
    * Database upgrades and downgrades can be automated
    * Network and firewall configuration can be automated
    * Automate as much as you possibly can
    * You can, and should, automate gradually over time

1. ### Keep everything in version control
    Give `git` some love
    * Keep everything you need to build, deploy, test and release your application in some form of versioned storage. `Git` is a very popular choice
    * It should be possible for a new team member to sit down at a new workstation, check out the project's revision control repository, and simply simply start working.
    * It should be possible to see which build of your varions applications is deployed into each of your environments, and which version in version control these builds come from
1. ### If it hurts, do it more frequently, and bring the pain forward
    * Integration is often a very painful process. 
    * If you can't release it to real users upon every change, release it to a production-like environment upon every check-in. 
    * If creating application documentation is painful, do it as you develop new features instead of leaving it to the end.
        >Make documentation for a feature part of the definition of done, and automate the process as far as possible.
    * Aim for intermediate goals, such as an internal release every few weeks or, if you're already doing that, every week. Gradually work to approach the ideal, even small steps will deliver great benefits.
1. ### Build Quality In
    * This principle comes from the _lean movement_
    * The earlier you catch defects, the cheaper they are to fix
    * Defects are fixed most cheaply if they are never checked in to the version control in the first place
    * Testing is not a phase, and certainly not one to begin after the development phase.
    * If testing is left to the end, it will be too late to fix all of the bugs
    * Everybody on the delivery team is responsible for the quality of the application all the time
1. ### Done means Released
    * For _agile_ delivery teams, "done" means released into production. 
        > This is the ideal situation for a software development project
    * There is no "80% done", either they are done or not.
    * Getting things done is a team effort
        > That is why it's so important for everybody to work together from the beginning

    It might not mean released to the public, but still released to production so we have a functional product for the given stage of the project, such as weekly releases.

1. ### Everybody is responsible for the delivery process
    When something goes wrong, people spend as much time blaming one another as they do fixing the defects that inevitably arise in siloed development (where the various teams of a project do not work directly together)
    * Ultimately the team succeeds or fails as a team, not as individuals
    * Break down the barriers between the silos that isolate people in different roles
    * Get everybody involved in the delivery process together from the start of a new project and use every oppertunity to communicate or regular basis
    * Initiate a system where everybody can see, at a glance, which tests have passed, and the state of the environments they can be deployed to. 

    This is one of the central principles for the _DevOps_ movement. It is a movement that is focused on the goal of encouraging greater collaboration between everyone involved in software delivery in order to release valuable software faster and more reliably

1. ### Continuous Improvement
    * All applications evolve, and more releases follow
    * Hold retrospective meetings on the delivery process
        > This means the team should reflect on what has gone well and what has gone badly, how can we improve the process
    * Essential that everybody is involved in the process
        > Allowing feedback to happen only within silos and not across them is a recipe for disaster and finger-pointing when things go wrong.