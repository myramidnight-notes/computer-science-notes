> Chapter 7
# The Commit stage
![](../images/simple-pipeline.png)

It is the stage we enter upon commiting our code to version control, and the entrence to the Continuous Integration pipeline.


### Steps
1. Compile/Build/Assemble
1. Run _commit tests_
    >Unit tests, selected component/integration/acceptance(end-to-end) tests
1. Create deployable binaries
    >The things we will eventually deploy, such as docker images
1. Perform code analysis
    > Analysis of the source code that is performed without actually executing programs. It involves the detection of vulnerabilities and functional errors in the deployed software (or soon-to-be deployed)
1. Assemble other artifects
    >Test data, database migrations...

## Principles and Practices
* ### Provide fast and useful feedback
    * Notify developers when commit stage fails
    * cost of fixing increases exponentially with time
        > Easier to fix something that is still fresh in our minds, not having to spend time recalling how we put something together to solve the issue, besides for the fact that things get more complicated over time, and that makes the bugs more complicated to fix.
    
* ### Things that fail in commit stage
    * Compilation/Building of the application
    * Testing (unit/module testing and such)
    * Environmental problems
    * Anythingelse that the _whole_ team agrees upon
        > Things that are related to how we have decided to work as a team or the definition of done/acceptable
        * Code style (linting)
        * Code coverage
        * Number of warnings
        * Cyclomatic complexity
        * Duplication
        * ...
* ### Tend the commit stage carefully
    * Maintain to same level as other code
    * Consider whether your design/project layout decisions will complicate the commit stage
* ### Give developers ownership
    * The developers must own the scripts
    * Specialists should act as teachers/trainers/assistants
* ### Use a build master for very large teams 
    >Or if the team is new to CI
    >
    > A team leader, someone with experience to guide the team

## Artifact Repository
> #### Artifacts
>An artifact is a __by-product of software development__. Anything that is created so a piece of software can be developed. This can include things like data models, diagrams, setup scripts, reports, test data...
>
>The name comes from how during archaeological dig sites, any man made object dug up is an artifact, and they can help us determine what civilization may have been like. 
>
>Artifacts help developers to see through the process behind what goes into a piece of software.

![Artifact repository](../images/artifact-repository.png)

* It is not your version control system
* Only needs to keep some versions
* Artifacts need to be tracable back to version control
* Most CI servers provide one

## The Commit testing suite
* Vast majority of tests should be unit tests
    * They are fast to execute
    * Do not touch filesystems, databases, libraries, frameworks or external systems
* Avoid the user interface
    > It is brittle and slow, constantly changing
* Use dependency injection
    > Necessary to isolate test scope
* Minimize state in tests
    * Tests should be idempotent (unchanging, always produce the same results regardless of the order or frequency of execution)
* Avoid the database
   * Interactions with databases are slow
   * They are stateful, hard to make tests idempotent
* Faking time
    >This is a __very important technique__ for testing, because time is constantly changing, which goes against idempotency of testing (to keep them unchanging), because then each test would be different because different times.
    * Provide your own interface for providing system time during testing
* Apply brute force
    * 10 minute builds is the max (<5 minutes is ideal)
    * Use parallization and split the commit stage (if possible)
    * CI servers provide build grid/cluster functionality
    *  Remember that computing power is cheap and people are expensive