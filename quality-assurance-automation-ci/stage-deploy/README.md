> Chapter 10
# The Deployment and Promotion stage

## First deployment (Showcase)
* This will become your showcase of the application that you can show to the customer
* You choose one or two stories or requirements that are high priority yet simple to deliver
    > User stories is essentially a use-case of something that the user needs to be able to do in the application, which can represent requirements. You can create stories from requirements as well (it does help to figure out how a user might acomplish the task that the requirement demands).
    >
    >The deployed application needs to be able to do the things you picked
* You can then take this showcase deployment and have real pepole try it out. 
    > We have entered the User Accaptence Testing! 


## Deployment pipeline
* Achieve confortmance by making the delivery process transparent
* Self servicing dpeloyments
    > Giving people permission to deploy specific applications to specific environments 
    >* Developers to deploy to development environments
    >* Testers to deploy to test environments
    >* Support team to deploy to environments for reproducing bugs
* Provides a system of records for auditing purpose
* Tools to lock down who can do what, control authority
* Comprehensive test automation provides a high level of confidence in the quality of the application

## The Maturity Model
For configuration and release managment

### Desired outcomes by using it
* Reduce cycle time, deliver value faster and increase profitability
* Reduce defects, improve efficency, less time on support
* Increase predictability of your software delivery lifecycle to make planning more effective
* The ability to adopt and maintain an attitude of compliance
* The ability to determine and manage the risks
* Reduced cost due to better risk managment and fewer issues delivering software

>#### Project lifecycle
>"You've got to be very careful if you don't know where you are going, because you might not get there" - Yogi Berra

## Phases of software development
* Identification
    > Opportunity to solve problems
* Inception
    > Plan
* Initiation
    > Team
* Develop and release
    > Product
* Operation
    > Product