# Testing
Not everyone agrees on the terms used for the types of tests, so it's best to be aware of the purpose of the tests rather than getting stuck on the terms. Many of these are very similar or essentially of the same.
>[What are the different types of testing?](https://www.perfecto.io/resources/types-of-testing)

Testing is a way to ensure that the application functions as it should, and to see if it meets requirements that are placed on the application.

Even though we cannot automate all types of testing, we can automate most of them, specially repetative that do not require any human decision making or opinions (if it can be set up as a if-statement with clear conditions, then I am sure a computer can handle it)

### Types of tests
* Accessability testing
    >Accessability testing is the practice of ensuring your application is working and usable for users without and with disabilities such as vision impairment, hearing disabilities, and other physical or cognative conditions.
    >
    >Accessability should not be considered something that only needs to be focused on if we expect people with disabilities to use the product (we don't want to be exclusive), but those without disabilities benefit as well, things tend to become clearer or easier to use for everybody if it has good accessability.
* End to end testing (E2E)
    > End to end testing is a technique that tests the application's workflow from beginning to end to make sure everything functions as expected. Basically the same as _acceptance testing_
* Unit testing 
    > Unit testing is the process of checking small pieces of code to ensure that the individual parts of a program work properly on their own, speeding up testing strategies and reducing wasted tests.
    * Unit testing: mock
        >Isolated unit tests that are run without depending on any other parts of the program. We would create _fakes_ of the dependencies (mockups of them) that are essentially placeholders so we can test the unit even if it doesn't actually have their real dependencies
* Integration test
    > Integration testing ensures that an entire, integrated system meets a set of requirements. It is performed in an integrated hardware and software environment to ensure that the entire system functions properly.
    >
    >It is the phase in software testing in which individual software modules are combined and tested as a group. Integration testing is conducted to evaluate the compliance of a system or component with specified functional requirements.
* Acceptance testing
    >also known as  ___User acceptance__ testing_ or ___fully integrated__ tests_
    >
    > Acceptance testing ensures that the end-user (customers) can achieve the goals set in the busness requirements, which determines whether the software is acceptable for delivery or not.
* Smoke tests / Deployment test
    > This type of software testing validates the stability of a software application, it is performed on the initial software build to ensure that the critical functions of the program are working.
* Stress testing
    > Stress testing is a software testing activity that tests beyond normal operational capacity to test the results
* Contract testing
    >also known as ___Focused integration__ testing_
    >
    >Contact testing is the interrogation of a deployed or mocked services endpoint to get information back or define an endpoint in testing prior to deployment. It provides complimentary testing coverage each time a change is planned or made in your code.
* Functional testing
    > Functional testing checks an application, website, or system to ensure it's doing exactly what it's supposed to be doing
* Non functional testing
    > Non functional testing verifies the readiness of a system according to nonfunctional parameters (performance, accessability, UX, etc.) which are never addressed by functional testing
* Performance testing
    > Performance testing examines the speed, stability, reliability, scalability and resource usage of a software application under a specified workload.
* Regression testing
    > Regression testing is performed to determine if code modifications break an application or consume resources.
* Black box testing
    > Black box testing involves testing against a system where the code and paths are invisible
* White box testing
    >White box testing involves testing the product's underlying structure, architecture, and code to validate input-output flow and enhance design, usability and security

## Roles when testing
* Analyst
    * They identify and prioritize requirements
    * Ensure that acceptance criteria are specified properly
    * Collects examples
* Testers
    * Ensure shared understanding on quality and production rediness
    * Define acceptance criterias
    * Work with developers automating acceptance testing
    * Performing manual tests (exploratory, usability, showcases)

## Doing tests
* ### [Acceptance testing](acceptance-testing.md)
* ### [Functional Testing](functional-testing.md)
    > The functionality that was required of the application to do the tasks it is expected to do
* ### [Non-functional testing](nonfunctional-testing.md)
    > Things that have nothing to do with functionality of the application, but it might improve the experience
* ### [User testing](user-testing.md)
    > What do real people think of the application? these kind of tests cannot be automated.

* ### Manual testing
    > Manual testing stages assert that the system is usable and fulfills its requirements, detect any defects not caught by automated tests, and verify that it provides value to its users. These stages might typically include exploratory testing environments, integration environments, and user acceptance testing

## Programming for Capacity
* Avoid premature optimization
    >Knuth's Optimization Principle:
    >* __"Premature optimization is the root of all evil"__ - Donald Knuth
    >* "Don't sacrifice clarity for percieved efficiency"
    >* Premature optimization only serves to complicate the code needlessly, making it more difficult to read and understand. 
    >* Only think about optimization when it is really effecting the performance of the application (when it is failing performance requirements), and that is after the code that will need to be optimized has been identified.
* Avoid two extremes of optimizing:
    * The assumption that you will be able to fix all capacity issues later 
    * Writing defensive and over-complex code in fear of future capacity problems (premature optimization)
