# Acceptance Testing
> also known as 
>* User acceptance testing 
>   > When you let users test the application. Cannot be automated
>* fully integrated testing
>* End to end testing (E2E)
>
>As mentioned before, don't get stuck on the terms, it is more about the concept of what the purpose any particular type of testing is.

Acceptance testing ensures that the end-user (customers) can achieve the goals set in the busness requirements, which determines whether the software is acceptable for delivery or not.

### Implementing Acceptance tests
Acceptance tests involve putting the application in a particular state, performing several actions on it, and verifying the results. Tests must be written to handle asynchrony and timeouts in order to avoid flakiness.

Test doubles are often required in order to allow any integration with external systems to be simulated.

### Parallel testing
When the isolation of your acceptance tests is good, you can speed things up by running the tests in parallel. 

If you can divide your tests so that there is no risk of interaction between them, then running tests in parallel against a single instance of the system will provide a significant decrease in the duration of your acceptance test stage overall