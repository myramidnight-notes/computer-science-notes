
## Non-functional requirements
They are requirements that have nothing to do with how the application features/functions, they still have an impact on how the end-user will experience the product, specially if it is a distributed system or online service.

* Performance
    > Time to process one transaction (either in isolation or under load)
* Throughput
    > How many transactions can the system process per time unit
* Capacity
    > Maximum throughput with acceptable response
* Security
* Scalability
* Availability
* Usability
* ... and more

### Managing nonfunctional requirements
* Generally a crosscutting conscern
    > Applies to every feature
* Must be tackled early
    > To have significant impact you usually have to change the architecture, and it is hard/expensive to change the system architecture. That is why we want to tackle these non-functional requirements early.
* Non-functional requirements can conflict
    * Flexability vs. Performance
    * Usability vs. Security
    * ...