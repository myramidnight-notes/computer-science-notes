# User testing
These are tests you cannot automate, because it requires feedback from the end-users. 

* What will they think the purpose of the application is?
* How will they try to use it?
* Will they manage to achieve their goals with the application?
* Did they like using it? 
* Are you going in the right direction with development of the application?

You might get completely different answers than you expected, so guessing the reaction of real users isn't a good practice if you want the application to succeed. 

Testing is cheap (cheaper than trying to fix something that was apparently wrong), 

User feedback gives you real answers about the experience of using the application that automated tests cannot give you (since they just tell you if it works, when used in the ways it was tested for). And with that feedback you can quickly change direction towards the right path that will make the application more successful.


#### "If I had asked people what they wanted, they would have said faster horses" 
>-Henry Ford
>
>You give them what they need, not what they want. Because they probably don't know if they need it yet if they haven't experienced it before. We wouldn't have cars if they knew what they wanted, we would be breeding faster horses.

## Usability Testing
>Nytsemi er íslenska orðið fyrir usability. 

What is usability?
* "_Usability is a quality attribute that assesses how easy user interfaces are to use_"  - Jakob Nielsen
* "_Extent to which a product, system or a service can be used by specified users to achieve specified goals with effectiveness, efficiency and satishfaction in a specified contest of use_" -  ISO standards (ISO 9241-210:2019(E))
    * Effectiveness
        > Accuracy and completeness with which users achieve specified goals
    * Efficiency
        > Resources expended in relation to the results (such as number of clicks, number of steps, or actions)
    * Satishfaction
        >The physical, cognative and emotional responses that result from the use of a system, product or service

If users have problems then the design is not good enough! There is no such thing as the clumsiness of the user.

User testing will tell you the usability of your design

### Major components in testing usability
1. Learnability
    > How easy/difficult is it to learn to use the application?
1. Efficiency
    > How efficently can they do what they want to achieve with the application?
1. Memorability
    > Does the application stand out enough to be memorable?
1. Errors
    > Does the application actually work as expected? Or do the users manage to trigger errors (things don't always happen the same when you get real users to try things out)
1. Satisfaction
    > how satisfied were they after using the application? Would they wish to use it again? perhaps recommend it to others?

## User Experience (UX) testing 
> Notendaupplifun

