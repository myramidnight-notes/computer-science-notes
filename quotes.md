## Tony Hoare
Computing Pioneer Sir Charles Antony Richard Hoare (aka C.A.R. Hoare) was mostly known for his Quicksort algorithm, but less remembered for the idea of the "__null-reference__". He noted in 2009 that it was his "billion-dollar mistake" which he regretted and only implemented it because it was so easy to do.
<hr>

#### Two ways of constructing software design
> There are two ways of constructing a software design: One way is to make it __so simple that there are obviously no deficiencies__, and the other way is to make it __so complicated that there are no obvious deficiencies__. The first method is far more difficult. 
>
> It demands the same skill, devotion, insight, and even inspiration as the discovery of the simple physical laws which underlie the complex phenomena of nature

<hr>

#### Invention of the null reference
> I call it my billion-dollar mistake. It was __the invention of the null reference__ in 1965. At that time, I was designing the first comprehensive type system for references in an object oriented language (ALGOL W). My goal was to ensure that all use of references should be absolutely safe, with checking performed automatically by the compiler. 
>
> But I couldn't resist the temptation to put in a null reference, simply because it was so easy to implement. __This has led to innumerable errors, vulnerabilities, and system crashes, which have probably caused a billion dollars of pain and damage in the last forty years__.
>
>In recent years, a number of program analysers like PREfix and PREfast in Microsoft have been used to check references, and give warnings if there is a risk they may be non-null. More recent programming languages like Spec# have introduced declarations for non-null references. This is the solution, which I rejected in 1965

<hr>

#### Price of reliability
> There is nothing a mere scientist can say that will stand against the flood of a hundred million dollars. But there is one quality that cannot be purchased in this way — and that is reliability. __The price of reliability is the pursuit of the utmost simplicity__. It is a price which the very rich find most hard to pay

<hr>

>__Failures are much more fun to hear about afterwards__; they are not so funny at the time.