![Scope](images/software-design.png)

Important to have good foundations to create good and long-lasting code.

1. ### Clean code
    >The key for creating long-lasting software is writing clean code. Consistency is key, and meaningful names for variables at least.
1. ### Programming Paradigms 
    >Style of programming that forces you to code in a specific way. 
    >* Imperative programming
    >   > Programmer instructs the machine how to change its state
    >   * Object-oriented programming
    >* Declarative programming
    >   > Programmer merely declares properties and desired results, not how to compute it. 
    >   * Functional programming
    >* Structural programming
1. ### Object-Oriented programming
    >We need to know the programming paradigms, and object-oriented programming is very common and clear.
1. ### [Design Principles](design_principles/README.md)
    >Best practices towards good software design
1. ### [Design Patterns](design_patterns/README.md)
    >Just about every problem in software has been categorized and solved already. There surely is a design pattern existing already for your problem.
    >
    >Design patterns are great, but they also add complexity. YAGNI! Only use design patterns when you're sure you need them.
    >* Creational patterns
    >* Structural patterns
    >* Behavioural patters
1. ### Architectural Principles
    >Organizing and building relationships between components will have a sagnificant impact on the maintainability, flexibility and testability of a project
1. ### [Architecture Styles](design_architecture/README.md)
    >Identifying what a system needs in order for it to be successful
1. ### Architecture Patterns
1. ### Enterprice patterns