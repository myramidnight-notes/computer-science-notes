
# Design Architecture

* ### [Architecture Characteristics](architecture_characteristics.md)

## [Architecture Styles / Patterns](architecture_styles.md)

* ### Layered Architecture
* ### Pipeline Architecture
* ### Microkernel Architecture
* ### Service-based Architecture
* ### [Event-driven Architecture](pattern-event-based.md)
    >Mediators and Message Brokers
* ### [Space-based Architecture](pattern-space-based.md)
    >Also called __cloud-based architecture__ 
* ### Microservice Architecture