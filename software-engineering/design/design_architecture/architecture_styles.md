# Architecture Styles
* Layered
* Pipeline
* Microkernel
* Service-based
* Event-driven
* Space-based
* Microservice

### Monolithic vs. Distributed Architectures
> #### Fallacies of Distributed Computing
