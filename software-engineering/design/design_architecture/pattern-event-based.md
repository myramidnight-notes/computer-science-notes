>* [Event based architecture (O'reilly)](https://www.oreilly.com/library/view/software-architecture-patterns/9781491971437/ch02.html)

# Event-Based Architecture Pattern
The event-driven architecture is made up of highly decoupled, single-purpose event processing components that asynchronously receive and process events.

* It is popular pattern used to produce highly scalable applications.
* It is highly adaptable and can be used for both large and small applications and complex ones.

### Topologies
It has two main topologies, the __mediator__ and the __broker__.
* Mediator
    >The mediator is useful for events that have multiple steps and require some level of orchestration to process the events.
* Broker
    >The broker topology is used when you want to chain events together without the use of a central mediator.

## Mediator topology
![Mediator](images/event-based-mediator.png)

## Broker topology
Essentially just throwing messages onto the correct _event channels_ like they were message boards, and whoever is listening to the channel gets the message and processes it. 

They might throw in a new event to the approriate channels when they're done processing the event, to notify whoever might be listening to those channels and so on. 
![Broker](images/event-based-broker.png)

## Pattern Analysis
* #### Agility: _High_
    >Agility is the ability to respond quickly to a constantly changing enviornment. Since event-processor components are single-purpose and completely decoupled from other event processor components, changes are generally isolated to one or fewer event processors and can be made quickly without impacting the other components.
* #### Ease of deployment: _High_
    >This pattern is relatively easy to deploy due to the decoupled nature of the event-processors. 
    >
    >The broker topology tends to be easier to deploy than the mediator, primarily because the event mediator component is somewhat tightly coupled to the event processors.
* #### Testability: _Low_
    >While individual unit testing might not be difficult, testing how they work together is complicated because of the asynchronous nature of this pattern.
* #### Performance: _High_
    >This pattern achieves high performance through its asynchronous capabilities. The ability to perform decoupled, parallel asynchronous operations outweighs the cost of queuing and dequeuing messages.
* #### Scalability: _High_
    >Scalability is naturally achieved in this pattern through highly independent and decoupled event processors. Each can be scaled separately, allowing for fine-grained scalability.
* #### Ease of development: _Low_
    > Development can be somewhat complicated due to the as