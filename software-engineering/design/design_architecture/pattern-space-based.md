>* [Space Based Architecture (O'reilly)](https://www.oreilly.com/library/view/software-architecture-patterns/9781491971437/ch05.html)
# Space-Based Architecture (Cloud architecture)
* __Shared memory__
    >This pattern gets it's name from the concept of _tuple space_, the idea of __distributed shared memory__. 

* __Removes the bottleneck of central database__
    >Because there is __no central database__ in this architecture pattern, the database bottleneck is removed, providing near-infinite scalability within the application. 
    
* __High scalability__
    >High scalability is achieved by removing the central database constraint and using replicated in-memory data grids instead. Application data is kept in-memory and replicated among all the active processing units.

The __space based__ pattern (also referred to as the __cloud architecture pattern__) minimizes the factors that limit application scaling. 

## The Space-based structure
![Space based architecture model](images/space-based-architecture.png)

### Closer look at processing unit
><img src="images/space-based-unit.png" width=400>
>
>* Each _cell_ of the __in-memory data grid__ is a _named cache_. Each unit can have as many _replicated name cache_ as it needs. 
>* When a _named cache_ changes in one unit, all the other units will be updated as well. This happens asynchronously and is very fast through the __data-replication engine__.
>* If a unit needs a _named cache_, then it fetches it from one of the other units through the _data replication engine_. If none of the units have the data, then we read from the database.
>* Each unit knows of about each other through a __member list__ that contains IP and port for every processing unit that use the same _named cache_.

### Virtualized Middleware
The _virtualized middleware_ component handles housekeeping and communications. It contains components that control various aspects of dta synchronization and request handling.

* __Messaging Grid__
    > It manages input requests and session information. When a request comes into the virtualized middleware component, the messaging-grid component determines which active processing components are available to receive the request and forwards the request.

* __Data Grid__
    >The _data grid_ component is perhaps the most important component in this pattern. It interacts with the _data-replication engine_ in each processing unit to manage the data replication between processing units when data updates occur.
    >
    >Because requests could arrive at any of the _processing units_, it is very important that each of them contain exactly the same data in its _in-memory_ data grid.

* __Processing Grid__
    > It is an optional component within the middleware that manages distributed request processing when there are multiple processing units, each handling a portion of the application. If a request comes in that requires coordination between processing unit types, it is the processing grid that mediates and orchastates the request between those two processing units.
* __Deployment Manager__
    >It manages the dynamic startup and shutdown of processing units based on load conditions. This is a critical component to achieving variable scalability needs within an application.

## Considerations
* ### Good for small applications with variable load
    > Even though space-based architecture is a complex and expensive pattern to implement, it is a very good choice for smaller web-based applications with variable load (e.g., social media sites, bidding and auction sites). 
    >
    >It is however not well suited for traditional large-scale relational database applications with large amounts of operational data
* ### _Cloud architecture_ not neccecarily in the cloud
    >Even though the alternative name for this pattern is __cloud architecture__, it does not have to reside on cloud-based hosting services or PaaS (Platform as a Service). It can just as easily reside on local servers.
* ### Optional datastore
    > Even if the _space-based_ pattern does not require a centralized datastore, one is commonly included to perform the __initial in-memory grid load__ and asynchronously perist data updates made by the processing units.
    >
    >It is also common practice to create separate partitions that isolate volatile and widely used transactional data from non-active data, in order to reduce the memory footprint of the in-memory data grid within each processing unit.

## Pattern Analysis
* #### Agility / Elasticity: high
    >Agility is the ability to respond quickly to a constantly changing enviornment. This pattern can respond quickly to variable load of data 
* #### Deployment: high
    >Even though this pattern is not generally decoupled and distributed, they are dynamic, and sophisticated cloud-based tool that allows for applications to easily be "pushed" out to servers, simplifying deployment.
* #### Testability: Low
    > Difficult to test the scalability aspect of the application
* #### Performance: High
    >Because of the _in-memory_ data access and caching mechanism
* #### Scalability: High
    >Because of the fact that there is little or no dependency on a centralized database, it removes the database bottleneck from the scalability equation.
* #### Ease of Development: Low
    > Sophisticated caching and in-memory data grid products make this pattern relatively complex to develop, mostly because of the lack of familiarity with the tools and products used to create this type of architecture. 
    >
    >Special care must be taken while developing these types of architectures to make sure nothing in the source code impacts performance and scalability.