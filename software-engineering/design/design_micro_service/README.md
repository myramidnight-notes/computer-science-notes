# Microservices



### How small should it be?
* _Small enough and no smaller_
    >Viljum ekki brjóta service í allt of litlar einingar. Ekki ofgera, því þá er það erfitt að hafa umsjón yfir því.
* Single purpose modules, do one thing and do it well.
* Þau ættu ekki að vera háð öðrum modules til að gera sitt, ætti að geta unnið þótt önnur modules hætti að virka.
* Forðast _communication overhead_, að hafa samskipti einföld á milli modules
* Því minni sem module eru, því betra og decoupled micro-service er, en of lítið er erfitt að testa, mætti ekki vera of dreift.
* Betra byrja gróft, og svo bara brjóta það niður eftir þörfum þar til þú ert kominn með gott micro-service (auðveldara að brjóta niður en að sameina)
* Ættir að skylja þitt domain/context áður en þú reynir að brjóta það niður, byrja kannski á monolith sem er domain particioned til að geta breytt því micro-architecture

## Domain driven design (DDD)
![](images/understanding.png)
>This image illustrates nicely how we might all process things differently, even if we're given the same object/word. But sometimes there is need to have a shared (but _fuzzy_) understanding of the concept. 

### What is a domain?
>Cambridge dictionary defines it as: "_an area of interest or an area over which a person has control._", when something is in the _public domain_, then everyone has access to it, opposed to _private domain_ where there is a restricted access.
>
>There is no standard rule on how domain boundries are defined, it is simply a choice we make.
>
>![defining boundries](images/defining-boundries.jpeg)
>
>### Strategy to define domains
>* __Purpose__
>* __Landscape__
>* __Climate__
>* __Doctrine__
>* __Leadership__

## Bounded Context
> Bounded context is a central pattern in Domain-driven design. It is the focus of DDD's strategic design section which is all about dealing with large models and teams. DDD deals with large models by dividing them into different Bounded contexts and being explicit about their interrelationships (limiting contact between the contexts)
>![](images/bounded-context.png)
> 
>It is simply the boundry within a domain where particular domain model applies. 
>
>To find these contexts, we can use a DDD pattern called the _context mapping pattern_. It allows you to identify the various contexts in the application and their boundries. It is common to have a different context and boundry for each small subsystem (micro-service).
## Granularity of a service
>Microservices can be declared with varying levels of capability, and the size of this functionality is typically referred to as _granularity_, that is, __the functional complexity coded in a service or number of use cases implemented by a microservice__. 
>
>![](images/granularity.png)
>* [How to choose wisely, microservice granularity (medium)](https://medium.com/@lviazrnio/how-to-choose-wisely-when-defining-microservices-granularity-8e223072636c)
>
>### Effects of bad granularity of microservices
>* The services can become too small. It is best to start big and break it down until it's "small enough".
>* If the services are too small, there will be more complexity on service choreography and compromising performance and reliablity
>* Lack of global vision and responsibility. "Not my job"

## Premature decomposition
>__It is the mistake of starting too small when designing a microservice. __
>
>Since microservices evolve during development, as we find the right granularity and boundries for each service, it is easier to split something into smaller sections, than combining smaller services into a larger one. 
>
>It is best to start with a domain partitioned monolithic system as we grain better understanding of the service and contexts, before we start to decompose that large service until we have a microservice with just the right size of granularity.

## Technology heterogeneity
>_Heterogeneity_ in software engineering is usually referring to the composition of a software system. A heterogeneous system is one that is made up of software that could be written in different languages, running on different operating systems, perhaps use different standards of communication. _The oposite would be a homogeneous system, where all of the components are the same and there is no variation_.


## Transactions in Microservices
>* [Patterns for distributed transactions within a microservice architecture](https://developers.redhat.com/blog/2018/10/01/patterns-for-distributed-transactions-within-a-microservices-architecture#)

>When a microservice architecture decomposes a monolithic system into self-encapsulated services, it can __break transactions__. This means _local transaction_ in monolithic system is now _distributed_ into multiple services that will be called in a sequence.
>
>### Possible solution: Two-phase commit (2pc) pattern
>_Two-phase commit_ is widely used in database systems. The name describes what it implies, the transaction is split into two: a _prepare phase_ and _commit phase_. In the prepare phase, all microservices will be asked to prepare for some data change that could be done atomically. Once all microservices are prepared, the commit phase will ask all the microservices to make the actual changes. Normally this requires a global coordinator to maintain the lifecycle of the transaction, and the coordinator will need to call the microservices in the prepare and commit phases. If the transaction is cancelled, the services will simply need to abort doing any changes (since they hadn't actually changed anything yet).
>
>Benefits of 2pc is a very strong consistency protocol. The prepare and commit phases guarantee that a transaction is atomic. Transactions will end with either all microservices successfully changing, or all of them not changing. 3pc also allows read-write isolation. This means the changes on a field are not visible until the coordinator commits the change.
>
>Disadvantages of 2pc would be that it is synchronous (blocking). The protocol will need to lock the object that will be changed before the transaction completes.
>
>### Possible solution: Saga pattern
>The saga pattern is another widely used pattern for distributed transactions. It is different from 2pc, which is synchronous. __The saga pattern is asynchronous and reactive__. The microservices communcate with each other through an event bus (event broker).
>
>Advantages of saga pattern is the support for long-lived transactions, because each microservice focuses only on its own local atomic transaction, other microservices are not blocked if a microservice is running for a long time.
>
>Disadvantages of saga pattern is that it is difficult to debug, especially when many microservices are involved, because of the asyncrhonous nature of the transaction.
>
>To address the complexity issue of the saga pattern, we can add a process manager as an orchestrator. They are responsible for listening to events and triggering endpoints.

## Choreography vs Orchestration
> Orchestration entails actively controlling all elements and interactions like a conductor directs musicians of an orchestra.
>
>Choreography entails establishing a pattern or routine that microservices follow as the music plays, without requiring supervision and instructions.
>
>![](images/orchestration-choreography.png)

## Operational Reuse
>Microservices essentially sacrifice reusability of code and operations for decoupling of services. But sometimes we want something to be reusable, something that we want to change for the whole system without having to ask every single microservice to change this.
>
>>Disadvantages: Using these patterns breaks the heterogeneity of the system a bit, since they would be building on a shared base. And with reusability comes coupling.
### Sidecar pattern (sidekick)
>* [Sidecar pattern (microsoft)](https://docs.microsoft.com/en-us/azure/architecture/patterns/sidecar)
>* [Sidecar pattern (o'reilly)](https://www.oreilly.com/library/view/designing-distributed-systems/9781491983638/ch02.html)
>
>The _sidecar pattern_ is a __single-node pattern made up of two containers__. The first is the _application container_, that contains the core logic. Then we have the _sidecar container_, that has the role of augmenting and improving the application container, often without the application container's knowledge. 
>
>Simply, the sidecar adds functionality to a container that might otherwise be difficult to improve.
>
>![](images/sidecar.png)
>* Sidecar is independant from its primary application in terms of runtime enviornment and programming language, so you don't need to develop one sidecar per language.
>* The sidecar can access the same resources as the primary application.
>* Because of its proximity to the primary application, there's no significant latency when communicating between them

### Service template
>It is another way to reuse operations, by having a template for creating similar services, almost (if not actually) inheriting those shared operations from the template.
