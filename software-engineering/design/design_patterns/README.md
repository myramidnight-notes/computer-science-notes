>* [Refactoring Guru: Design patterns](https://refactoring.guru/design-patterns)

# Design Patterns
Where do patterns come from? The concept of patterns was first described by Christopher Alexander in _A pattern language: Towns, buildings, constructions_, describing a "language" for designing the urban enviornment.

## What are Design Patterns?
>* They are solutions to common problems that occur in software engineering.
>* The patterns are not exactly a piece of code, but an abstract idea of how to structure the code to solve these problems
>* Design patterns are a toolkit of __tried and tested solutions__ to common problems in software design. 
>* These design patterns define a common language that you can use regardless of what programming language you are using, everyone who knows these patterns will know what you mean.

### What are they not?
>* Design patterns are __NOT__ _algorithms_
>* They are __NOT__ _Design Principles_
>   >__Design Principals__ are guidelines that disctate what is "clean code" in the project.
>* They are __NOT__ code _libraries_ or _frameworks_



### Main types of Design patterns:
>* Creational Patterns
>* Structural Patterns
>* Behavioural Patterns

## Creational Patterns
Creational patterns provide various object creation mechanisms, which increase flexibility and reuse of existing code.

* ### Factory Pattern
    * ![](images/pattern-factory-method.png) __Factory Method__
        > It defines an interface for creating an object, but lets subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses
        >
        >Provides an interface for creating objects in a superclass, but allows subclasses to alter the type of object that will be created
    * ![](images/pattern-factory-abstract.png) __Abstract Factory__
        > It provides an interface for creating families of related or dependent objects without specifying their concrete class
* ### ![](images/pattern-singleton.png) Singleton
    >The singleton pattern ensures a class has only one instance, and provides a global point of access to it
* ### ![](images/pattern-prototype.png) Prototype
    > Prototype is a creational design pattern that lets you copy existing objects without making your code dependent on their classes
* ### ![](images/pattern-builder.png) Builder Pattern
    > Builder is a creational pattern that lets you construct complex objects step by step. The pattern allows you to produce different types of representations of an object using the same construction code.
## Structural Patterns
Structural patterns explain how to assemble objects and classes into larger structures while keeping these structures flexible and efficient.
* ### ![](images/pattern-decorator.png) Decorator Pattern 
    >The Decorator pattern attaches additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality
    >
    > ![](images/decorator_pattern.png)
    >
    > Lets you attach new behaviours to objects by placing these objects inside special wrapper objects that contain the behaviour.
* ### ![](images/pattern-adapter.png)Adapter Pattern
    > The adapter pattern converts the interface of a class into another interface the clients expect. Adapter lets classes work together that couldn't otherwise because of incompatible interfaces.
    >
    >Allows objects with incompatible interfaces to collaborate.
* ### ![](images/pattern-facade.png) Facade Pattern
    > Facade is a structurual design pattern that provides a simplified interface to a library, a framework, or any other complex set of classes.
* ### ![](images/pattern-composite.png) Composite Pattern
    > Lets you compose objects into tree structures and then work with these structures as if they were individual objects
* ### ![](images/pattern-proxy.png) Proxy Pattern
    > Lets you provide a substitute or placeholder for another object. A proxy controls access to the original object, allowing you to perform something either before or after the request gets through to the original object.
* ### ![](images/pattern-bridge.png) Bridge Pattern
    > Lets you split a large class or a set of closely related classes onto two separate hierarchies - abstraction and implementation - which can be developed independently of each other
* ### ![](images/pattern-flyweight.png) Flyweight
    > Lets you fit more objects into the available amount of RAM by sharing common parts of state between multiple objects instead of keeping all of the data in each object.
## Behavioural Patterns
Behavioural design patterns are concerned with algorithms and the assignment of responsibilities between objects

* ### ![](images/pattern-chain.png) Chain of repsonsibility
    > Lets you pass requests along a chain of handlers. Upon receiving a request, each handler decides either to process the request or pass it to the next handler in the chain.
* ### ![](images/pattern-command.png) Command Pattern
    > The command pattern encapsulates a request as an object, thereby letting you parameterize other objects with different requests, queue or log requests, and support undoable operations
    >
    >Turns a request into a stand-alone object that contains all information about the request. This transformation lets you pass requests as a method arguments, delay or queue a request's execution, and support undoable operations.
* ### ![](images/pattern-iterator.png) Iterator Pattern
    >Lets you traverse elements of a collection without exposing its underlying representation (list, stack, tree, etc.)
* ### ![](images/pattern-strategy.png) Stategy Pattern
    > The Strategy pattern defines a family of algorithms, encapsulates each one, and makes them interchangeable. Strategy lets the algorithm vary independently from clients that use it.
    >
    >Lets you define a family of algorithms, put each of them into a spearate class, and make their objects interchangable.
* ### ![](images/pattern-observer.png) Observer Pattern
    > The observer pattern defines a one-to-many dependency between objects so that when one object changes state, all of its dependents are notified and updated automatically
    >
    >Publisher-Subscriber relationships
    >
    >Lets you define a subscription mechanism to notify multiple objects about any events that happen to the object they're observing
* ### ![](images/pattern-template.png) Template Method Pattern
    > The template method pattern defines the skeleton of an algorithm in a method, deferring some steps to subclasses. Template method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure.
    >
    >Defines the skeleton of an algorithm in the superclass but lets subclasses override specific steps of the algorithm without changing its structure.
* ### ![](images/pattern-mediator.png) Mediator
    > Lets you reduce chaotic dependencies between objects. The pattern restricts direct communications between the objects and forces them to collaborate only via a mediator object.
* ### ![](images/pattern-memento.png) Memento
    > Lets you save and restore the previous state of an object without revealing the details of its implementation
* ### ![](images/pattern-state.png) State Pattern
    > Lets an object alter its behaviour when its internal state changes. It appears as if the object changed its class
* ### ![](images/pattern-visitor.png) Visitor
    > Lets you separate algorithms from the objects on which they operate

## Other Patterns

* ### <img src="images/injection.jpg" alt="drawing" width="120"/> Dependency Injection
    >It is part of IoC (Inversion of Control) principle. Instead of the client specifying which service it will use, the injector tells the client what service to use. The _injection_ refers to the passing of dependency (service) into the client that uses it. 
    >
    >This means the client does not need to know about the injector, how to construct the services, or even which services it is actually using. Client only needs to know the interfaces of the services.
    >
    >DI seperates the responcibility of _use_ from the responsibility of _construction_