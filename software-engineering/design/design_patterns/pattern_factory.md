# Factory Patterns
There are a few different factory patterns available
## Simple Factory 
> Simple factory is NOT _the factory pattern_, it simply encapsulates the object creation as a specific class
>
>Allows you to create objects through __composition__.

## Factory Method pattern
* [Factory method ](https://refactoring.guru/design-patterns/factory-method)
>![](images/factory_method2.png)


> The factory method pattern defines an interface for creating an object, but lets subclasses decide which class to initiate. Factory method lets a class defer initiation to subclasses.
>
>Creates objects through __inheritance__

## Abstract factory pattern
* [Abstract factory ](https://refactoring.guru/design-patterns/abstract-factory)
> Abstract factory pattern provides an interface for creating families of related or dependent objects without specifying their concrete classes
>
>Creates objects through __composition__
>
>![](images/factory_abstract2.png)

## Singelton pattern
* [singleton pattern](https://refactoring.guru/design-patterns/singleton)
> Only keep a single instance of an object
>![](images/factory_singleton.png)
>* Can have issue when multiple threads are interacting with the singelton object at once, need to make it "thread safe"
