## Structural Patterns
1. ### Adapter Pattern
    > The adapter pattern converts the interface of a class into another interface the clients expect. Adapter lets classes work together that couldn't otherwise because of incompatible interfaces.
1. ### Facade Pattern
    > Facade is a structurual design pattern that provides a simplified interface to a library, a framework, or any other complex set of classes.
1. ### Template Method Pattern
    > The template method pattern defines the skeleton of an algorithm in a method, deferring some steps to subclasses. Template method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure.
1. ### Iterator Pattern
1. ### Composite Pattern
1. ### State Pattern
1. ### Proxy Pattern
    * Virtual Proxy
    * Protection proxy
    * Caching Proxy
    * Synchronization Proxy
    * Complexity hiding Proxy
    * Copy-on-Write Proxy
1. ### Bridge Pattern
1. ### Builder Pattern
1. ### Chain of repsonsibility
1. ### Flyweight
1. ### Mediator
1. ### Memento
1. ### Prototype
1. ### Visitor
1. ### Dependency Injection