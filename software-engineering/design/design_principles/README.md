# Design Principles

* ### KISS (Keep it simple stupid)
    > Simple things work best
* ### YAGNI (You aren't going to need it)
    > Implement features when they are needed, do not code in features that you expect or predict might be used beforehand.
* ### Rule of Three
    > Don't refactor code (make it reusable) until there are at least 3 similar instances of the code.
* ### DRY (Don't Repeat Yourself)
    > Avoid having to repeat code segments. 
    >* Try to think of the __rule of three__ first, because we don't want to abstract too early, it makes the code more complex. 
* ### Knuth's Optimization Principle
    >* "premature optimization is the root of all evil" - Donald Knuth
    >* "Don't sacrifice clarity for percieved efficiency."
    >* "clever is the enemy of clear"
    >* Readability is king. Should never sacrifice readability for optimization unless you seriously need to and you've proven where the bootleneck is. 
    >* Most optimization is not needed and doesn't provide any noticible increase in speed.
* ### Strive for loosely coupled designs
    > "loosely coupled" means that components communicate as litle as possible and have clear boundries. 
* ### Law of Demeter (principle of least knowledge)
    > ![](images/law_of_demeter.png)
    > * Generally it is a specific case of loose coupling.
    > * "Don't talk to strangers"
    >   * only talk to your immediate friends
    > * Advantages: 
    >   * Tends to make the software more maintainable and adaptable, because of the loosely coupled design.
    > * Disadvantages: 
    >   * It results in having to write many wrapping mathods to propagate calls to components. This can add noticable time and space overhead.
* ### Program to an interface, not an implementation
* ### Encapsulate what varies
    >![](images/encapsulate_what_varies.jpg)
    > * The software should be designed so it can be adapted to changes.
    >   * These changes could be details that change over time or be related to requirements. 
    > * It isolates code that might change often
    > * In a perfect world, code should just need to be changed in a single place to make such changes.
* ### Favor composition over inheritance
    >![](images/composition_over_inherit.png)
* ### SOLID principles
    >* Single Responsibility Principle (SRP)
    >  * 
    >* Open-Closed Principle (OCP)
    >   * "software entities should be open for extention, not modification"
    >* Liskov Substitution Principle (LSP)
    >   * 
    >* Interface Segregation Principle (ISP)
    >   * "Clients should not be forced to depend upon methods they do not use"
    >   * Essentially don't overcrowd the interface with things the client might not need or want, only have things relevant to what they are using, subsets of interfaces that cover specific things (much like sub menues, we don't want all options unfolded, we just select what we want to open)
    >* Dependency Inversion Principle (DIP)