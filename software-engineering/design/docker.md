# Docker

## Deploying applications the old way
* ### Dedicated servers for everything
    >Servers are expensive, and having a dedicated server for every single application regardless of how little resources it might need, is expensive
    >
    >Tt means that every application has their own computer. A gaming server and a web server, both essentially taking as much space.
    >
    > Server utilization was very low

* ### Virtual Machines
    >Virtual Machines are essentially a full computers within another computer, which can have it's own OS. 
    >
    >Every application would need their VM, each having their own OS installed, which makes them very bloated (because even if you need the same OS on different VM, they cannot share resources)

## Containers! (Docker)
The __new way__ of deploying applications is to use containers (such as __Docker__).

It is a form of system virtualization without being a full blown Virtual machine. 
> The key difference is how it can share resorces (OS and kernel) to make deployment fast, no need to waste time to install a full OS for every application.

* They focus on decoupling the application code from the enviornment they run in.
* A single server can run multiple containers of varying sizes, and easily discard them when needed.
* No bloat from duplicate OS, more space for containers!
* It can be compared to running a __virtual environment__ that now includes the OS, not just the installed packets.

### Advantages of Containers:
>* Less overhead
>    >since less space is wasted on system resources than VM.
>* More portability
>    >It doesn't matter where the application is deployed.
>* Constent operation
>* Efficient
>* Better development enviornment