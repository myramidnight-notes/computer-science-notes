## Imperative paradigms
> Step by step, we tell the program every little thing it should do
* Object Oriented programming
    > Programming based on classes and how they work together
    * Java, C#, Python...
* Procedural programming
    > Simply a list of steps to take
    * C, BASIC, Java, C#...
* Constraint
    > Setting up the constraints, and the program/engine will fulfill those. 
    * MiniZinc


## Declerative paradigms
> We describe what we want the program to do, without specifically telling it every single step it should take.
* Functional programming
    > Chaining together functions, essentially everything is immutable
    * Haskell, Clojure, F#, Scala...

* Logic
    * Prolog

### Functional Programming 
| Pros | Cons 
| -- | --
| No sideeffects | More difficult than imperative
| Modular and testable | Difficult to read
| Maintainable | Can be heavier to compute
| Makes concurrency "easier" | Can use up more memory


###  Functional vs. Imperative (example)
This example multiplies all even numbers with 10 and adds them together, storing the sum as `result`
* Imperative
    ```c#
    const numList = [1,2,3,4,5,6,7,8,9,10];
    let result = 0;
    for (let i = 0; i < numList.length; i++) {
        if (numList[i] % === 0) .{
            result += numList[i] * 10
        }
    }
    ```
* Functional
    ```c#
    const result = [1,2,3,4,5,6,7,8,9,10]
                    .filter(n => n % 2 === 0)
                    .map(a => a*10)
                    .reduce((a,b) => a + b);
    ```

    
### Object Oriented Programming

| Pros | Cons
| -- | --
| Breaks up the code into simpler and reusable units | Shared state
| Easier to read and write
| Protects information with encapsulation
| has inheritance, polymorphism and abstractions

#### Class vs. Object
* Class
    ```py
    class Dog:
        def __init__(self, name: str) -> None:
            self.name = name

        def bark(self):
            print(f'My name is {self.name}')
    ```
* Object
    ```py
    dog = Dog('Anton')
    dog.bark() #prints: My name is Anton
    ```