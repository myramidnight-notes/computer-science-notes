* Conway‘s Law
    > “Any organization that designs a system (defined broadly) will produce a design whose structure is a copy of the organization's communication structure.”
* Brook‘s Law
    > “Adding manpower to a late project makes it later.”
* Hofstadter’s Law
    > “It always takes longer than you expect. (Even when you factor in Hofstadter’s law.)”
* Linus’s Law
    > “Given enough eyeballs, all bugs are shallow.”
* Gall’s Law
    > “A complex system that works has evolved from a simple system that worked. A complex system built from scratch won’t work.”
* Eagleson’s Law
    > “Any code of your own that you haven’t looked at for six or more months might as well have been written by someone else.”
* Miller’s Law
    > “To understand what another person is saying, you must assume that it is true and try to imagine what it could be true of.”
* The 90-90 rule
    > “The first 90 percent of the code accounts for the first 90 percent of the development time. The remaining 10 percent of the code accounts for the other 90 percent of the development time.”
* Humphrey’s Law
    > “For a new software system, the requirements will not be completely known until after the users have used it.”
* North’s Law
    > “Every decision is a trade off.
* Occam’s Razor
    > If we face two possible explanations which make the same predictions, the one based on the least number of unproven assumptions is preferable, until more evidence comes along.
    >
    >“The simplest solution is almost always the best (simple meaning having few assumptions)”
