\section{Creational Patterns}
%=========================================================================
\subsection{Factory Method}
\begin{quote}
\begin{definition}[Factory Method Pattern]
The Factory Method Pattern defines an interface for creating an object, but lets subclasses decide which class to instantiate (creating an instance). Factory Method lets a class defer instantiation to subclasses.
\end{definition}
\subsubsection{What is a factory?}
\begin{quote}

\begin{definition}[Factory]
In Object oriented programming, a factory is an object for creating other objects. Formally a factory is a function or method that returns objects of a varying prototype or class from some method call.
\end{definition}
\begin{definition}[Class-based factory]
A class-based factory is an \textbf{abstraction of a \\constructor of a class}.
\end{definition}

A constructor is concrete in that it creates objects as instances of a single class, and by a specified process (class initiation), while a factory can create objects by instantiating various classes, or by using other allocation schemes such as an object pool. 
\begin{definition}[Prototype-based factory]
A prototype based factory is an abstraction of a prototype object. 
\end{definition}
A prototype object is concrete in that it is used to create objects by being \textbf{cloned}, while a factory can create objects by cloning various prototypes, or by other allocation schemes.

There are various ways to implement a factory, most often it is implemented as a \textbf{method} (class function), hence the name \textbf{Factory method}.

Sometimes it is implemented as a function, in which case it is called a \textbf{factory function}. 
\end{quote}
\end{quote}
%=========================================================================
\subsection{Abstract Factory}
\begin{quote}
\begin{definition}[Abstract Factory]
The abstract factory pattern creates an interface that is \\responsible for creating a factory of related objects without specifying their subclasses. Each generated factory can produce objects based on the \textbf{factory pattern}. This pattern has also been described as a super-factory or a \textbf{factory of factories}. 
\end{definition}


\subsubsection{The problem that needs solving}
\begin{quote}
If we had a factory that produces furniture, which are rather standard items (chair, table, sofa...), and we wanted to create variations of those items, how would we go about producing different yet essentially same objects? 
\end{quote}
\subsubsection{Solution}
\begin{quote}
The first thing the abstract factory pattern suggests is to explicitly declare interfaces for each distinct product of the product family (such as chair, table, sofa...). Then we create the variants (factories that inherit from the abstract factory). All those child factories would essentially be the same factory, producing the same objects with some variation, so anyone using those factories doesn't have to treat them differently.
\begin{center}
\includegraphics[width=0.5\textwidth]{design-patterns/images/abstract-factory-example.png}
\end{center}
\end{quote}
\subsubsection{Comparing patterns}
\begin{itemize}
\item The \textbf{Factory Pattern} is often used in software design that end up evolving into \textbf{Abstract Factories}, \textbf{Prototypes} or \textbf{Builders}
\item Abstract factory returns a product immediately, while a \textit{builder} would have to be constructed in steps before actually being able to get a product from it.
\item Abstract factory can serve as a \textbf{facade} when you want to hide how the subsystem objects are created.
\item Abstract factories are often based on a set of \textbf{Factory Methods}, but you can also use \textbf{Prototype} to compose the methods on these classes. 
\item \href{https://refactoring.guru/design-patterns/factory-comparison}{Factory comparison (refactoring.guru)}
\end{itemize}

\end{quote}
%=========================================================================
\subsection{Builder}
\begin{quote}
\begin{flushleft}
\includegraphics[width=0.15\textwidth]{design-patterns/images/builder-mini.png}
\end{flushleft}
\subsubsection{The Problem that needs solving}
\begin{quote}
The trouble of having to initiate complex objects step by step, with many options and fields and even nested objects. It becomes very complicated to define all that with parameters upon initiation. 

Essentially it would be a \textbf{very ugly constructor}.
\end{quote}
\subsubsection{The Solution}
\begin{quote}
By relocating the construction of the object to methods (these types of methods would be called \textbf{builders}), that can be called on after the object has been created, so the object can be configured in steps whenever needed. When everything has been configured, we could just call a final method to get the resulting object.
\end{quote}

\begin{extra}{Adding a Director}{}
To organize the creation of the object with builder, we can add a \textbf{director} that defines the order of steps, while the builder simply provides the implementation of those steps.

It allows us to hide the builder and it's methods, things that the client doesn't need to mess with directly.
\end{extra}

\begin{itemize}
\item Pros
\begin{itemize}
\item Encapsulates the way a complex object is constructed
\item Allows us to construct an object in multiple steps
\item Hides the internal representation of the product from client
\item Product implementations can be swapped out because the client only sees the abstract interface
\end{itemize}
\item Cons
\begin{itemize}
\item Constructing objects requires more domain knowledge of the client than when using a factory
\end{itemize}
\end{itemize}
\end{quote}
%=========================================================================
\subsection{Prototype (cloning)}
\begin{quote}
\begin{flushleft}
\includegraphics[width=0.15\textwidth]{design-patterns/images/prototype-mini.png}
\end{flushleft}
\subsubsection{The Problem that needs solving}
\begin{quote}
How should we make a copy of something? Manually copying values from the object you want to copy? What if it has private attributes you cannot see from the outside? You might not have the ability to perfectly copy something just by looking at it. \textbf{Copying can be very expensive} (in time and memory/space), specially when you need a lot of copies of said object. 
\end{quote}
\subsubsection{The Solution}
\begin{quote}
We create a \textbf{prototype} that is made to be copied, to create new instances of the same object without relying on a parent class or any such coupling of subclasses. It simply clones itself, creating a new instance.

Simply the act of supporting cloning makes an object a prototype. It would create a \textbf{new instance} of a prototype, hence the keyword "new" in many programming languages

It might seem like copying is the same as cloning, but copying takes effort, we have to create the object and copy all the contents, while cloning would simply be "hey, I want another one of you" and you get it.

\begin{example}{Expensive copying}{}
An example of how expensive operation of cloning could be if you had to fetch the values of the object from a database, but with prototyping you could just make a copy of an object that has already been created with the values from the database, so we wont have to spend time going all the way to the database for every new instance we want to create.
\end{example}
\end{quote}

	\begin{itemize}
	\item Prototype lets you copy existing objects without making your code depend on their classes
	\item It is used when the type of objects to create is determined by a prototypical instance, which is cloned to produce new objects. 
	\item This pattern is used to avoid subclasses of an object creator in the client application
	\item It is a useful pattern when it is expensive to create a new object
	\item \href{https://refactoring.guru/design-patterns/prototype/python/example}{Python example (Refactoring.guru)}
	\item \href{https://refactoring.guru/design-patterns/prototype/cpp/example}{C++ example (Refactoring.guru)}
	\end{itemize}
	
\begin{extra}{Deep Copy vs. Shallow Copy}{}
Shallow copy essentially just makes a clone with the surface contents, which is quicker, but it can be dangerous because of references. A shallow copy would only copy the surface essentially, and the anything deeper would  simply be left as a reference, same reference as the object you decided to clone. The danger lies in the fact that some of the attributes of the new instance could edit the values through those references, effecting the prototype itself.
 
\textbf{Deep copy} is just what it says, it goes deep and copies every layer of the prototype, not leaving anything to the prototype.
\end{extra}
\end{quote}

%=========================================================================
\subsection{Singleton}
\begin{quote}
\subsubsection{The problem to be solved}
\begin{quote}
It solves the issue of breaking the \textbf{Single Responsibility Principle}, and providing \textbf{global access to the instance}.
\end{quote}
\subsubsection{Solution}
\begin{quote}
We first have to make the default constructor private, to prevent others from creating new instances. And then the creation method that others get access to will actually just give them a reference to the singular instance that exists. So even if they \textit{created} an instance of their own, on the inside it's all the same singular instance.
\end{quote}
\begin{itemize}
\item Pros
\begin{itemize}
\item You can insure that a class only has a single instance
\item You gain a global access point to that instance
\item The singleton object is only initialized when it is requested for the first time
\end{itemize}
\item Cons
\begin{itemize}
\item Isn't actually Single Responsibility, because it solves two things at once (the responsibility AND global access).
\item The pattern requires special treatment in multi-threaded environment in order to prevent multiple threads from creating a singleton object multiple times.
\item Difficult to unit-test, because it is a global instance.
\end{itemize}
\end{itemize}
\textbf{Abstract Factories}, \textbf{Builders} and \textbf{Prototypes} can be implemented as \textbf{Singletons}
\end{quote}