[...back](../README.md)
> Teacher: Grischa Liebel (HUGB 2020)
# Software Engineering

>"__Software engineering__ is the systematic application of scientific and technological knowledge, methods, and experience to the design, implementation, testing, and documentation of software"
>>_Institute of Electrical and Electronics Engineers (IEEE)_

Or simply put, __software engineering__ is the systematic application of __engineering approaches__ to the development of software.

### History
1. The first digital computers appeared in the early 1940s
1. Programming languages started to appear in the early 1950s
1. The term "_software engineering_" appeared in a list of services offered by companies in June 1965.
1. The __Software Engineering Institute__ was established in 1984
1. Standardization of best-practices for software pracices was first established in 1987
    * It was submited by _ISO_ and the __International Electrotechnical Comission__ (IEC)
    * It is called [__ISO/IEC JTC 1/SC 7__](https://en.wikipedia.org/wiki/ISO/IEC_JTC_1/SC_7)

## Index
* ### [Requirements Engineering](se_requirements.md)
    * Elicitation
    * Specification
    * Validation
    * Managment
* ### [Processes](se_processes.md)
    * Linear Process Models
    * Scrum and Agile methods
* ### Modeling
    * [__System Modeling__](se_system_modeling.md)
* ### Quality Assurance (QA)
    * Inspection
    * Testing
* ### [Architecture](se_architecture.md)
    * UML and Diagrams
    * Model-View-Controller
    * Layered System
    * Repository
    * Client-server
    * Pipe-and-filter
    * Application Architectures
    * Conway's Law
    * Service-Oriented Architecture
    * [__Microservices__](se_microservices.md)
* ### [Design](se_design.md)
    * SOLID
    * [__Design patterns__](se_patterns.md)
* ### [Security](se_security.md)
    * Assets and Threats
    * Secure Design and Programming
* ### [Human Factors](se_humanfactor.md)
    * Biases
    * Ethics
    * Productivity
    * [__Diversity and Inclusion__](se_diversity.md)
* ### [Model Based Engineering (MBE)](se_model_based_engineering.md)
    * Model Transformations
    * Meta Modelling
* ### [Continuous Software Engineering](se_continuous.md)
    * CI/CD
    * CE
    * Measurement
    * DevOps