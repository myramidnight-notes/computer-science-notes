# Software Architecture
The idea comes from planning when you build a structure, such as houses, in our cases application. 

* Organization
* Overall structure
* Components and interfaces

* Communication (helps explain your system)
* Analyces of structure
* Reusability
* Planned evolution (allow the application to grow)

There are many ways to represent the architecture, the use of UML is most common because it makes the application and the connections between components more visual.

Architecture is very important because it also acts as a blueprint for the developer, the shape of the application. Specially important when working in a team.

There exist many styles of architecture, and best practicies for a structure. The type of application and requirements will decide what style would be best to use.
### Convway's Law
Conway's law says that a Organization that develops the product will sooner or later look like the product. Splitting up a project up between teams based on the architecture during development, so the structure of the organization will resemble that arrangement. If a component of one team communicates to another, those teams will need to communicate in a similar manner as the application is designed.

That makes it important to plan what the architecture is and if it suits the project, since it can be difficult to change the architecture later since the organization has adjusted to what is currently being used.

### Concerns about architecture
* Generic structures? Why re-invent the wheel if it exists
* Distribution (how to deploy the application, OS, Depenedencies)
* Patterns and Styles (best practices)
* Documentation (how will we explain the structure to the developer?)
* Decomposition (the parts and interactions)
* Quality requirements. Need to have a plan to be able to meet requirements.
    * Performance, Secuity, Safety, Availability, Maintainability
    * Different architectures suite different requirements

## Model - View - Controller (MVC) style
This is a very basic style, where we have a controller that manages the application, but the viewer displayes the data to the user/client. The viewer could even be outside of your scope (such as when creating a API).

Very common for websites and apps. This style creates a seperation of the functionality from how it'll be displayed. 
* The state of data is stored in Models. The data is outside the layers and stays uneffected by how the layers migth evolove.
* View might make a query for data, and it is delivered in a model.
* There can be events in the viewer that trigger something in the controller, which updates the data. 
* Models could notify the viewer of any changes (real time updates for example)

## Layered style
This style seperates the system into layers, where each layer only talk to the lower layers and any lower levels will never know what is above it, it will simply react to the call and return the results (if there is anything to return).

Usually these styles make use of models to store and transfer data between layers. So the Models aren't actually part of any one layer.
#### Pros/Cons of layered style
* Each layer is indipendant and has a fixed way of communicaiton. It makes it easy to add layers in between.
* Since each layer is indipendant, it is easy to swap out whole layers as long as the replacement has the same mode of communication (method names, parameters and return value).
* It is hard to get good performance from a layered style, because requests have to travel through all the layers to reach the bottom, no skipping layers.
* It can be difficult to design a pure layered architecture, to prevent skipping layers.

## Repository style
This style has a central repository, and is surrounded by indipendant components that all access the shared repository.

* All the components/applications that access the repository  do not talk to each other, completely indipendant. 
* Repository has to manage all the requests, which can create heavy bottle necks
* All the other applications rely heavily on the repository, if it goes down then everything goes down. 

## Client - Server 
 A network handles communication between clients and servers. Very sucessful system (as we've seen of the internet) but can be difficult to maintain.
 * Everyting relies on the network.
 * Difficult to predict performance, because it relies of the performance/access to the network and status of the servers.

 ## Pipe and Filter style
 This style is less common for interent applications, but better known in the cli terminal and data processing. The data flows through a pipe, and transforms through them. We can chain many pipes together to create pipelines. Each pipe has a input and output. 
 
It can be difficult to agree on what the input and output should be for each pipe, because the pipe itself will not think about the data and decide what to do with it. It expects specic input and will give expected output.
 
 ## Common/Generic Application Architectures
 There exist many businesses that are similar and require similar software. 
 
 Int these cases there often exist syThis requires less problem solving other than tailoring a existing solultion/architecture/system to suite a specific situation. It might even layer them so they can make use of multiple solutions.

 ## Service Oriented style
 The system is based on services that are indipendant of the system, rather than components or layers.
 * The services can be reused and used in different situations. 
 * Services should be loosely coupled when used
 * Combine the services in any way you want
 * Access to the services should be platform indipendant
 
### Service Oriented Architecture (SOA)
This could be considered a platoform where service providers can register their service so others can find and use them. This would mean that the services within the SOA would be very standardized to fit the system.

SOA style evolved into microserevices.

### Microservices 
* Each service should be a specialized component
* Smart endpoints, dumb pipes.
* Decentralized governance / data (then they do no thave to agree on the design of data)
* designed for failure, the whole system should be able to function even if some services stop working.
* Alot of automation (testing, builds, deploys, feedback...)
* Each service is independant of any other service.
* The tech can be indipendant also, the services do not need to be coded in same language.
* Each service is deployed indipendantly

Netflix is a great example of a microservice system.