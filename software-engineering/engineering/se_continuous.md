# Continuous Software Engineering
* Continuous Integration (CI)
* Continuous Deployment (CD)
* Continuous Experimentation
* DevOps

    (optional) Meyer, 2014, Continuous Integration and Its Tools,
    IEEE Software 31(3)
    (optional) Zhu et al., 2016, DevOps and Its Practices,
    IEEE Software 33(3)
    (optional) Claps et al., 2015, On the journey to continuous deployment: Technical and social challenges along the way, IST 57
    (optional) McGarry et al., Practical Software Measurement,
    Addison-Wesley, 2002
    (optional) Staron, Meding, Nilsson, 2009, A framework for developing
    measurement systems and its industrial evaluation, IST 51(4)
    (optional) Montperrus, 2016, Introduction to Empirical Software
    Engineering,
    https://www.monperrus.net/martin/introduction-to-empirical-software-engineering.pdf (Links to an external site.)
    (optional) Fagerholm et al., 2014, Building Blocks for Continuous
    Experimentation, RCoSE '14

## Continuous Integration
* Automated tests
    > write unit tests that can be automated
* CI solution
    > Some system that can notice that you pushed new code, can automatically test it and build it and perhaps publish it. This can seem like a pipeline that does things in a specific order.
    * Some repositories have CI services
* Discipline
    > Some disciplines to prevent breaking the master branch.
    * GitFlow 
* Configuration managment
    > Directions that tell the CI what branches to follow, what to do to create builds and so on.
## Continuous Deployments
This extends on CI by also deploying it directly to the end user. 
* We need a "good" test suite so you can trust the automated deployment will only deploy if the code meets requirements.
* Some find it hard to trust the automated deployment and would have this step just make everything ready so they can just press a "deploy" button.
* Option to deploy
    > Not all platforms would allow you to automatic deploy the product, needing to be audited to check if it meets regulations of the platform. 

## Continuous Experimentation
Live testing. To continuously run experiments among the users to see how they react and measure.

#### Canary release
* Deploying new features that only a portion of the user-base would get to try out, while the rest would still get the "current" version. Such as 5% testing the new features.
* You would keep track of who are viewing the test version and see if there might be a increase in error reports from the users or complaints, which will be a clear indicator that the new feature is not an improvement or might be broken.
* Doing this type of test releases will allow experimentation without crashing the whole busness with a undesirable update.

#### A/B testing
This kind of testing should only be done when you are confident that the feture works but want to see the reactions of the users to each version.
* You have two versions of same feature, or two different versions of a feature. 
* Test 50% of user-base with version A, and rest will test version B.

#### Dark launch
This is like a secret test on users actions without showing them the test version, by simply duplicating their actions as if they were also using the test version. It's can be considered a simulation. You get real interactions but not reactions.

### The technical aspect  
Many ways to manage the routing of users towards the test features. Could have a _feature toggle_ that would enable test features based on conditions or randomized. 

* How do we measure the results of the experiments? 
    > You have to be able to measure the reactions/results in order to see if it's having a effect.

* Sciencific considerations. 
    > How do you pick the sample groups? You have to care about who you show the experimental features to. Loyal users might not care if there are bugs or less likely to report them even if they experience them, so a canary release might not be so good loyal users. You should also only test a single feature per group, not mix them together, else you cannot measure their individual effects.

### Measurement
>![Product quality](../images/se/product_quality_se.png)
> The standards that form this division include a software product quality measurement reference model, mathematical definitions of quality measures, and practical guidance for their application. Presented measures apply to software product quality and quality in use. Currently, this division consists of the following standards:
>* <b>ISO/IEC 2502<u>0</u></b> - Measurement reference model and guide: Presents introductory explanation and a reference model that is common to quality measure elements,measures of software product quality and quality in use. Also provides guidance tousers for selecting or developing, and applying measures.
>* <b>ISO/IEC 2502<u>1</u></b> - Quality measure elements: Defines a set of recommended base andderived measures, which are intended to be used during the whole software development life cycle. The document describes a set of measures that can be used as an input forthe software product quality or software quality in use measurement.
>* <b>ISO/IEC 2502<u>2</u></b> - Measurement of quality in use: Describes a set of measures andprovides guidance for measuring quality in use.
>* <b>ISO/IEC 2502<u>3</u></b> - Measurement of system and software product quality: Describes a setof measures and provides guidance for measuring system and software product quality.
>* <b>ISO/IEC 2502<u>4</u></b> - Measurement of data quality: Defines quality measures for quantitatively measuring data quality in terms of characteristics defined in ISO/IEC 25012.

Things we can measure the quality of:
* Product
    > There is the general product quality, how secure is it? how good is it? there is also quality-in-use, which is the usability of that product. It might be very good/secure but unusable.
* Process
    > Meeting deadlines? Number of bugs? How many change requests? Productivity? How tracable are the issues? A good process produces a better product.

You need to gather information and be consistent. 
1. Find attributes to measure. 
1. Base measurements of the attributes. 
    > Has to be something measurable in numbers. How much code is in the system? with or without comments? file sizes? number of files? 
1. Derive a measurement by comparing base measurements.
    > Derive a measurement by comparing base measurements. How many bugs are there in per number of code lines? Does the file sizes effect run time? produce numbers
1. Indicator 
    > Astract way to read from the derived measurements. Is some measurments good or bad? X many bugs per Y lines of codes is bad.
1. Product dashboard
    > Might be a dashboard that will notify the developers if something is going well or not. More code does not mean better productivity after all if it's full of issues.

A measurement is pointless if people loose confidence in what they are telling you, then it's worthless. We don't want flaky tests that would constantly be jumping between conclusions.

## DevOps
![DevOps loop](../images/se/devops_loop.png)

| | Development | Operations |
| --- | --- | --- |
| 1| Plan | Release
| 2| Code | Deploy
| 3| Build | Operate |
| 4| Test | Monitor |

* Architecture
    * can it be deployed regularily?
    * can code be rolled back? 
        > Can you have a test feature that is easily removed?
    * How to monitor the system code?
    * Devops fit very well for microservice systems.
* Infrastructure
* Testing
* Configuration managment
* Team roles
* Coordination between teams (not to break something for others)
* Discovery (finding new requrirements)
* Customer feedback (how to ensure regular feedback)