> Remember, these are just notes, explaining how I understood the subject.
# Design of Implementation
Visual design is not the only design. Design also comes into play when planning how things work together as a whole. The design aspect might not be very prominent to the stakeholders/customers/users, but it becomes essential to a good development workflow to ensure that a good product is produced.
## Design vs Architecture
### Reusability of code/functionality
It saves alot of time and money to be able to reuse a functionality or code. This could mean the reuse of systems, components or even code libraries that are available to us. We have to evaluate if the reuse will actually save us any work.

### Configuration and Managment
Keeping track of what is going on in the project is important, what is being used, different versions of the project, who/what is responsible for something working or breaking. 
* `Git` is a great versioning tool. It keeps track of all changes and who made them. It has become essential for any project these days regardless if it is maintained by multiple people or not.
    > The _commit message_ becomes essential for overview of the changes that have been made to the project. If you find yourself unable to describe your commit witha single message, then it is a sign that you are not committing often enough. on the oposite end, if you deem the changes too minimal to mention, then perhaps it was a needless change? If they were needed then they are worth mentioning.
* It is essential to keep track of what the system requires to function, in terms of compilers and dependencies. Perhaps the project needs a specific version of a tool. 
* Keeping track of issues that might pop up during the development, so they might be addressed.

### Host and Target
It is not always sure that the system is being developed in the same enviorment that it is expected to be used in. The hardware and operating system might be different between the host and target. Good exapmle would be developing a iphone app on a windows computer. In such cases the system being developed will need to be tested in the target enviorment somehow, such as using simulations/emulations or even _in-the-loop_ testing. It is essential that the system being developed is compatible with the target. 

## `SOLID` object oriented design
1. Single responsibility
    * Each module/class has a single purpose.
2. Open-Close. 
    * Open for Extensions, closed to modification. 
3. Liskov substitution
    * Any sub-class that inherits from original should still work. Dependencies.
2. Interface segregation 
    * Avoid having a super interface, break it down. Each interface should be specialized. 
3. Dependency inversion.
    * Avoid hardcoding algorithms if possible. For example having the caller of a function also provide the algorithm/module in the arguments. Then it becomes abstract, flexible. 

## Design patterns
> "Timeless entities called patterns" - from [_A Pattern Language_](https://en.wikipedia.org/wiki/A_Pattern_Language) by Christopher Alexander (1977).

These are best practices, such as guidelines on how things should be done. Anti-patterns would be examples of what does not work well. There are infinite number of patterns out there. Here are a few useful patterns explained that are useful for softeware engineering.
### Creational patterns
#### Singelton pattern
> Only a single instance is allowed of a class, it prevents the creation of new instances and makes sure that only that specific instance is accessable through the class. This insures that no matter where the instance might be used, they are all using the same object. This would be specially important in cases of data storage, we only want there to be a single instance of the database to prevent inconsistencies when retrieving information from it.
>* This actually violites the __single responsibility__ because it both ensures the single instance, but also provides a global access to that instance.
>* It is difficult to test singelton cases, cannot ensure that it's in a specific state.
>* If possible, you should try using a __dependency injection__ instead of a singelton case, by letting the instance be passed around through arguments/parameters. 
#### Factory pattern
> A factory class would have methods to create new instances, rather than creating a new instance of the class with the `new` keyword. It would return a new instance of a specific design. These new instances would be sub-classes that inherit from a shared parent. 

>* This also satisfies the __open-close__ by not modifying the classes but still extending the class with instances of the sub-classes that inherit their methods from parent. 
### Structural patterns
#### Adaptor pattern
> In the same fashion that electrical adaptors change the flow of electricty from an outlet so that a device can use it without complications. Adaptors in programming would be like middle layer that translates the communication between a interface and service so that neither has to change their internal structure in order to function together.
>* Perhaps the data format had to be different, the adaptor would solve that.
>* __Open-CLose__ is satisfied because nothing is changed, but it extends on existing system.
>* __Single Responsibility__ is satisified because the adaptor is only made for that specific interaction.

#### Facade pattern
> A facade hides what is actually underneeth. This can create a more user-friendly interface for example. The functionality of the system through facade might be more limited than when used directly. 
>* __interface segrigation__ is satisfied, there could be specific facade for connections to specific services. 
>* __single responsibility__ is satisfied, as each facade would have a specific purpose/responsibility.

#### Decorator pattern
> It basically _decorates_ a subsystem with extra functionality while still having access to the original subsystem through inheridence. The decorations could perhaps a buffer, encrypt or adding whatever decoration that was needed.
>* This pattern is ideally used in Graphical interfaces. The system itself wouldn't be visual, but the decorator could be used to make it pretty and easy to use for the client.
>* This pattern satisfies the __open-close__ prinsipal of `SOLID` by not changing the originals but extending on them. 
>* It also satisfies the __single responsibilty__ because of the decorator being added for specific purpose.

### Behavioural patterns
#### Observer patterns. 
> In example, the subject class would notify the observer that there were changes. Concrete observers would be directly connected to the concrete subject (directly connected to the subject in question) and can then see exactly what was changed and can therefor get the latest data to stay updated. The observer could be the frontend while subject is on the backend.
> * Event listeners are a good example of observers.
> * Satisfies the __Open-Close__  by attaching the observers externally on the subject, instead of modifying it. 
>* __Liskov__ refers to the inheridence of functionality of sub-classes (in this case the concrete observers)
#### Strategy pattern
> A class could have a method that gives access to a strategy through an external/imported interface. This strategies could change based on situation or demand when the program is run. 
>* Satisfies the __Open-CLose__ because the strategy is not modified within the class. 
>* It also satisfies the __dependency inversion__ because the method isn't hardcoded. 

# Overview
* Application of patterns might over time, such as some patterns might be used for different things they were designed for
* Language can effect what pattern is chosen/needed. Some patterns might be language specific
* Think about if a pattern is needed, because it's not always needed. Limit their use based on need.
    * Patterns might simplify some cases, it might complicate in other cases.