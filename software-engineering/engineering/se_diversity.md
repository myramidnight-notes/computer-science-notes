
# Diversity and Inclusion (and equity)
> Guest lecture with Andrew Begel, principal researcher from Microsoft Research.
* __Diversity__: valuing human differences and recognizing diverse talents. Ensuring that the software industry reflects the diversity of society.
* __Inclusion__: ensuring that everyone feels welcome, included, respected and safe
* __Equity__: removing barriers, biases and obstacles that impede equal access and opportunity to succeed.

## Diversity
There is alot of research that shows that diverse teams are innovating, creative, adaptive and more productive. Easier to recruiting. Lower turnover. Busnesses that invest in diversity tend to be more stable.

> 20% of the world population has some kind of disability. Even in software you should think about accessability. Don't discount 20% of the population, you will be missing out on 20% of the possible profit.

What defines diversity?
* Personality, age, gender, culture, income, race, sexuality, marital status, relition, ancestry, disability, education, neurodiversity (mental differences)...
> Check out `Brotopia - breaking up the boys' club of silicon valley` by Emily Chang

Diversity does help difuse blindspots in a project.

#### Microsoft Principles for Responsible AI
* Fairness
* Reliability and Safety
* Privacy and Security
* Inclusiveness
* Transparency
    > Whatever an AI says about a person should be understandable, to be able to see how the AI came to that conclusion
* Accountability
    > You should be able to interrigate the AI to find out how it came to that conclusion, to be able to tell it if it's wrong and have someone fix it.

> https://www.microsoft.com/en-us/ai/responsible-ai

### People need to feel like they belong
If diversity is a _fact_ and inclusion is a _choice_, belonging is a _feeling_ that can be enfroced by a culture you can purposefully create.
> https://medium.com/@AnitaSands/diversity-and-inclusion-arent-what-matter-belonging-is-what-counts-4a75bf6565b5

> "When you do not intentionally, deliberatly and proactivly include, then you unintentionally exclude" - Joe Gertstandt

* Recognize exclusion
* Learn from diversity
*  Solve for one, extend to many
> microsoft.com/design/inclusive