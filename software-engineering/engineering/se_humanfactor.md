# Human factors in software engineering
[ACM Ethics](https://ethics.acm.org/code-of-ethics/software-engineering-code/)
* Cognitive bias
    * People each see the world differently, make different decissions based own own experiences
* Ethics
* Productivity 

## Cognitive bias
Since people each see the world differently and have different experiences, they might expect different things in some situations.

We are not immune to bias, but it is good to be aware of them and take steps to prevent them from having effect on the project.

### Confirmation bias
Tendencies of an individual to ignore information that goes against your beliefs and pay overly much attention to sources that support your belief. If someone has a very strong opinion of something, they will have a hard time to accept anything that goes against it.
> Time estimates can be a good example, perhaps the estimate would be made by someone who does not take into account that there is a team with different people (each having their own work pase), perhaps the estimate would be made by someone who doesn't know what goes on behind the scenes, push for timeframes that are unrealistic, make you provide incorrect estimates ("how long will this take, how much will be done after x months?"), usually shorter time frames than realistic.

* __Positive testing__ is good example for confirmation bias in software engineering, testing only for how you would expect the system, and as soon as someone tries something outside of expectations then the system might crash because it hadn't been tested.
* Search _known_ documentation, if something isn't where you expect it to be found then we might not search further even if the exect information you were looking for might be in the next chapter.
* Resist large change requests, you might unconciously try to argue against it, find reason why it's not needed, even if the reasons for the change are valid.
* Ignore results of trade-off studies if they go against our opinion.

Tracability is a good way to support something that goes against what you might belief, because it has sources and references.

### Anchoring bias
We have a tendency to anchor ourselves/get stuck in certain numbers/facts that we've heard early on. 
* Initial estimates (when we didn't have the data to make good estimates), might bind ourselves to them even if they might be unrealistic.
* Parallel estimates can somehwat prevent that, by having multiple estimates then people can debate on what is the most realistic estimate, instead of locking on an initial estimate.
* Model based estimates instead of _professional estimates_ because they might not have the whole picture.

### Availability bias
The dentency of relying only on information that is readily available and easily recallable
* Ignore unfamiliar parts of the code/documents/requirements
* Participatory decisions can prevent this bias, to have multiple people because we each have different knowledge and might recall different things (less likely to overlook something than when making decisions as individuals)
* Tracability does help to support/argue against code/documentation.
* Retrospectives to share information about what is going on in the project

### Framing effect
Questions can frame or give suggestions about what you will answer, such as "how long will this _little_ project take?" and you might give a shorter estimate than is realistic.
* __Ideas vs. requirements.__ People react differently depeneding on how things are phrased.

Keep in mind how you frame questions and answers.

### Bandwagon effect
In a group, people tend to adjust to what they think is the majority opinion.
* People tend to often agree with the "leader"
* Brainstorming as a group, you might start aligning your idea with someone else who had a strong/early opinions even if it went against what you were originally thinking. 
* Parallel estimates can help against this, then there isn't any initial idea/opinion that might leading the rest.
* Doing individual brainstorming might be good, without voicing opinions, these opinions could then be compared afterwards.

## Ethics
Disipline of what is good and bad, moral oblications and duties, principles of certain areas. 

Doing what is easy or what is right? Following the code of ethics (CoE)? 

### The ACM and IEEE policies
For programming there are standards set by the ACM (Association for computing machinery) and the IEEE (Institute of Electrical and Electronics Engineers) .

* Public interest
    > Addiction does fall under this, exploiting addicion is not for public interest. 
* Client and Employer
    > Don't use code from a project in workplace, that can be considered stealing. You should work in the interest of the employer, as well as for the public interest.
* Product
    > Do not release products that are not tested or known to be insecure that might cause harm. 
* Judgment
    > You might be an expert in your field, you should be professional and indipendant in your judement.
* Managment
    > Responsibility towards those you manage, not to push them to do unethical things or burning them out. 
* Profession
    > A profession has it's own policies and expectations of the profession. It is our responsibility to not to do unethical things that will reflect negativly on the profession and make it seem untrustworthy.
* Colleagues
    > Responsibility towards your colleagues, they should be able to trust you to do your part as you want to trust them.
* Self
    > Keep learning and keep up with changes in your field because displines change, techniques and information changes. This is specially important if you wish to be a registered engineer.


## Productivity 
Productivity of development, teams, individuals.

### The development enviorment
* Programming language might effect productivity.
* Documentation, how well is the project documented? does it help?
* Tools in the development env. Such as linters, highlighters, autocomplete, anything that helps make programming easier.
* Practices, such as automated testing can help productivity
* Agile development seems to be associated with higher productivity.
* Completeness of the design can make things go faster, to work with a plan.
* Process maturity, are people familiar with the process? is it an experiment?
* Requiements stability. They might change, but we want them to stay stable, no drastic changes.

### The human side of productivity
* Time fragmentation, better to have longer uninterupted work sessions.
* Office layout, open spaces can be distracting.
* Personality or mix of personalites can effect things. 
* Happiness is a valid factor to raise productivity.
* Communication is important.
* Comradery (how well does the team stick together)
* Psycological safety. Allowing people to make mistakes, the fear of failure can have negative effect on productivity.
* Respect
