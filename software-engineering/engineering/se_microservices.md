# Microservices
[Martin Fowler, Guide to Microservices](https://martinfowler.com/microservices/)
>[GOTO 2014 • Microservices • Martin Fowler, precentation](https://www.youtube.com/watch?v=wgdBVIX9ifA)

Microservices is a subset of SOA (System Oriented Architecture). The name implies small services, but how big is micro? Single objective? Single funtionality?

## Monoliths vs Microservice?
Be sure that you use the system that fits your project.
### Monolith advantages
* Simpicity
* Consistency
* Inter-module refactoring (easier to refactor)
### Microservice advantages
* Partial deployment, just upgrade the services that need to be upgraded.
* Availability, different services can function even if a single service fails.
* Preserve Modularity, solid boundries. No shared mutable states.
* Multiple platforms, each system is indipendant.

## Defenitions
* Components are separate services
    > Software is often split down into components, such as objects, functions, classes. Splitting things up by purpose. And each component can be replaced without having to rewrite everything around it. Dependencies are also components. 
* Organized around business capabilities
    > Instead of organizing around for example UI, Database, Server and such, we would organize around purpose and capabilities, such as Orders, Shipping, Catalog. Focusing on the end users.
* Products not Projects
* Smart endpoints and dumb pipelines 
    > Instead of processing everything in a super smart middle ware, we should provide all the information through the endpoints and the pipeline doesn't think about it. 
* Decentralized governance
* Decentarlized data managment
    > Each service would be responsible for it's own data, not all the data in one place. Each service might store data in different ways that suits them. This also includes each system having the code and hardware that suits them, it does not effect other services.
* Infrastructure Automatino (things function rather automatically)
* Design for failure
    > For example, AWS (Amazon Web Services) has the `chaos monkey` that deliberatly destroys nodes to test the resilience of the system as a whole, to make sure that even if something fails, the whole system stays functioning.
* Evolutionary Design (allows the system to evolve)


## Keep in mind
* Rabit provisioning (dont take too long setting up servers, microservices are well suited for cloud servers)
* Basic monitoring (keep track of each service in the ystem)
* Rabit Application deployment
* Devops Culture.