# Model based engineering (MBE)
Basing the software engineering on model files/code. It can be alot more powerful instead of having a image of the model.
* Communcation 
    > Explain the system architecture/purpose
* Analysis
    > To analyze the system
* Certifications
* Simulation
    > To simulate the system or it's enviorment
* Generation
    > You could generate code, tests, documentation or other things based on models

1. Create a model
1. Formal language to create the model
1. Model file (not a image)
1. Transformation, be able t read the model and generate something based on it.
    * GPL (General Purpose Language) such as Python, C#, C++..
    * Dedicated transformation languages (created specifically for transforming models)

### Why use MBE?
* Increase quality
* Decrease costs
* Better reusability

## Adoption of MBE
* In Complex / large systems, they tend to adopt Model based engineering
    * Often seen in safety-critical systems
* Systems engineering (Software and hardware), such as airplanes, cars, railways...
* Information systems 
* Model based testing
* __Domain specific languages (DSLs)__, such as a interface specific to a domain that transform something into code. Also called language engineering. People don't always know they're using models when they write code that transforms into something else.

### Issues with MBE?
* Acceptance by software engineers
* Resistance to change
* Tooling (such as graphical modeling tools)
    * Can be done, the automobile industry is a good example.

## Model transformation (Domain specific languages)
We will have a source model (which could be a UML model), and through a transformation it will create a target model (such as documentation or funcitonal code). 

The transformation compiler will understand the source model language and able to translate it to the target language.

For code generation, we'll have something that is generic or repetitive. If the code is very unique/individual then it's harder create something to generate it as code on demand.

> Generating test cases is specially useful way to use MBE, specially when the system has multiple states, it can become very complex and repetitive to do manually, which makes it eaiser to make mistakes.

## Meta modelling
A language has grammar, and in the MDE we might have meta-models that describe that source language to the target model/language. Could create a diagram describing the language to the target language first, to make the language clear before programming the transformations/compiler.


