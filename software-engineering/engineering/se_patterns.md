# Patterns 
Design patterns are typical solutions to common problems in software design. Each pattern is like a blueprint that you can customize to solve a particular design problem in your code.
* [Refactoring guru](https://refactoring.guru/design-patterns/)

### Creational patterns
* Builder
    > Lets you construct complex objects step by step. The pattern allows you to produce different types and representations of an object using the same construction code.
* Factory method
    > Provies an interface for creating objects in a superclass, but allows subclasses to alter the type of objects that will be created.
* Singelton
    > Lets you ensure that a class has only one instance, while providing a global access point to this instance.

### Structural patterns
* Adaptor
    > Allows objects with incompatible interfaces to collaborate
* Decorator
    > Lets you attach new behaviours to objects by placing these objects inside special wrapper objects that contain the behaviours.

    > These decorators could be stacked like a Babushka stackable doll.
* Composite
    > A structural design pattern that lets you compose objects into tree structures and then work with the tree structure as if they were individual objects.

    > This pattern only makes sense if your core model can be represented as a tree.

### Behavioural patterns
* Observer
    > Lets you define a subscription mechanism to notify multiple objects about any events that happen to the object they are observing.
* Mediator
    > Letes you reduce chaotic dependencies between objects. This pattern restricts direct communication between objects and forces them to collaborate only via the mediator object

    > Objects don't talk to each other, only through the mediator.