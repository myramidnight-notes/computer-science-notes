
# Requirements engineering
 In order to create software, we will need to discover and describe the requirements that the software will have. It is not about what the stakeholder wants but what they need for the most part. 
* Discover what is needed: Interviews, Surveys, Observations, Study existing examples.
   * Requirements are what the stakeholder needs rather than wants.
* Have a descriptions of what is expected of the system. 
   * Use cases can describe how it is expected to be used
   * Decide what are functional (_essential_) and non-functional requirements.
       * If the system would work without it, then it's a non-functional requirement.
* There should be measurable goals that clearly show if something is actually meeting expectations.
* Validate that the project is going in the right direction. This can be done with checklists, testing prototypes or even reviewing.
   * Do this often and regularily, it is cheaper to fix things early before the product is completed rather than afterwards.

## Waterfall-Model


## V-Model
1. Analysis
1. Design
1. Implementations
1. Unit test
1. Integration test
1. system test
1. Acceptance test

## Agile methods
* SCRUM