# Security

* __Security__ is protection against intrusion into the system. It can be accidental or intentional. 
* __Safety__ is the ability to operate without catastrophic failure
    * There is no safety without security.
* __Confidentiality__ (only those who are allowed should see the data)
* __Integrity__ (the data is correct, nobody has changed the data unexpectedly)
* __Availability__ (the system is accessable, doesn't crash)

Types of security
* Infrastructure security
* Application security
* Operation security
## Planning for application security
Security is expencive, more security does not always mean better system because at what cost do you implement the security? There are many things to consider, such as risk, value, performance, user acceptance. 
### Considering the value of security
Consider the consiquence of the security risk and the likelyhood of it happening. If something is very likely to happen, will it have very little and unimportant consiquence? Will it break the system?
Maybe the many layers of security might be displeasing to the user, have to consider how important the security is.
### Policies for security
* Assets (the data), is there sensitive information?
* Procedures 
* Responsibilities.
* Existing policies

## Value of assets vs threats
It is very good to make a table and evaluate the value of the asset and the consiquences of exposure it has on the system.

| Asset | Value | Exposure |
| --- | --- | --- |
| Information system | High | High |
| Patient record | low | medium |

Then compose a second table where you have to think of the possible threats, the probability of it happening, how to control those threats and how feasable is it?

| Threat | Prob. | Control | Feasible |
| --- | --- | --- | --- |
| Unauthorized access | High | 2FA, biometry... | Depends on the userbase, costs, user acceptance |

Then figure out the misuse case (like making use cases).

## Secure design
* Keep in mind that the trade off will be: performance, usability
* Technology:  
    * COTS (Components off the shelf) such as libraries, 
    * programming languages might provide some security
    * deployment enviorment can effect security
* Architecture:
    * Protection vs. distribution
    * Easier to protect if everything is in one place, but if someone gets in then the whole system is at risk. Distributed assests have the oposite tradeoffs, cannot get all the assets at once but harder to protect multiple distributions (perhaps you don't have contole, using a service).
* Design guidelines
    * Eplicit policies
    * Avoid having Single point of faliure
    * Log user actions 
    * Compartmentalize the assets (try making them lower valued out of context if someone manages to access any of them)
    * Design for deployment. Try to make it simple, protecting configurations. Complex deployment can create larger area for accidents of exposing configuration that should not be exposed out of development enviorment.

System is only as secure as you make it, what it is made of.

## Secure programming
* Different programming languages have different things to keep in mind to be secure
* Frameworks and libraries can have vulnerabilities that you have to keep in mind
* Domains (what are you programming for? web application? airplane? Inner-web for a company?). 
* OWASP top 10 (list of mostly used points of access to attack systems)
* MITRE att&ck list 
* Limit visibility of data
* Check inputs (code injection is high on OWASP list), do not let input have direct access to system.
* Hande exceptions, they  might expose information you might not want people to know about the system.
* Provide restart if system crashes
* check boundries. 
    * Prevent overflow, it can trigger some unexpected things to happen, might become serious
    * one serious case of overflow having consiquences is when a integer overflowed in a space rocket and triggered the safety feature which instantly caused it to explode.


### Python
* each version has different vulnerabilities
* Avoid assert unless doing a debug, assert is ignored outside of debug/dev.

### Points that apply to multiple programming languages
* Check any external pacages used, be sure that it's the correct ones or if the code is secure, because such packages are written by other people and not all have good intentions (such as creating fake versions of popular packages to exploit people that might download them instead of the real package).
* Check dependencies (packages usually install more packages that they use in their code), with each dependency come new vulnerabilities you might not have been aware of.