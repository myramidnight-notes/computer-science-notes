# System Modeling
The process of developing abstract models of a system, with each model presenting a different view or perspective of that system. 

Models are abstract representations of a system in specific contexts.

### System perspectives
* External perspective
    > where your model is a context or enviorment of the system.
* Interaction perspective
    > where you model the interactions between a system and it's enviorment, or between the components of a system
    
    > Use cases and sequence diagrams are well known ways to present this perspective. 
* Structural perspective
    > Where you model the organization of a system or the structure of the data that is processed by the sistem.

    > Class diagrams are primarily used to present this perspective.
* Behavioural perspective
    > Where you model the dynamic behaviour of the system and how it responds to events

    > Activity and Sequence diagrams are often used to present this perspective.

## Graphical models
* Graphical models can generate discussions about an existing or proposed system. 
* Can be used as a documentation of a existing system. 
* It also can work as a detailed system description for developers how to implement a system.

### Well known diagram types in UML
* Activity diagram
    > Shows the activity involoved in a process or in data processing
* Use case diagram
    > Shows the interactions between the system and it's enviorment
* Sequence diagram
    > Show the interactions between actors and the system and between system components. Storyboard diagrams.
* Class diagram
    > Show the object classes in the system and the associations between the classes. 
    
    > Object-oriented model to show the classes in a system, or the database structure.
* State diagram
    > Show how the system reactions to internal and external events.

### Types of models
* Behaviour Models
    > used to describe the dynamic behaviour of an executing system. This behaviour can be modeled from the perspective of the data processed by the system, or by the events that stimulate responses from a system.
    * Data-Driven models
    * Event-Driven models
* State models
    > used to model a system's behaviour in response to internal or external events
* Context models.
    > show how a system that is being modeled is positioned in an environment with other systems and processes.

## Model Driven Architecture/Engineering (MDA/MDE)

If a model is created using a modeling language such as UML (Universal Modeling Language) then it could possible be used for Model Based Engineering, to either generate code or documentation based on the model.

It is cheaper to be able to generate code if something is generic or repetative.

### Types of MDA
* Computational Indipendant Model (CIM)
    > These model the important domain abstractions used in a system. Sometimes called Domain models.
* Platform Indipendant Model (PIM)
    > These model the operation of the system without reference to it's implementation. Usually described using UML models that show static system structure and how it responds to external and internal events.
* Platform Specific Model (PSM)
    > These are transformations of the platform indipendant model with a separate PSM for each appplication platform. In primciple, there may be layers of PSM, with each layer adding some platform specific detail.