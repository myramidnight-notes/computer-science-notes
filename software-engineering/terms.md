* ### DRY
    > Don't Repeat Yourself
    >
    > "Every piece of knowledge must have a single, unambiguous, authoritative representation within a system"
* ### KISS
    > Keep it Simple, Stupid
    >
    >most systems work best if they are kept simple rather than made complicated
    >
    >"Simplicity is the ultimate sophistication"
* ### YAGNI
    > You aren't gonna need it
    >
    >  "Always implement things when you actually need them, never when you just foresee that you need them."
