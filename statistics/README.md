# Statistics
_Machine learning_ is basically just statistics, because the program has to learn from the data it aquires, which requires statistics.

* __Descriptive statistics__
> Consists of the collection, organization, summarization and presentation of data
>* Extracting appropriate information from data
>* Trying to understand and describe the data
* __Inferential statistics__
> Consists of generalizing from samples to population, performing estimtations and hypothesis tests, determining relationships among variables and making predictions.
>* Infer on unknown data

## Definitions
*  __Variables__ is a characteristic of condition that can change or take on different values
    * __discrete variables__ consist of individual categories (e.g. grades)
    * __continouus variables__ are infinitely divisible into units ( e.g. time or weight)
* __Population__ is an entire group of individuals/objects
* __Sample__ is a selection meant to represent the population. The goal is to use the result from working with the sample to answer questions about the population

### Primary scales of measurements
* Nominal
    > Numbers identify and classify objects
* Ordinal
    > Numbers indicate the relative position of objects (not magnitude of difference between them)
* Interval
    > Differences between objects can be compared (zero point is artribary)
* Ratio
    > Zero point is fixed, ratios of scale values can be compared

### Data type comparison
![data types](images/stat-data-types.png)

### Measures of central tendency (_miðlæg tilhneiging_)
* Mean
* Median
* Mode
### Measures of dispersion (_dreifing_)
* Variance
    > The average of the squares of distance each value is from the mean. Deviation from the mean.
* Standard deviation
    > Square root of the variance
* Range
    > Difference between the largest and smallest value

### Measures of position (_staðsetning_)
* Min and Max
* Standard score (_z score_)
    > Number of standard deviation of a value is the mean. (value-mean devided with standard deviation)
* Quartiles
    > Relative position data values in four quartiles
* Percentiles, deciles

### Percentile Rank
> A percentile rank of a score is the percentage of score in it's frequency distribution that are equal to or lower than it.

## Representing the data
### Histogram
> A graph that shows the frequency (or probability) of each value.
### Box plot
> Displays the distribution of data based on a five number summary (quartiles, min and max). Shows outliers and what their values are. Shows if data is symmetrical or skewed nd how tightly the data is grouped.
![box plot example](images/box-plot.png)

## Exploratory data analysis - EDA
> The process of analyzing data by combining discriptive statistics with visualizations.
>* Understand distribution of variables. 
>* Understand relationship between two or more variables. 
>* Detect patterns in data. 
>* Spot outliers. Formulate hypotheses about casual relationships. 
>* Inform about engineering of new variables (feature engineering). 
>* Inform about possible formal inferential statistical steps.  Essential step!

### Inferential statistics
> We use inferntial statistics to infer from the sample data to the population. To determine probability of characteristics of pupulationbased on characteristics of the sample. Assess strength of the relationship between independant (casual) variables and dependent (effect) variables. 
>#### Methods
>* Confidence intervals
>* Hypothesis tests
>* Predictions - ML

## Random variables
* Random variable is a variable whose possible values are numerical outcomes of a random phenomenon
    * Discrete random variable has a countable set of distinct possible values
    * Continuous random variable is where the data can take on new values (such as the height of a person)

### Probability mass function (PMF)
> PMF gives you probabilities for discrete random variables. 

### Dealing with large and noisy data
> As the number of values increases, the probabilities associated with each value gets smaller and the __effect of random noise increases.__
>
> __Binning the data__ can help, that is dividing the domain into non-overlapping intervals and counting the number of values in each bin.
>
> Using __cumulative distribution functions__ is a better alternative.

### Comulative distribution functions (CDF)
> CDF is the function that maps values to their percentile rank in a distribution. The CDF of a random variable `X`, evaluated for a particular value `x`, is defined as the probability that a value generated by a random process `X` is less than or equal to `x`.

>#### Probability Density Function
> The derivative of a CDF is called a probability density function (PDF). It measures the probability per unit of x.

### Binomial Random Variable (_Slembi-tvíliða_) 
> Discrete random variable that counts how often a particular event occurs in a fixed number of tries/ trails.

### Continuous Distributions
>#### Exponential distribution
> Exponential  distribution come up when looking at a series of events, where time is measured between events. 

>#### Normal Distribution
> The normal distribution, also known as _Gaussian_, is the most commonly used distribution since it approximates many natural phenomena. 

>#### Lognormal distribution


Check out podcast from BBC radio: `More or less: behind the statistics`. Making sense of statistics in everyday life.

### Covariance
> Covariance measures the (average) covariation between two variables.

### Correlation
> Like covariance, the correlation describes which two or more variables change together. A positive correlation indicates the extent to which those variables increase or decrease in parallel; a negative correlation indicates the extent to which one variable increases as the other decreases.