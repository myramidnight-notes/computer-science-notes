## Expected Value
> Mean of a random variable
> 
>1. You find the probability of all possible outcomes (they should total to 100% together)
>1. multiply each pair (outcome * probability)
>1. Take the sum of those resuls to find the Expected value.

## Calculate the Variance
>1. Square the sum of the sample 
>1. divide the result with sample size. Let's store it as A
>1. Find the sum of all the samples squared
>1. Subtract A from the result. Let's store it as B
>1. Now divide B with the sample size - 1. That is the variance.