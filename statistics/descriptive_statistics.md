# Probability and Statistics

We will rarely be able to access the whole _population_ to get the exact results, so we usually work with _samples_ of that population. Your statistics are only as good as your samples.

The sample is usually notated by a lower case `n`, while a upper case `N` would imply the population.
## Measuring the center
* Mean
    > The "average" number, found by adding all data points and dividing by the number of data points.
* Median
    > The middle number, found by ordering all data points and picking out the one in the middle (or the mean of the two middle numbers).
* Mode
    > The most frequent number, the one that appears the highest number of times.
## Sample
### Sample Variance
> [KA: sample variance](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/variance-standard-deviation-sample/v/sample-variance)

We would calculate the __sample mean__ (average of the sample) before we can calculate the _sample variance_. 

1. Get the sum of `(x-(sample mean)) squared` of each item in sample
2. Then we divide it by the sample size (two ways, depending on situation)
    * Divide by n 
        > We divide the sum with the number of items in the sample. This is a accurate variance of the sample, or if the sample is the population. 
    * Divide by n-1
        >We are __underestimating__ if we devide by `n` to estimate the population variance based on sample (when we don't have data of whole population). Then we want to devide the sum by `n-1` to get a __unbiased__ sample variance. It is _unbiased_ because the sample isn't the whole population.

### Sample Standard deviation
> [KA: Sample standard deviation and bias](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/variance-standard-deviation-sample/v/sample-standard-deviation-and-bias)

* First we calculate the _sample mean_ and _sample variance_
* To get the sample standard deviation, we take the square root of the unbiased sample variance.
    * This actually creates a biased estimate of standard deviation of the population, but it's the best tool we have.
