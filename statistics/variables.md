# Variables

## Random Variables
If we have a coin, and variable `x` would be the outcome of flipping that coin, then that is a random variable because it's value is undetermined. But it is better to talk about the __probability__ of `x` being either heads or tails.
## Binomial random variables
They are a type of random variable
```
x = number of heads after 10 flips of a coin
```
* Made up of indipendent trials
    > Based on previous examples, then each flip of the coin is a trail, and them being idipendant means that we are not calculating the probability based on previous trails (such as getting heads heads tail).
* Each trail has two outcomes, success or failure.
* You have a fixed number of trials
* Probability of success on each trail is constant (always the same probability)

### Binomial distribution
* figure out the total possible outcomes
* figure out the chances of each outcome of variable.